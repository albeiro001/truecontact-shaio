# -*- coding: utf-8 -*-
# Copyright (C) 2018 Freetech Solutions

# This file is part of OMniLeads

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
#

"""En este modulo se encuentran las vistas basicas para inicializar el sistema,
usuarios, modulos, grupos, pausas

DT:Mover la creacion de agente a otra vista
"""

from __future__ import unicode_literals

import logging
from django.http.response import HttpResponse
import requests

from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.http import JsonResponse
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from django.conf import settings
from django.views.generic import (
    ListView, CreateView, UpdateView, DeleteView, FormView, TemplateView, View
)
from django.utils.decorators import method_decorator

from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import Group

from constance import config as config_constance

from defender import utils
from defender import config

from datetime import datetime

from ominicontacto_app.models import (
    User, AgenteProfile, Grupo, Pausa,
    Chat, MensajeChat, ClienteWebPhoneProfile, Campana
)
from ominicontacto_app.forms import PausaForm, GrupoForm, RegistroForm
from ominicontacto_app.services.kamailio_service import KamailioService
from ominicontacto_app.utiles import fecha_local
from ominicontacto_app import version
from configuracion_telefonia_app.regeneracion_configuracion_telefonia import (
    RestablecerConfiguracionTelefonicaError, SincronizadorDeConfiguracionPausaAsterisk)
from reportes_app.models import LlamadaLog

from utiles_globales import AddSettingsContextMixin

logger = logging.getLogger(__name__)

# =========================== TRUECONTACT ======================================
from truecontact.chatapi.models import Dialogo, Mensaje
from truecontact.contactos.forms import GestionContactoForm, AgregarContactoForm, GestionContactoForm1
from truecontact.contactos.models import GestionContacto , CalificacionGestion, Motivo

from truecontact.custom_client.models import Conmutador
from truecontact.custom_client.forms import ConmutadorForm

from django.db.models import Q


import ast
import datetime as dt
import json


def test_supervisor(user):
    if ( user.groups.filter(name='Supervisor').exists() ) or ( user.groups.filter(name='Administrador').exists()) :
        return True
    else:
        return False

def login_view(request):
    detail = None
    user_is_blocked = False
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        login_unsuccessful = False
        if utils.is_already_locked(request, username=username):
            intentos_fallidos = config.FAILURE_LIMIT + 2
            detail = _("Haz tratado de loguearte {intentos_fallidos} veces,"
                       " sin exito. Tu cuenta y dirección IP"
                       " permanecerán bloqueadas por {cooloff_time_seconds} segundos."
                       " Contacta al Administrador".format(intentos_fallidos=intentos_fallidos,
                                                           cooloff_time_seconds=config.COOLOFF_TIME)

                       )
            user_is_blocked = True
            login_unsuccessful = True
        user = authenticate(username=username, password=password)
        form = AuthenticationForm(request, data=request.POST)
        if not form.is_valid():
            login_unsuccessful = True
        utils.add_login_attempt_to_db(request, login_valid=not login_unsuccessful,
                                      username=username)
        user_not_blocked = utils.check_request(request, login_unsuccessful=login_unsuccessful,
                                               username=username)

        # TODO: Si es cliente webphone lo bloqueo
        if ClienteWebPhoneProfile.objects.filter(user__username=username).exists():
            user_is_blocked = True
            detail = _("Este tipo de usuario no puede loguearse en este momento.")

        if user_not_blocked and not user_is_blocked and not login_unsuccessful:
            if form.is_valid():
                primer_log = user.last_login is None
                login(request, user)
                user.set_session_key(request.session.session_key)
                if user.get_supervisor_profile() is not None and primer_log:
                    return HttpResponseRedirect(reverse('user_change_password'))
                if 'next' in request.GET and request.GET.get('next') != reverse(
                        'api_agente_logout'):
                    return redirect(request.GET.get('next'))
                if user.is_agente:
                    return HttpResponseRedirect(reverse('tc-index'))
                else:
                    return HttpResponseRedirect(reverse('index'))

    else:
        if request.user.is_authenticated and not request.user.borrado:
            if request.user.is_agente and request.user.get_agente_profile().is_inactive:
                form = AuthenticationForm(request)
                logout(request)
            elif 'next' in request.GET:
                return redirect(request.GET.get('next'))
            elif request.user.is_agente:
                return HttpResponseRedirect(reverse('tc-index'))
            else:
                return HttpResponseRedirect(reverse('index'))
        else:
            form = AuthenticationForm(request)
            if request.user.is_authenticated:
                logout(request)
    context = {
        'form': form,
        'detail': detail,
        'user_is_blocked': user_is_blocked,
    }
    template_name = 'truecontact/login.html'
    return TemplateResponse(request, template_name, context)

def logout_view(request):
    logout(request)
    return redirect('/tc/')

class ConsolaAgenteView(AddSettingsContextMixin, TemplateView):
    template_name = "truecontact/base_agente.html"

    def dispatch(self, request, *args, **kwargs):
        user = self.request.user
        agente_profile = request.user.get_agente_profile()
        if agente_profile.is_inactive:
            message = _("El agente con el cuál ud intenta loguearse está inactivo, contactese con"
                        " su supervisor")
            messages.warning(request, message)
            logout(request)
        # if user.groups.filter(name = 'Supervisor').exists():
        #     url_supervisor = '/tc/supervisor'
        #     return  redirect(url_supervisor)

        return super(ConsolaAgenteView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ConsolaAgenteView, self).get_context_data(**kwargs)
        campanas_preview_activas = []
        usuario_agente = self.request.user
        agente_profile = usuario_agente.get_agente_profile()
        kamailio_service = KamailioService()
        sip_usuario = kamailio_service.generar_sip_user(agente_profile.sip_extension)
        sip_password = kamailio_service.generar_sip_password(sip_usuario)

        agente = self.request.user.get_agente_profile()
        campanas_queues = agente.get_campanas_activas_miembro()
        # print('CAMPAÑA cantidad :', campanas_queues)
        
        calificaciones_campanas = []
        id_campanas=['0']
        nombre_campanas=[]
        dialogos_nuevos_campana=[]
        dialogos_nuevos=Dialogo.objects.none()
        if (campanas_queues.count() == 0 ):
            context['campanas_queues'] = campanas_queues.count()
            context['id_campana_whatsapp'] = id_campanas
        else:
            print("SI HAY CAMPANAS camapnas..........")
            for id_nombre in campanas_queues.values_list('id_campana', flat=True):
                split_id_nombre = id_nombre.split('_')
                id_campana = split_id_nombre[0]
                campana = Campana.objects.get(pk=id_campana)
                nombre_campana = campana.nombre
                print('id campanaaaaaaaaa: ',id_campana)
                calificaciones = CalificacionGestion.objects.filter(campana=campana)
                print('calificaciones : ', calificaciones)
                for calificacion in calificaciones:
                    calificaciones_campanas.append({
                        'id': calificacion.id,
                        'nombre': calificacion.nombre,
                        'accion': calificacion.accion_programacion,
                        'campana': calificacion.campana.nombre,
                    })
                # calificaciones_campanas.append(calificaciones_campana)
                id_campanas.append(id_campana)
                nombre_campanas.append(nombre_campana)
                dialogos_nuevos_campana = Dialogo.objects.filter(estado='nuevo', campana = campana.nombre).order_by('-fecha_modificacion')
            dialogos_nuevos = dialogos_nuevos | dialogos_nuevos_campana
            context['campanas_queues'] = campanas_queues.count()
            context['calificaciones_campana'] = calificaciones_campanas
            context['id_campana_whatsapp'] = id_campanas
            context['json_id_campana_whastapp'] = json.dumps(id_campanas)
            print("CAMPANAS AGENTE:>>>>>>>>>>>>>>>>>> ", json.dumps(id_campanas))
            context['nombre_campana'] = nombre_campanas
            context['nombre_campana_1'] = nombre_campana
        
        # if not (campanas_queues.count() == 0):
            context['dialogos_en_gestion'] = Dialogo.objects.filter(estado='en_gestion', user=usuario_agente ).order_by('-fecha_modificacion')
            context['cantidad_en_gestion'] = Dialogo.objects.filter(estado='en_gestion', user=usuario_agente ).count()
            context['dialogos_nuevos'] = dialogos_nuevos
            context['cantidad_nuevos'] = dialogos_nuevos.count()
        
        hoy = fecha_local(now())
        registros = LlamadaLog.objects.obtener_llamadas_finalizadas_del_dia(agente_profile.id, hoy)
        campanas_preview_activas = \
            agente_profile.has_campanas_preview_activas_miembro()
        context['pausas'] = Pausa.objects.activas
        context['registros'] = registros
        context['tipos_salientes'] = LlamadaLog.TIPOS_LLAMADAS_SALIENTES
        context['campanas_preview_activas'] = campanas_preview_activas
        context['agente_profile'] = agente_profile
        context['sip_usuario'] = sip_usuario
        context['sip_password'] = sip_password
        context['agentes'] = AgenteProfile.objects.obtener_activos().exclude(id=agente_profile.id)
        context['campanas'] = Campana.objects.all().exclude(pk=1)

        id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])
        campana = Campana.objects.get(pk=id_campana)


        if campana.nombre != 'Conmutador':

            if not (campanas_queues.count() == 0):
                gestiones_telefonia_en_gestion = []
                for gestion in GestionContacto.objects.filter(medio='telefonia', estado='pendiente', creado_por=usuario_agente).order_by('-fecha_creacion'):
                    meta_data_gestion = ast.literal_eval(gestion.metadata)
                    gestiones_telefonia_en_gestion.append(
                        {
                            'id': gestion.pk,
                            'call_id': meta_data_gestion['call_id'],
                            'imagen': gestion.contacto.obtener_imagen_url() if gestion.contacto else '',
                            'nombre': gestion.contacto.get_short_name() if gestion.contacto else gestion.get_telefono(),
                            'telefono': meta_data_gestion['telefono'],
                            'fecha_modificacion': gestion.fecha_creacion,
                            'industrias': gestion.contacto.get_industrias() if gestion.contacto else '',
                            'obtener_campana': gestion.obtener_campana(),
                        }
                    ) 

                context['cantidad_en_gestion'] += len(gestiones_telefonia_en_gestion)

                context['gestiones_telefonia_gestion'] = gestiones_telefonia_en_gestion

            context['form_gestion'] = GestionContactoForm1

        else:
            context['form_conmutador'] = ConmutadorForm
            gestiones_conmutador_en_gestion = []
            for gestion in Conmutador.objects.filter(medio="telefonia", estado="pendiente", creado_por=usuario_agente):
                meta_data_gestion = ast.literal_eval(gestion.metadata)
                gestiones_conmutador_en_gestion.append(
                    {
                        'id' : gestion.pk,
                        'call_id': meta_data_gestion['call_id'],
                        'imagen': '',
                        'nombre': gestion.directorio.nombre if gestion.directorio.nombre != "Desconocido" else meta_data_gestion['telefono'],
                        'telefono': meta_data_gestion['telefono'],
                        'fecha_modificacion': gestion.created,
                        'obtener_campana': gestion.obtener_campana(),
                    }
                )

            context['cantidad_en_gestion'] += len(gestiones_conmutador_en_gestion)

            context['gestiones_telefonia_gestion'] = gestiones_conmutador_en_gestion

        context['form_contacto'] = AgregarContactoForm
        context['nombre_campana'] = campana.nombre

        return context



class BlancoView(TemplateView):
    template_name = 'blanco.html'


class SupervisorView(AddSettingsContextMixin, TemplateView):
    template_name = "truecontact/base_supervisor.html"

    def dispatch(self, request, *args, **kwargs):
        agente_profile = request.user.get_agente_profile()
        if agente_profile.is_inactive:
            message = _("El agente con el cuál ud intenta loguearse está inactivo, contactese con"
                        " su supervisor")
            messages.warning(request, message)
            logout(request)

        return super(SupervisorView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SupervisorView, self).get_context_data(**kwargs)
        campanas_preview_activas = []
        usuario_agente = self.request.user
        agente_profile = usuario_agente.get_agente_profile()
        kamailio_service = KamailioService()
        sip_usuario = kamailio_service.generar_sip_user(agente_profile.sip_extension)
        sip_password = kamailio_service.generar_sip_password(sip_usuario)

        agente = self.request.user.get_agente_profile()
        campanas_queues = agente.get_campanas_activas_miembro()
        print('CAMPAÑA cantidad :', campanas_queues)

        if (campanas_queues.count() == 0 ):
            context['campanas_queues'] = campanas_queues.count()

        hoy = fecha_local(now())
        registros = LlamadaLog.objects.obtener_llamadas_finalizadas_del_dia(agente_profile.id, hoy)
        campanas_preview_activas = \
            agente_profile.has_campanas_preview_activas_miembro()
        context['pausas'] = Pausa.objects.activas
        context['registros'] = registros
        context['tipos_salientes'] = LlamadaLog.TIPOS_LLAMADAS_SALIENTES
        context['campanas_preview_activas'] = campanas_preview_activas
        context['agente_profile'] = agente_profile
        context['sip_usuario'] = sip_usuario
        context['sip_password'] = sip_password
        context['agentes'] = AgenteProfile.objects.obtener_activos().exclude(id=agente_profile.id)


        context['dialogos_en_gestion'] = Dialogo.objects.filter(estado='en_gestion', user=usuario_agente).order_by('-fecha_modificacion')
        context['dialogos_nuevos'] = Dialogo.objects.filter(estado='nuevo').order_by('-fecha_modificacion')
        gestiones_telefonia_en_gestion = []
        for gestion in GestionContacto.objects.filter(medio='telefonia', estado='pendiente', creado_por=usuario_agente).order_by('-fecha_creacion'):
            meta_data_gestion = ast.literal_eval(gestion.metadata)
            gestiones_telefonia_en_gestion.append(
                {
                    'call_id': meta_data_gestion['call_id'],
                    'imagen': gestion.contacto.obtener_imagen_url() if gestion.contacto else '',
                    'nombre': gestion.contacto.get_short_name() if gestion.contacto else meta_data_gestion['telefono'],
                    'fecha_modificacion': gestion.fecha_creacion,
                }
            )

        context['gestiones_telefonia_gestion'] = gestiones_telefonia_en_gestion

        # Eliminar

        context['form_gestion'] = GestionContactoForm
        context['form_contacto'] = AgregarContactoForm
        context['categorias'] = Categoria.objects.all()
        context['productos'] = Producto.objects.all()
        context['etiquetas'] = Tag.objects.all()

        return context

# ################################
# # Vista de SUPERVISOR
# ################################

class GestionesListView(ListView):
    """
    Esta vista lista las gestiones de forma general
    """
    model = GestionContacto
    context_object_name = 'gestiones'
    paginate_by = 30
    template_name = 'supervisor/gestiones_list.html'

    def get_queryset(self):
        user = self.request.user
        if user.groups.filter(name = 'Supervisor').exists():
            queryset = GestionContacto.objects.all().order_by('-id')
        else:
            queryset = GestionContacto.objects.filter(creado_por=user).order_by('-id')


        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            queryset = queryset.filter(
                    Q(metadata__icontains=buscar) |
                    Q(creado_por__username__icontains=buscar) |
                    Q(contacto__nombres__icontains=buscar) |
                    Q(contacto__documento__icontains=buscar) )
    # -----------FILTROS DE FECHA------------------------------------------------------------------
        # is_filter = False
        f_inicial = self.request.GET.get('f_inicial')
        f_final = self.request.GET.get('f_final')
        if not (f_inicial == None or f_inicial==''):
            f_inicial = dt.datetime.strptime(f_inicial, '%Y-%m-%d')
        else:
            f_inicial = None
        if not (f_final == None or f_final==''):
            f_final = dt.datetime.strptime(f_final, '%Y-%m-%d') + dt.timedelta(days=1)
        else:
            f_final = None
        if not (f_inicial == None or f_final == None):
            queryset = queryset.filter(fecha_creacion__range=(f_inicial, f_final))
        return queryset

    def get_context_data(self, **kwargs):
        context = super(GestionesListView, self).get_context_data(**kwargs)
        buscar =  self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar

        is_filter = False
        f_inicial = self.request.GET.get('f_inicial')
        f_final = self.request.GET.get('f_final')
        if not (f_inicial == None or f_inicial==''):
            context['f_inicial'] = f_inicial
        else:
            f_inicial = None
        if not (f_final == None or f_final==''):
            context['f_final'] = f_final
        else:
            f_final = None
        if not (f_inicial == None or f_final == None):
            context['filtro'] = '&f_inicial={0}&f_final={1}'.format(context['f_inicial'], context['f_final'])
            is_filter = True

        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context
        
class GestionesAgentesView(ListView):
    """Vista para listar los agentes

    """
    model = AgenteProfile
    template_name = 'supervisor/gestiones_agentes.html'

    def get_context_data(self, **kwargs):
        context = super(GestionesAgentesView, self).get_context_data(
            **kwargs)
        agentes = AgenteProfile.objects.exclude(borrado=True)

        # -----------FILTROS DE FECHA si no hay fecha traiogo los de hoy   -------------------------------------------
        f_inicial = self.request.GET.get('f_inicial')
        f_final = self.request.GET.get('f_final')


        if not (f_inicial == None or f_inicial==''):
            context['f_inicial'] = f_inicial

            f_inicial = dt.datetime.strptime(f_inicial, '%Y-%m-%d')
        else:
            context['f_inicial'] = ""
            f_inicial = None

        if not (f_final == None or f_final==''):
            context['f_final'] = f_final
            f_final = dt.datetime.strptime(f_final, '%Y-%m-%d') + dt.timedelta(days=1)
        else:
            context['f_final'] = ""
            f_final = None

        if not (f_inicial == None or f_final == None):
            gestiones = GestionContacto.objects.filter(fecha_creacion__range=(f_inicial, f_final))
        else:
            gestiones = GestionContacto.objects.filter(fecha_creacion__date = dt.datetime.today())

        agentes_custom = []
        a=0
        for agente in agentes:
            if gestiones.filter(creado_por=agente.user).count() > 0 :
                nombre = agente.user.get_full_name()
                a = a+1
                agentes_custom.append({
                    'nombre':nombre,
                    'username':agente.user,
                    'gestiones': gestiones.filter(creado_por=agente.user).count(),
                    'gestiones_terminadas': gestiones.filter(creado_por=agente.user).filter(estado='finalizado').count(),
                    'porcentaje': round((100*gestiones.filter(creado_por=agente.user).filter(estado='finalizado').count())/gestiones.filter(creado_por=agente.user).count()),
                    'llamadas': gestiones.filter(creado_por=agente.user).filter(medio='telefonia').count(),
                    'whatsapp': gestiones.filter(creado_por=agente.user).filter(medio='whatsapp').count(),

                }) #CONSTRUYO EL LIST PERSONALIZADO


        context['filtro'] = '?f_inicial={0}&f_final={1}'.format(context['f_inicial'], context['f_final'])
        context['agentes'] = agentes_custom

        return context
