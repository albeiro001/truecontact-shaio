from django.urls import include, path
from django.contrib.auth.decorators import login_required

from . import views, views_csv

urlpatterns = [
    path('accounts/login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    # url(r'^consola/$',
    path('', login_required(views.ConsolaAgenteView.as_view()), name="tc-index"),
    path('consola/', login_required(views.ConsolaAgenteView.as_view()), name="consola_de_agente"),
    path('', include('truecontact.ajustes.urls')),
    path('', include('truecontact.chatapi.urls')),
    path('', include('truecontact.contactos.urls')),
    path('', include('truecontact.sms.urls')),
    path('', include('truecontact.llamadas_perdidas.urls')),
    path('', include('truecontact.custom_client.urls')),

    path('supervisor', login_required(views.SupervisorView.as_view()), name="tc-supervisor"),
    path('supervisor/gestiones/agentes/',    views.GestionesAgentesView.as_view(), name='tc-gestiones-agentes'),
    path('supervisor/gestiones/list/',      views.GestionesListView.as_view(), name='tc-gestiones-list'),
    path('supervisor/csv/gestiones/',       views_csv.CsvGestionesView,                 name='tc-csv-gestiones'),

]
