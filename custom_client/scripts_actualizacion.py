from truecontact.sanautos.models import Vehiculo

dic_vehiculos = {}
for vehiculo in Vehiculo.objects.all():
    if not (vehiculo.placa in dic_vehiculos.keys()):
        dic_vehiculos[vehiculo.placa] = 1
    else:
        dic_vehiculos[vehiculo.placa] += 1

lista_depurar = []
for placa in dic_vehiculos:
    if dic_vehiculos[placa] > 1:
        print("eliminando repetido")
        print(placa)
        lista_depurar.append(placa)
        Vehiculo.objects.filter(placa = placa)[0].delete()
