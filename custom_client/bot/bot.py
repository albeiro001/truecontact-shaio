# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q
from django.conf import settings
from django.core import files
from django.utils.crypto import get_random_string
from os.path import basename
from ominicontacto_app.models import *
from truecontact.chatapi.models import Dialogo, Mensaje, Encuesta_bot,HorarioWhatapp
from truecontact.contactos.models import Contacto, ContactoDatosContacto
from django.utils import timezone
from datetime import datetime
import time

import ast
import datetime as dt
from datetime import  datetime, timedelta
import pytz
import json
import requests
import tempfile





LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)

PACIENTE_A = '''
Para generar correctamente su agendamiento por favor enviar la  siguente información:
1 Nombre completo  del  paciente  
2 Numero y tipo de documento del paciente  
3 Orden médica  
4 Autorización vigente y dirigida a la clínica ( en caso de ser medicina prepagada por favor especificar el plan  que maneja)  
5 Números de contacto

'''

PACIENTE_B = '''
Por favor enviar la  siguente información: 
1. Nombre completo  del  paciente
2. Número y tipo de documento del paciente , indicar qué cita desea modificar

'''

PACIENTE_C = '''
Por favor enviar la  siguente información: 
1 Nombre completo  del  paciente
2 Número y tipo de documento del paciente
por favor indicar que cita desea cancelar y motivo

'''
PACIENTE_D = '''
¿Qué información requiere?
'''

MENU = '''
A continuación indique qué servicio requiere:

1 Asignación de cita
2 Modificación de cita
3 Cancelación de cita 
4 Información general

'''

MENSAJE_SERVICIOS_LOGYTEC = '''
Gracias por comunicarse con el servicio de agendamiento de citas de la fundación clínica shaio a traves de  Whatsapp,
"Si es Shaio es Corazon"
Nuestro horario de atención es de Lunes a Viernes de 7:00am a 6:00pm, confirmamos el recibido de su solicitud.
¿Uted autoriza el tratamiento de datos con base a la ley 1581 del año 2012? 
Recuerde que esta ley se encuentra disponible en la pagina web www.shaio.org. 
1 SI 
2 NO 
'''

class Bot():

    def __init__(self):
        self.APIUrl = settings.CHATAPI_URL
        self.token = settings.CHATAPI_TOKEN

    def _send_request(self, method, resource, data, parameters=''):
        url_parameters = ''
        for parameter in parameters:
            url_parameters = url_parameters + '&{}={}'.format(parameter[0], parameter[1])

        url = "{}{}?token={}{}".format(self.APIUrl, resource, self.token, url_parameters)
        headers = {'Content-type': 'application/json'}
        if method == 'GET':
            r = requests.get(url, data=json.dumps(data), headers=headers)
        elif method == 'POST':
            r = requests.post(url, data=json.dumps(data), headers=headers)
        else:
            r = requests.get(url, data=json.dumps(data), headers=headers)

        if r.status_code == 200:
            answer = r.json()
        else:
            answer = {'ERROR': r.content}
        return answer

    def _enviar_mensaje_api(self, chat_id=None, number=None, body=None):
            parameters = []
            method = 'POST'
            resource = 'sendMessage'
            data = {}
            if (chat_id != None):
                if not (body == '' or body == None):
                    data.update(
                        {
                            'chatId': chat_id,
                            'body': body,
                        }
                    )
            elif (number != None):
                if not (body == '' or body == None):
                    data.update(
                        {
                            'number': number,
                            'body': body,
                        }
                    )

            if len(parameters) > 0:
                answer = self._send_request(method, resource, data, parameters)
            else:
                answer = self._send_request(method, resource, data)

            if 'ERROR' in answer:
                return answer
            else:
                return answer


    def enviar_mensaje(self, dialogo=None, number=None, body=None):
        respuesta = self._enviar_mensaje_api(chat_id=dialogo.chat_id, number=number, body=body)
        print('RESPUESTA CONTROLADOR: ', respuesta)
        return respuesta

    
    def ini_metadata(self,dialogo):
        if dialogo !=None :
            print(">>> CONTROLADOR >>> ini_metadata > ESTADO DEL DIALOGO.........", dialogo.estado )
            if dialogo.estado == 'bot':
                if dialogo.contacto == None:
                    metadata = "{'tiempo_espera':'0', 'error_respuesta':'0', 'bandera_no_contesta': False, 'bandera_cerrar':False, 'bandera_sin_respuesta':True, 'bandera_datos':True, 'bandera_numero':False}"
                else:
                    metadata = "{'tiempo_espera':'0', 'error_respuesta':'0', 'bandera_no_contesta': False, 'bandera_cerrar':False, 'bandera_sin_respuesta':True, 'bandera_datos':False, 'bandera_numero':True}"
            if dialogo.estado == 'nuevo':
                metadata = "{'tiempo_espera':'0', 'error_respuesta':'0', 'bandera_no_contesta': True, 'bandera_cerrar':False, 'bandera_sin_respuesta':False, 'bandera_datos':False, 'bandera_numero':False}"
            if dialogo.estado == 'en_gestion' or dialogo.estado == 'encuesta':
                metadata = "{'tiempo_espera':'0', 'error_respuesta':'0', 'bandera_no_contesta': True, 'bandera_cerrar':False, 'bandera_sin_respuesta':False, 'bandera_datos':False, 'bandera_numero':False}"

        else:
            metadata = "{'tiempo_espera':'0', 'error_respuesta':'0', 'bandera_no_contesta': False, 'bandera_cerrar':False, 'bandera_sin_respuesta':True, 'bandera_datos':True, 'bandera_numero':False}"
        return metadata

    def error_respuesta_numero(self, pk_dialogo):
        pk_dialogo = pk_dialogo
        dialogo = Dialogo.objects.get(pk=pk_dialogo)

        if (dialogo.metadata):
            call_data_dialogo = ast.literal_eval(dialogo.metadata)
        else:
            metadata = self.ini_metadata(dialogo)
            dialogo.metadata = metadata
            dialogo.save()
        error_respuesta= int(call_data_dialogo['error_respuesta'])
        if error_respuesta <= 1:
            respuesta_incorrecta = '''No hemos logrado identificar su solicitud, asegurese de marcar la opción correcta.'''
            error_respuesta +=1
            call_data_dialogo['error_respuesta'] = error_respuesta
            dialogo.metadata = call_data_dialogo  
            dialogo.esperar_respuesta = True
            dialogo.save()
        
        else:
            call_data_dialogo['bandera_sin_respuesta'] = False
            call_data_dialogo['bandera_datos'] = False
            call_data_dialogo['bandera_numero'] = False
            call_data_dialogo['bandera_no_contesta'] = True
            call_data_dialogo['error_respuesta'] = 0
            call_data_dialogo['tiempo_espera'] = 0
            dialogo.metadata = call_data_dialogo 
            dialogo.estado = 'gestionado'    # evaluar si lo quitamos por el boot
            dialogo.bot = False
            dialogo.modulo = 999999
            dialogo.esperar_respuesta = False
            dialogo.save()
            respuesta_incorrecta = 'Lo sentimos, no logramos identificar su solictud, Gracias por comunicarse con la Fundación Clinica  Shaio "Si es Shaio es Corazón"'
            
        return respuesta_incorrecta

    
    def validar_horarios(self, dialogo):
        print(">>>validar_horarios >> DIALOGO: ", dialogo.pk)
        # dialogo = dialogo
        nombre_campana = dialogo.campana
        print('este es el nombre campana', nombre_campana)
        if(nombre_campana != None):
            campana = Campana.objects.get(nombre=nombre_campana)
            if (dialogo.metadata):
                print(">>>>HOARRIO >> METADATA DEL DIALOGO: ", dialogo.metadata)
                call_data_dialogo = ast.literal_eval(dialogo.metadata)  
            else:
                metadata = self.ini_metadata(dialogo)
                dialogo.metadata = metadata
                dialogo.save()
                call_data_dialogo = ast.literal_eval(dialogo.metadata)

            dicdias = {'MONDAY':'lunes','TUESDAY':'martes','WEDNESDAY':'miercoles','THURSDAY':'jueves','FRIDAY':'viernes','SATURDAY':'sabado','SUNDAY':'domingo'}
            fecha = datetime.today()            
            dia_hoy = dicdias[fecha.strftime('%A').upper()]

            if HorarioWhatapp.objects.filter(campana=campana,dia__icontains=dia_hoy,status=True):
                print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
                print('si hay horario para la campaña')

                horario = HorarioWhatapp.objects.filter(campana=campana, dia__icontains=dia_hoy, status=True)[0]
                lista_dias=[]
            # for horario in horarios:
            #     print(horario)
                for dia in horario.dia:
                    lista_dias.append(dia)
                # print('la lista es ', lista_dias)
                if  dia_hoy in lista_dias:
                    # print('esta en la lista de dias')
                    hora_inicial = horario.hora_inicio
                    hora_final = horario.hora_final
                    print(horario.nombre)
                    # print(horario.hora_inicio)
                    # print(horario.hora_final)
                    now = datetime.now()
                    hora = now.hour
                    minuto =now.minute
                    hoy =dt.time(hora, minuto)

                    if hora_inicial < hoy < hora_final: #si cumple el horario pasa normal 
                        dialogo.modulo = 999999
                        dialogo.bot = False
                        dialogo.estado = 'nuevo'
                        dialogo.fecha_entrada_cola = dt.datetime.now(pytz.utc)
                        dialogo.save()
                        return True
                    else: #si no cumple el horario normal le envio un mensaje de fuera de horario y lo paso
                        dialogo.modulo = 0
                        dialogo.bot = True
                        dialogo.estado = 'bot'
                        dialogo.fecha_entrada_cola = dt.datetime.now(pytz.utc)
                        dialogo.save()
                        mensaje_espera=horario.mensaje_espera
                        self.enviar_mensaje(dialogo=dialogo, body=mensaje_espera)
                        return False
                else:
                    dialogo.modulo = 0
                    dialogo.bot = True
                    dialogo.estado = 'bot'
                    dialogo.fecha_entrada_cola = dt.datetime.now(pytz.utc)
                    dialogo.save()
                    mensaje_espera=horario.mensaje_espera
                    self.enviar_mensaje(dialogo=dialogo, body=mensaje_espera)
                    return False
            elif HorarioWhatapp.objects.filter(campana=campana):
                horario = HorarioWhatapp.objects.filter(campana=campana, status=True)[0]
                dialogo.modulo = 0
                dialogo.bot = True
                dialogo.estado = 'bot'
                dialogo.fecha_entrada_cola = dt.datetime.now(pytz.utc)
                dialogo.save()
                mensaje_espera=horario.mensaje_espera
                self.enviar_mensaje(dialogo=dialogo, body=mensaje_espera)
                return False
            else:
                
                # dialogo.modulo = 888888
                # dialogo.bot = False
                # dialogo.estado = 'nuevo'
                # dialogo.fecha_entrada_cola = dt.datetime.now(pytz.utc)
                # dialogo.save()
                return True
        else:
            return True

    def temporizador(self):
        print('llego al temporizador')
        segundos_final =  datetime.now()+timedelta(hours=5)
        dialogos = Dialogo.objects.filter(
            Q(bot = True)&
            Q (estado='bot')
            # Q (estado='encuesta')
        )
        for dialogo in dialogos:
            segundos_inicial = str(dialogo.fecha_modificacion)
            segundos_inicial = datetime.strptime(segundos_inicial[:19],"%Y-%m-%d %H:%M:%S")
            delta_tempoizador = (segundos_final-segundos_inicial).total_seconds()
            print('Segundos ',delta_tempoizador)
            print('Segundos ',dialogo.esperar_respuesta)
            


            if delta_tempoizador > 300 and dialogo.esperar_respuesta == True :# 60 para un minuto 60*5 para 300 q serian 5 minutos
                if (dialogo.metadata):
                    call_data_dialogo = ast.literal_eval(dialogo.metadata)
                else:
                    metadata = self.ini_metadata(dialogo)
                    dialogo.metadata = metadata
                    dialogo.save()
                tiempo_espera =int(call_data_dialogo['tiempo_espera'])

                tiempo_espera +=1
                print("tiempo_espera ", tiempo_espera)
                call_data_dialogo['tiempo_espera']= str(tiempo_espera)
                dialogo.metadata = call_data_dialogo
                dialogo.save()

                
                #temporizador recordatorios para continuar
                if (tiempo_espera >= 1 and call_data_dialogo['bandera_datos'] == True):
                    # {'tiempo_espera':'0', 'error_respuesta':'0', 'bandera_no_contesta': False, 'bandera_cerrar':False, 'bandera_sin_respuesta':True, 'bandera_datos':True, 'bandera_numero':False}
                    call_data_dialogo['bandera_datos'] = False
                    call_data_dialogo['tiempo_espera'] =0
                    dialogo.metadata = call_data_dialogo
                    dialogo.save()

                    cerrada_tiempo='''No hemos recibido una respuesta  valida , señor usuario nuestros asesores estan a la espera de su respuesta'''
                    self.enviar_mensaje(dialogo=dialogo, body=cerrada_tiempo)

                
                # temporizador si no rezponde el usuario en 5 minuto
                elif (tiempo_espera >= 1 and call_data_dialogo['bandera_sin_respuesta'] == True):
                    # {'tiempo_espera':'0', 'error_respuesta':'0', 'bandera_no_contesta': False, 'bandera_cerrar':False, 'bandera_sin_respuesta':True, 'bandera_datos':True, 'bandera_numero':False}
                    self.enviar_mensaje(dialogo=dialogo, body='Lo sentimos, no hemos recibido una respuesta, su solicitud se  ha dado por finalizada, Gracias por comunicarse con la Fundación Clinica  Shaio " Si es Shaio es Corazón')
                    # call_data_dialogo['tiempo_espera'] = 0
                    # call_data_dialogo['bandera_datos'] = True
                    # call_data_dialogo['bandera_numero'] = True
                    # call_data_dialogo['bandera_sin_respuesta'] = True
                    # call_data_dialogo['bandera_no_contesta'] = True
                    # dialogo.metadata = call_data_dialogo
                    dialogo.metadata = ''
                    dialogo.estado = 'gestionado'
                    dialogo.campana = ''
                    dialogo.esperar_respuesta = True
                    dialogo.bot = True
                    dialogo.modulo = 999999
                    print(f'el estado es {dialogo.estado} , espera respuesta {dialogo.esperar_respuesta} , ')
                    dialogo.save()
                ##temporizador para encuestas
                # elif (tiempo_espera == 48 and call_data_dialogo['bandera_no_contesta'] == True): # 48 para que a los 5 minutos envie el mensaje de cierre de encuetas
                #     dialogo.modulo = 999999
                #     dialogo.save()
                #     cerrada_tiempo='''Muchas gracias, se cerro la encuesta por tiempo'''
                #     self.enviar_mensaje(dialogo=dialogo, body=cerrada_tiempo)

                # elif (tiempo_espera == 48 and call_data_dialogo['bandera_cerrar'] == True): # 48 para que a los 5 minutos se eperan 5 minutos mas por si el usuario contesta algo
                #     dialogo.modulo = 999999
                #     dialogo.save()

                # elif (tiempo_espera >= 96 and (call_data_dialogo['bandera_cerrar'] == True or  call_data_dialogo['bandera_no_contesta'] == True) ): # 96 el doble de bandera cerrar y no contesta
                #     call_data_dialogo['tiempo_espera'] =0
                #     call_data_dialogo['bandera_sin_respuesta'] =True
                #     call_data_dialogo['bandera_cerrar'] = False
                #     call_data_dialogo['bandera_no_contesta'] =False
                #     dialogo.metadata = call_data_dialogo
                #     dialogo.esperar_respuesta = False
                #     dialogo.modulo = 0
                #     dialogo.estado = 'gestionado'
                #     dialogo.user = None
                #     dialogo.gestion = None
                #     dialogo.campana = None
                #     dialogo.bot = False
                #     dialogo.save()

    def processing_bot(self, dic_analisis_dialogos):

        for dialogo_pk in dic_analisis_dialogos:
            lista_mensajes = dic_analisis_dialogos[dialogo_pk]
            dialogo = Dialogo.objects.get(pk=dialogo_pk)
            primer_mensaje = lista_mensajes[0]
            respuesta = primer_mensaje.body
            dialogo.campana = 'Call'
            dialogo.save()
            validar_horarios=self.validar_horarios(dialogo)
            if validar_horarios == True:

                if ( dialogo.estado == 'bot'):
                    if (dialogo.metadata):
                        call_data_dialogo = ast.literal_eval(dialogo.metadata)
                    else:
                        metadata = self.ini_metadata(dialogo)
                        dialogo.metadata = metadata
                        dialogo.save()
                        call_data_dialogo = ast.literal_eval(dialogo.metadata)
                    if (dialogo.modulo == 0):
                        if not(ContactoDatosContacto.objects.filter(contacto = dialogo.contacto).exists() ):
                            mensaje_bienvenida_conocidos = MENSAJE_SERVICIOS_LOGYTEC
                            # self.enviar_mensaje(dialogo=dialogo, body=MENSAJE_SERVICIOS_LOGYTEC)
                            dialogo.modulo = 1
                            dialogo.esperar_respuesta = True
                            dialogo.save()

                        else:
                            mensaje_bienvenida_conocidos = '''
Sr/a {} {}
Gracias por comunicarse con el servicio de agendamiento de citas de la fundación clinica shaio a traves de  Whatsapp ,
"Si es Shaio es Corazon"
Nuestro horario de atención es de Lunes a Viernes de 7:00am a 6:00  pm, confirmamos el recibido de su solicitud.
¿Uted autoriza el tratamiento de datos con base a la ley 1581 del año 2012 ? 
recuerde que esta ley se encuentra disponible en la pagina web www.shaio.org. 
1 SI 
2 NO
'''.format(dialogo.contacto.nombres, dialogo.contacto.apellidos)

                        self.enviar_mensaje(dialogo=dialogo, body=mensaje_bienvenida_conocidos)
                        dialogo.modulo = 1
                        dialogo.esperar_respuesta = True
                        dialogo.save()
                    

                    elif(dialogo.modulo == 1):

                        if respuesta.upper() == '1' or respuesta.upper() == '2' or respuesta.upper() == 'SI' or respuesta.upper() == 'NO':
                            if (ContactoDatosContacto.objects.filter(contacto = dialogo.contacto).exists() ):    
                                mensaje_bienvenida = '''
Sr/a {} {}

A continuacion indique que servicio requiere 

1 Asignacion de cita
2 Modificacion de cita
3 Cancelacion de cita 
4 Informacion general
'''.format(dialogo.contacto.nombres, dialogo.contacto.apellidos)
                            else:
                                mensaje_bienvenida = '''
A continuacion indique que servicio requiere 

1 Asignacion de cita
2 Modificacion de cita
3 Cancelacion de cita 
4 Informacion general
'''
                            self.enviar_mensaje(dialogo=dialogo, body=mensaje_bienvenida)
                            
                            call_data_dialogo['bandera_datos'] == False
                            dialogo.metadata = call_data_dialogo
                            dialogo.modulo = 2
                            dialogo.esperar_respuesta = True
                            dialogo.save()
                        else:
                            respuesta_incorrecta= self.error_respuesta_numero(pk_dialogo=str(dialogo.pk))
                            self.enviar_mensaje(dialogo=dialogo, body=respuesta_incorrecta)

                    elif(dialogo.modulo == 2):

                        if (respuesta == '1'):
                            self.enviar_mensaje(dialogo=dialogo, body=PACIENTE_A)
                            call_data_dialogo['bandera_numero'] =False
                            call_data_dialogo['error_respuesta'] =0
                            call_data_dialogo['tiempo_espera'] =0
                            dialogo.metadata = call_data_dialogo 
                            dialogo.modulo = 3
                            dialogo.estado = 'nuevo'
                            dialogo.campana = 'Call'
                            dialogo.save()
                        elif (respuesta == '2'):
                            self.enviar_mensaje(dialogo=dialogo, body=PACIENTE_B)
                            call_data_dialogo['bandera_numero'] =False
                            call_data_dialogo['error_respuesta'] =0
                            call_data_dialogo['tiempo_espera'] =0
                            dialogo.metadata = call_data_dialogo 
                            dialogo.modulo = 3
                            dialogo.estado = 'nuevo'
                            dialogo.campana = 'Call'
                            dialogo.save()
                        
                        elif (respuesta == '3'):
                            self.enviar_mensaje(dialogo=dialogo, body=PACIENTE_C)
                            call_data_dialogo['bandera_numero'] =False
                            call_data_dialogo['error_respuesta'] =0
                            call_data_dialogo['tiempo_espera'] =0
                            dialogo.metadata = call_data_dialogo 
                            dialogo.modulo = 3
                            dialogo.estado = 'nuevo'
                            dialogo.campana = 'Call'
                            dialogo.save()
                        
                        elif (respuesta == '4'):
                            self.enviar_mensaje(dialogo=dialogo, body=PACIENTE_D)
                            call_data_dialogo['bandera_numero'] =False
                            call_data_dialogo['error_respuesta'] =0
                            call_data_dialogo['tiempo_espera'] =0
                            dialogo.metadata = call_data_dialogo 
                            dialogo.modulo = 3
                            dialogo.estado = 'nuevo'
                            dialogo.campana = 'Call'
                            dialogo.save()

                        else:
                            respuesta_incorrecta= self.error_respuesta_numero(pk_dialogo=str(dialogo.pk))
                            self.enviar_mensaje(dialogo=dialogo, body=respuesta_incorrecta) 
                