from truecontact.sanautos.models.vehiculo import * 
from truecontact.contactos.models import *
from ominicontacto_app.models import User



hoy = dt.datetime.now()
hoy_00 = dt.datetime.strptime('{} 00:00:00'.format(hoy.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S') 
dias_15 = dt.datetime.now() + dt.timedelta(days=15) 
dias_15_23_00 = dt.datetime.strptime('{} 23:59:59'.format(dias_15.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

lista_vehiculos = Vehiculo.objects.filter(fecha_soat__range=(hoy_00,dias_15_23_00),char_5='SOAT');      


for vehiculo in lista_vehiculos:
    if (Programacion.objects.filter(char_4=vehiculo.placa).exists()):
        programaciones = Programacion.objects.filter(char_4=vehiculo.placa)
        for programacion in programaciones:
            if (programacion.estado_programacion == 'gestionado'):
                print('se creara ina programacion por q estaba gestionada')
                programacion_nueva_1 = Programacion(
                    contacto = programacion.gestion.contacto,
                    agente = programacion.gestion.creado_por,
                    estado_programacion = 'nuevo',
                    observaciones = programacion.gestion.observaciones,
                    fecha_gestionar = hoy_00,
                    date_1 = vehiculo.fecha_soat,
                    char_4 = vehiculo.placa,
                    char_1 = programacion.char_1 if programacion.char_1 else '',
                    char_2 = programacion.char_2 if programacion.char_2 else '', 
                    char_3 = programacion.char_3 if programacion.char_3 else '',
                    char_5 = programacion.char_5 if programacion.char_5 else '',
                )
                programacion_nueva_1.save()
                print('nueva programacion para hoy ', programacion_nueva_1)
    else:
        print('se creara una programacion por q no existia la placa en las programaciones')
        programacion_nueva_2 = Programacion(
            contacto = vehiculo.contacto,
            agente = User.objects.get(pk=3),
            estado_programacion = 'nuevo',
            observaciones = '',
            fecha_gestionar = hoy_00,
            date_1 = vehiculo.fecha_soat,
            char_4 = vehiculo.placa,
            char_1 = vehiculo.modelo if vehiculo.modelo else '', 
            char_5 = 'SOAT',
        )
        programacion_nueva_2.save()
        print('programacion nueva por q no exitia', programacion_nueva_2)

#### para el problema de las programaciones viejas con el carge nuevo########
from datetime import datetime

from truecontact.contactos.models import *

fecha_ini = '29/04/2021'
fecha_ini_dt = datetime.strptime(fecha_ini, '%d/%m/%Y')

fecha_fin = '01/05/2021'
fecha_fin_dt = datetime.strptime(fecha_fin, '%d/%m/%Y')

lista_placa = {}
for programacion in Programacion.objects.all(): # la programaciones a depurar
    if not(programacion.char_4 in lista_placa.keys()):
        lista_placa[programacion.char_4] = 1
    else:
        lista_placa[programacion.char_4] += 1

lista_depurar = []
for placa in lista_placa:
    if lista_placa[placa] > 1:
        count_placa += 1
        lista_depurar.append(placa)
        print(placa, lista_placa[placa])

for placa in lista_depurar:
    print("Analizando placa: {}".format(placa))
    programaciones = Programacion.objects.filter(char_4=placa).order_by('-pk')

    if programaciones.filter(estado_programacion='gestionado'):
        for programacion in programaciones.filter(Q(estado_programacion='nuevo') | Q(estado_programacion='pendiente')):
            if (programacion.estado_programacion == 'nuevo'):
                a = 1
                programacion.delete()
            elif (programacion.estado_programacion == 'pendiente'):
                programacion.estado_programacion == 'gestionado'
                programacion.save()
    else:
        if (programaciones.filter(estado_programacion='pendiente').count() > 0):
            for programacion in programaciones.filter(estado_programacion='nuevo'):
                programacion.delete()

            else:
                for x, programacion in enumerate(programaciones.order_by('-id')):
                    print("x {} Validar programacion {} - estado {}".format(x, programacion, programacion.estado_programacion))
                    if not (x == 0):
                        a = 1
                        programacion.delete()