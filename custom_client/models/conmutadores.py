""" Modelo para el formulario de la campaña de conmutador. """

# Django
from django.db import models

# Omnileads
from ominicontacto_app.models import User, Campana
from reportes_app.models import LlamadaLog

# TrueContact
from truecontact.contactos.models import MEDIO_GESTION, TipoGestion, ESTADO_GESTION

# Utilidades
import ast


class BaseModel(models.Model):
    """ Modelo base.
    
    BaseModel actúa como una clase base abstracta de la que heredarán todos los demás modelos del proyecto. 
    Esta clase proporciona a cada tabla los siguientes atributos:

         + created (DateTime): Almacena la fecha y hora en que se creó el objeto.
         + modified (DateTime): Almacena la última fecha y hora en que se modificó el objeto.
    """

    created = models.DateTimeField(
        'create at',
        auto_now_add=True,
        help_text='Fecha y hora en la cual el objeto fue creado.'
    )

    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Fecha y hora de la última modificación del objeto.'
    )

    class Meta:
        """ Opciones Meta. """
        abstract = True

        get_latest_by = 'created'
        ordering = ['-created', '-modified']

class Directorio(BaseModel):
    """ Modelo para el directorio de las diferentes áreas. """
    nombre = models.CharField(max_length=128)
    is_active = models.BooleanField('Directorio activo', default=True, blank=True, null=True)

    def __str__(self):
        """ Retorna el nombre del directorio """
        return self.nombre

    def obtener_telefonos(self):
        lista_telefonos = []

        for dato_contacto in DatosDirectorio.objects.filter(
            directorio=self).exclude(is_active=False):

            lista_telefonos.append(
                {
                    'id': dato_contacto.pk,
                    'label': dato_contacto.label,
                    'telefono': dato_contacto.numero,
                    'telefono_mask': 'x'*6 + dato_contacto.numero[-4:]
                }
            )

        return lista_telefonos

    def obtener_gestiones_pendientes(self):
        directorio_gestiones = Conmutador.objects.filter(directorio=self.pk).exclude(estado='finalizado')
        return directorio_gestiones

OPTION_DATO_DIRECTORIO = [
    ('casa', 'Casa'),
    ('oficina', 'Oficina'),
    ('extension', 'Extensión'),
    ('otro', 'Otro'),
]

class DatosDirectorio(BaseModel):
    """ Datos del directorio """
    directorio = models.ForeignKey(Directorio, on_delete=models.PROTECT)
    label = models.CharField(max_length=32, choices=OPTION_DATO_DIRECTORIO, default='otro')
    numero = models.CharField(max_length=20)
    is_active = models.BooleanField('Dato activo',default=True)

    def __str__(self):
        """ Retorna el id del dato y el nombre del directorio al que pertenece. """   
        return "Dato {} del directorio {}".format(self.id, self.directorio.nombre)

RESULTADO_CONMUTADOR = [
    ('efectiva', 'Llamada efectiva'),
    ('mensaje', 'Mensaje a tercero'),
    ('transferencia', 'Llamada transferida'),
    ('radio', 'Mensaje por radio'),
]

class Conmutador(BaseModel):
    nombre = models.CharField('Quién llama', max_length=255, blank=True, null=True)
    directorio = models.ForeignKey(Directorio, on_delete=models.PROTECT, related_name="directorio_origen")
    directorio_destino = models.ForeignKey(Directorio, on_delete=models.PROTECT, related_name="directorio_destino", blank=True, null=True)
    telefono = models.ForeignKey(DatosDirectorio, on_delete=models.PROTECT, blank=True, null=True)
    resultado = models.CharField(max_length=32, choices=RESULTADO_CONMUTADOR, blank=True, null=True)
    observaciones = models.TextField(blank=True, null=True)
    estado = models.CharField('Estado de la gestión', max_length=16, choices=ESTADO_GESTION, default="pendiente")
    tipo = models.ForeignKey(TipoGestion, on_delete=models.PROTECT,  blank=True, null=True)
    medio = models.CharField(max_length=64, choices=MEDIO_GESTION, default='otro')
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="conmutador_creado_por")
    metadata = models.TextField(default='no_data')

    def __str__(self):
        """ Retorna el id del conmutador. """
        return str(self.id)
    
    def get_url_grabacion(self):
        INCLUDED_EVENTS = ['COMPLETEAGENT', 'COMPLETEOUTNUM', 'BT-COMPLETE',
                           'COMPLETE-BT', 'CT-COMPLETE', 'COMPLETE-CT', 'CAMPT-COMPLETE',
                           'COMPLETE-CAMPT', 'BTOUT-COMPLETE', 'COMPLETE-BTOUT', 'CTOUT-COMPLETE',
                           'COMPLETE-CTOUT']

        call_data = ast.literal_eval(self.metadata)
        grabaciones = LlamadaLog.objects.filter(archivo_grabacion__isnull=False,
                                  duracion_llamada__gt=0,
                                  event__in=INCLUDED_EVENTS)

        grabaciones = grabaciones.exclude(
            archivo_grabacion='-1').exclude(event='ENTERQUEUE-TRANSFER')


        if (call_data['medio'] == 'telefonia'):

            if (grabaciones.filter(callid=call_data['call_id']).exists()):
                grabacion = grabaciones.filter(callid=call_data['call_id'])[0]
                return grabacion.url_archivo_grabacion

            else:
                return ''
        else:
            return ''

    def obtener_campana(self):
        call_data = ast.literal_eval(self.metadata)
        if 'id_campana' in call_data:
            campana_pk = call_data['id_campana']
            nombre = Campana.objects.get(pk=campana_pk).nombre
        else:
            nombre = ""
        return str(nombre)
    
    def get_call_id(self):
        call_data = ast.literal_eval(self.metadata)
        if 'call_id' in call_data:
            call_id = call_data['call_id']
        else:
            call_id = ''
        return str(call_id)