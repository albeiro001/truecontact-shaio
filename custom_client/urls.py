from rest_framework import routers
from django.urls import path, re_path
from django.contrib.auth.decorators import login_required

from .views import programacion as programacion_views
from .views import conmutadores
from .views import reportes as reportes_views

urlpatterns = [

    # ========================== PROGRAMACIONES ===============================
    path(
        'shaio/programacion/list/',
        programacion_views.ProgramacionListView.as_view(),
        name='tc-shaio-programacion-list'
    ),
    path(
        'shaio/programacion/add/',
        programacion_views.ProgramacionAddView.as_view(),
        name='tc-shaio-programacion-add'
    ),
    path('shaio/programacion/<int:pk>/update/',
        programacion_views.ProgramacionUpdateView.as_view(),
        name='tc-shaio-programacion-update'
    ),
    path('shaio/programacion/<int:pk>/gestionar/',
        programacion_views.ProgramacionGestionarView.as_view(),
        name='tc-shaio-programacion-gestionar'
    ),
    path(
        'shaio/programacion/export/',
        programacion_views.ProgramacionListExportView.as_view(),
        name='tc-shaio-programacion-export'
    ),
    path(
        'campanas/programacion/manual/',
        programacion_views.CampanasProgramacionManual.as_view(),
        name='tc-campanas-programacion-manual'
    ),

    # ============================= REPORTES ==================================
    path(
        'sanautos/soat/reportes/',
        reportes_views.DashboardSoatView.as_view(),
        name='tc-sanautos-soat'
    ),
    # ============================ Conmutador ================================
    path(
        'conmutador/identificar_directorio/', 
        conmutadores.IdentificarDirecctorioLlamadaView.as_view(),
        name="tc_identificar_directorio"
    ),
    path(
        'conmutador/gestionar/',
        conmutadores.ConmutadorGestionView.as_view(),
        name="tc_conmutador_gestionar"
    ),
    path(
        'conmutador/cargar_gestion/',
        conmutadores.CargarGestionConmutadorView.as_view(),
        name="tc_cargar_gestion_conmutador"
    ),
    path(
        'conmutador/consultar_telefono/',
        conmutadores.ConsularTelefonoView.as_view(),
        name="tc_consultar_telefono_directorio"
    ),
    path(
        'conmutador/crear_directorio/',
        conmutadores.CrearDirectorioView.as_view(),
        name="tc_crear_directorio"
    ),
    path(
        'conmutador/obtener_telefonos_directorio/',
        conmutadores.ObtenerTelefonosDirectorio.as_view(),
        name="tc_obtener_telefonos_directorio"
    ),
    path(
        'conmutador/guardar_gestion/',
        conmutadores.GuardarGestionView.as_view(),
        name="tc_guardar_gestion_conmutador"
    ),
    path(
        'conmutador/consultar_directorio/',
        conmutadores.ConsultarDirectorioView.as_view(),
        name='tc_directorio_ver'
    ),
    path(
        'conmutador/modificar_directorio/',
        conmutadores.ModificarDirectorioView.as_view(),
        name='tc_modificar_directorio'
    ),
    path(
        'conmutador/eliminar_telefono/',
        conmutadores.EliminarTelefonoDirectorioView.as_view(),
        name='tc_eliminar_telefono_directorio'
    ),
    path(
        'conmutador/listar_gestiones/',
        conmutadores.GestionesConmutadorListView.as_view(),
        name='tc_gestiones_conmutador_list'
    ),
    path(
        'conmutador/listar_directorios/',
        conmutadores.DirectorioListView.as_view(),
        name='tc_directorios_list'
    )
]

