""" Formulario para la campaña de conmutador. """

# Django
from django import forms

# Modelo de conmutador
from truecontact.custom_client.models import Conmutador

class ConmutadorForm(forms.ModelForm):
    class Meta:
        model = Conmutador
        fields = ['tipo', 'medio', 'nombre', 'directorio_destino', 'telefono', 'resultado', 'observaciones']