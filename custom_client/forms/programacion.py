from django import forms

from truecontact.contactos.models import (Contacto, ContactoDatosContacto, Programacion,
    TipoIdentificacion)

from ominicontacto_app.models import User, Campana

from truecontact.chatapi.models import Dialogo


class ProgramacionForm(forms.ModelForm):

    nombres = forms.CharField(
        max_length = 512,
        label = "Nombres"
    )

    apellidos = forms.CharField(
        max_length = 512,
        label = "Apellidos"
    )

    tipo_identificacion = forms.ModelChoiceField(
        queryset = TipoIdentificacion.objects.all(),
        label = 'Tipo Identificacion'
    )

    documento = forms.CharField(
        max_length = 512,
        label = "Identificacion"
    )

    telefono1 = forms.CharField(
        max_length = 512,
        label = "Telefono 1",
        required = False
    )

    telefono2 = forms.CharField(
        max_length = 512,
        label = "Telefono 2",
        required = False
    )

    date_1 = forms.DateTimeField(
        input_formats=["%Y-%m-%dT%H:%M"],
        #2021-12-10T12:22
    )

    date_2 = forms.DateTimeField(
        input_formats=["%Y-%m-%dT%H:%M"],
    )

    class Meta:
        model = Programacion
        fields = ['contacto', 'observaciones', 'char_1', 'char_2',
             'char_3', 'date_1', 'date_2']       

    def save(self):
        programacion = super(ProgramacionForm, self).save(commit=False)

        identificacion = self.cleaned_data['documento'].strip()
        if (Contacto.objects.filter(documento=identificacion).exists()):
            contacto = Contacto.objects.get(documento=identificacion)
        else:
            contacto = Contacto.objects.create(
                nombres = self.cleaned_data['nombres'],
                apellidos = self.cleaned_data['apellidos'],
                tipo_identificacion = self.cleaned_data['tipo_identificacion'],
                documento = self.cleaned_data['documento'],
            )

        for telefono in [self.cleaned_data['telefono1'], self.cleaned_data['telefono2']]:
            if not (telefono == '' or telefono == None):
                if not (ContactoDatosContacto.objects.filter(
                    tipo='telefono', value_1=telefono).exists()):

                    telefono = ContactoDatosContacto(
                        contacto = contacto,
                        label = 'otro',
                        tipo = 'telefono',
                        value_1 = telefono
                    )
                    telefono.save()
                    if (len(str(telefono.value_1)) == 10):
                        num_wp = "57" + telefono.value_1
                        chat_id = '{}@c.us'.format(num_wp)

                        if not(Dialogo.objects.filter(chat_id=chat_id).exists()):
                            dialogo = Dialogo(
                                chat_id = chat_id,
                                numero = chat_id.split('@')[0],
                                es_grupo = False,
                                contacto = contacto
                            )
                            dialogo.save()

        programacion.contacto = contacto
        programacion.save()

        return programacion

    def update(self):
        programacion = super(ProgramacionForm, self).save(commit=False)
        programacion.save()
        return programacion
