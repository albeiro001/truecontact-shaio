from django.db.models import Q
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect, StreamingHttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic  import (View, ListView, CreateView, UpdateView, DeleteView, FormView, TemplateView, DetailView )
from openpyxl import load_workbook
from tablib import Dataset

import ast
import csv
import io
import json
import pytz
import requests
import datetime as dt
from django.utils.timezone import now, timedelta
from pytz import timezone

from ominicontacto_app.models import Campana, User
from truecontact.chatapi.models import Dialogo
from truecontact.contactos.models import (Programacion, TipoIdentificacion,
    Contacto, ContactoDatosContacto, TipoGestion, ResultadoGestion, CalificacionGestion,
    GestionContacto)

from datetime import datetime

LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)

local_tz = timezone("America/Bogota")


class DashboardSoatView(LoginRequiredMixin,TemplateView):
    context_object_name = 'dashboard'
    template_name = 'sanautos/reportes/dashboard.html'
    paginate_by = 20
    def get_context_data(self, **kwargs ):
        context = {}
        hoy = now().astimezone(local_tz)
        antes = hoy-timedelta(days=23)
        antessoat = hoy-timedelta(days=35)
        print(antessoat)
        get_t = self.request.GET
        print('este es el get ', get_t)
        detalle_gestion = []
        detalle_gestion_venta = []
        detalle_ciudad = []
        print(self.request.GET.get('f_inicio_ventas'), self.request.GET.get('f_fin_ventas'), )

        if (self.request.GET.get('f_inicio_ciudad') == '' or self.request.GET.get('f_inicio_ciudad') == 'None' or self.request.GET.get('f_inicio_ciudad') == None):

            print('entro a sin fechaaaaaa a colocarle fechaaaaa f_inicio_ciudad')
            f_fin_ciudad = dt.datetime.now()

            f_fin_ciudad_filtro = dt.datetime.strptime(f_fin_ciudad.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_fin_ciudad_filtro = f_fin_ciudad_filtro.strftime('%d-%m-%Y')
            context['f_fin_ciudad'] = f_fin_ciudad_filtro
            f_fin_ciudad = dt.datetime.strptime('{} 23:59:59'.format(f_fin_ciudad.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

            f_inicio_ciudad = dt.datetime.now() - dt.timedelta(days=63)

            f_inicio_ciudad_filtro = dt.datetime.strptime(f_inicio_ciudad.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_inicio_ciudad_filtro = f_inicio_ciudad_filtro.strftime('%d-%m-%Y')
            context['f_inicio_ciudad'] = f_inicio_ciudad_filtro
            f_inicio_ciudad = dt.datetime.strptime('{} 00:00:00'.format(f_inicio_ciudad.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

        elif (self.request.GET.get('f_inicio_ciudad') != '' or self.request.GET.get('f_inicio_ciudad') != 'None' or self.request.GET.get('f_inicio_ciudad') != None):
            
            f_inicio_ciudad = self.request.GET.get('f_inicio_ciudad')
            f_fin_ciudad = self.request.GET.get('f_fin_efectividad')
            
            f_inicio_ciudad = datetime.strptime('{} 00:00:00'.format(f_inicio_ciudad,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            f_fin_ciudad = datetime.strptime('{} 23:59:59'.format(f_fin_ciudad,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            
            context['f_inicio_ciudad'] = self.request.GET.get('f_inicio_ciudad')
            context['f_fin_ciudad'] = self.request.GET.get('f_fin_ciudad')


        if (self.request.GET.get('f_inicio_efectividad') == '' or self.request.GET.get('f_inicio_efectividad') == 'None' or self.request.GET.get('f_inicio_efectividad') == None):
            # if (self.request.GET.get('f_inicio_efectividad') == '' or self.request.GET.get('f_inicio_efectividad') == 'None' or self.request.GET.get('f_inicio_efectividad') == None):

            print('entro a sin fechaaaaaa a colocarle fechaaaaa f_inicio_efectividad')
            f_fin_efectividad = dt.datetime.now()

            f_fin_efectividad_filtro = dt.datetime.strptime(f_fin_efectividad.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_fin_efectividad_filtro = f_fin_efectividad_filtro.strftime('%d-%m-%Y')
            context['f_fin_efectividad'] = f_fin_efectividad_filtro
            f_fin_efectividad = dt.datetime.strptime('{} 23:59:59'.format(f_fin_efectividad.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

            f_inicio_efectividad = dt.datetime.now() - dt.timedelta(days=28)

            f_inicio_efectividad_filtro = dt.datetime.strptime(f_inicio_efectividad.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_inicio_efectividad_filtro = f_inicio_efectividad_filtro.strftime('%d-%m-%Y')
            context['f_inicio_efectividad'] = f_inicio_efectividad_filtro
            f_inicio_efectividad = dt.datetime.strptime('{} 00:00:00'.format(f_inicio_efectividad.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

        elif (self.request.GET.get('f_inicio_efectividad') != '' or self.request.GET.get('f_inicio_efectividad') != 'None' or self.request.GET.get('f_inicio_efectividad') != None):
            
            f_inicio_efectividad = self.request.GET.get('f_inicio_efectividad')
            f_fin_efectividad = self.request.GET.get('f_fin_efectividad')
            
            f_inicio_efectividad = datetime.strptime('{} 00:00:00'.format(f_inicio_efectividad,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            f_fin_efectividad = datetime.strptime('{} 23:59:59'.format(f_fin_efectividad,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            
            context['f_inicio_efectividad'] = self.request.GET.get('f_inicio_efectividad')
            context['f_fin_efectividad'] = self.request.GET.get('f_fin_efectividad')


        if (self.request.GET.get('f_inicio_detalle') == '' or self.request.GET.get('f_inicio_detalle') == 'None' or self.request.GET.get('f_inicio_detalle') == None):
            print('entro a sin fechaaaaaa a colocarle fechaaaaa')
            f_fin_detalle = dt.datetime.now()

            f_fin_detalle_filtro = dt.datetime.strptime(f_fin_detalle.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_fin_detalle_filtro = f_fin_detalle_filtro.strftime('%d-%m-%Y')
            context['f_fin_detalle'] = f_fin_detalle_filtro
            f_fin_detalle = dt.datetime.strptime('{} 23:59:59'.format(f_fin_detalle.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

            f_inicio_detalle = dt.datetime.now() - dt.timedelta(days=15)

            f_inicio_detalle_filtro = dt.datetime.strptime(f_inicio_detalle.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_inicio_detalle_filtro = f_inicio_detalle_filtro.strftime('%d-%m-%Y')
            context['f_inicio_detalle'] = f_inicio_detalle_filtro
            f_inicio_detalle = dt.datetime.strptime('{} 00:00:00'.format(f_inicio_detalle.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

        elif (self.request.GET.get('f_inicio_detalle') != '' or self.request.GET.get('f_inicio_detalle') != 'None' or self.request.GET.get('f_inicio_detalle') != None):
            f_inicio_detalle = self.request.GET.get('f_inicio_detalle')
            f_fin_detalle = self.request.GET.get('f_fin_detalle')
            f_inicio_detalle = datetime.strptime('{} 00:00:00'.format(f_inicio_detalle,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            f_fin_detalle = datetime.strptime('{} 23:59:59'.format(f_fin_detalle,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            context['f_inicio_detalle'] = self.request.GET.get('f_inicio_detalle')
            context['f_fin_detalle'] = self.request.GET.get('f_fin_detalle')

            print('entro aca y transformo la fecha')
        
        if (self.request.GET.get('f_inicio_ventas') == '' or self.request.GET.get('f_inicio_ventas') == 'None' or self.request.GET.get('f_inicio_ventas') == None):
            print('entro a sin fechaaaaaa a colocarle fechaaaaa')
            f_fin_ventas = dt.datetime.now()

            f_fin_ventas_filtro = dt.datetime.strptime(f_fin_ventas.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_fin_ventas_filtro = f_fin_ventas_filtro.strftime('%d-%m-%Y')
            context['f_fin_ventas'] = f_fin_ventas_filtro
            f_fin_ventas = dt.datetime.strptime('{} 23:59:59'.format(f_fin_ventas.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

            f_inicio_ventas = dt.datetime.now() - dt.timedelta(days=15)

            f_inicio_ventas_filtro = dt.datetime.strptime(f_inicio_ventas.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_inicio_ventas_filtro = f_inicio_ventas_filtro.strftime('%d-%m-%Y')
            context['f_inicio_ventas'] = f_inicio_ventas_filtro
            f_inicio_ventas = dt.datetime.strptime('{} 00:00:00'.format(f_inicio_ventas.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

        elif (self.request.GET.get('f_inicio_ventas') != '' or self.request.GET.get('f_inicio_ventas') != 'None' or self.request.GET.get('f_inicio_ventas') != None):
            f_inicio_ventas = self.request.GET.get('f_inicio_ventas')
            f_fin_ventas = self.request.GET.get('f_fin_ventas')
            f_inicio_ventas = datetime.strptime('{} 00:00:00'.format(f_inicio_ventas,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            f_fin_ventas = datetime.strptime('{} 23:59:59'.format(f_fin_ventas,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            context['f_inicio_ventas'] = self.request.GET.get('f_inicio_ventas')
            context['f_fin_ventas'] = self.request.GET.get('f_fin_ventas')
        
        if (self.request.GET.get('f_inicio_contacto') == '' or self.request.GET.get('f_inicio_contacto') == 'None' or self.request.GET.get('f_inicio_contacto') == None):
            print('entro a sin fechaaaaaa a colocarle fechaaaaa')
            f_fin_contacto = dt.datetime.now()

            f_fin_detalle_filtro = dt.datetime.strptime(f_fin_contacto.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_fin_detalle_filtro = f_fin_detalle_filtro.strftime('%d-%m-%Y')
            context['f_fin_contacto'] = f_fin_detalle_filtro
            f_fin_contacto = dt.datetime.strptime('{} 23:59:59'.format(f_fin_contacto.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

            f_inicio_contacto = dt.datetime.now() - dt.timedelta(days=15)

            f_inicio_detalle_filtro = dt.datetime.strptime(f_inicio_contacto.strftime('%d-%m-%Y'), '%d-%m-%Y')
            f_inicio_detalle_filtro = f_inicio_detalle_filtro.strftime('%d-%m-%Y')
            context['f_inicio_contacto'] = f_inicio_detalle_filtro
            f_inicio_contacto = dt.datetime.strptime('{} 00:00:00'.format(f_inicio_contacto.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')
            

        elif (self.request.GET.get('f_inicio_contacto') != '' or self.request.GET.get('f_inicio_contacto') != 'None' or self.request.GET.get('f_inicio_contacto') != None):
            f_inicio_contacto = self.request.GET.get('f_inicio_contacto')
            f_fin_contacto = self.request.GET.get('f_fin_contacto')
            f_inicio_contacto = datetime.strptime('{} 00:00:00'.format(f_inicio_contacto,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            f_fin_contacto = datetime.strptime('{} 23:59:59'.format(f_fin_contacto,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            context['f_inicio_contacto'] = self.request.GET.get('f_inicio_contacto')
            context['f_fin_contacto'] = self.request.GET.get('f_fin_contacto')
            


        print('FECHAS A COLOCAR Y FIILTRAR')
        print('f_i: ', f_inicio_contacto , f_inicio_detalle, f_inicio_ventas)
        print('f_f: ', f_fin_contacto , f_fin_detalle, f_fin_ventas)
        sergio_alonso = User.objects.get(pk=3)
        lista_gestiones = Programacion.objects.filter(gestion__fecha_creacion__range=(f_inicio_detalle,f_fin_detalle), estado_programacion='gestionado',agente=sergio_alonso);
        print('lista_gestiones: ', lista_gestiones)
        dic_gestion_finalizado = {}
        for programacion in lista_gestiones:
            if (programacion.gestion):
                if (programacion.gestion.calificacion):
                    if not (programacion.gestion.calificacion.nombre in dic_gestion_finalizado.keys()):
                        dic_gestion_finalizado[programacion.gestion.calificacion.nombre] = {
                            'cantidad': 1,
                        }
                    else:
                        dic_gestion_finalizado[programacion.gestion.calificacion.nombre]['cantidad'] += 1
                else:
                    if not ('SIN_CALIFICACION' in dic_gestion_finalizado.keys()):
                        dic_gestion_finalizado['SIN_CALIFICACION'] = {
                            'cantidad': 1,
                        }
                    else:
                        dic_gestion_finalizado['SIN_CALIFICACION']['cantidad'] += 1

        lista_colores = ['#b6fff1','#ffeccb','#dccff7','#acb0c3','#fbccdd','#ffe1e2','#182750','#816cfd','#989ebf','#640893','#93082E','#25d5e4']

        counter = 0
        for item in dic_gestion_finalizado:
            detalle_gestion.append(
                {
                    'calificacion': item,
                    'cantidad': dic_gestion_finalizado[item]['cantidad'],
                    'porcentaje': round((dic_gestion_finalizado[item]['cantidad']/len(lista_gestiones)) * 100),
                    'color': lista_colores[counter] 
                }

            )
            counter += 1

        if len(detalle_gestion) == 0:
            context['detalle_gestion'] = 'None'
            context['lista_colores'] = 'None'
        else:

            context['detalle_gestion'] = detalle_gestion
            context['lista_colores'] = lista_colores

        lista_gestiones_efectividad = Programacion.objects.filter(date_1__range=(f_inicio_efectividad,f_fin_efectividad), estado_programacion='gestionado',agente=sergio_alonso);
        base_efectividad= lista_gestiones_efectividad.count()
        fuera_fecha = Programacion.objects.filter(date_1__range=(f_inicio_efectividad,f_fin_efectividad), estado_programacion='gestionado',agente=sergio_alonso, gestion__calificacion__nombre = 'FUERA DE FECHA').count();
        veiculo_vendido = Programacion.objects.filter(date_1__range=(f_inicio_efectividad,f_fin_efectividad), estado_programacion='gestionado',agente=sergio_alonso, gestion__calificacion__nombre = 'VEHICULO VENDIDO').count();
        queryset_vendidas = lista_gestiones_efectividad.filter(
                Q(gestion__calificacion__nombre='VENTA EFECTIVA') | Q(gestion__calificacion__nombre='VENTA EFECTIVA + AP')
            ).count()
        print('vendidaaaaaaaaaaaas : ', queryset_vendidas)

        context['base_efectividad'] = base_efectividad
        context['veiculo_vendido'] = veiculo_vendido
        context['fuera_fecha'] = fuera_fecha
        base_disponible = base_efectividad-(fuera_fecha+veiculo_vendido)
        context['base_disponible'] = base_disponible
        context['objetivo_ventas'] = round(base_disponible*0.3)
        context['ventas'] = queryset_vendidas
        if(queryset_vendidas != 0):
            ventas_base = round((queryset_vendidas/base_disponible)*100,2)
            context['ventas_base'] = ventas_base
            context['ventas_base_objetivo'] = round(queryset_vendidas/round(base_disponible*0.3)*100,2)
        else:
            context['ventas_base'] = 0
            context['ventas_base_objetivo'] = 0     
        
        context['gestion_diario_ventas'] = reporte_gestion_diario_ventas(f_inicial_2 = f_inicio_ventas,f_final_2 = f_fin_ventas)
            
        context['gestion_diario_contacto'] = reporte_gestion_diario(f_inicial = f_inicio_contacto,f_final = f_fin_contacto)
        
        dic_ciudad = {}
        programaciones = Programacion.objects.filter(gestion__fecha_creacion__range=(f_inicio_ciudad,f_fin_ciudad))
        
        for programacion in programaciones:          
            if (programacion.char_4):
                if not(programacion.char_3 == '' or programacion.char_3 == None):
                    # if (programacion.gestion):
                    #     if (programacion.gestion.calificacion):
                    if (programacion.char_2):
                        if not (programacion.char_3 in dic_ciudad.keys()):
                        
                            dic_ciudad[programacion.char_3] = {
                                programacion.char_2 : {
                                    'cantidad':1,
                                },
                            }
                        else:
                            dic_ciudad[programacion.char_3]
                            if not(programacion.char_2 in dic_ciudad[programacion.char_3].keys()):
                                dic_ciudad[programacion.char_3][programacion.char_2] = {
                                    'cantidad':1,
                                }
                            else:
                                dic_ciudad[programacion.char_3][programacion.char_2]['cantidad']+=1 
                                    
        print('FINALLLLL:: ', type(dic_ciudad))
        
        
        for ciudad in dic_ciudad:
            print('ciudad : ',ciudad)
            for sala in dic_ciudad[ciudad]:
                base_cuidad = Programacion.objects.all().count()
                queryset_vendidas_tmp = programaciones.filter(
                        Q(gestion__calificacion__nombre='VENTA EFECTIVA') | Q(gestion__calificacion__nombre='VENTA EFECTIVA + AP') 
                    )
                queryset_vendidas_filtrado = queryset_vendidas_tmp.filter(char_2=sala , char_3=ciudad)
                base_cargada = programaciones.filter(char_2=sala).count()
                base_disponible = programaciones.filter(char_2=sala).exclude(gestion__calificacion__nombre='FUERA DE FECHA').exclude(gestion__calificacion__nombre='VEHICULO VENDIDO').count()
                cantidad_ventas = queryset_vendidas_filtrado.count()
                print('base_cuidad : ', base_cuidad)
                print('base_cargada : ', base_cargada)
                print('base_disponible : ', base_disponible)
                print('cantidad_ventas : ', cantidad_ventas)
                detalle_ciudad.append(
                    {
                        'ciudad': ciudad,
                        'sala': sala,
                        'cantidad': dic_ciudad[ciudad][sala]['cantidad'],###ventas original
                        'base_cargada': base_cargada,
                        'base_disponible':base_disponible,
                        'venta_efectiva':cantidad_ventas,
                        'efectividad_porcentaje' : round((dic_ciudad[ciudad][sala]['cantidad']/base_disponible)* 100, 2)
                    }

                )
        print('XXXXXXX', detalle_ciudad)
        
        context['detalle_ciudad'] = detalle_ciudad        
    

        return context


def reporte_gestion_diario_ventas(f_inicial_2=None, f_final_2=None, objetivo_ventas=0.3, delta_days=15):
    print('llego a la funcion ventas reporte_gestion_diario_ventas')
    print('final: ',f_final_2)
    print('inicial: ',f_inicial_2)
    if not (f_final_2):
        f_final_tmp = dt.datetime.now()
        f_final_2 = dt.datetime.strptime('{} 23:59:59'.format(f_final_tmp.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

    if not(f_inicial_2):
        f_inicial_tmp = dt.datetime.now() - dt.timedelta(days=delta_days)
        f_inicial_2 = dt.datetime.strptime('{} 00:00:00'.format(f_inicial_tmp.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

    if (f_final_2 and f_inicial_2):
        if (f_inicial_2 > f_final_2):
            return []

    sergio_alonso = User.objects.get(pk=3)
    queryset_gestionadas = Programacion.objects.filter(date_1__range=[f_inicial_2, f_final_2], estado_programacion = 'gestionado',agente=sergio_alonso)
    print('query :: ', queryset_gestionadas)
    lista_fechas = []
    f_analisis = f_inicial_2
    while (f_analisis < f_final_2):
        print('entro en el while')
        f_analisis_final = dt.datetime.strptime('{} 23:59:59'.format(f_analisis.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')
        queryset_gestionadas_xfecha = queryset_gestionadas.filter(date_1__range=[f_analisis, f_analisis_final])
        print('query despues de el range de fechas ', queryset_gestionadas_xfecha)
        if (queryset_gestionadas_xfecha.count() > 0):
            queryset_vendidas_xfecha = queryset_gestionadas_xfecha.filter(
                Q(gestion__calificacion__nombre='VENTA EFECTIVA') | Q(gestion__calificacion__nombre='VENTA EFECTIVA + AP')
            )
            cantidad_gestiones = queryset_gestionadas_xfecha.count()
            cantidad_objetivo_ventas = round(queryset_gestionadas_xfecha.count() * objetivo_ventas)
            cantidad_ventas = queryset_vendidas_xfecha.count()
            try:
                porcentaje_cumplimiento = round((cantidad_ventas / cantidad_objetivo_ventas) * 100, 2)
            except:
                porcentaje_cumplimiento = 0

            lista_fechas.append(
                {
                    'fecha': f_analisis,
                    'fecha_label': f_analisis.strftime('%d %b'),
                    'cantidad_gestiones': cantidad_gestiones,
                    'objetivo_ventas': cantidad_objetivo_ventas,
                    'cantidad_ventas': cantidad_ventas,
                    'cumplimiento': porcentaje_cumplimiento,
                }
            )
        f_analisis += dt.timedelta(days=1)
    print('LISTA VENTAS ', lista_fechas)

    return lista_fechas

def reporte_gestion_diario(f_inicial=None, f_final=None, objetivo_diario=0.7, delta_days=15):

    print('llego a la funcion ventas reporte_gestion_diario')
    print('final: ',f_final)
    print('inicial: ',f_inicial)

    if not (f_final):
        f_final_tmp = dt.datetime.now()
        f_final_2 = dt.datetime.strptime('{} 23:59:59'.format(f_final_tmp.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

    if not(f_inicial):
        f_inicial_tmp = dt.datetime.now() - dt.timedelta(days=delta_days)
        f_inicial = dt.datetime.strptime('{} 00:00:00'.format(f_inicial_tmp.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')

    if (f_final and f_inicial):
        if (f_inicial > f_final):
            return []
    
    sergio_alonso = User.objects.get(pk=3)
    queryset_gestionadas = Programacion.objects.filter(gestion__fecha_creacion__range=[f_inicial, f_final], estado_programacion = 'gestionado',agente=sergio_alonso)
    lista_fechas2 = []
    f_analisis = f_inicial

    while (f_analisis < f_final):

        f_analisis_final = dt.datetime.strptime('{} 23:59:59'.format(f_analisis.strftime('%d/%m/%Y')), '%d/%m/%Y %H:%M:%S')
        queryset_gestionadas_xfecha = queryset_gestionadas.filter(gestion__fecha_creacion__range=[f_analisis, f_analisis_final])
        if (queryset_gestionadas_xfecha.count() > 0):
            cantidad_gestiones = queryset_gestionadas_xfecha.count()
            cantidad_objetivo_diario = round(queryset_gestionadas_xfecha.count() * objetivo_diario)
            try:
                porcentaje_cumplimiento = round((cantidad_gestiones / objetivo_diario))
            except:
                porcentaje_cumplimiento = 0
            lista_fechas2.append(
                {
                    'fecha': f_analisis,
                    'fecha_label': f_analisis.strftime('%d %b'),
                    'cantidad_gestiones': cantidad_gestiones,
                    'objetivo_diario': cantidad_objetivo_diario,
                    'cumplimiento': porcentaje_cumplimiento,
                }
            )
        f_analisis += dt.timedelta(days=1)
    print('LIST 2_______:__: ', lista_fechas2)

    return lista_fechas2
