from collections import namedtuple
from django.db.models import Q
from django.conf import settings
from django.contrib.auth.models import Group, UserManager
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect, StreamingHttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic  import (View, ListView, CreateView, UpdateView, DeleteView, FormView, TemplateView, DetailView )
from openpyxl import load_workbook
from tablib import Dataset

import ast
import csv
import io
import json
import pytz
import requests
import datetime as dt
from django.utils.timezone import now, timedelta
from pytz import timezone

from ominicontacto_app.models import Campana, User
from truecontact.chatapi.models import Dialogo
from truecontact.contactos.models import (Programacion, Resultado, TipoIdentificacion,
    Contacto, ContactoDatosContacto, TipoGestion, ResultadoGestion, CalificacionGestion,
    GestionContacto, Motivo, Especialidad, Entidad)

from ..forms.programacion import ProgramacionForm
from datetime import datetime

LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)


local_tz = timezone("America/Bogota")


#===============================================================================
class ProgramacionListView(ListView):
    model = Programacion
    paginate_by = 25
    template_name = 'sanautos/programacion/programacion_list.html'


    def _obtener_campanas(self):
        if (self.request.user.get_agente_profile()):
            agente = self.request.user.get_agente_profile()
            campanas_queues = agente.get_campanas_activas_miembro()
            ids_campanas = []
            for id_nombre in campanas_queues.values_list('id_campana', flat=True):
                split_id_nombre = id_nombre.split('_')
                id_campana = split_id_nombre[0]
                campana = Campana.objects.get(pk=id_campana)
                type_campana = campana.type
                nombre_campana = '_'.join(split_id_nombre[1:])
                ids_campanas.append((id_campana, nombre_campana, type_campana))
            return ids_campanas
        else:
            ids_campanas = []
            return ids_campanas

    def get_queryset(self):
        grupo_supervisor = Group.objects.get(name='Supervisor')
        # grupo_ventas = Group.objects.get(name='VENTAS')
        # grupo_atencion= Group.objects.get(name='ATENCION_CLIENTE')
        # grupo_soporte = Group.objects.get(name='SOPORTE')
        # grupo_logistica = Group.objects.get(name='LOGISTICA')
        # grupo_tecnologia = Group.objects.get(name='TECNOLOGIA')
        user = self.request.user

        agente = user.agenteprofile
        id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])
        campana = Campana.objects.get(pk=id_campana)
    
        queryset = Programacion.objects.filter(char_4=campana)

        # if (grupo_ventas in user.groups.all()):
        #     queryset = queryset.filter(char_5='VENTAS')

        # elif (grupo_atencion in user.groups.all()):
        #     queryset = queryset.filter(char_5='ATENCION_CLIENTE')
        
        # elif (grupo_soporte in user.groups.all()):
        #     queryset = queryset.filter(char_5='SOPORTE')
        
        # elif (grupo_logistica in user.groups.all()):
        #     queryset = queryset.filter(char_5='LOGISTICA')
        
        # elif (grupo_tecnologia in user.groups.all()):
        #     queryset = queryset.filter(char_5='TECNOLOGIA')

        if not (grupo_supervisor in user.groups.all()):
            f_estado_programacion = 'abiertas'
            queryset = queryset.filter(agente=user).order_by('date_1')

        else:
            f_estado_programacion = 'todas'

        if not (self.request.GET.get('estado_programacion') == '' or self.request.GET.get('estado_programacion') == None):
            f_estado_programacion = self.request.GET.get('estado_programacion')

        if not (f_estado_programacion == '' ):
            if(f_estado_programacion == 'todas'):
                queryset = queryset

            elif (f_estado_programacion == 'abiertas'):
                queryset = queryset.filter(
                    Q(estado_programacion='nuevo') | Q(estado_programacion='pendiente')
                )

            elif(f_estado_programacion == 'cerradas'):
                queryset = queryset.filter(
                    estado_programacion='gestionado'
                )

            else:
                queryset = queryset.filter(
                    estado_programacion=f_estado_programacion
                )
        print('este es el request :::' , self.request.GET);

        ########FILTRAR POR FECHA LLAMAR############
        if not (self.request.GET.get('f_inicio_llamar') == '' or self.request.GET.get('f_inicio_llamar') == 'None' or self.request.GET.get('f_inicio_llamar') == None):
            print("===============================================")
            print(self.request.GET.get('f_inicio_llamar'))
            f_inicial_str_llamar = self.request.GET.get('f_inicio_llamar')
            f_inicio_llamar = datetime.strptime('{} 00:00:00'.format(f_inicial_str_llamar,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            f_final_str_llamar = self.request.GET.get('f_fin_llamar')
            f_fin_llamar = datetime.strptime('{} 23:59:59'.format(f_final_str_llamar,'%d-%m-%Y'), '%d-%m-%Y %H:%M:%S')
            print("ete es el boton borrar", self.request.GET.get('boton-borrar'))
            queryset = queryset.filter(date_2__range=(f_inicio_llamar,f_fin_llamar))
            print("filtro" ,queryset )

        agente_programacion = self.request.GET.get('agente_programacion')
        if not(agente_programacion == '' or agente_programacion == None):
            print("Entro como supervisor a revisar si estan los filtros!")
            print("FILTRO: ", agente_programacion)
            if (agente_programacion == 'todas'):
                queryset = queryset
            else:
                agente = User.objects.get(pk=agente_programacion)
                queryset = queryset.filter(agente=agente)


        buscar_tmp = self.request.GET.get('buscar')
        print('este es el buscar tmp ', buscar_tmp)
        if not (buscar_tmp == '' or buscar_tmp == None):
            buscar = buscar_tmp.strip()
            queryset = queryset.filter(
                    Q(contacto__documento__icontains=buscar) |
                    Q(contacto__nombres__icontains=buscar) |
                    Q(contacto__apellidos__icontains=buscar)
            )

        return queryset.order_by('-created_on')

    def get_context_data(self, **kwargs):
        context = super(ProgramacionListView, self).get_context_data(**kwargs)
        grupo_supervisor = Group.objects.get(name='Supervisor')
        # grupo_ventas = Group.objects.get(name='VENTAS')
        # grupo_atencion= Group.objects.get(name='ATENCION_CLIENTE')
        # grupo_soporte = Group.objects.get(name='SOPORTE')
        # grupo_logistica = Group.objects.get(name='LOGISTICA')
        # grupo_tecnologia = Group.objects.get(name='TECNOLOGIA')
        user = self.request.user

        hoy = now().astimezone(local_tz)

        if not (grupo_supervisor in user.groups.all()):
            f_estado_programacion = 'abiertas'
        else:
            f_estado_programacion = 'todas'

        if not (self.request.GET.get('estado_programacion') == '' or self.request.GET.get('estado_programacion') == None):
            f_estado_programacion = self.request.GET.get('estado_programacion')
        f_inicial = self.request.GET.get('f_inicial')
        f_final = self.request.GET.get('f_final')
        if f_inicial !="" or f_final !="":
            context['boton-borrar'] = 'block'

        f_inicial_llamar = self.request.GET.get('f_inicio_llamar')
        f_final_llamar = self.request.GET.get('f_fin_llamar')

        if not f_inicial_llamar == 'None' or f_inicial_llamar == None:
            context['f_inicio_llamar'] = self.request.GET.get('f_inicio_llamar')

        if not f_final_llamar == 'None' or f_final_llamar == None:
            context['f_fin_llamar'] = self.request.GET.get('f_fin_llamar')

        if f_inicial_llamar !='' or f_inicial_llamar !=None or f_inicial_llamar != 'None':
            context['boton-borrar'] = 'block'

        context['contacto_campanas'] = self._obtener_campanas()
        context['motivos'] = Motivo.objects.all()
        context['especialidades'] = Especialidad.objects.all()
        context['entidades'] = Entidad.objects.all()
        context['tipo_consulta'] = [
            ('informacion', 'Información'),
            ('presencial', 'Presencial'),
            ('teleconsulta', 'Teleconsulta'),
            ('no_confirmada', 'No confirmada')
        ]


        context['estados_programacion'] = [
            ('abiertas', 'Abiertas'),
            ('cerradas', 'Cerradas'),
            ('nuevo', 'Nuevo'),
            ('pendiente', 'Pendiente'),
            ('gestionado', 'Gestionada'),
        ]

        agente = user.agenteprofile
        id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])
        campana = Campana.objects.get(pk=id_campana)

        agentes = campana.obtener_agentes()

        # if (grupo_ventas in user.groups.all()):
        #     agentes = User.objects.filter(
        #         Q(username='agente1.ventas')|
        #         Q(username='agente2.ventas')
        #     )

        # elif (grupo_atencion in user.groups.all()):
        #     agentes = User.objects.filter(
        #         Q(username='agente1.atencion')
        #     )
        
        # elif (grupo_soporte in user.groups.all()):
        #     agentes = User.objects.filter(
        #         Q(username='agente1.soporte')
        #     )

        # elif (grupo_logistica in user.groups.all()):
        #     agentes = User.objects.filter(
        #         Q(username='agente1.logistica')|
        #         Q(username='agente2.logistica')
        #     )

        # elif (grupo_tecnologia in user.groups.all()):
        #     agentes = User.objects.filter(
        #         Q(username='agente1.tecnologia')|
        #         Q(username='agente2.tecnologia')
        #     )

        # else:
        #     agentes = User.objects.all().exclude(pk=user.pk)            
        lista_agentes = []
        for i in range(len(agentes)):
            if not (len(agentes[i].user.groups.values_list('name', flat=True))>1):
                lista_agentes.append((str(agentes[i].user.pk), str(agentes[i].user.get_full_name())))            

        context['agentes_programaciones'] = lista_agentes
        context['f_estado_programacion'] = f_estado_programacion

        agente_programacion = self.request.GET.get('agente_programacion')
        if not(agente_programacion == '' or agente_programacion == None):
            context['f_agente'] = agente_programacion

        buscar =  self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar

        is_filter = False

        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages
        print(context)
        return context

class ProgramacionAddView(View):
    # form_class = ProgramacionForm
    template_name = 'sanautos/programacion/programacion_add.html'
    success_url = reverse_lazy('tc-shaio-programacion-list')

    def get(self, request, *args, **kwargs):
        context ={}
        agente = request.user.agenteprofile
        id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])
        campana = Campana.objects.get(pk=id_campana)
        agentes = campana.obtener_agentes()
        lista_agentes = []
        for i in range(len(agentes)):
            if not (len(agentes[i].user.groups.values_list('name', flat=True))>1):
                lista_agentes.append((str(agentes[i].user.pk), str(agentes[i].user.get_full_name())))
        
        context['agentes'] = lista_agentes
        context['form'] = ProgramacionForm()
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        context = {}
        if (request.FILES):
            archivo = request.FILES['archivo']
            format_file = archivo.name.split('.')[-1]
            if (format_file in ['xls', 'xlsx']):
                self._guardar_masivo(archivo)
            else:
                context['form'] = ProgramacionForm
                context['error_masivo'] = '''
                    Formato de archivo incompatible.
                    Formatos permitidos (.xls) - (.xlsx) '''
                return render(request, self.template_name, context)

        else:
            form = ProgramacionForm(request.POST)
            print('errors:', form.errors)
            if form.is_valid():
                programacion = form.save()
                grupo_supervisor = Group.objects.get(name='Supervisor')
                # grupo_ventas = Group.objects.get(name='VENTAS')
                # grupo_atencion= Group.objects.get(name='ATENCION_CLIENTE')
                # grupo_soporte = Group.objects.get(name='SOPORTE')
                # grupo_logistica = Group.objects.get(name='LOGISTICA')
                # grupo_tecnologia = Group.objects.get(name='TECNOLOGIA')

                # if (grupo_ventas in request.user.groups.all()):
                #     programacion.char_5 = 'VENTAS'
                #     programacion.save()

                # elif (grupo_atencion in request.user.groups.all()):
                #     programacion.char_5 = 'ATENCION_CLIENTE'
                #     programacion.save()
                
                # elif (grupo_soporte in request.user.groups.all()):
                #     programacion.char_5 = 'SOPORTE'
                #     programacion.save()

                # elif (grupo_logistica in request.user.groups.all()):
                #     programacion.char_5 = 'LOGISTICA'
                #     programacion.save()

                # elif (grupo_tecnologia in request.user.groups.all()):
                #     programacion.char_5 = 'TECNOLOGIA'
                #     programacion.save()

                if grupo_supervisor in request.user.groups.all():
                    user = request.POST['agentepro']
                    user = User.objects.get(pk=user)

                else:
                    user = request.user
                    programacion.creado_por = user
                
                agente = user.agenteprofile
                print('>>>> ProgramacionAddView AGENTE >>>>', agente)
                id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])
                campana = Campana.objects.get(pk=id_campana)
                print('>>>> ProgramacionAddView CAMPAÑA >>>>', campana)
                programacion.char_4 = campana
                programacion.agente = user
                programacion.save()

            else:
                context['form'] = form
                return render(request, self.template_name, context)

        return HttpResponseRedirect(self.success_url)

    def _guardar_masivo(self, archivo):
        workbook = load_workbook(filename=archivo, read_only=True)
        first_sheet = workbook.sheetnames[0]
        worksheet = workbook[first_sheet]
        registros_procesados = 0
        registros_omitidos = 0
        resultado = []
        for i, row in enumerate(worksheet.iter_rows()):
            if not i == 0:
                programacion = row[0].value
                agente = row[1].value
                documento = row[2].value
                nombre = row[3].value
                telefono1 = row[4].value
                telefono2 = row[5].value
                tipo_estudio = row[6].value
                entidad = row[7].value
                fecha_cita = row[8].value
                hora_cita = row[9].value
                doctor = row[10].value

                if (isinstance(fecha_cita, dt.datetime)):
                    fecha_cita = fecha_cita
                else:
                    try:
                        fecha_cita = dt.datetime.strptime(str(fecha_cita), '%Y%m%d')
                    except:
                        print("Error al procesar la fecha de cita")
                        fecha_cita = None

                if (isinstance(programacion, dt.datetime)):
                    programacion = programacion
                else:
                    try:
                        programacion = dt.datetime.strptime(programacion, '%d/%m/%Y')
                    except:
                        print("Error al procesar la fecha de programación")
                        programacion = None


                if not (documento == '' or documento == None):
                    print('tiene documento')

                    if (nombre and documento):
                        nombre = str(nombre).strip().title()
                        documento = str(documento).strip()

                        # if(TipoIdentificacion.objects.filter(nombre=tipo_documento).exists()):
                        #     tipo_documento = TipoIdentificacion.objects.get(nombre=tipo_documento)
                        # else:
                        #     TipoIdentificacion.objects.get(id='0')

                        if (Contacto.objects.filter(documento=documento).exists()):
                            try:
                                contacto = Contacto.objects.get(documento=documento)
                            except:
                                contacto = Contacto.objects.filter(documento=documento)[0]

                            contacto.save()
                        else:
                            contacto = Contacto.objects.create(
                                nombres = nombre,
                                # apellidos = apellidos,
                                # tipo_identificacion = tipo_documento,
                                documento = documento,
                            )
                            contacto.save()

                        for telefono in [telefono1, telefono2]:
                            if not (telefono == '' or telefono == None):
                                telefono = str(telefono)
                                if not (ContactoDatosContacto.objects.filter(
                                    tipo='telefono', value_1=telefono).exists()):

                                    telefono = ContactoDatosContacto(
                                        contacto = contacto,
                                        label = 'otro',
                                        tipo = 'telefono',
                                        value_1 = telefono
                                    )
                                    telefono.save()

                                    if (len(str(telefono.value_1)) == 10):
                                        num_wp = "57" + telefono.value_1
                                        chat_id = '{}@c.us'.format(num_wp)

                                        if not(Dialogo.objects.filter(chat_id=chat_id).exists()):
                                            dialogo = Dialogo(
                                                chat_id = chat_id,
                                                numero = chat_id.split('@')[0],
                                                es_grupo = False,
                                                contacto = contacto,
                                                estado = 'gestionado'
                                            )
                                            dialogo.save()

                        # correo = None
                        # if not (correo == '' or correo == None):
                        #     correo = str(correo)
                        #     if not (ContactoDatosContacto.objects.filter(
                        #         tipo='email', value_1=correo).exists()):

                        #         correo = ContactoDatosContacto(
                        #             contacto = contacto,
                        #             label = 'otro',
                        #             tipo = 'email',
                        #             value_1 = correo
                        #         )
                        #         correo.save()

                        # direccion = None
                        # if not (direccion == '' or direccion == None):
                        #     direccion = str(direccion)
                        #     if not (ContactoDatosContacto.objects.filter(
                        #         tipo='direccion', value_1=direccion).exists()):

                        #         direccion = ContactoDatosContacto(
                        #             contacto = contacto,
                        #             label = 'otro',
                        #             tipo = 'direccion',
                        #             value_1 = direccion
                        #         )
                        #         direccion.save()

                        
                        if (Campana.objects.filter(nombre="Confirmaciones").exists()):
                            campana = Campana.objects.get(nombre="Confirmaciones")
                        else:
                            campana = None
                        
                        if (User.objects.filter(username=agente).exists()):
                            agente = User.objects.get(username=agente)
                        else:
                            agente = None
                        
                        hora_cita = dt.datetime.strptime(str(hora_cita), '%H%M%S').time()
                        fechahora_cita = dt.datetime.combine(fecha_cita, hora_cita)
                        fechahora_cita = dt.datetime.strftime(fechahora_cita, '%Y-%m-%dT%H:%M')
                        if (Programacion.objects.filter(contacto__documento=documento).exists()):
                            print('si existe, creará solo si esta en gestionado')
                            programaciones_tmp = Programacion.objects.filter(contacto__documento=documento)

                            for programacion_tmp in programaciones_tmp:
                                if (programacion_tmp.estado_programacion != 'gestionado'):
                                    programacion_tmp.estado_programacion = 'gestionado'
                                    programacion_tmp.observaciones = "Programación cerrada por programación entrante."
                                    programacion_tmp.save()
                            
                            programacion = Programacion.objects.create(
                                contacto = contacto,
                                agente = agente,
                                char_1 = tipo_estudio,
                                char_2 = entidad,
                                char_3 = doctor,
                                char_4 = campana,
                                date_2 = programacion,
                                date_1 = fechahora_cita
                            )
                            programacion.save()

                                        

                        else:
                            print('la documento no existia voy a crear una nueva y una programacion nueva')
                            programacion = Programacion.objects.create(
                                contacto = contacto,
                                agente = agente,
                                char_1 = tipo_estudio,
                                char_2 = entidad,
                                char_3 = doctor,
                                char_4 = campana,
                                date_2 = programacion,
                                date_1 = fechahora_cita
                            )
                            # print(nombre_campana)
                            programacion.save()
                                
                        
                        registros_procesados +=1
                        r_cargue = "Datos en la linea cargados a programaciones!"


                    else:
                        registros_omitidos += 1
                        r_cargue = "Error, no se pudo cargar los datos en la linea!"

                    resultado.append(
                        {
                            "linea-{}".format(i): r_cargue
                        }
                    )
                else:
                    print('no tiene placa')

        return registros_procesados, registros_omitidos, resultado

class ProgramacionUpdateView(View):
    form_class = ProgramacionForm
    template_name = 'sanautos/programacion/programacion_update.html'
    success_url = reverse_lazy('tc-shaio-programacion-list')

    def get(self, request, *args, **kwargs):
        programacion = get_object_or_404(Programacion, pk=kwargs['pk'])
        context = {
            'form': self.form_class(initial={
                'contacto': programacion.contacto,
                'agente': programacion.agente,
                'nombres': programacion.contacto.nombres,
                'apellidos': programacion.contacto.apellidos,
                'tipo_identificacion': programacion.contacto.tipo_identificacion,
                'documento': programacion.contacto.documento,
                'observaciones': programacion.observaciones,
                'date_1': programacion.date_1.strftime("%Y-%m-%dT%H:%M"),
                'date_2': programacion.date_2.strftime("%Y-%m-%dT%H:%M"),
                'char_1': programacion.char_1,
                'char_2': programacion.char_2,
                'char_3': programacion.char_3,
            })
        }
        return render(request, self.template_name, context)


    def post(self, request, *args, **kwargs):
        context = {}
        programacion = get_object_or_404(Programacion, pk=kwargs['pk'])

        form = self.form_class(request.POST, instance=programacion)
        if form.is_valid():
            programacion = form.update()
            grupo_supervisor = Group.objects.get(name='Supervisor')
            if grupo_supervisor in request.user.groups.all():
                user = request.POST['agente']
                user = User.objects.get(pk=user)
            
            else:
                user = request.user

            agente = user.agenteprofile
            id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])
            campana = Campana.objects.get(pk=id_campana)
            programacion.char_4 = campana
            programacion.agente = user
                
            programacion.save()

        else:
            context['form'] = form
            return render(request, self.template_name, context)
        return HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        context = super(ProgramacionUpdateView, self).get_context_data(**kwargs)
        context['programacion_pk'] = self.kwargs['pk']
        return context

class ProgramacionGestionarView(View):

    def post(self, request, *args, **kwargs):
        print(request.POST)
        user = request.user
        programacion = get_object_or_404(Programacion, pk=kwargs['pk'])
        accion = request.POST.get('accion')

        if (accion == 'finalizar'):
            
            print("Finalizando programacion ! {}".format(programacion.pk))
            tipo_gestion = TipoGestion.objects.get(pk=request.POST.get('tipo'))
            medio = request.POST.get('medio')
            motivo = Motivo.objects.get(pk=request.POST.get('motivo'))
            resultado = Resultado.objects.get(pk=request.POST.get('resultado'))
            especialidad = Especialidad.objects.get(pk=request.POST.get('especialidad'))
            entidad = Entidad.objects.get(pk=request.POST.get('entidad'))
            tipoconsulta = request.POST.get('tipocon')
            observaciones = request.POST.get('observaciones')

            contacto = programacion.contacto

            gestion = GestionContacto.objects.create(
                tipo = tipo_gestion,
                medio = medio,
                motivo = motivo,
                resul = resultado,
                especialidad = especialidad,
                entidad = entidad,
                tipo_consulta = tipoconsulta,
                contacto = contacto,
                metadata = str(
                    {
                        'id_contacto': contacto.pk,
                        'medio': 'manual',
                        'programacion_id': programacion.pk
                    }
                ),
                creado_por = user,
                estado = 'finalizado',
                observaciones = observaciones,
            )

            programacion.estado_programacion = 'gestionado'
            programacion.gestion = gestion
            programacion.save()

        respuesta = {'status': 'ok'}

        return JsonResponse(respuesta)



class CampanasProgramacionManual(View):
    def get(self, request, *args, **kwargs):
        id_campana = self.request.GET.get('id_campana')
        print(">>>>>>>>>>>>>>>><",id_campana)
        
        campana = Campana.objects.get(pk=id_campana)
        calificaciones = CalificacionGestion.objects.filter(campana=campana)
        lista_calificaciones=[]
        for calificacion in calificaciones:
     
            lista_calificaciones.append({
                'id_calificacion':calificacion.pk,
                'nombre_calificacion' : calificacion.nombre 
            })


        return JsonResponse({'calificaciones' : lista_calificaciones})


# ============================== EXPORT CSV ====================================
class Echo:         #Buffer de apoyo para streaming
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value

def stream_data(encabezados, data):
    if encabezados:
        yield encabezados
    for obj in data:
        yield object_get_row(obj)

def object_get_row(obj):
    row = [
        obj.pk,
        obj.created_on.strftime('%d/%m/%Y'),
        obj.agente.username if obj.agente else '',
        obj.contacto.get_full_name() if obj.contacto else '',
        obj.contacto.documento if obj.contacto else '',
        obj.get_estado_programacion_display(),
        obj.char_3,
        obj.char_1,
        obj.chat_2,
        obj.chat_4,
    ]

    if (obj.gestion):
        row = row + [
            obj.gestion.pk if obj.gestion else '',
            obj.gestion.fecha_creacion.strftime('%d/%m/%Y') if obj.gestion else '',
            obj.gestion.get_medio_display() if obj.gestion else '',
            obj.gestion.tipo.nombre if obj.gestion.tipo else '',
            obj.gestion.motivo.nombre if obj.gestion.resultado else '',
            obj.gestion.resul.nombre if obj.gestion.calificacion else '',
            obj.gestion.creado_por.username if obj.gestion else '',
        ]
    else:
        row = row + [
            '',
            '',
            '',
            '',
            '',
            '',
            '',
        ]

    return row

class ProgramacionListExportView(View):

    def get(self, request, *args, **kwargs):
        print("REQUEST ", request.GET)
        queryset = Programacion.objects.all()



        f_estado_programacion = request.GET.get('estado_programacion')

        if not (f_estado_programacion == '' or f_estado_programacion == None):
            if(f_estado_programacion == 'todas'):
                queryset = queryset

            elif (f_estado_programacion == 'abiertas'):
                queryset = queryset.filter(
                    Q(estado_programacion='nuevo') | Q(estado_programacion='pendiente')
                )
            elif(f_estado_programacion == 'cerradas'):
                queryset = queryset.filter(
                    estado_programacion='gestionado'
                )

            else:
                queryset = queryset.filter(
                    estado_programacion=f_estado_programacion
                )

        agente_programacion = self.request.GET.get('agente_programacion')
        if not(agente_programacion == '' or agente_programacion == None):
            if (agente_programacion == 'todas'):
                queryset = queryset
            else:
                agente = User.objects.get(pk=agente_programacion)
                queryset = queryset.filter(agente=agente)


        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            queryset = queryset.filter(
                    Q(contacto__documento__icontains=buscar) |
                    Q(contacto__nombres__icontains=buscar) |
                    Q(contacto__apellidos__icontains=buscar) |
                    Q(agente__username__icontains=buscar)
            )

        print("Esportando {} registros".format(queryset.count()))

        ########## Filtros de fecha llamar en programaciones list ###########
        f_inicio_llamar = self.request.GET.get('f_inicio_llamar')
        f_fin_llamar = self.request.GET.get('f_fin_llamar')

        if not (f_inicio_llamar == '' or f_inicio_llamar == None or f_inicio_llamar == 'None' or f_fin_llamar == '' or f_fin_llamar == None or f_fin_llamar == 'None'):
            f_inicio_llamar = datetime.strptime(f_inicio_llamar, '%d-%m-%Y')
            f_fin_llamar = datetime.strptime(f_fin_llamar, '%d-%m-%Y')
            queryset = queryset.filter(
                date_2__range=(f_inicio_llamar,f_fin_llamar),
            )

        ########## Filtros de Reportes graficas ############
        # f_inicio_detalle = self.request.GET.get('f_inicio_detalle')
        # f_fin_detalle = self.request.GET.get('f_fin_detalle')

        # if not (f_inicio_detalle == '' or f_inicio_detalle == None or f_inicio_detalle == 'None' or f_fin_detalle == '' or f_fin_detalle == None or f_fin_detalle == 'None'):
        #     f_inicio_detalle = datetime.strptime(f_inicio_detalle, '%d-%m-%Y')
        #     f_fin_detalle = datetime.strptime(f_fin_detalle, '%d-%m-%Y')
        #     queryset = queryset.filter(
        #         gestion__fecha_creacion__range=(f_inicio_detalle,f_fin_detalle), estado_programacion='gestionado'
        #     )

        # ########## Filtros de Reportes graficas ############
        # f_inicio_ventas = self.request.GET.get('f_inicio_ventas')
        # f_fin_ventas = self.request.GET.get('f_fin_ventas')

        # if not (f_inicio_ventas == '' or f_inicio_ventas == None or f_inicio_ventas == 'None' or f_fin_ventas == '' or f_fin_ventas == None or f_fin_ventas == 'None'):
        #     f_inicio_ventas = datetime.strptime(f_inicio_ventas, '%d-%m-%Y')
        #     f_fin_ventas = datetime.strptime(f_fin_ventas, '%d-%m-%Y')
        #     queryset = queryset.filter(
        #         date_1__range=(f_inicio_ventas,f_fin_ventas), estado_programacion='gestionado'
        #     )

        # ########## Filtros de Reportes graficas ############
        # f_inicio_contacto = self.request.GET.get('f_inicio_contacto')
        # f_fin_contacto = self.request.GET.get('f_fin_contacto')

        # if not (f_inicio_contacto == '' or f_inicio_contacto == None or f_inicio_contacto == 'None' or f_fin_contacto == '' or f_fin_contacto == None or f_fin_contacto == 'None'):
        #     f_inicio_contacto = datetime.strptime(f_inicio_contacto, '%d-%m-%Y')
        #     f_fin_contacto = datetime.strptime(f_fin_contacto, '%d-%m-%Y')
        #     queryset = queryset.filter(
        #         gestion__fecha_creacion__range=(f_inicio_contacto,f_fin_contacto), estado_programacion='gestionado'
        #     )
        
        # ########## Filtros de Reportes graficas ############
        # f_inicio_ciudad = self.request.GET.get('f_inicio_ciudad')
        # f_fin_ciudad = self.request.GET.get('f_fin_ciudad')

        # if not (f_inicio_ciudad == '' or f_inicio_ciudad == None or f_inicio_ciudad == 'None' or f_fin_ciudad == '' or f_fin_ciudad == None or f_fin_ciudad == 'None'):
        #     f_inicio_ciudad = datetime.strptime(f_inicio_ciudad, '%d-%m-%Y')
        #     f_fin_ciudad = datetime.strptime(f_fin_ciudad, '%d-%m-%Y')
        #     queryset_tmp = queryset.filter(
        #         created_on__range=(f_inicio_ciudad,f_fin_ciudad),estado_programacion='gestionado'
        #     )
        #     queryset_tmp = queryset_tmp.filter(
        #          Q(gestion__calificacion__nombre='VENTA EFECTIVA') | Q(gestion__calificacion__nombre='VENTA EFECTIVA + AP')
        #     )
        #     agente = User.objects.get(pk=agente_programacion)
        #     queryset = queryset_tmp.filter( agente=agente, gestion__creado_por__username=agente)

        print('esto descargare :',queryset)

        pseudo_buffer = Echo()  # Metodo para grabar archivos muy grandes con apoyo de buffer temporal
        encabezados = ['PROGRAMACION ID', 'FECHA_CARGUE', 'PROGRAMACION AGENTE', 'NOMBRE CONTACTO', 'DOCUMENTO CONTACTO',
            'ESTADO PROGRAMACION', 'DOCTOR', 'TIPO DE ESTUDIO', 'ENTIDAD', 'CAMPAÑA','GESTION ID', 'GESTION FECHA', 'GESTION MEDIO',
            'GESTION TIPO', 'GESTION MOTIVO', 'GESTION RESULTADO', 'GESTION AGENTE',
        ]
        writer = csv.writer(pseudo_buffer)
        response = StreamingHttpResponse((writer.writerow(row) for row in stream_data(encabezados, queryset.order_by('-id'))) , content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="informe_programaciones_{0}.csv"'.format(dt.datetime.now(LOCAL_TZ).strftime('%d_%m_%Y'))
        return response
