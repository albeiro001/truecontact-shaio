""" Vistas para la campaña de conmutador """

# Django
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import JsonResponse
from django.views.generic import View, ListView
from django.db.models import Q

# Modelos
from truecontact.custom_client.models import Conmutador, Directorio, DatosDirectorio

# Modelos de omnileads
from ominicontacto_app.models import User, Campana

# Modelos de la app de ChatApi
from truecontact.chatapi.models import Dialogo

# Modelos de la app de contactos
from truecontact.contactos.models import TipoGestion

# Formularios
from truecontact.custom_client.forms import ConmutadorForm

# Utilidades
import json
import ast
import datetime as dt

@method_decorator(login_required, name='dispatch')
class IdentificarDirecctorioLlamadaView(View):
    """Vista para identificar si el número marcado tiene directorio, si no, lo crea."""

    def get(self, request, *args, **kwargs):
        call_data = json.loads(request.GET.get('call_data'))
        telefono = call_data['telefono']

        if DatosDirectorio.objects.filter(numero=telefono, is_active=True).exists():
            directorio = DatosDirectorio.objects.filter(numero=telefono, is_active=True)[0].directorio
    
        else:
            directorio = Directorio(
                nombre = "Desconocido"
            )
            directorio.save()

            dato_directorio = DatosDirectorio(
                directorio = directorio,
                numero = telefono
            )
            dato_directorio.save()

        respuesta = {
            'id' : directorio.pk,
            'nombre' : directorio.nombre,
        }

        return JsonResponse(respuesta)

class ConmutadorGestionView(View):
    """Vista para gesitonar la creción de la gestión de conmutador."""
    def get(self, request, *args, **kwargs):
        call_data = json.loads(request.GET.get('call_data'))

        medio = call_data['medio']
        id_directorio = call_data['id_directorio']
        tipo_llamada = call_data['call_type'] # call_type=1 is OUTBOUND CALL call_type=3 is INBOUND CALL

        if tipo_llamada == '1':
            tipo = 'LLAMADA SALIENTE'
            campana_d = call_data['id_campana']
            campana_d = Campana.objects.get(pk=campana_d)
            campana_d = campana_d.nombre

        elif tipo_llamada == '2':
            tipo = 'MANUAL'
        elif tipo_llamada == '3':
            tipo = 'LLAMADA ENTRANTE'
            campana_d = call_data['id_campana']
            campana_d = Campana.objects.get(pk=campana_d)
            campana_d = campana_d.nombre

        elif tipo_llamada == '4':
            tipo = 'WHATSAPP'
        else:
            tipo = 'MANUAL'

        if (tipo_llamada == '4'):
            chat_id = call_data['chat_id']
            dialogo = Dialogo.objects.get(chat_id=chat_id)
            campana_d = dialogo.campana

        dic_gestion_guardar = {
            'tipo': tipo,
            'medio': medio,
            'directorio': id_directorio,
            'metadata': call_data,
            'agente': request.user,
        }
        
        gestion = autoguardado_gestion(dic_gestion_guardar)


        respuesta = {
            'gestion': gestion.pk,
            'nombre_campana': campana_d,
        }

        return JsonResponse({'respuesta': respuesta})

def autoguardado_gestion(dic_gestion):
    """Función para crear la gestión de conmutador."""
    tipo = TipoGestion.objects.get(nombre__icontains=dic_gestion['tipo'])
    medio = dic_gestion['medio']
    agente = dic_gestion['agente']
    metadata = dic_gestion['metadata']
    directorio = Directorio.objects.get(pk=dic_gestion['directorio'])

    gestion = Conmutador(
        directorio = directorio,
        tipo = tipo,
        medio = medio,
        creado_por = agente,
        metadata = metadata,
    )
    gestion.save()

    return gestion

@method_decorator(login_required, name='dispatch')
class CargarGestionConmutadorView(View):
    """Vista para enviar la informaición de una gestión."""
    def get(self, request, *args, **kwargs):
        id_gestion = request.GET.get('id_gestion')
        gestion = Conmutador.objects.get(pk=id_gestion)
        print('>>> CargarGestionConmutador >>> gestion.directorio > ',gestion.directorio)
        if gestion.directorio != None:
            historial = []
            gestiones = Conmutador.objects.filter(directorio = gestion.directorio).exclude(id=id_gestion)[0:6]
            for pasadas in gestiones:
                historial.append(
                    {
                        'id': pasadas.pk,
                        'fecha_creacion': pasadas.created.strftime('%d/%m/%Y, %H:%M:%S %p'),
                        'medio': "No.{}: {}".format(pasadas.pk, pasadas.get_medio_display()),
                        'estado': pasadas.get_estado_display()
                    }
                )
        else:
            historial = []

        call_data = ast.literal_eval(gestion.metadata)
        respuesta = {
            'id': gestion.pk,
            'id_directorio': gestion.directorio.pk if gestion.directorio else '',
            'directorio_full_name': gestion.directorio.nombre if gestion.directorio else call_data['telefono'],
            'call_data': call_data,
            'tipo': gestion.tipo.pk if gestion.tipo else '',
            'tipo_nombre': gestion.tipo.nombre if gestion.tipo else '',
            'medio': gestion.medio if gestion.medio else '',
            'medio_display' : gestion.get_medio_display() if gestion.medio else '',
            'url_grabacion': gestion.get_url_grabacion(),
            'agente': gestion.creado_por.get_full_name(),
            'metadata': gestion.metadata,
            'historial': historial,
            'nombre' : gestion.nombre if gestion.nombre else '',
            'directorio_destino' : gestion.directorio_destino.id if gestion.directorio_destino else '',
            'directorio_destino_nombre' : gestion.directorio_destino.nombre if gestion.directorio_destino else '',
            'telefono' : gestion.telefono.id if gestion.telefono else '',
            'telefono_numero' : gestion.telefono.numero if gestion.telefono else '',
            'resultado' : gestion.resultado if gestion.resultado else '',
            'resultado_display' : gestion.get_resultado_display() if gestion.resultado else '',
            'observaciones' : gestion.observaciones if gestion.observaciones else '',
            'fecha_gestion': gestion.created.strftime("%d/%m/%Y %H:%M"),
            'estado': gestion.get_estado_display() if gestion.estado else '' 
        }

        return JsonResponse({'respuesta': respuesta})

class ConsularTelefonoView(View):
    """Vista para consultar si está registrado un teléfono o no en la base de datos."""
    def get(self, request, *args, **kwargs):
        numero_telefono = request.GET.get('numero_telefono')

        respuesta = {}

        if DatosDirectorio.objects.filter(numero=numero_telefono, is_active=True).exists():
            dato = DatosDirectorio.objects.filter(numero=numero_telefono, is_active=True).values("directorio__nombre")[0]
            respuesta['message'] = "El número de teléfono {} ya está resgistrado y pertenece al directorio {}.".format(numero_telefono, dato['directorio__nombre'])
            respuesta['data'] = {
                'existe' : True
            }
        
        else :
            respuesta['message'] = "El número de teléfono {} no está registrado".format(numero_telefono)
            respuesta['data'] = {
                'existe' : False
            }
        
        return JsonResponse(respuesta)

class CrearDirectorioView(View):
    """Vista para crear un directorio con los números dados."""
    def post(self, request, *args, **kwargs):
        post_data = json.loads(request.body.decode("utf-8"))

        nombre_directorio = post_data['nombre_directorio']
        labels = post_data['labels']
        numeros = post_data['numeros']

        respuesta = {}

        if Directorio.objects.filter(nombre=nombre_directorio).exists():
            respuesta['message'] = "Este nombre de directorio ya existe."
            respuesta['errors'] = True
        else:
            directorio = Directorio(
                nombre=nombre_directorio
            )
            directorio.save()
            for i, numero in enumerate(numeros):
                if not DatosDirectorio.objects.filter(numero=numero, is_active=True).exists():
                    DatosDirectorio.objects.create(
                        directorio = directorio,
                        label = labels[i],
                        numero=numero
                    )
            
            respuesta['message'] = "Directorio creado correctamente"
            respuesta['errors'] = False
            respuesta['data'] = {
                'nombre' : directorio.nombre,
                'id' : directorio.id
            }

        return JsonResponse(respuesta)

class ObtenerTelefonosDirectorio(View):
    """Vista para obtener los teléfonos de un directorio específico."""
    def get(self, request, *args, **kwargs):
        id_directorio = request.GET.get('id_directorio')
        directorio = Directorio.objects.get(pk=id_directorio)
        telefonos = DatosDirectorio.objects.filter(directorio=directorio, is_active=True).values('id','numero')

        respuesta = {
            'data' : list(telefonos)
        }

        return JsonResponse(respuesta)

class GuardarGestionView(View):
    """Vista para guardar la gestión de conmutador."""

    def post(self, request, *args, **kwargs):
        post_data = json.loads(request.body.decode("utf-8"))

        id_gestion = post_data['id_gestion_conmutador']

        gestion_conmutador = Conmutador.objects.get(pk=id_gestion)

        form = ConmutadorForm(post_data, instance=gestion_conmutador)

        respuesta = {}

        if form.is_valid():
            if form.has_changed():
                form.save()
                respuesta['message'] = "Formulario actualizado correctamente."
                respuesta['errors'] = False

            if post_data['finalizar'] == True:
                gestion_conmutador.estado = "finalizado"
                gestion_conmutador.save()
                respuesta['message'] = "¡Gestión guardada con éxito!"
                respuesta['errors'] = False
                respuesta['data'] = {
                    'div_gestion' : 'gestion-{}'.format(gestion_conmutador.get_call_id()),
                    'id_gestion' : gestion_conmutador.id
                }
        else:
            respuesta['message'] = "Hubo un error al actualizar o guardar el formulario."
            respuesta['erros'] = True

        return JsonResponse(respuesta)

class ConsultarDirectorioView(View):
    """Vista para consultar un directorio y sus números."""
    def get(self, request, *args, **kwargs):
        id_directorio = request.GET.get('id_directorio')

        directorio = Directorio.objects.get(pk=id_directorio)

        telefonos = DatosDirectorio.objects.filter(directorio=directorio, is_active=True).values("id","label","numero")

        respuesta = {
            "directorio_nombre" : directorio.nombre,
            "telefonos": list(telefonos)
          }

        return JsonResponse(respuesta)

class ModificarDirectorioView(View):
    """Vista para modificar un directorio."""

    def post(self, request, *args, **kwargs):
        post_data = json.loads(request.body.decode("utf-8"))

        id_directorio = post_data['id_directorio']
        nombre_directorio = post_data['nombre_directorio']
        labels = post_data['labels']
        numeros = post_data['numeros']

        respuesta = {}

        if Directorio.objects.filter(nombre=nombre_directorio).exclude(id=id_directorio).exists():
            respuesta['message'] = "Este nombre de directorio ya existe."
            respuesta['errors'] = True                
        else:
            directorio = Directorio.objects.get(pk=id_directorio)
            directorio.nombre = nombre_directorio
            directorio.save()

            for i, numero in enumerate(numeros):
                if not DatosDirectorio.objects.filter(numero=numero, is_active=True).exists():
                    DatosDirectorio.objects.create(
                        directorio = directorio,
                        label = labels[i],
                        numero=numero
                    )

            respuesta['message'] = "Directorio actualizado con éxito."
            respuesta['errors'] = False


        return JsonResponse(respuesta)

class EliminarTelefonoDirectorioView(View):
    """Vista para actualizar el campo is_active del modelo de datos del directorio."""
    def post(self, request, *args, **kwargs):
        post_data = json.loads(request.body.decode("utf-8"))

        id_telefono = post_data['id_telefono']
        respuesta = {}
        if DatosDirectorio.objects.filter(id=id_telefono).exists():
            dato = DatosDirectorio.objects.get(id=id_telefono)
            dato.is_active = False
            dato.save()
            respuesta['message'] = "Teléfono eliminado con éxito."
            respuesta['errors'] = False
        else:
            respuesta['message'] = "¡Oh no!"
            respuesta['errors'] = True
        
        return JsonResponse(respuesta)

class GestionesConmutadorListView(ListView):
    """Vista para listar las gestiones de conmutador."""
    model = Conmutador
    context_object_name = 'gestiones'
    paginate_by = 20
    template_name = 'conmutador/gestiones_conmutador_list.html'

    def get_queryset(self):
        user = self.request.user
        if user.groups.filter(name = 'Supervisor').exists():
            queryset = Conmutador.objects.all().order_by('-id')
        else:
            queryset = Conmutador.objects.filter(creado_por=user).order_by('-id')

        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            queryset = queryset.filter(
                    Q(metadata__icontains=buscar) |
                    Q(creado_por__username__icontains=buscar) |
                    Q(directorio__nombre__icontains=buscar) |
                    Q(telefono__numero__icontains=buscar) )

        f_inicial = self.request.GET.get('f_inicial')
        f_final = self.request.GET.get('f_final')
        if not (f_inicial == None or f_inicial==''):
            f_inicial = dt.datetime.strptime(f_inicial, '%Y-%m-%d')
        else:
            f_inicial = None
        if not (f_final == None or f_final==''):
            f_final = dt.datetime.strptime(f_final, '%Y-%m-%d') + dt.timedelta(days=1)
        else:
            f_final = None
        if not (f_inicial == None or f_final == None):
            queryset = queryset.filter(created__range=(f_inicial, f_final))

        return queryset

    def get_context_data(self, **kwargs):
        context = super(GestionesConmutadorListView, self).get_context_data(**kwargs)
        buscar =  self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar

        f_inicial = self.request.GET.get('f_inicial')
        f_final = self.request.GET.get('f_final')
        if not (f_inicial == None or f_inicial==''):
            context['f_inicial'] = f_inicial
        else:
            f_inicial = None
        if not (f_final == None or f_final==''):
            context['f_final'] = f_final
        else:
            f_final = None
        if not (f_inicial == None or f_final == None):
            context['filtro'] = '&f_inicial={0}&f_final={1}'.format(context['f_inicial'], context['f_final'])
            is_filter = True

        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context

@method_decorator(login_required, name='dispatch')
class DirectorioListView(ListView):
    """docstring for UserListView """
    model = Directorio
    context_object_name = 'directorios'
    template_name = 'conmutador/directorio_list.html'
    paginate_by = 18

    def _obtener_campanas(self):
        if (self.request.user.get_agente_profile()):
            agente = self.request.user.get_agente_profile()
            campanas_queues = agente.get_campanas_activas_miembro()
            ids_campanas = []
            for id_nombre in campanas_queues.values_list('id_campana', flat=True):
                split_id_nombre = id_nombre.split('_')
                id_campana = split_id_nombre[0]
                campana = Campana.objects.get(pk=id_campana)
                type_campana = campana.type
                nombre_campana = '_'.join(split_id_nombre[1:])
                ids_campanas.append((id_campana, nombre_campana, type_campana))
            return ids_campanas
        else:
            ids_campanas = []
            return ids_campanas


    def get_queryset(self):
        queryset = Directorio.objects.filter(is_active=True)

        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            for termino in buscar.split():
                queryset_temp = queryset.filter(nombre__icontains=termino)
                queryset_final = queryset_temp | queryset_temp
            queryset = queryset_final

        return queryset

    def get_context_data(self, **kwargs):
        context = super(DirectorioListView, self).get_context_data(**kwargs)
        context['total_directorios'] = Directorio.objects.all().count()

        context['contacto_campanas'] = self._obtener_campanas()
        display = self.request.GET.get('display')
        if display == None:
            display = 'kanban'
        context['display'] = display

        buscar =  self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar

        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context