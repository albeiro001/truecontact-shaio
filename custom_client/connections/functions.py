from ..models.oportunidad import Oportunidad

def obtener_cantidad_oportunidades():
    oportunidades_en_gestion = {
        'cantidad': 0
    }
    oportunidades_nuevos = {
        'cantidad': Oportunidad.objects.filter(estado='nuevo').count()
    }

    return oportunidades_en_gestion, oportunidades_nuevos
