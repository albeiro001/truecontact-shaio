from django.apps import AppConfig


class SanautosConfig(AppConfig):
    name = 'sanautos'
