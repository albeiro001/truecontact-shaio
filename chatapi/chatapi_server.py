import os
import sys
import django
sys.path.append('/opt/omnileads/ominicontacto')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ominicontacto.settings")
django.setup()
from datetime import datetime
from django.conf import settings
from django.utils import timezone

import base64
import json
import datetime as dt
import pytz
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import sys
import ssl
import time

from ominicontacto_app.models import AgenteProfile, User, Campana

from truecontact.chatapi.controlador import ChatApi
from truecontact.chatapi.models import Dialogo, Mensaje

LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)

chat_api = ChatApi()
clients = []

def write_to_clients():
    dialogos_activos = chat_api.obtener_dialogos_activos()

    datos = {
        'accion': 'agregar_dialogos',
        'dialogos_en_gestion': dialogos_activos[0],
        'dialogos_nuevos': dialogos_activos[1],
    }
    
    for cliente in clients:
        # print("Enviando datos: ", datos)
        cliente.write_message(json.dumps(datos)) #MANDAR LOS DIALOGOS A LOS AGENTES CONECTADOS 01.

class WSHandler(tornado.websocket.WebSocketHandler):

    def sendMessage(self, mensaje):
        for client in clients:
            client.write_message(mensaje)

    def check_origin(self, origin):
        return True

    def open(self):
        clients.append(self)

    def on_message(self, message):
        data = json.loads(message)
        accion = data['accion']
        if (accion == 'cambiar_estado'):
            self.sendMessage(message)

        elif (accion == 'enviar_mensaje'):
            print('DATA: ', data.keys())
            id_dialogo = data['id_dialogo']
            tipo = data['tipo']
            mensaje = data['body']

            id_agente = data['id_agente']
            user = AgenteProfile.objects.get(pk=id_agente).user

            if ( Dialogo.objects.filter(pk=id_dialogo).exists() ):
                dialogo = Dialogo.objects.get(pk=id_dialogo)
            else:
                dialogo = Dialogo(
                    chat_id = id_dialogo,
                    numero = id_dialogo.split('@')[0],
                    es_grupo = False,
                )
                dialogo.save()

            if (tipo == 'chat'):
                respuesta = chat_api.enviar_mensaje(
                    dialogo = dialogo,
                    body = mensaje
                )

            elif (tipo == 'image'):
                respuesta = chat_api.enviar_archivo(
                    dialogo = dialogo,
                    body = mensaje,
                    filename = data['filename'],
                    caption = data['caption']
                )

            elif (tipo == 'document'):
                respuesta = chat_api.enviar_archivo(
                    dialogo = dialogo,
                    body = mensaje,
                    filename = data['filename']
                )

            elif (tipo == 'ptt'):
                respuesta = chat_api.enviar_audio(
                    dialogo = dialogo,
                    body = mensaje,
                    filename = data['filename']
                )
            print("RESPUESTA RECIBIDA: ", respuesta)

            mensaje_model = Mensaje(
                id = respuesta['id'],
                dialogo = dialogo,
                body = mensaje,
                tipo = tipo,
                from_me = True,
                fecha_recibido = dt.datetime.now(LOCAL_TZ),
                leido = True,
                agente = user,
            )
            print('Guardando mensaje')
            mensaje_model.save()
            print(mensaje_model.id)

            print("Respuesta Enviar Mensaje: ", respuesta)

        elif (accion == 'transferir_dialogo'):
            if ( data['id_agente'] == '' ):
                id_dialogo = data['id_dialogo']
                id_campana = data['id_campana']

                campana_transf = Campana.objects.get(pk=id_campana)
                dialogo = Dialogo.objects.get(pk=id_dialogo)

                dialogo.user = None
                dialogo.campana = campana_transf.nombre
                dialogo.estado = 'nuevo'
                dialogo.save()
            else:
                id_dialogo = data['id_dialogo']
                id_agente = data['id_agente']
                dialogo = Dialogo.objects.get(pk=id_dialogo)
                agente = AgenteProfile.objects.get(pk=id_agente)
                dialogo.user = agente.user
                dialogo.save()

    def on_close(self):
        print ('conexion Cerrada !')
        clients.remove(self)

    def default(self):
        pass

application = tornado.web.Application([
  (r'/ws', WSHandler),
])

if __name__ == "__main__":
    # if sys.platform == 'win32':
    #     asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    # ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    # ssl_ctx.load_cert_chain(os.path.join('/opt/omnileads/nginx_certs/', "cert.pem"),
    #                     os.path.join('/opt/omnileads/nginx_certs/', "key.pem"))

    ssl_options = {
        'certfile': '/etc/nginx/ssl/nginxlogytec.crt',
        'keyfile': '/etc/nginx/ssl/nginxlogytec.key'
    }

    http_server = tornado.httpserver.HTTPServer(application, ssl_options=ssl_options)
    # http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(9090,"0.0.0.0")
    tornado.ioloop.PeriodicCallback(write_to_clients, 5000).start()
    tornado.ioloop.IOLoop.instance().start()
