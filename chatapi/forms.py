from django import forms
from truecontact.chatapi.models import HorarioWhatapp

DIAS = [ 
            'lunes',
            'martes',
            'miercoles',
            'jueves',
            'viernes',
            'sabado',
            'domingo',
        ]


class HorarioWhatsappForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        campana_queryset = kwargs.pop('campana_queryset', None)
        super().__init__(*args, **kwargs)

        if campana_queryset:
            self.fields['campana'].queryset = campana_queryset
        
    class Meta:
        model = HorarioWhatapp
        fields = ['campana', 'dia','hora_inicio', 'hora_final', 'nombre', 'mensaje_espera']
