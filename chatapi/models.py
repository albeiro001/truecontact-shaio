from django.db import models
from django.db.models import Max
from ominicontacto_app.models import User, Campana
from truecontact.contactos.models import Contacto, GestionContacto


from multiselectfield import MultiSelectField


ESTADO_DIALOGO = [
    ('nuevo', 'Nuevo'),
    ('bot', 'Bot'),
    ('encuesta', 'Encuesta'),
    ('en_gestion', 'En Gestion'),
    ('gestionado', 'Gestionado'),
]
class Dialogo(models.Model):
    chat_id = models.CharField(primary_key=True, max_length=64)
    numero = models.CharField(max_length=32)
    imagen = models.ImageField(upload_to='chatapi/dialogo/img/', blank=True, null=True)
    estado = models.CharField(max_length=32, choices=ESTADO_DIALOGO, default='bot')
    es_grupo = models.BooleanField(default=False)
    metadata = models.TextField(blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True)
    contacto = models.ForeignKey(Contacto, models.DO_NOTHING, blank=True, null=True)
    gestion = models.ForeignKey(GestionContacto, models.DO_NOTHING, blank=True, null=True)
    ultimo_mensaje_gestion = models.BigIntegerField(default=0)
    esperar_respuesta = models.BooleanField(default=True)
    modulo = models.IntegerField(default=0)
    bot = models.BooleanField(default=True)
    campana = models.CharField(blank=True, null=True,max_length=32)
    fecha_entrada_cola = models.DateTimeField(blank=True, null=True)
    

    class Meta:
        ordering = ['numero']
        verbose_name = 'dialogo'

    def __str__(self):
        return str(self.chat_id)

    def obtener_total_mensajes(self):
        return self.mensaje_set.count()

    def obtener_total_mensajes_nuevos(self):
        return Mensaje.objects.filter(dialogo=self, leido=False).count()

    def obtener_total_mensajes_leidos(self):
        return Mensaje.objects.filter(dialogo=self, leido=True).count()

    def obtener_nombre_contacto(self):
        if self.contacto:
            nombre = self.contacto.get_full_name()
            nombre_corto = self.contacto.get_short_name()
        else:
            nombre = self.numero
            nombre_corto = self.numero

        return nombre, nombre_corto

    def obtener_imagen_url(self):
        if self.imagen:
            imagen_url = self.imagen.url
        else:
            imagen_url = ''
        return imagen_url

class MensajeNumeroChatApi(models.Model):
    ''' Modelo para mantener el último mensaje en la instancia de WhatsApp '''
    mensaje_numero = models.BigIntegerField(default=0)

class MensajeManager(models.Manager):

    def obtener_ultimo_mensaje(self, dialogo=None):
        if dialogo:
            ultimo_mensaje = self.filter(dialogo=dialogo).aggregate(ultimo_mensaje_numero=Max('mensaje_numero'))
            if (ultimo_mensaje['ultimo_mensaje_numero'] == None):
                ultimo_mensaje['ultimo_mensaje_numero'] = 0
            
            return ultimo_mensaje
        else:
            # por compatibilidad con funciones ya establecidas el ultimo mensaje se envia en
            # diccionario, esta funcion es la que retorna el numero de mensajes hacia arriba que debe traer de
            # chatapi
            info_ultimo_mensaje = {'ultimo_mensaje_numero': 0}
            ultimo_mensaje_numero = MensajeNumeroChatApi.objects.all().order_by('mensaje_numero').last()
            if (ultimo_mensaje_numero):
                info_ultimo_mensaje['ultimo_mensaje_numero'] = ultimo_mensaje_numero.mensaje_numero
            else:
                ultimo_mensaje_numero = MensajeNumeroChatApi.objects.create(
                    mensaje_numero = 0
                )
                info_ultimo_mensaje['ultimo_mensaje_numero'] = ultimo_mensaje_numero.mensaje_numero

            return info_ultimo_mensaje

CREADO_POR = [
    ('usuario', 'Usuaio'),
    ('agente', 'Agente'),
    ('bot', 'Bot'),
]

def agregar_mensaje_numero():
    ultimo_mensaje_numero = Mensaje.objects.aggregate(ultimo_mensaje_numero=Max('mensaje_numero'))
    if ultimo_mensaje_numero['ultimo_mensaje_numero'] == None :
        ultimo_mensaje_numero['ultimo_mensaje_numero'] = 0
    
    return ultimo_mensaje_numero['ultimo_mensaje_numero'] + 1

class Mensaje(models.Model):
    id = models.CharField(primary_key=True, max_length=256)
    archivo = models.FileField(upload_to='chatapi/mensajes/files/%Y/%m/%d/', blank=True, null=True)
    nombre_archivo = models.CharField(max_length=1024, default='')
    dialogo = models.ForeignKey(Dialogo, models.PROTECT)
    body = models.TextField(blank=True)
    tipo = models.CharField(max_length=256) #### REVISAR
    remitente_nombre = models.CharField(max_length=256, blank=True, null=True)
    from_me = models.BooleanField(default=False) #### REVISAR / CAMBIO A ENTERO 0/1
    autor = models.CharField(max_length=256, blank=True, null=True)
    fecha_recibido = models.DateTimeField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    mensaje_numero = models.BigIntegerField(default=agregar_mensaje_numero,blank=True, null=True)
    leido = models.BooleanField(default=False)
    agente = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True)
    caption = models.TextField(blank=True)
    quote_msg_body = models.TextField(blank=True, null=True)
    quote_msg_id = models.CharField(max_length=256, blank=True, null=True)
    quote_msg_type = models.CharField(max_length=256, blank=True, null=True)
    is_forwarded = models.BooleanField(default=False) #### REVISAR / CAMBIO A ENTERO 0/1
    creado_por =  models.CharField(max_length=64, choices=CREADO_POR, default='')

    objects = MensajeManager()

    class Meta:
        ordering = ['-mensaje_numero']
        verbose_name = 'Mensaje'

    def __str__(self):
        return self.id

class Respuesta(models.Model):
    atajo = models.CharField(max_length=50)
    texto = models.TextField(max_length=512)
    activo = models.BooleanField(default=True)

    class Meta:
        ordering = ['atajo']
        verbose_name = 'Respuesta'
        verbose_name_plural = 'Respuestas'
    def __str__(self):
        return self.atajo


class FilesTemp(models.Model):

    file = models.FileField(upload_to="files/",)

    class Meta:
        ordering = ['file']

    def __str__(self):
        return '{}'.format(self.file)

class Encuesta_bot(models.Model):
    gestion = models.ForeignKey(GestionContacto, models.PROTECT,blank=True, null=True)
    recomendaria_bot = models.CharField(max_length=256, blank=True, null=True, default='no')
    calificacion_bot =  models.CharField(max_length=256, blank=True, null=True,  default='1')

    class Meta:
        ordering = ['pk']
        verbose_name = 'Encuesta bot'
        verbose_name_plural = 'Encuestas bots'

DIAS = [
    ('lunes', 'Lunes'),
    ('martes', 'Martes'),
    ('miercoles', 'Miercoles'),
    ('jueves', 'Jueves'),
    ('viernes', 'Viernes'),
    ('sabado', 'Sabado'),
    ('domingo', 'Domingo'),
]

class HorarioWhatapp(models.Model):
    campana = models.ForeignKey(Campana, on_delete=models.PROTECT, related_name='horario_campana')
    nombre = models.CharField(max_length=32)
    hora_inicio = models.TimeField(['%I:%H %p'])
    hora_final = models.TimeField(['%I:%H %p'])
    mensaje_espera =  models.CharField(max_length=256)
    dia =  MultiSelectField(max_length=64, choices=DIAS)
    status = models.BooleanField(default=True)
