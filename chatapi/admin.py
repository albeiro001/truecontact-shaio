from django.contrib import admin
from .models import *

@admin.register(Dialogo)
class DialogoAdmin(admin.ModelAdmin):
    list_display = ('chat_id', 'contacto', 'numero', 'estado', 'es_grupo', 'fecha_creacion', 'imagen')
    list_filter = ('es_grupo',)

@admin.register(Mensaje)
class MensajeAdmin(admin.ModelAdmin):
    list_display = ('id', 'dialogo', 'tipo', 'from_me', 'leido','mensaje_numero', 'fecha_recibido')
    list_filter = ('dialogo__chat_id', 'tipo')

@admin.register(Encuesta_bot)
class Encuesta_botAdmin(admin.ModelAdmin):
    list_display = ('id', 'gestion')

@admin.register(MensajeNumeroChatApi)
class MensajeNumeroChatApiAdmin(admin.ModelAdmin):
    list_display = ('id', 'mensaje_numero')

# @admin.register(ContactoRelacion)
# class ContactoRelacionAdmin(admin.ModelAdmin):
#     list_display = ('contacto', 'contacto_relacionado', 'relacion')
#
# @admin.register(ContactoDatosContacto)
# class ContactoDatosContactoAdmin(admin.ModelAdmin):
#     list_display = ('contacto', 'label', 'tipo')
#     list_filter = ('tipo', )
#
# @admin.register(ResultadoGestion)
# class ResultadoGestionAdmin(admin.ModelAdmin):
#     list_display = ('id', 'nombre', 'medio')
#     list_filter = ('medio', )
#
# @admin.register(GestionContacto)
# class GestionContactoAdmin(admin.ModelAdmin):
#     list_display = ('id', 'tipo', 'medio', 'contacto')
#
# admin.site.register(Pais)
# admin.site.register(PaisEstado)
# admin.site.register(Industria)
# admin.site.register(Relacion)
# admin.site.register(CalificacionGestion)
