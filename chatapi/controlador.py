# -*- coding: utf-8 -*-
from os import pipe
from django.db import models
from django.db.models import Q
from django.conf import settings
from django.core import files
from django.utils.crypto import get_random_string
from os.path import basename
from ominicontacto_app.models import *
from truecontact.chatapi.models import Dialogo, Mensaje, Encuesta_bot, MensajeNumeroChatApi
from truecontact.contactos.models import Contacto, ContactoDatosContacto
from truecontact.custom_client.bot.bot import Bot
from django.utils import timezone
from datetime import datetime
import time

import ast
import datetime as dt
from datetime import  datetime, timedelta
import pytz
import json
import requests
import tempfile

LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)

class ChatApi():

    def __init__(self):
        self.APIUrl = settings.CHATAPI_URL
        self.token = settings.CHATAPI_TOKEN

    def _send_request(self, method, resource, data, parameters=''):
        url_parameters = ''
        for parameter in parameters:
            url_parameters = url_parameters + '&{}={}'.format(parameter[0], parameter[1])

        url = "{}{}?token={}{}".format(self.APIUrl, resource, self.token, url_parameters)
        headers = {'Content-type': 'application/json'}
        if method == 'GET':
            r = requests.get(url, data=json.dumps(data), headers=headers)
        elif method == 'POST':
            r = requests.post(url, data=json.dumps(data), headers=headers)
        else:
            r = requests.get(url, data=json.dumps(data), headers=headers)

        if r.status_code == 200:
            answer = r.json()
        else:
            answer = {'ERROR': r.content}
        return answer

    def _obtener_dialogos_api(self):
        method = 'GET'
        resource = 'dialogs'
        data = {}
        answer = self._send_request(method, resource, data)
        if 'ERROR' in answer:
            return answer
        elif resource in answer:
            dialogs = []
            for dialog in answer[resource]:
                if dialog['id']:
                    if not dialog['image']:
                        dialog['image'] = "/static/img/unknow-user.png"
                    dialog['number'] = dialog['id'].split('-')[0].split('@')[0]
                    dialog['is_group'] = dialog['metadata']['isGroup']
                    dialogs.append(dialog)

            return dialogs
        else:
            return answer

    def _obtener_mensajes_api(self, chat_id=None, last_message_number=None, limit=None ):
        parameters = []
        method = 'GET'
        resource = 'messages'
        data = {}
        if not chat_id == None:
            parameters.append(('chatId', chat_id))
        if not last_message_number == None:
            parameters.append(('lastMessageNumber', last_message_number))
        if not limit == None:
            parameters.append(('limit', limit))
        if len(parameters) > 0:
            answer = self._send_request(method, resource, data, parameters)
        else:
            answer = self._send_equest(method, resource, data)
        if 'ERROR' in answer:
            return answer
        elif resource in answer:
            messages = []
            for message in answer[resource]:
                messages.append(message)
            return messages
        else:
            return answer

    def _enviar_mensaje_api(self, chat_id=None, number=None, body=None):
        parameters = []
        method = 'POST'
        resource = 'sendMessage'
        data = {}
        if (chat_id != None):
            if not (body == '' or body == None):
                data.update(
                    {
                        'chatId': chat_id,
                        'body': body,
                    }
                )
        elif (number != None):
            if not (body == '' or body == None):
                data.update(
                    {
                        'number': number,
                        'body': body,
                    }
                )

        if len(parameters) > 0:
            answer = self._send_request(method, resource, data, parameters)
        else:
            answer = self._send_request(method, resource, data)

        if 'ERROR' in answer:
            return answer
        else:
            return answer

    def _enviar_archivo_api(self, chat_id=None, number=None, body=None, filename=None, caption=''):
        parameters = []
        method = 'POST'
        resource = 'sendFile'
        data = {}
        if (chat_id != None):
            if not (body == '' or body == None):
                data.update(
                    {
                        'chatId': chat_id,
                        'body': body,
                        'filename': filename,
                        'caption': caption
                    }
                )
        elif (number != None):
            if not (body == '' or body == None):
                data.update(
                    {
                        'number': number,
                        'body': body,
                        'filename': filename,
                        'caption': caption
                    }
                )

        if len(parameters) > 0:
            answer = self._send_request(method, resource, data, parameters)
        else:
            answer = self._send_request(method, resource, data)

        if 'ERROR' in answer:
            return answer
        else:
            return answer

    def _enviar_ptt_api(self, chat_id=None, number=None, body=None, filename=None, caption=''):
        parameters = []
        method = 'POST'
        resource = 'sendPTT'
        data = {}
        if (chat_id != None):
            if not (body == '' or body == None):
                data.update(
                    {
                        'chatId': chat_id,
                        'audio': body,
                        'filename': filename,
                        'caption': caption
                    }
                )
        elif (number != None):
            if not (body == '' or body == None):
                data.update(
                    {
                        'number': number,
                        'audio': body,
                        'filename': filename,
                        'caption': caption
                    }
                )

        if len(parameters) > 0:
            answer = self._send_request(method, resource, data, parameters)
        else:
            answer = self._send_request(method, resource, data)

        if 'ERROR' in answer:
            return answer
        else:
            return answer

    def validar_telefono_existe(self, phone=None):
        parameters = []
        method = 'GET'
        resource = 'checkPhone'
        data = {}
        if not phone == None:
            parameters.append(('phone', phone))
        if len(parameters) > 0:
            answer = self._send_request(method, resource, data, parameters)
        else:
            answer = self._send_request(method, resource, data)
        if 'ERROR' in answer:
            return answer
        elif 'result' in answer:
            if(answer['result'] == 'exists'):
                return True
            else:
                return False
        else:
            return answer

    def actualizar_dialogos(self):
        dialogos = self._obtener_dialogos_api()
        for item in dialogos:
            chat_id = item['id']
            if Dialogo.objects.filter(chat_id=chat_id).exists():
                dialogo = Dialogo.objects.get(chat_id=chat_id)
            else:
                dialogo = Dialogo(
                    chat_id = chat_id,
                    numero = item['number'],
                    es_grupo = item['is_group'],
                    metadata = item['metadata'],
                )
                dialogo.save()

            imagen_url = item['image']
            if imagen_url != '':
                try:
                    r = requests.get(imagen_url, stream=True)
                    if r.status_code == 200:
                        format_file = imagen_url[:imagen_url.find('?')].split('.')[-1]
                        name_file = '{}.{}'.format(item['number'], format_file)
                        temp_file = tempfile.NamedTemporaryFile()
                        for block in r.iter_content(1024 * 8):
                            if not block:
                                break
                            temp_file.write(block)
                        temp_file.seek(0)
                        dialogo.imagen.delete()
                        dialogo.imagen.save(basename(name_file), files.File(temp_file))
                except:
                    print ("Generar log, error al consultar la URL")

                if (dialogo.contacto and dialogo.imagen):
                    dialogo.contacto.imagen = dialogo.imagen
                    dialogo.contacto.save()

                elif (dialogo.contacto and dialogo.contacto.imagen):
                    dialogo.imagen = dialogo.contacto.imagen
                    dialogo.save()

    def obtener_mensajes(self, dialogo=None):
        if dialogo:
            ultimo_mensaje = Mensaje.objects.obtener_ultimo_mensaje(dialogo)['ultimo_mensaje_numero']
            mensajes = self._obtener_mensajes_api(chat_id=dialogo.chat_id, last_message_number=ultimo_mensaje, limit=0)
        else:
            ultimo_mensaje = Mensaje.objects.obtener_ultimo_mensaje()['ultimo_mensaje_numero']
            mensajes = self._obtener_mensajes_api(last_message_number=ultimo_mensaje, limit=0)


        mensajes_dialogo = []
        mensajes_dialogo_test = []
        dic_analisis_dialogos = {}
        bot=Bot()

        for item in mensajes:
            chat_id = item['chatId']
            # print (item)
            if Dialogo.objects.filter(chat_id=chat_id).exists():
                dialogo = Dialogo.objects.get(chat_id=chat_id)
                if dialogo.estado == 'gestionado':  # ya no deberia venir aqui
                    if not (dialogo.modulo == 999999): # 1000
                        dialogo.estado = 'bot'    # evaluar si lo quitamos por el boot
                        dialogo.bot = True
                        dialogo.esperar_respuesta = False
                        dialogo.modulo = 0
                        dialogo.metadata = bot.ini_metadata(dialogo=dialogo)
                        dialogo.save()
                    else:
                        mensaje_sinleer = Mensaje.objects.filter(dialogo=dialogo , leido = False)
                        for m in mensaje_sinleer:
                            m.leido = True
                            m.save()
                        dialogo.modulo = 0
                        dialogo.esperar_respuesta = False
                        dialogo.save()
                
                dialogo.fecha_modificacion = dt.datetime.now(pytz.utc)
                dialogo.save()
       
                if settings.CONTACTO_ASOCIADO_AGENTE:
                    if (dialogo.contacto):
                        contacto = dialogo.contacto
                        if (contacto.asesor):
                            dialogo.user = contacto.asesor
                            dialogo.estado = 'en_gestion'
                            dialogo.save()

            else:
                dialogo = Dialogo(
                    chat_id = chat_id,
                    numero = chat_id.split('-')[0].split('@')[0],
                    es_grupo = True if len(chat_id.split('-')) > 1 else False,
                    estado = 'bot',
                    bot = True,
                    metadata = bot.ini_metadata(dialogo=dialogo)

                )
                dialogo.save()

            mensaje_id = item['id']

            if Mensaje.objects.filter(id=mensaje_id).exists():
                mensaje = Mensaje.objects.get(id=mensaje_id)

                if not (mensaje.mensaje_numero):
                    fecha_temp = dt.datetime.fromtimestamp(item['time'])
                    fecha_recibido = LOCAL_TZ.localize(fecha_temp)
                    mensaje.body = item['body']
                    mensaje.tipo = item['type']
                    mensaje.remitente_nombre = item['senderName']
                    mensaje.autor = item['author']
                    mensaje.fecha_recibido = fecha_recibido.astimezone(pytz.utc)
                    #mensaje.mensaje_numero = item['messageNumber']
                    mensaje.caption = item['caption'] if 'caption' in item.keys() and item['caption'] else ''
                    mensaje.save()
                    mensaje_numero_chatapi = MensajeNumeroChatApi.objects.all()[0]
                    mensaje_numero_chatapi.mensaje_numero = item['messageNumber']
                    mensaje_numero_chatapi.save()

                    #########################
                    if (mensaje.from_me == False):
                        print ("Usuario ")
                        if dialogo.estado == 'encuesta':
                            mensaje.creado_por = 'usuario'
                            mensaje.leido = True
                            mensaje.save()
                        else:
                            mensaje.creado_por = 'usuario'
                            mensaje.save()

                    elif (mensaje.dialogo.bot == False and mensaje.dialogo.user != None ):
                        print ("Agente")
                        mensaje.creado_por = 'agente'
                        mensaje.save()
                    elif (mensaje.dialogo.user == None ):
                        print ("esss booot ")
                        dialogo = mensaje.dialogo       #Garantizo que lleguen mensajes nuevos al pasarlo a la cola
                        if dialogo.estado == 'encuesta':
                            mensaje.creado_por = 'bot'
                            mensaje.leido = True
                            mensaje.save()
                        else:
                            mensaje.creado_por = 'bot'
                            mensaje.save()

                    else:
                        print ("nada ")
                    #############


                    if item['type'] in ['image', 'ptt', 'document', 'audio', 'video' ]:
                        url = item['body']
                        if item['type'] == 'document':
                            mensaje.nombre_archivo = item['caption']
                            mensaje.save()

                        format_file = url.split('.')[-1]
                        random_string = get_random_string(length=16)
                        name_file = '{}-{}.{}'.format(mensaje.dialogo.numero, random_string, format_file)
                        try:
                            r = requests.get(url, stream=True)
                            if r.status_code == 200:
                                temp_file = tempfile.NamedTemporaryFile()
                                for block in r.iter_content(1024 * 8):
                                    if not block:
                                        break
                                    temp_file.write(block)
                                temp_file.seek(0)
                                mensaje.archivo.save(basename(name_file), files.File(temp_file))
                        except:
                            print ("Generar log, error al consultar la URL")

            else:
                fecha_temp = dt.datetime.fromtimestamp(item['time'])
                fecha_recibido = LOCAL_TZ.localize(fecha_temp)
                mensaje = Mensaje(
                    id = mensaje_id,
                    dialogo = dialogo,
                    body = item['body'],
                    tipo = item['type'],
                    remitente_nombre = item['senderName'],
                    from_me = True if item['fromMe'] == 1 else False,
                    autor = item['author'],
                    fecha_recibido = fecha_recibido.astimezone(pytz.utc),
                    #mensaje_numero = item['messageNumber'],
                    caption = item['caption'] if 'caption' in item.keys() and item['caption'] else '',
                    quote_msg_id = item['quotedMsgId'],
                    is_forwarded = True if item['isForwarded'] == 1 else False
                )
                mensaje.save()
                mensaje_numero_chatapi = MensajeNumeroChatApi.objects.all()[0]
                mensaje_numero_chatapi.mensaje_numero = item['messageNumber']
                mensaje_numero_chatapi.save()

                #########################
                if (mensaje.from_me == False):
                    print ("Usuario ")
                    if dialogo.estado == 'encuesta':
                        mensaje.creado_por = 'usuario'
                        mensaje.leido = True
                        mensaje.save()
                    else:
                        mensaje.creado_por = 'usuario'
                        mensaje.save()

                elif (mensaje.dialogo.bot == False and mensaje.dialogo.user != None ):
                    print ("Agente")
                    mensaje.creado_por = 'agente'
                    mensaje.save()
                elif (mensaje.dialogo.user == None ):
                    print ("esss booot ")
                    dialogo = mensaje.dialogo       #Garantizo que lleguen mensajes nuevos al pasarlo a la cola
                    if dialogo.estado == 'encuesta':
                        mensaje.creado_por = 'bot'
                        mensaje.leido = True
                        mensaje.save()
                    else:
                        mensaje.creado_por = 'bot'
                        mensaje.save()

                else:
                    print ("nada ")
                #############
     
                if item['type'] in ['image', 'ptt', 'document', 'audio', 'video' ]:
                    url = item['body']
                    if item['type'] == 'document':
                        mensaje.nombre_archivo = item['caption']
                        mensaje.save()

                    format_file = url.split('.')[-1]
                    random_string = get_random_string(length=16)
                    name_file = '{}-{}.{}'.format(mensaje.dialogo.numero, random_string, format_file)
                    try:
                        r = requests.get(url, stream=True)
                        if r.status_code == 200:
                            temp_file = tempfile.NamedTemporaryFile()
                            for block in r.iter_content(1024 * 8):
                                if not block:
                                    break
                                temp_file.write(block)
                            temp_file.seek(0)
                            mensaje.archivo.save(basename(name_file), files.File(temp_file))
                    except:
                        print ("Generar log, error al consultar la URL")
                
                dialogo_m = mensaje.dialogo
                if (dialogo_m.modulo == 999999):
                    dialogo_m.ultimo_mensaje_gestion = (Mensaje.objects.obtener_ultimo_mensaje(dialogo=dialogo_m)['ultimo_mensaje_numero']) if Mensaje.objects.obtener_ultimo_mensaje(dialogo=dialogo_m)['ultimo_mensaje_numero'] !=None else 0
                    dialogo_m.save()

                if (mensaje.dialogo.bot == False):
                    mensajes_dialogo.append(mensaje)
                else:
                    if (mensaje.from_me == False):
                        mensajes_dialogo_test.append(mensaje)
                 


        for mensaje in mensajes_dialogo_test:
            if not mensaje.dialogo.pk in dic_analisis_dialogos.keys():
                dic_analisis_dialogos.update(
                    {mensaje.dialogo.pk: []}                    
                )
            
            l_temporal = dic_analisis_dialogos[mensaje.dialogo.pk]
            l_temporal.append(mensaje)
            dic_analisis_dialogos[mensaje.dialogo.pk] = l_temporal
        bot.processing_bot(dic_analisis_dialogos)
        print('va al temporizador')
        bot.temporizador()
        return mensajes_dialogo


    def obtener_dialogos_activos(self):
        self.obtener_mensajes()
        dialogos_en_gestion = []
        dialogos_nuevos = []
        # print (">>> obtener_dialogos_activos >>> DIALOGOS VIVOS: ", Dialogo.objects.filter(Q(estado='en_gestion') | Q(estado='nuevo')).order_by('fecha_modificacion'))
        for dialogo in Dialogo.objects.filter(Q(estado='en_gestion') | Q(estado='nuevo')).order_by('fecha_modificacion'):
            # if dialogo.obtener_total_mensajes_nuevos() > 0:
            campana = dialogo.campana
            print('la campañaaaaaaaaaa', campana)
            info_dialogo = {
                'medio': 'whatsapp',
                'id': dialogo.pk,
                'estado': dialogo.estado,
                'numero': dialogo.numero,
                'imagen': dialogo.obtener_imagen_url(),
                'nombre_corto': dialogo.obtener_nombre_contacto()[1],
                'nombre_largo': dialogo.obtener_nombre_contacto()[0],
                'mensajes_nuevos': dialogo.obtener_total_mensajes_nuevos(),
                'fecha_modificacion': dialogo.fecha_modificacion.astimezone(LOCAL_TZ).strftime("%b. %-d, %Y, %-I:%M %p"),
                'id_agente': '{}'.format(dialogo.user.get_agente_profile().id) if dialogo.user != None else '',
                'id_campana': campana,
                'nombre_campana': dialogo.campana,
            }
            if dialogo.estado == 'en_gestion':
                dialogos_en_gestion.append(info_dialogo)

            if dialogo.estado == 'nuevo':
                dialogos_nuevos.append(info_dialogo)
            

        return dialogos_en_gestion, dialogos_nuevos

    def enviar_mensaje(self, dialogo=None, number=None, body=None):
        respuesta = self._enviar_mensaje_api(chat_id=dialogo.chat_id, number=number, body=body)
        print('>>>>CONTROLADOR >>> enviar_mensaje >> RESPUESTA CONTROLADOR: ', respuesta)
        return respuesta

    def enviar_archivo(self, dialogo=None, number=None, body=None, filename=None, caption=''):
        respuesta = self._enviar_archivo_api(chat_id=dialogo.chat_id, number=number, body=body, filename=filename, caption=caption)
        print('>>>>CONTROLADOR >>> ENVIAR ARCHIVO >> RESPUESTA CONTROLADOR: ', respuesta)
        return respuesta

    def enviar_audio(self, dialogo=None, number=None, body=None, filename=None, caption=''):
        respuesta = self._enviar_ptt_api(chat_id=dialogo.chat_id, number=number, body=body, filename=filename, caption=caption)
        print('>>>>CONTROLADOR >>> ENVIAR AUDIO >> RESPUESTA CONTROLADOR: ', respuesta)
        return respuesta

    def inicializar(self, ultimo_mensaje=None):
        if ultimo_mensaje:
            mensajes = self._obtener_mensajes_api(last_message_number=ultimo_mensaje, limit=0)
        else:
            mensajes = self._obtener_mensajes_api(limit=0)

        mensajes_dialogo = []
        for item in mensajes:
            chat_id = item['chatId']
            if Dialogo.objects.filter(chat_id=chat_id).exists():
                dialogo = Dialogo.objects.get(chat_id=chat_id)
                if dialogo.estado == 'gestionado':
                    dialogo.estado = 'nuevo'
                dialogo.fecha_modificacion = dt.datetime.now(pytz.utc)
                dialogo.fecha_entrada_cola = dt.datetime.now(pytz.utc)
                dialogo.save()
            else:
                dialogo = Dialogo(
                    chat_id = chat_id,
                    numero = chat_id.split('-')[0].split('@')[0],
                    es_grupo = True if len(chat_id.split('-')) > 1 else False,
                )
                dialogo.save()

            mensaje_id = item['id']

            if not Mensaje.objects.filter(id=mensaje_id).exists():
                fecha_temp = dt.datetime.fromtimestamp(item['time'])
                fecha_recibido = LOCAL_TZ.localize(fecha_temp)
                mensaje = Mensaje(
                    id = mensaje_id,
                    dialogo = dialogo,
                    body = item['body'],
                    tipo = item['type'],
                    remitente_nombre = item['senderName'],
                    from_me = True if item['fromMe'] == 1 else False,
                    autor = item['author'],
                    fecha_recibido = fecha_recibido.astimezone(pytz.utc),
                    mensaje_numero = item['messageNumber'],

                )
                mensaje.save()

                if item['type'] in ['image', 'ptt', 'document', 'audio', 'video' ]:
                    url = item['body']
                    format_file = url.split('.')[-1]
                    random_string = get_random_string(length=16)
                    name_file = '{}-{}.{}'.format(mensaje.dialogo.numero, random_string, format_file)
                    try:
                        r = requests.get(url, stream=True)
                        if r.status_code == 200:
                            temp_file = tempfile.NamedTemporaryFile()
                            for block in r.iter_content(1024 * 8):
                                if not block:
                                    break
                                temp_file.write(block)
                            temp_file.seek(0)
                            mensaje.archivo.save(basename(name_file), files.File(temp_file))
                    except:
                        print ("Generar log, error al consultar la URL")

                mensajes_dialogo.append(mensaje)

        return mensajes_dialogo

   
