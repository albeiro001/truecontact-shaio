from django.conf import settings
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic  import (
    View, ListView, CreateView, UpdateView, DeleteView, FormView, TemplateView, DetailView
)
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from .models import Dialogo, Mensaje, Respuesta, FilesTemp, HorarioWhatapp
from truecontact.chatapi.forms import HorarioWhatsappForm
from .serializers import MensajeSerializer
from django.urls import reverse_lazy

from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from ominicontacto_app.models import Campana

import ast
import datetime as dt
import json
import pytz
import base64
from truecontact.chatapi.controlador import ChatApi
from truecontact.custom_client.bot.bot import Bot

from django.db.models import Q

from functools import reduce


from truecontact.contactos.models import *
LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)

class DialogoMensajesList(View):
    '''
    docstring: 
    data-calldata="{"medio":"whatsapp","id_contacto":3,"telefono":"3016600848","call_type":"4","chat_id":"573016600848@c.us","mensaje_inicial":1126}" ejemplo
    '''
    def get(self, request, *args, **kwargs):
        print(">>>> DialogoMensajesList >>>  GET : ", self.request.GET)
        user = self.request.user
        estado = self.request.GET.get('estado')
        nuevo_chat = self.request.GET.get('nuevo_chat')
        print(nuevo_chat)
        chat_id = kwargs['pk']
        if (Dialogo.objects.filter(pk=chat_id).exists()):
            dialogo = Dialogo.objects.get(pk=chat_id)
        else:
            dialogo = Dialogo(
                chat_id = chat_id,
                numero = chat_id.split('-')[0].split('@')[0],
                es_grupo = True if len(chat_id.split('-')) > 1 else False,
            )
            dialogo.save()
        #Si es un dialogo nuevo que ejecuta el agente nuevo_chat=true
        if nuevo_chat == 'SI':
            dialogo.estado = 'nuevo'
            dialogo.save()
            nombre_campana = request.GET.get('nombre_campana')
        else:
            print(">>>> DialogoMensajesList >> dialogo no es nuevo: ", dialogo )
            nombre_campana = dialogo.campana
        
        if not self.request.user.groups.filter(name = 'supervisores').exists():#Si el usuario que lo quiere jalar es diferente al que lo tiene paila
            if (dialogo.estado == 'en_gestion' and dialogo.user != user )  : 
                return JsonResponse({'puede_gestionar': False, 'dialogo': '', 'mensajes': []})

    
        mensaje_inicial = request.GET.get('mensaje_inicial')
        mensaje_final = request.GET.get('mensaje_final')

        queryset = Mensaje.objects.filter(dialogo=dialogo).order_by('fecha_recibido')
        if mensaje_inicial:
            queryset = queryset.filter(mensaje_numero__gt=mensaje_inicial).order_by('fecha_recibido')

        if mensaje_final:
            queryset = queryset.filter(mensaje_numero__lte=mensaje_final).order_by('fecha_recibido')
        
        
        
        if not (mensaje_inicial and mensaje_final):
        
            if dialogo.estado == 'nuevo':
                dialogo.fecha_modificacion = dt.datetime.now(pytz.utc)
                dialogo.user = user
                dialogo.estado = 'en_gestion'
                dialogo.campana = nombre_campana
        
            if dialogo.estado == 'gestionado': #SI ES UN DILOGO DISPARADO POR EL AGENTE lo paso a en gestion
                nombre_campana = request.GET.get('nombre_campana')
                dialogo.fecha_modificacion = dt.datetime.now(pytz.utc)
                dialogo.user = user
                dialogo.estado = 'en_gestion'
                dialogo.campana = nombre_campana
                dialogo.bot = False
                dialogo.modulo = 1000
                
            dialogo.save()
            ## ACTUALIZO EL METADATA###
            bot = Bot()
            dialogo.metadata = bot.ini_metadata(dialogo)
            dialogo.save()
            ## END ACTUALIZO EL METADATA###

            queryset = queryset.filter(
                        mensaje_numero__gt=dialogo.ultimo_mensaje_gestion
                    ).order_by('fecha_recibido')

        if estado == 'leido':
            queryset = queryset.filter(leido=True).order_by('fecha_recibido')
            print(f'Mensajes leidos {queryset.count()}')

        elif estado == 'no_leido':
            queryset = queryset.filter(leido=False).order_by('fecha_recibido')
            print(f'Mensajes no leidos {queryset.count()}')
            
        # Mediante el usuario que está haciendo la petición debo rescatar la campaña del agente y colocarle sólo el pk  
        # if Campana.objects.filter(nombre=str(dialogo.campana)).exists():
        #     campana = Campana.objects.get(nombre=dialogo.campana)
        #     id_campana = int(campana.id)
        # else :
        #     id_campana='?'
        agente = user.agenteprofile
        id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])

        campana_dialogo = Campana.objects.get(pk=id_campana)
        dialogo.campana = campana_dialogo.nombre
        dialogo.save()

        lista_mensajes = []
        dialogo_info = {
            'medio': 'whatsapp',
            'id': dialogo.pk,
            'id_contacto': dialogo.contacto.pk if dialogo.contacto else '',
            'industrias_contacto': dialogo.contacto.get_industrias() if dialogo.contacto else '',
            'estado': dialogo.estado,
            'numero': dialogo.numero,
            'pk_gestion': dialogo.gestion.pk if dialogo.gestion else '',
            'imagen': dialogo.obtener_imagen_url(),
            'nombre_corto': dialogo.obtener_nombre_contacto()[1],
            'nombre_largo': dialogo.obtener_nombre_contacto()[0],
            'mensaje_inicial': dialogo.ultimo_mensaje_gestion,
            'id_campana' : id_campana,
        }
        print(">>>> DialogoMensajesList >>> DIALOGO INFO:...", dialogo_info)
        for mensaje in queryset:
            mensaje.leido = True
            mensaje.save()

            if mensaje.quote_msg_id:
                if Mensaje.objects.filter(id=mensaje.quote_msg_id).exists():
                    mensaje_quote = Mensaje.objects.get(id=mensaje.quote_msg_id)
                else:
                    mensaje_quote = None

                id = mensaje.quote_msg_id
                if (mensaje_quote):
                    tipo = mensaje_quote.tipo
                    body = mensaje_quote.body
                    archivo = mensaje_quote.archivo.url if mensaje_quote.archivo else ''
                    nombre_archivo = mensaje_quote.nombre_archivo
                    autor = mensaje_quote.remitente_nombre
                    caption = mensaje_quote.caption
                else:
                    body = ''
                    archivo = ''
                    nombre_archivo = ''
                    autor = ''
                    caption = ''
                    tipo = ''

                mensaje_asociado = {
                    'id': id,
                    'tipo': tipo,
                    'archivo': archivo,
                    'nombre_archivo': nombre_archivo,
                    'body': body,
                    'autor': autor,
                    'caption': caption
                }
            else:
                mensaje_asociado = None

            lista_mensajes.append(
                {
                    'id': mensaje.id,
                    'tipo': mensaje.tipo,
                    'from_me': mensaje.from_me,
                    'archivo': mensaje.archivo.url if mensaje.archivo else '',
                    'nombre_archivo': mensaje.nombre_archivo,
                    'body': mensaje.body,
                    'autor': mensaje.dialogo.obtener_nombre_contacto()[1],
                    'fecha_recibido': (mensaje
                                        .fecha_recibido
                                        .astimezone(LOCAL_TZ)
                                        .strftime('%Y-%m-%d %H:%M')),
                    'agente': mensaje.agente.username if mensaje.agente else '',
                    'caption': mensaje.caption,
                    'tiene_mensaje_asociado': True if mensaje_asociado else False,
                    'mensaje_asociado': mensaje_asociado,
                }
            )
        return JsonResponse({ 'puede_gestionar': True, 'dialogo': dialogo_info, 'mensajes': lista_mensajes})

class ConsultaRespuestaRapida(View):

    def get(self, request, *args, **kwargs):


        respuesta_rapida = request.GET.get('atajo')
        respuestas_rapidas = Respuesta.objects.filter(atajo__icontains=respuesta_rapida).filter(activo=True)
        # print(f"Respuesta es ", respuestas_rapidas)
        lista_respuesta = []
        metadata_str = request.GET.get('metadata')
        metadata_str = metadata_str.replace("\'","\"")
        metadata = json.loads(metadata_str)
        
        id_contacto = metadata['id_contacto']
        if not id_contacto == '':
            contacto = Contacto.objects.get(pk=id_contacto)
            Contacto_Nombre = contacto.get_full_name()
            Contacto_Telefono =metadata['telefono']
            if len(contacto.obtener_direcciones()) != 0:
                print("entro")
                Contacto_Direccion = contacto.obtener_direcciones()[0]['d1']
            else:
                Contacto_Direccion =''
                print("else")
            if len(contacto.obtener_emails()) != 0 :
                Contacto_Email = contacto.obtener_emails()[0]['email']
            else:
                Contacto_Email =''
        else:
            Contacto_Nombre = ''
            Contacto_Telefono = metadata['telefono']
            Contacto_Direccion = ''
            Contacto_Email = ''
       
        user = self.request.user
        agente_nombre_completo = str(user.get_full_name())
        agente_nombre_corto = str(user.get_short_name())
        agente_email = str(user.email)


        
        for resp in respuestas_rapidas:
            #################  Etiquetas de contacto #####################
            respuesta = (resp.texto).replace('{{Contacto.Nombre}}',Contacto_Nombre)
            respuesta = respuesta.replace('{{Contacto.Telefono}}',Contacto_Telefono)
            respuesta = respuesta.replace('{{Contacto.Direccion}}',Contacto_Direccion)
            respuesta = respuesta.replace('{{Contacto.Email}}',Contacto_Email)
            ############# Etiquetas de agente################3
            respuesta = respuesta.replace('{{Agente.Nombre.Completo}}',agente_nombre_completo)
            respuesta = respuesta.replace('{{Agente.Nombre.Corto}}',agente_nombre_corto)
            respuesta = respuesta.replace('{{Agente.Correo}}',agente_email)

            #print("respuesta",respuesta)

            lista_respuesta.append({'id':resp.pk,'atajo':resp.atajo, 'texto': respuesta})
        respuesta = json.dumps({'respuesta': lista_respuesta})

        mimetype = 'aplication/json'
        return HttpResponse(respuesta, mimetype)

#---------      RESPUESTAS RAPIDAS VIEWS            -------------------------------.
class RespuestaCreate( CreateView ):
    model = Respuesta
    fields = '__all__'
    template_name = 'respuesta/create.html'
    success_url = reverse_lazy('tc-respuesta-list')

class RespuestaUpdate( UpdateView ):
    model = Respuesta
    fields = '__all__'
    template_name = 'respuesta/update.html'
    success_url = reverse_lazy('tc-respuesta-list')

class RespuestaList( ListView ):

    model = Respuesta
    paginate_by = 20
    template_name = 'respuesta/list.html'
    context_object_name = 'respuestas'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

def delete(request, pk):
    instancia = Respuesta.objects.get(id=pk)
    instancia.delete()
    return redirect('/tc/respuesta/list')

#---------      END RESPUESTAS RAPIDAS VIEWS            -------------------------------.

class CargarFiles(View):

    def post(self, request, *args, **kwargs):
        user = self.request.user
        files = request.FILES.getlist('file')
        lista_imagenes = []
        lista_tipos = ['png','jpg','jpeg','gif']
        
        for f in files:
            archivo = FilesTemp(
                file = f
            )
            archivo.save()
            url = archivo.file
            url_str = str(url)
            formato = url_str.split('.')[-1]
            tipo_64 = url.read()
            data = base64.b64encode(tipo_64).decode('utf-8')

            if formato in lista_tipos:
                nueva_data = f'data:image/{formato};base64,{data}'

                lista_imagenes.append({
                    'name' : url_str,
                    'tipo' : 'image',
                    'archivo' : nueva_data,
                })

            elif(formato == 'pdf'):
                nueva_data = f'data:application/{formato};base64,{data}'

                lista_imagenes.append({
                    'name' : url_str,
                    'tipo' : 'document',
                    'archivo' : nueva_data,
                })
            elif(formato == 'xlsx'):
                nueva_data = f'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,{data}'

                lista_imagenes.append({
                    'name' : url_str,
                    'tipo' : 'document',
                    'archivo' : nueva_data,
                })
            elif(formato == 'svg'):
                nueva_data = f'data:image/{formato}+xml;base64,{data}'

                lista_imagenes.append({
                    'name' : url_str,
                    'tipo' : 'image',
                    'archivo' : nueva_data,
                })
            elif(formato == 'csv'):
                nueva_data = f'data:text/comma-separated-values;base64,{data}'

                lista_imagenes.append({
                    'name' : url_str,
                    'tipo' : 'document',
                    'archivo' : nueva_data,
                })
            elif(formato == 'docx'):
                nueva_data = f'data:application/vnd.openxmlformats-officedocument.wordprocessingml.documenr;base64,{data}'

                lista_imagenes.append({
                    'name' : url_str,
                    'tipo' : 'document',
                    'archivo' : nueva_data,
                })

        respuesta = json.dumps({'server': lista_imagenes, })
        mimetype = 'aplication/json'
        return HttpResponse(respuesta, mimetype)



@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.add_contacto', raise_exception=True), name='dispatch')
class MensajesMasivosView(View):

    template_name = 'respuesta/mensajes_masivos.html'
    
    def get(self, request):
        context = {}

        return render(request, self.template_name, context )

    def post(self, request, *args, **kwargs):
        
        print("REQUEST: ", request)
        print(f"REQUEST POST {request.POST}")

        contactos_pk = request.POST.getlist('contacto')
        chat_api = ChatApi()
        
        for x , contacto_pk in enumerate(contactos_pk):
            print(x , contacto_pk)
            contacto = Contacto.objects.get(pk=contacto_pk)
            
            Contacto_Nombre = contacto.get_full_name()
            if len(contacto.obtener_direcciones()) != 0:
                print("entro")
                Contacto_Direccion = contacto.obtener_direcciones()[0]['d1']
            else:
                Contacto_Direccion =''
                print("else")
            if len(contacto.obtener_emails()) != 0 :
                Contacto_Email = contacto.obtener_emails()[0]['email']
            else:
                Contacto_Email =''

            user = self.request.user
            agente_nombre_completo = str(user.get_full_name())
            agente_nombre_corto = str(user.get_short_name())

            mensaje_body = request.POST.get('id_texto')
            mensaje_body = (mensaje_body).replace('{{Contacto.Nombre}}',Contacto_Nombre)
            mensaje_body = mensaje_body.replace('{{Contacto.Direccion}}',Contacto_Direccion)
            mensaje_body = mensaje_body.replace('{{Contacto.Email}}',Contacto_Email)
            ############# Etiquetas de agente################3
            mensaje_body = mensaje_body.replace('{{Agente.Nombre.Completo}}',agente_nombre_completo)
            mensaje_body = mensaje_body.replace('{{Agente.Nombre.Corto}}',agente_nombre_corto)

            dialogo = Dialogo.objects.filter(contacto__pk= contacto_pk)[0]

            respuesta = chat_api.enviar_mensaje(dialogo=dialogo, body=mensaje_body)

            mensaje_model = Mensaje(
                id = respuesta['id'],
                dialogo = dialogo,
                body = mensaje_body,
                tipo = 'chat',
                from_me = True,
                fecha_recibido = dt.datetime.now(LOCAL_TZ),
                leido = False,
                agente = user,
            )
            print('Guardando mensaje')
            mensaje_model.save()
            print(mensaje_model.id)



        return redirect('tc-mensajes-masivos')

class ContactoBuscar(View):

    def get(self, request, *args, **kwargs):
        context = {}
        
        buscarcontacto=self.request.GET.get('datos')
        print(f"busqueda {buscarcontacto}")

        
        if(buscarcontacto !=''):
            print("BUSCANDO......", buscarcontacto)
            object_list = ContactoDatosContacto.objects.filter(tipo='telefono', contacto__nombres__icontains= buscarcontacto) \
                | ContactoDatosContacto.objects.filter(tipo='telefono', contacto__apellidos__icontains = buscarcontacto) \
                | ContactoDatosContacto.objects.filter(tipo='telefono', contacto__documento__icontains = buscarcontacto)
            print(object_list)
        
        lista_busqueda=[]
        for datoscontacto in object_list:
            print('XXXXXXX ', datoscontacto.contacto.nombres, datoscontacto.contacto.apellidos, datoscontacto.value_1)
            lista_busqueda.append({'id': datoscontacto.contacto.id, 'nombres': datoscontacto.contacto.nombres, 'apellidos': datoscontacto.contacto.apellidos,'numero': datoscontacto.value_1 })
        respuesta = json.dumps({'datoscontactos': lista_busqueda })#'contactos'tiene q ser la misma q recibe en el javascrip en---> var contactos = data["contactos"];
        print("ojoooo",respuesta)
        mimetype = 'application/json'
        return HttpResponse(respuesta, mimetype)

class CampanasWhatsappManual(View):
    def get(self, request, *args, **kwargs):
        bot= Bot()
        user = self.request.user
        nombre_campana=self.request.GET.get('nombre_campana')
        campana = Campana.objects.get(nombre=nombre_campana)
        id_chatapi=self.request.GET.get('id_chatapi')
        dialogo = Dialogo.objects.get(pk=id_chatapi)
        dialogo.campana = campana.nombre
        dialogo.estado ='en_gestion'
        dialogo.bot = False
        dialogo.metadata = bot.ini_metadata(dialogo)
        dialogo.esperar_respuesta = False
        dialogo.user = user
        dialogo.modulo = 1000
        dialogo.save()
        call_data_dialogo = dialogo.metadata
        print("metadata >>>>>>>>>>>><<<<<<<<<<<<<<<<",)
        call_data_dialogo['bandera_no_contesta'] = True
        call_data_dialogo['bandera_sin_respuesta'] = False
        dialogo.metadata = call_data_dialogo 
        dialogo.save()
        gestion =dialogo.gestion
        call_data_gestion = ast.literal_eval(gestion.metadata)
        call_data_gestion['id_campana'] = campana.pk
        gestion.metadata = call_data_gestion
        gestion.save()
        return HttpResponse('ok')
class HorarioWhatappList( ListView ):

    model = HorarioWhatapp
    paginate_by = 50
    template_name = 'horario_whatsapp/list.html'


    def _obtener_campanas(self):
        agente = self.request.user.get_agente_profile()
        campanas_queues = agente.get_campanas_activas_miembro()
        ids_campanas = []
        for id_nombre in campanas_queues.values_list('id_campana', flat=True):
            split_id_nombre = id_nombre.split('_')
            id_campana = split_id_nombre[0]
            campana = Campana.objects.get(pk=id_campana)
            type_campana = campana.type
            nombre_campana = '_'.join(split_id_nombre[1:])
            ids_campanas.append((id_campana, nombre_campana, type_campana))
        return ids_campanas

    def get_queryset(self):
        grupo_supervisor = Group.objects.get(name='Supervisor')
        # grupo_ventas = Group.objects.get(name='VENTAS')
        # grupo_atencion_cliente = Group.objects.get(name='ATENCION_CLIENTE')
        # grupo_soporte = Group.objects.get(name='SOPORTE')

        user = self.request.user
        queryset = HorarioWhatapp.objects.filter(status=True)
        # queryset = queryset.filter(activo=True)
        print(user.groups.all())
        if (grupo_supervisor in user.groups.all()):
            print('entro a todas')
            queryset = queryset

        # if (grupo_ventas in user.groups.all()):
        #     print('entro a VENTAS')
        #     queryset = queryset.filter(campana__nombre='VENTAS')
        
        # if (grupo_atencion_cliente in user.groups.all()):
        #     print('entro a ATENCION_CLIENTE')
        #     queryset = queryset.filter(campana__nombre='ATENCION_CLIENTE')
        
        # if (grupo_soporte in user.groups.all()):
        #     print('entro a SOPORTE')
        #     queryset = queryset.filter(campana__nombre='SOPORTE')

        else:
            agente = user.agenteprofile
            id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])
            campana = Campana.objects.get(pk=id_campana)
            queryset = queryset.filter(campana=campana)
        
        print(queryset)
        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            queryset = queryset.filter(
                    Q(nombre__icontains=buscar) 
            )

        return queryset.order_by('-pk')
    
    def get_context_data(self, **kwargs):

        context = super(HorarioWhatappList, self).get_context_data(**kwargs)
  
        context['contacto_campanas'] = self._obtener_campanas()
        
        buscar =  self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar

        is_filter = False

        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages
        print("ssssssssssssssss",context)
        return context

class HorarioWhatappCreate(View):
    form_class = HorarioWhatsappForm
    template_name = 'horario_whatsapp/add.html'
    success_url = reverse_lazy('horario-whatsapp-list')

    def get(self, request, *args, **kwargs):
        context = {}
        # grupo_supervisor = Group.objects.get(name='Supervisor')
        # grupo_ventas = Group.objects.get(name='VENTAS')
        # grupo_atencion_cliente = Group.objects.get(name='ATENCION_CLIENTE')
        # grupo_soporte = Group.objects.get(name='SOPORTE')
        dias = [ 
            'lunes',
            'martes',
            'miercoles',
            'jueves',
            'viernes',
            'sabado',
            'domingo',
        ]


        user = self.request.user
        campanas = user.agenteprofile.get_campanas_activas_miembro().values() 
        ids_campanas = []
        for i in range(len(campanas)):
            ids_campanas.append(int(campanas[i]['id_campana'].split('_')[0]))

        print(ids_campanas)

        campanas = Campana.objects.filter(reduce(lambda x, y: x | y, [Q(pk = item) for item in ids_campanas]))
        
        print('campana' , campanas)

        form_horario = self.form_class(campana_queryset = campanas)

        context['form'] = form_horario
        context['dias'] = dias
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        user = self.request.user
        context = {}        
        print(self.request.POST)
        form = self.form_class(request.POST)

        if form.is_valid():
            form.save()
        else:
            print("Error de formulario", form.errors)
            context['form'] = form
            return render(request, self.template_name, context)

        return HttpResponseRedirect(self.success_url)

class HorarioWhatappUpdate(View):
    form_class = HorarioWhatsappForm
    template_name = 'horario_whatsapp/update.html'
    success_url = reverse_lazy('horario-whatsapp-list')

    def get(self, request, *args, **kwargs):
        context = {}
        horario =HorarioWhatapp.objects.get(pk=kwargs['pk'])
        dias_select = horario.dia

        siete_dias = ['lunes','martes','miercoles','jueves','viernes','sabado','domingo']
        
        # grupo_supervisor = Group.objects.get(name='Supervisor')
        # grupo_ventas = Group.objects.get(name='VENTAS')
        # grupo_atencion_cliente = Group.objects.get(name='ATENCION_CLIENTE')
        # grupo_soporte = Group.objects.get(name='SOPORTE')

        # usuario_agente = self.request.user
        # agente_profile = usuario_agente.get_agente_profile()
        
        # if(grupo_ventas in usuario_agente.groups.all()):
        #     campanas = Campana.objects.filter(nombre=grupo_ventas.name)

        # elif (grupo_atencion_cliente in usuario_agente.groups.all()):
        #     campanas = Campana.objects.filter(nombre=grupo_atencion_cliente.name) 

        # elif (grupo_soporte in usuario_agente.groups.all()):
        #     campanas = Campana.objects.filter(nombre=grupo_soporte.name)                       
        
        # else:
        #     campanas = Campana.objects.filter(nombre=grupo_supervisor.name)
        

        user = self.request.user
        campanas = user.agenteprofile.get_campanas_activas_miembro().values() 
        ids_campanas = []
        for i in range(len(campanas)):
            ids_campanas.append(int(campanas[i]['id_campana'].split('_')[0]))

        print(ids_campanas)

        campanas = Campana.objects.filter(reduce(lambda x, y: x | y, [Q(pk = item) for item in ids_campanas]))
        
        otros_dias= list(set(siete_dias)-set(dias_select))

        

        form_horario = self.form_class(
            instance = horario,
            campana_queryset = campanas,
        )

        context['form'] = form_horario
        context['dias'] = otros_dias
        context['dias_selected'] = dias_select
        context ['hora_inicio'] = horario.hora_inicio
        context ['hora_final'] = horario.hora_final
       
        return render(request, self.template_name, context)


    def post(self, request, *args, **kwargs):
        context = {}
        horario = get_object_or_404(HorarioWhatapp, pk=kwargs['pk'])

        form = self.form_class(request.POST)
        print(self.request.POST)
        if form.is_valid():
            horario_update = HorarioWhatapp.objects.get(pk=kwargs['pk'])
            # horario_update.resultado = ResultadoGestion.objects.get(pk=self.request.POST.get('resultado')),
            horario_update.campana = Campana.objects.get(pk=self.request.POST.get('campana'))
            horario_update.nombre = self.request.POST.get('nombre')
            horario_update.mensaje_espera = self.request.POST.get('mensaje_espera')
            horario_update.dia = self.request.POST.getlist('dia')
            horario_update.hora_final = self.request.POST.get('hora_final')
            horario_update.hora_inicio = self.request.POST.get('hora_inicio')
            horario_update.save()
        else:
            context['form'] = form
            print(' no valido el formulario')
            return render(request, self.template_name, context)
        return HttpResponseRedirect(self.success_url)
