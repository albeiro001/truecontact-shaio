from django.urls import include, path

from . import views

urlpatterns = [
    path('dialogo/<str:pk>/mensajes/', views.DialogoMensajesList.as_view(), name='tc-dialogo-mensajes'),

    ####-----------------Respuestas Rapidas----------------#####
    path('respuesta/create/',               views.RespuestaCreate.as_view(),         name='tc-respuesta-create'),
    path('respuesta/list/',                 views.RespuestaList.as_view(),           name='tc-respuesta-list'),
    path('respuesta/update/<int:pk>',       views.RespuestaUpdate.as_view(),         name='tc-respuesta-update'),
    path('respuesta/delete/<int:pk>',       views.delete ,                           name='tc-respuesta-delete'),
    path('respuesta/ajax/consulta/',        views.ConsultaRespuestaRapida.as_view(), name='tc-consulta-ajax-respuesta'),

    path('ajax_file_upload_handler/',        views.CargarFiles.as_view(),            name='tc-cargar-ajax-files'),

    path('mensajes_masivos/', views.MensajesMasivosView.as_view(), name='tc-mensajes-masivos'),
    path('contacto/buscar/numero/', views.ContactoBuscar.as_view(), name='tc-buscar-contacto-ajax'),
    path('campanas/whatsapp/manual/', views.CampanasWhatsappManual.as_view(),name='tc-campana-whatsapp-manual'),

    ###---------------Horarios de WhatsApp------------------###,

    path('horario/whatsapp/list/',           views.HorarioWhatappList.as_view(),           name='horario-whatsapp-list'),
    path('horario/whatsapp/create/',         views.HorarioWhatappCreate.as_view(),         name='horario-whatsapp-create'),
    path('horario/whatsapp/<int:pk>/update/',         views.HorarioWhatappUpdate.as_view(),         name='horario-whatsapp-update'),

]
