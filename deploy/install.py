# =============================================================================
#                           Instalamos Dependencias
# =============================================================================

# pip install pip --upgrade
# pip install django-crontab
# pip install tornado


# =============================================================================
#                    Clonamos los repositorios de TrueContac
# =============================================================================

# Hay dos repositorios de truecontact, uno referente a las aplicaciones y otro
# referente a los archivos estaticos.
# truecontact referente a las aplicaciones se debe ubicar en /opt/omnileads/ominicontacto/
# y se debe llamar la carpeta < truecontact >
# truecontact refetente a los estaticos se debe ubicar en /opt/omnileads/static/
# y se debe llamar la carpeta < truecontact >

# =============================================================================
#                  en /ominicontacto/settings/defaults.py
# =============================================================================

# cambiamos INSTALLED_APPS  por INSTALLED_APPS_OMNILEADS
# agregamos INSTALLED_APPS_TRUECONTACT


INSTALLED_APPS_TRUECONTACT = [
    'django_crontab',
    'truecontact',
    'truecontact.ajustes',
    'truecontact.contactos',
    'truecontact.chatapi',
    'truecontact.catalogo',
]

INSTALLED_APPS = INSTALLED_APPS_TRUECONTACT + INSTALLED_APPS_OMNILEADS

# Agregamos los datos de las conexiones contra ChatApi

CHATAPI_URL = ''
CHATAPI_TOKEN = ''
CONTACTO_ASOCIADO_AGENTE = False
CRONTAB_DJANGO_PROJECT_NAME='cron-truecontact'
CRONJOBS = [
#  ---------------- minuto (0-59) (*/x cada x minutos)
#    |  .------------ hora (0-23) (*/x cada x horas)
#    |  |  .--------- dia del mes (1-31)
#    |  |  |  .------ mes (1-12) o jan,feb,mar,apr,may,jun,jul...(meses en ingles)
#    |  |  |  |  .--- dia de la semana (0-6) (domingo=0 o 7) o sun,mon,tue,wed,thu,fri,sat (dias en ingles)
#    |  |  |  |  |
#    *  *  *  *  *  comando a ejecutar
    ('00 23 * * * ','truecontact.chatapi.funciones.actualizar_dialogos'),
]

# =============================================================================
#                  en /ominicontacto/urls.py
# =============================================================================

# incluir:
#
url(r'^tc/', include('truecontact.urls')),
#
# y cambiar la linea de accounts/login por:
#
url(r'^accounts/logout/$', auth_views.LogoutView.as_view(next_page='/tc/accounts/login/'),
        name="logout"),

# =============================================================================
#                  en /ominicontacto_apps/urls.py
# =============================================================================
# comentar la linea
# # url(r'^accounts/login/$', views.login_view, name='login'),
# # url(r'^consola/$',
# #     login_required(views.ConsolaAgenteView.as_view()),
# #     name='consola_de_agente'),
