from django.core.paginator import Paginator

def getPaginatorItems(queryset, page, items_per_page=25, buscar=None):
    paginator = Paginator(queryset, items_per_page)
    items = paginator.get_page(page)
    if page is None:
        page = 1
    paginator_list = []
    if not paginator.num_pages <= 5:
        first_page = int(page) - 3
        last_page = int(page) + 3
        if (first_page <= 0 ):
            first_page = 1
        if (last_page > paginator.num_pages ):
            last_page = paginator.num_pages
        list = range(first_page, last_page+1)
        if not 1 in list:
            paginator_list.append({'class': '', 'number': '1', 'url': 'page=1'})
        if not 2 in list:
            paginator_list.append({'class': '', 'number': '2', 'url': 'page=2'})
        if not 3 in list:
            paginator_list.append({'class': '', 'number': '3', 'url': 'page=3'})
        for i in list:
            if (int(page) == int(i)):
                paginator_list.append({'class': 'active', 'number': i,
                    'url': 'page={0}'.format(i),
                })
            else:
                paginator_list.append({'class': '', 'number': i,
                    'url': 'page={0}'.format(i),
                })
        if not (paginator.num_pages-2 in list):
            paginator_list.append({'class': 'disabled', 'number': '...'})
        if not (paginator.num_pages-1 in list):
            paginator_list.append({'class': '', 'number': paginator.num_pages-1,
                'url': 'page={0}'.format(paginator.num_pages-1)
            })
        if not (paginator.num_pages in list):
            paginator_list.append({'class': '', 'number': paginator.num_pages,
                'url': 'page={0}'.format(paginator.num_pages)
            })
    else:
        list = range(1, paginator.num_pages+1)
        for i in list:
            if (int(page) == int(i)):
                paginator_list.append({'class': 'active', 'number': i,
                    'url': 'page={0}'.format(i)
                })
            else:
                paginator_list.append({'class': '', 'number': i,
                    'url': 'page={0}'.format(i)
                })
    return items, {
            'total_items': paginator.count,
            'paginator_list': paginator_list,
            'first_page': 1,
            'last_page': paginator.num_pages,
        }
