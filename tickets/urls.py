from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^ticket/$', views.Tickets.as_view(), name='tc-tickets'),
    url(r'^ticket/create/$', views.TicketCreate.as_view(), name='tc-ticket-create'),
    url(r'^ticket/<int:pk>/detail/$', views.TicketDetail.as_view(), name='tc-ticket-detail'),
    url(r'^ticket/<int:pk>/update/$', views.TicketUpdate.as_view(), name='tc-ticket-update'),
    url(r'^ticket/<int:pk>/delete/$', views.TicketDelete.as_view(), name='tc-ticket-delete'),
    url(r'^ticket/gestion/$', views.TicketGestion.as_view(), name='tc-gestion'),
    url(r'^ticket/observacion/$', views.TicketObservacion.as_view(), name='tc-observacion'),
    url(r'^ticket/ver/$', views.TicketVer.as_view(), name='tc-ticket-ver'),
]
