from django.contrib import admin
from .models import *
# Register your models here.


# @admin.register(UserProfile)
# class UserProfile(admin.ModelAdmin):
#     list_display = ('user', 'telefono')

@admin.register(EstadoTicket)
class EstadoTicket(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'color', 'descripcion')

@admin.register(TipoUrgencia)
class TipoUrgencia(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'tiempo_solucion', 'descripcion')

@admin.register(Ticket)
class Ticket(admin.ModelAdmin):
    list_display = ( 'pk', 'nombre', 'responsable', 'estado', 'f_registro','evaluacion_cliente')
@admin.register(AuditoriaTicketEstado)
class AuditoriaTicketEstado(admin.ModelAdmin):
    list_display = ('pk','ticket', 'estado', 'usuario', 'usuario_responsable', 'f_registro')
    list_filter = ('ticket', 'estado')
@admin.register(Observacion)
class Observacion(admin.ModelAdmin):
    list_display = ( 'pk', 'ticket', 'registro', 'f_registro')
