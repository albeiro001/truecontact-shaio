from datetime import datetime
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic import DetailView, ListView, View
from django.views.generic.edit import CreateView, UpdateView, FormView, DeleteView

import json

from .models import *
from .functions import getPaginatorItems

# Create your views here.

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('tickets.view_ticket', raise_exception=True), name='dispatch')
class Tickets(ListView):
    model = Ticket
    context_object_name = 'tickets'
    template_name = 'tickets/ticket_list.html'
    paginate_by = 10

    def get_queryset(self):
        user = self.request.user
        if (user.groups.filter(name__in=['helpdesk_administrador']).exists() or
            user.groups.filter(name__in=['helpdesk_supervisor']).exists()
        ):
            queryset = Ticket.objects.all()

        elif (user.groups.filter(name__in=['helpdesk_tecnico']).exists() ):
            queryset = Ticket.objects.filter(
                responsable=user
            ).exclude(
                estado=EstadoTicket.objects.get(id=50)
            ).exclude(
                estado=EstadoTicket.objects.get(id=70)
            )
        else:
            queryset = Ticket.objects.filter(solicitante=user)

        buscar = self.request.GET.get('buscar')

        if not (buscar == '' or buscar == None):
            queryset = queryset.filter(nombre__icontains = buscar) #\

        return queryset

    def get_context_data(self, **kwargs):
        context = super(Tickets, self).get_context_data(**kwargs)

        display = self.request.GET.get('display')
        if (display == None or display == ''):
            display = 'kanban'
        context['display'] = display

        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar

        paginator = context['paginator']
        num_pages = paginator.num_pages

        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('tickets.add_ticket', raise_exception=True), name='dispatch')
class TicketCreate(CreateView):
    model = Ticket
    fields = '__all__'
    template_name = 'tickets/ticket_add.html'

    def form_valid(self, form):
        user = self.request.user
        if form.instance.estado == None:
            form.instance.estado = EstadoTicket.objects.get(id=10)
        form.instance.solicitante = user
        return super().form_valid(form)

    def get_success_url(self):
        ticket= Ticket.objects.get(pk=self.object.id)
        auditoria = AuditoriaTicketEstado(
            ticket = ticket,
            tipo = '2', # 1 = Cambio de Estado
            estado = EstadoTicket.objects.get(id=10),
            usuario=self.request.user,
            usuario_responsable=ticket.responsable,
            observaciones=ticket.descripcion,
        )
        auditoria.save()
        return reverse('tc-tickets')

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('tickets.view_ticket', raise_exception=True), name='dispatch')
class TicketDetail(DetailView):
    model = Ticket
    template_name = 'tickets/ticket_detail.html'

    def get_context_data(self, **kwargs):
        context = super(TicketDetail, self).get_context_data(**kwargs)
        ticket = self.get_object()
        context['gestiones_ticket'] = AuditoriaTicketEstado.objects.filter(ticket=ticket).order_by('f_registro')
        return context

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('tickets.change_ticket', raise_exception=True), name='dispatch')
class TicketUpdate(UpdateView): #Funciona  para asignar un Ticket a un  responsable
    model = Ticket
    fields = '__all__'
    template_name = 'tickets/update.html'
    success_url = '/ticket'

    def form_valid(self, form):
        form.instance.estado= EstadoTicket.objects.get(id=20) #el ticket se pasa a "asignado"
        ticket= Ticket.objects.get(pk=self.object.id)
        auditoria = AuditoriaTicketEstado(ticket=ticket, estado=EstadoTicket.objects.get(id=20), usuario=self.request.user,
                    usuario_responsable=self.object.responsable, observaciones=form.instance.notas )
        auditoria.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs): # Paso ambos contextos
        context = super(TicketUpdate, self).get_context_data(**kwargs)
        context['observaciones'] = Observacion.objects.filter(ticket=self.object.id)
        return context

class TicketReasignado(UpdateView):
    model = Ticket
    fields = '__all__'
    template_name = 'tickets/update.html'
    success_url= '/ticket'

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('tickets.delete_ticket', raise_exception=True), name='dispatch')
class TicketDelete(DeleteView):
    model = Ticket
    template_name = 'tickets/delete.html'

class TicketGestion(View): #esta funcion hace todos los cambios de estado de tickets
    def get(self, request, *args, **kwargs):
        estado = request.GET['estado_id']
        ticket = Ticket.objects.get(pk=request.GET['id'])
        if request.is_ajax():
            if estado == 'ASIGNADO': #Asignado PAso a Aceptado
                ticket.estado=EstadoTicket.objects.get(id=30)
                nuevo_estado = "Aceptado"
                ticket.save()
            if estado == 'ACEPTADO': #ACEPTADO PAso a EN EJECUCION Toma tiempo inicio
                ticket.estado=EstadoTicket.objects.get(id=40)
                ticket.f_inicio= datetime.now(timezone.utc)
                nuevo_estado = "En ejecucion"
                ticket.save()
            if estado == 'EN_EVALUACION': #Pasa a en evaluacion si se soluciono
                ticket.estado=EstadoTicket.objects.get(id=50)
                ticket.solucion=request.GET['mensaje']
                nuevo_estado = request.GET['mensaje']
                ticket.save()
            if estado == 'NO_CONCLUIDO': #pasa ene valuacion si no se pudo solucionar
                ticket.estado=EstadoTicket.objects.get(id=80)
                ticket.solucion=request.GET['mensaje']
                nuevo_estado = request.GET['mensaje']
                ticket.save()
            if estado == 'FINALIZADO': #SI SE SOLUCIONO
                ticket.estado=EstadoTicket.objects.get(id=70)
                ticket.retroalimentacion = request.GET['mensaje']
                ticket.calificacion = request.GET['calificacion']
                ticket.f_fin= datetime.now(timezone.utc)
                nuevo_estado = request.GET['mensaje']
                ticket.save()

            auditoria = AuditoriaTicketEstado(ticket=ticket, estado=ticket.estado, usuario=self.request.user,
                        usuario_responsable=ticket.responsable, observaciones=nuevo_estado)
            auditoria.save()

        else:
            print("Resquest not is ajax")
            return render(self.request, 'contactos/contacto.html', {})
        return JsonResponse({'status': 'ok', 'nuevo_estado': nuevo_estado })

class TicketVer(View): #esta funcion hace consultas que vienen de AJAX
    def get(self, request, *args, **kwargs):
        tipo_consulta = request.GET['tipo']
        if request.is_ajax():
            ticket = Ticket.objects.get(pk=request.GET['pk'])
            objeto_dato = {}
            lista_json = []
            objeto_dato["nombre"]= ticket.nombre
            objeto_dato["urgencia"]= ticket.urgencia.nombre
            objeto_dato["descripcion"]= ticket.descripcion
            objeto_dato["estado"]= ticket.estado.nombre
            objeto_dato["notas"]= ticket.notas
            if ticket.responsable:
                objeto_dato["responsable"]= ticket.responsable.get_full_name()
            objeto_dato["solucion"]= ticket.solucion
            lista_json.append(objeto_dato)
            print ("respondiendo JSON:..", lista_json )
            return JsonResponse({'ticket':lista_json})
        else:
            print("Resquest not is ajax")
            return render(self.request, 'contactos/contacto.html', {})
        return JsonResponse({'status': 'ok', 'nuevo_estado': nuevo_estado })

class TicketObservacion(View): #esta funcion guarda las observaciones de todos los tickets
    def get(self, request, *args, **kwargs):
        nota = request.GET['observacion']
        ticket = Ticket.objects.get(pk=request.GET['id'])
        print("llego:.........", nota)
        if request.is_ajax():
            observacion = Observacion(ticket=ticket, registro=nota )
            observacion.save()
        else:
            print("Resquest not is ajax")
            return render(self.request, 'contactos/contacto.html', {})
        return JsonResponse({'status': 'ok', 'observacion': nota })
