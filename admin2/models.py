from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class UserProfile(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.PROTECT, related_name="admin2_user")
    phone = models.CharField(max_length=31, blank=True)

    class Meta:
        verbose_name = 'user-profile'
        verbose_name_plural = 'users-profile'

    def __str__(self):
        return self.usuario.username
