import unicodedata

from django.contrib import messages
from django.contrib.auth import password_validation
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User, Group
from django.contrib.auth.views import PasswordChangeView

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect, render
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext, gettext_lazy as _
from django.views import View
from django.views.generic import DetailView, ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, FormView, UpdateView


from .forms import UserCreateForm, UserUpdateForm, UserPasswordChangeForm

@method_decorator(login_required, name='dispatch')
class IndexView(TemplateView):
    template_name = 'index.html'

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.view_user', raise_exception=True), name='dispatch')
class UserListView(ListView):
    """docstring for UserListView """
    model = User
    queryset = User.objects.filter(is_active=True).order_by('username')
    context_object_name = 'users'
    template_name = 'admin2/user.html'

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.add_user', raise_exception=True), name='dispatch')
class UserCreateView(FormView):
    """docstring for ."""
    template_name = 'admin2/user_create.html'
    form_class = UserCreateForm
    success_url = '/admin/usuario/'

    def form_valid(self, form):
        password2 = form.clean_password2()
        form.save()
        messages.add_message(self.request, messages.SUCCESS, 'Usuario creado con exito!')
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.change_user', raise_exception=True), name='dispatch')
class UserUpdateView(UpdateView):
    """docstring for UserListView """
    model = User
    form_class = UserUpdateForm
    template_name = 'admin2/user_update.html'
    success_url = '/admin/usuario/'

    def form_valid(self, form):
        messages.add_message(self.request, messages.INFO, 'Usuario modificado con exito!')
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.change_user', raise_exception=True), name='dispatch')
class AdminChangePasswordView(View):
    form_class = UserPasswordChangeForm
    template_name = 'admin2/user_password_change.html'
    success_url = '/admin/usuario/{}/update/'

    def get(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        form = self.form_class()
        return render(request, self.template_name, {'user': user_to_update, 'form': form})

    def post(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        form = self.form_class(self.request.POST)
        if form.is_valid():
            form.save(user_to_update)
            messages.add_message(self.request, messages.INFO, 'Se ha actualizado la contraseña para el Usuario {}!'.format(user_to_update.username))
            return redirect(self.success_url.format(user_to_update.pk))
        else:
            return render(request, self.template_name, {'user': user_to_update, 'form': form})

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.delete_user', raise_exception=True), name='dispatch')
class UserDeleteView(View):
    form_class = UserPasswordChangeForm
    template_name = 'admin2/user_delete.html'
    success_url = '/admin/usuario/'

    def get(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        return render(request, self.template_name, {'user': user_to_update})

    def post(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        user_to_update.is_active = False
        user_to_update.save()
        messages.add_message(self.request, messages.WARNING, 'Se ha desactivado el usuario {}, para reactivar por favor contacte con su administrador!'.format(user_to_update.username))
        return redirect(self.success_url)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.view_group', raise_exception=True), name='dispatch')
class GroupListView(ListView):
    """docstring for UserListView """
    model = Group
    context_object_name = 'groups'
    template_name = 'admin2/group.html'

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.add_group', raise_exception=True), name='dispatch')
class GroupCreateView(FormView):
    """docstring for ."""
    template_name = 'admin2/user_create.html'
    form_class = UserCreateForm
    success_url = '/admin/usuario/'

    def form_valid(self, form):
        password2 = form.clean_password2()
        form.save()
        messages.add_message(self.request, messages.SUCCESS, 'Usuario creado con exito!')
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.change_group', raise_exception=True), name='dispatch')
class GroupUpdateView(UpdateView):
    """docstring for UserListView """
    model = User
    form_class = UserUpdateForm
    template_name = 'admin2/user_update.html'
    success_url = '/admin/usuario/'

    def form_valid(self, form):
        messages.add_message(self.request, messages.INFO, 'Usuario modificado con exito!')
        return super().form_valid(form)

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('auth.delete_group', raise_exception=True), name='dispatch')
class GroupDeleteView(View):
    form_class = UserPasswordChangeForm
    template_name = 'admin2/user_delete.html'
    success_url = '/admin/usuario/'

    def get(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        return render(request, self.template_name, {'user': user_to_update})

    def post(self, request, *args, **kwargs):
        user_to_update = get_object_or_404(User, pk=kwargs['pk'])
        user_to_update.is_active = False
        user_to_update.save()
        messages.add_message(self.request, messages.WARNING, 'Se ha desactivado el usuario {}, para reactivar por favor contacte con su administrador!'.format(user_to_update.username))
        return redirect(self.success_url)
