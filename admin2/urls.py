from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import views as auth_views
from django.conf.urls import url, include

from . import views

urlpatterns = [
    # url(r'^login/$', auth_views.LoginView.as_view(template_name='admin2/login.html', ), name='login'),
    # url(r'^logout/$', auth_views.logout_then_login, name='logout'),
    url(r'^usuario/$', views.UserListView.as_view(), name='usuario'),
    url(r'^usuario/add/$', views.UserCreateView.as_view(), name='usuario-add'),
    url(r'^usuario/<int:pk>/update/$', views.UserUpdateView.as_view(), name='usuario-update'),
    url(r'^usuario/<int:pk>/password/$', views.AdminChangePasswordView.as_view(), name='usuario-password-change'),
    url(r'^usuario/<int:pk>/delete/$', views.UserDeleteView.as_view(), name='usuario-delete'),

    url(r'^grupo/$', views.GroupListView.as_view(), name='grupo'),
    url(r'^grupo/add/$', views.GroupCreateView.as_view(), name='grupo-add'),
    url(r'^grupo/<int:pk>/update/$', views.GroupUpdateView.as_view(), name='grupo-update'),
    url(r'^grupo/<int:pk>/delete/$', views.GroupDeleteView.as_view(), name='grupo-delete'),

]
