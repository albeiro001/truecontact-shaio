from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import (UserCreationForm, UserChangeForm,
    UsernameField, ReadOnlyPasswordHashField, PasswordChangeForm
)
from django.contrib.auth.models import User, Group
from django.utils.translation import gettext, gettext_lazy as _

from .models import UserProfile

class UserCreateForm(UserCreationForm):
    group = forms.ModelMultipleChoiceField(queryset=Group.objects.all())
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')
        field_classes = {'username': UsernameField}

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        groups = self.cleaned_data.get('group')
        if commit:
            user.save()
            user.groups.set(groups)
            user_profile = UserProfile(
                usuario=user,
            )
            user_profile.save()
        return user


class UserUpdateForm(UserChangeForm):
    #group = forms.ModelMultipleChoiceField(queryset=Group.objects.all())
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email')

class UserPasswordChangeForm(forms.Form):
    """
    A form used to change the password of a user in the admin interface.
    """
    error_messages = {
        'password_mismatch': _('The two password fields didn’t match.'),
    }
    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password', 'autofocus': True}),
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password (again)"),
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        password_validation.validate_password(password2)
        return password2

    def save(self, user_to_update, commit=True):
        """Save the new password."""
        password = self.cleaned_data["password1"]
        user_to_update.set_password(password)
        if commit:
            user_to_update.save()
            message = 'Usuario modificado con exito!'.format(user_to_update.username)
        return user_to_update, message
