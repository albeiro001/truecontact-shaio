from django.contrib.auth.models import Group
from ominicontacto_app.models import User
from truecontact.contactos.models import *

# ======================= CONTACTOS ============================================
TIPOSGESTION = [
    ('LLAMADA ENTRANTE'),
    ('LLAMADA SALIENTE'),
    ('SMS'),
    ('WHATSAPP'),
    ('MANUAL'),
]

for tipo in TIPOSGESTION:
    TipoGestion.objects.create(nombre=tipo)
    print("Creado tipo gestion", tipo )

ACCIONES_PROGRAMACION = [
    ('sin_accion', 'MANTENER PROGRAMACION'),
    ('sin_accion', 'MANTENER OPORTUNIDAD'),
    ('finalizar', 'FINALIZAR'),
    ('reprogramar', 'REPRORAMAR'),
    ('rellamar', 'RELLAMAR')
]

CALIFICACIONES_VENTAS = [
    'VENTA EXITOSA',
    'SE ENVIA COTIZACION',
    'NO INTERESADO',
    'BUSCABA BENEFICIO',
    'INFORMACION',
    'NO APLICA',
]
campana=Campana.objects.get(nombre='VENTAS')

for calificacion in CALIFICACIONES_VENTAS:
    if calificacion ==  'SE ENVIA COTIZACION':
        CalificacionGestion.objects.create(
            nombre = calificacion,
            campana = campana,
            accion_programacion = 'reprogramar'
        )
    else:
        CalificacionGestion.objects.create(
        nombre = calificacion,
        campana = campana,
        accion_programacion = 'finalizar'
    )
    print("Creado calificacion", calificacion )

CALIFICACIONES_SOPOTE = [
    'SERVICIO TECNICO',
    'GARANTIA',
    'INFORMACION',
    'NO APLICA',
]
campana=Campana.objects.get(nombre='SOPORTE')

for calificacion in CALIFICACIONES_SOPOTE:
    CalificacionGestion.objects.create(
    nombre = calificacion,
    campana = campana,
    accion_programacion = 'finalizar'
    )
    print("Creado calificacion", calificacion )

CALIFICACIONES_ATENCION_CLIENTE = [
    'PQR',
    'INFORMACION',
    'NO APLICA',
   
]
campana=Campana.objects.get(nombre='ATENCION_CLIENTE')

for calificacion in CALIFICACIONES_ATENCION_CLIENTE:
    CalificacionGestion.objects.create(
    nombre = calificacion,
    campana = campana,
    accion_programacion = 'finalizar'
    )
    print("Creado calificacion", calificacion )

CALIFICACIONES_LOGISTICA = [
    'SOLICITA SERVICIO',
    'NO INTERESADO',
    'INFORMACION',
    'NO APLICA',
   
]
campana=Campana.objects.get(nombre='LOGISTICA')

for calificacion in CALIFICACIONES_LOGISTICA:
    CalificacionGestion.objects.create(
    nombre = calificacion,
    campana = campana,
    accion_programacion = 'finalizar'
    )
    print("Creado calificacion", calificacion )
    
CALIFICACIONES_TECNOLOGIA = [
    'VENTA',
    'SOLICITUD DEMO',
    'INFORMACION',
    'NO APLICA',
   
]
campana=Campana.objects.get(nombre='TECNOLOGIA')

for calificacion in CALIFICACIONES_TECNOLOGIA:
    CalificacionGestion.objects.create(
    nombre = calificacion,
    campana = campana,
    accion_programacion = 'finalizar'
    )
    print("Creado calificacion", calificacion )



INDUSTRIAS = ['Agricultura, Plantaciones', 'Alimentacion, bebidas, tabaco',
     'Comercio', 'Construccion', 'Educacion', 'Funcion Publica', 'Hoteleria, restaurantes, turismo',
     'Industrias Quimicas', 'Ingenieria Mecanica y Electrica', 'Medios de Comunicacion, Cultura, Graficos',
     'Mineria', 'Petroleo y Gas', 'Servicios de Salud', 'Servicios Financieros y Personales', 'Servicios Publicos',
     'Telecomunicaciones', 'Textiles', 'Transporte',
]
# for industria in INDUSTRIAS:
#     Industria.objects.create(
#         nombre = industria
#     )
#     print("Creado industria", industria )


RESULTADOS_GESTION = [
    ('LLAMADA ENTRANTE', 'NUMERO EQUIVOCADO'),
    ('LLAMADA ENTRANTE', 'CONTACTO EXITOSO'),

    ('LLAMADA SALIENTE', 'CONTACTO EXITOSO'),
    ('LLAMADA SALIENTE', 'NUMERO EQUIVOCADO'),
    ('LLAMADA SALIENTE', 'BUZON DE VOZ'),
    ('LLAMADA SALIENTE', 'NO CONTESTA'),
    ('LLAMADA ENTRANTE', 'ERROR EN LLAMADA'),

    ('WHATSAPP', 'CONTACTO EXITOSO'),
    ('WHATSAPP', 'NUMERO EQUIVOCADO'),
    ('WHATSAPP', 'NO CONTESTA'),

    ('MANUAL', 'MANUAL'),
]

for resultado in RESULTADOS_GESTION:
    resultado_gestion = ResultadoGestion(
        tipogestion = TipoGestion.objects.get(nombre=resultado[0]),
        nombre = resultado[1],
    )
    resultado_gestion.save()
    print("Creado resultado", resultado_gestion.nombre )

TIPO_DOCUMENTOS = [
    ('0','Desconocido'),
    ('1','Cedula Ciudadania'),
    ('2','Cedula Extranjeria'),
    ('3','NIT'),
    ('4','Pasaporte'),
    ('5','Registro Unico de Identificacion'),
    ('6','Tarjeta de Identidad'),

]

for tipo in TIPO_DOCUMENTOS:
    tipo_documento = TipoIdentificacion(
        id = tipo[0],
        nombre = tipo[1],
    )
    tipo_documento.save()
    print("Creado documento", tipo_documento.nombre )

PAISES = [
    ('1', 'Canadá'),
    ('1', 'Estados Unidos de América'),
    ('51', 'Perú'),
    ('52', 'México'),
    ('53', 'Cuba'),
    ('54', 'Argentina'),
    ('55', 'Brasil'),
    ('56', 'Chile'),
    ('57', 'Colombia'),
    ('58', 'Venezuela'),
]





A = [
    ('1', 'Canadá'),
    ('1', 'Estados Unidos de América'),
    ('1', 'Puerto Rico'),
    ('7', 'Kazajistán'),
    ('7', 'Rusia'),
    ('20', 'Egipto'),
    ('27', 'Sudáfrica'),
    ('30', 'Grecia'),
    ('31', 'Países Bajos'),
    ('32', 'Bélgica'),
    ('33', 'Francia'),
    ('34', 'España'),
    ('36', 'Hungría'),
    ('39', 'Ciudad del Vaticano'),
    ('39', 'Italia'),
    ('40', 'Rumanía'),
    ('41', 'Suiza'),
    ('43', 'Austria'),
    ('44', 'Guernsey'),
    ('44', 'Isla de Man'),
    ('44', 'Jersey'),
    ('44', 'Reino Unido'),
    ('45', 'Dinamarca'),
    ('46', 'Suecia'),
    ('47', 'Noruega'),
    ('47', 'Svalbard y Jan Mayen'),
    ('48', 'Polonia'),
    ('49', 'Alemania'),
    ('51', 'Perú'),
    ('52', 'México'),
    ('53', 'Cuba'),
    ('54', 'Argentina'),
    ('55', 'Brasil'),
    ('56', 'Chile'),
    ('57', 'Colombia'),
    ('58', 'Venezuela'),
    ('60', 'Malasia'),
    ('61', 'Australia'),
    ('61', 'Isla de Navidad'),
    ('61', 'Islas Cocos (Keeling)'),
    ('62', 'Indonesia'),
    ('63', 'Filipinas'),
    ('64', 'Nueva Zelanda'),
    ('65', 'Singapur'),
    ('66', 'Tailandia'),
    ('81', 'Japón'),
    ('82', 'Corea del Sur'),
    ('84', 'Vietnam'),
    ('86', 'China'),
    ('90', 'Turquía'),
    ('91', 'India'),
    ('92', 'Pakistán'),
    ('93', 'Afganistán'),
    ('94', 'Sri lanka'),
    ('95', 'Birmania'),
    ('98', 'Irán'),
    ('211', 'República de Sudán del Sur'),
    ('212', 'Marruecos'),
    ('212', 'Sahara Occidental'),
    ('213', 'Algeria'),
    ('216', 'Tunez'),
    ('218', 'Libia'),
    ('220', 'Gambia'),
    ('221', 'Senegal'),
    ('222', 'Mauritania'),
    ('223', 'Mali'),
    ('224', 'Guinea'),
    ('225', 'Costa de Marfil'),
    ('226', 'Burkina Faso'),
    ('227', 'Niger'),
    ('228', 'Togo'),
    ('229', 'Benín'),
    ('230', 'Mauricio'),
    ('231', 'Liberia'),
    ('232', 'Sierra Leona'),
    ('233', 'Ghana'),
    ('234', 'Nigeria'),
    ('235', 'Chad'),
    ('236', 'República Centroafricana'),
    ('237', 'Camerún'),
    ('238', 'Cabo Verde'),
    ('239', 'Santo Tomé y Príncipe'),
    ('240', 'Guinea Ecuatorial'),
    ('241', 'Gabón'),
    ('242', 'República del Congo'),
    ('243', 'República Democrática del Congo'),
    ('244', 'Angola'),
    ('245', 'Guinea-Bissau'),
    ('246', 'Islas Ultramarinas Menores de Estados Unidos'),
    ('246', 'Territorio Británico del Océano Índico'),
    ('248', 'Seychelles'),
    ('249', 'Sudán'),
    ('250', 'Ruanda'),
    ('251', 'Etiopía'),
    ('252', 'Somalia'),
    ('253', 'Yibuti'),
    ('254', 'Kenia'),
    ('255', 'Tanzania'),
    ('256', 'Uganda'),
    ('257', 'Burundi'),
    ('258', 'Mozambique'),
    ('260', 'Zambia'),
    ('261', 'Madagascar'),
    ('262', 'Mayotte'),
    ('262', 'Reunión'),
    ('263', 'Zimbabue'),
    ('264', 'Namibia'),
    ('265', 'Malawi'),
    ('266', 'Lesoto'),
    ('267', 'Botsuana'),
    ('268', 'Swazilandia'),
    ('269', 'Comoras'),
    ('290', 'Santa Elena'),
    ('291', 'Eritrea'),
    ('297', 'Aruba'),
    ('298', 'Islas Feroe'),
    ('299', 'Groenlandia'),
    ('350', 'Gibraltar'),
    ('351', 'Portugal'),
    ('352', 'Luxemburgo'),
    ('353', 'Irlanda'),
    ('354', 'Islandia'),
    ('355', 'Albania'),
    ('356', 'Malta'),
    ('357', 'Chipre'),
    ('358', 'Finlandia'),
    ('358', 'Islas de Åland'),
    ('359', 'Bulgaria'),
    ('370', 'Lituania'),
    ('371', 'Letonia'),
    ('372', 'Estonia'),
    ('373', 'Moldavia'),
    ('374', 'Armenia'),
    ('375', 'Bielorrusia'),
    ('376', 'Andorra'),
    ('377', 'Mónaco'),
    ('378', 'San Marino'),
    ('380', 'Ucrania'),
    ('381', 'Serbia'),
    ('382', 'Montenegro'),
    ('385', 'Croacia'),
    ('386', 'Eslovenia'),
    ('387', 'Bosnia y Herzegovina'),
    ('389', 'Macedônia'),
    ('420', 'República Checa'),
    ('421', 'Eslovaquia'),
    ('423', 'Liechtenstein'),
    ('500', 'Islas Georgias del Sur y Sandwich del Sur'),
    ('500', 'Islas Malvinas'),
    ('501', 'Belice'),
    ('502', 'Guatemala'),
    ('503', 'El Salvador'),
    ('504', 'Honduras'),
    ('505', 'Nicaragua'),
    ('506', 'Costa Rica'),
    ('507', 'Panamá'),
    ('508', 'San Pedro y Miquelón'),
    ('509', 'Haití'),
    ('590', 'Guadalupe'),
    ('590', 'San Bartolomé'),
    ('591', 'Bolivia'),
    ('592', 'Guyana'),
    ('593', 'Ecuador'),
    ('594', 'Guayana Francesa'),
    ('595', 'Paraguay'),
    ('596', 'Martinica'),
    ('597', 'Surinám'),
    ('598', 'Uruguay'),
    ('670', 'Timor Oriental'),
    ('672', 'Antártida'),
    ('672', 'Isla Norfolk'),
    ('673', 'Brunéi'),
    ('674', 'Nauru'),
    ('675', 'Papúa Nueva Guinea'),
    ('676', 'Tonga'),
    ('677', 'Islas Salomón'),
    ('678', 'Vanuatu'),
    ('679', 'Fiyi'),
    ('680', 'Palau'),
    ('681', 'Wallis y Futuna'),
    ('682', 'Islas Cook'),
    ('683', 'Niue'),
    ('685', 'Samoa'),
    ('686', 'Kiribati'),
    ('687', 'Nueva Caledonia'),
    ('688', 'Tuvalu'),
    ('689', 'Polinesia Francesa'),
    ('690', 'Tokelau'),
    ('691', 'Micronesia'),
    ('692', 'Islas Marshall'),
    ('850', 'Corea del Norte'),
    ('852', 'Hong kong'),
    ('853', 'Macao'),
    ('855', 'Camboya'),
    ('856', 'Laos'),
    ('870', 'Islas Pitcairn'),
    ('880', 'Bangladesh'),
    ('886', 'Taiwán'),
    ('960', 'Islas Maldivas'),
    ('961', 'Líbano'),
    ('962', 'Jordania'),
    ('963', 'Siria'),
    ('964', 'Irak'),
    ('965', 'Kuwait'),
    ('966', 'Arabia Saudita'),
    ('967', 'Yemen'),
    ('968', 'Omán'),
    ('970', 'Palestina'),
    ('971', 'Emiratos Árabes Unidos'),
    ('972', 'Israel'),
    ('973', 'Bahrein'),
    ('974', 'Qatar'),
    ('975', 'Bhután'),
    ('976', 'Mongolia'),
    ('977', 'Nepal'),
    ('992', 'Tayikistán'),
    ('993', 'Turkmenistán'),
    ('994', 'Azerbaiyán'),
    ('995', 'Georgia'),
    ('996', 'Kirguistán'),
    ('998', 'Uzbekistán'),
]

for pais in PAISES:
    pais = Pais(
        codigo = pais[0],
        nombre = pais[1],
    )
    pais.save()
    print("Creado pais", pais.nombre )

# parametros iniciales tickets
# ESTADOS_TICKETS = [
#     ('10', 'POR_ASIGNAR', '#65c7ff'),
#     ('20', 'ASIGNADO', '#984dff'),
#     ('30', 'ACEPTADO', '#41e5c0'),
#     ('40', 'EN_EJECUCION', '#ab8903'),
#     ('50', 'EN_EVALUACION', '#29B765'),
#     ('60', 'REASIGNADO', '#D67520'),
#     ('70', 'FINALIZADO', '#3498DB'),
#     ('80', 'NO_CONCLUIDO', '#E74C3C')]

# for estado in ESTADOS_TICKETS:
#     EstadoTicket.objects.create(id=estado[0], nombre=estado[1], color=estado[2])

# TIPOS_URGENCIA = [
#     ('10', 'ALTA'),
#     ('20', 'MEDIA'),
#     ('30', 'BAJA')]

# for tipo in TIPOS_URGENCIA:
#     TipoUrgencia.objects.create(id=tipo[0], nombre=tipo[1])

Group.objects.create(
    name='administrador'
)
print("Creado grupo administrador")


Group.objects.create(
    name='supervisor'
)
print("Creado grupo supervisor")


Group.objects.create(
    name='agente'
)
print("Creado grupo agente")


# group = Group.objects.get(name='contactos_comerciales')
# user = User.objects.get(username='username')
# user.groups.add(group)

# parametros iniciales PARA RELACIONES
# RELACIONES = [
#     ('RECEPCIONISTA'),
#     ('CLIENTE'),
#     ]
# for relacion in RELACIONES:
#     Relacion.objects.create(nombre=tipo[0])


# ================================== DATOS OMNILEADS ===========================
# Java Script
# drop table contactos_calificaciongestion cascade;
# drop table contactos_resultadogestion cascade;
# drop table contactos_contacto cascade;
# drop table contactos_contacto_industria cascade;
# drop table contactos_contactodatoscontacto;
# drop table contactos_contactorelacion;
# drop table contactos_gestioncontacto cascade;
# drop table contactos_industria cascade;
# drop table contactos_pais cascade;
# drop table contactos_paisestado cascade;
# drop table contactos_relacion cascade;
# drop table contactos_resultado cascade;
# drop table contactos_tipogestion cascade;
# drop table sms_mensajesms cascade;
# drop table chatapi_dialogo cascade;
# drop table chatapi_mensaje cascade;


# =============================================================================
# ADMIN DE OMINICONTACTO_APP
