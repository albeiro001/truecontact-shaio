from django.apps import AppConfig

class LlamadasPerdidasConfig(AppConfig):
    name = 'llamadas_perdidas'