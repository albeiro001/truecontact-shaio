# Django
from django.conf import settings
from django.views.generic import ListView, View
from django.utils.decorators import method_decorator
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.http import JsonResponse, StreamingHttpResponse

# Modelos
from truecontact.llamadas_perdidas.models import LlamadaPerdida

# Modelos de la app de Omnileads
from ominicontacto_app.models import User, Campana

# Utilidades 
from datetime import datetime
import csv
import pytz

LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)

class LlamadasPerdidasListView(ListView):
    """ Vista para listar las llamadas perdidas. """

    model = LlamadaPerdida
    context_object_name = 'llamadas_perdidas'
    paginate_by = 15
    template_name = 'llamadas/llamadas_perdidas.html'

    def _obtener_campanas(self):
        agente = self.request.user.get_agente_profile()
        ids_campanas = []

        if agente == None:
            supervisor = self.request.user.get_supervisor_profile()
            campanas_queues = supervisor.campanas_asignadas_actuales_no_finalizadas()
            print("camapanas: ", campanas_queues)
            for campana in campanas_queues:
                id_campana = campana.pk
                type_campana = campana.type
                nombre_campana = campana.nombre
                ids_campanas.append((id_campana, nombre_campana, type_campana))
        else:
            campanas_queues = agente.get_campanas_activas_miembro()
            for id_nombre in campanas_queues.values_list('id_campana', flat=True):
                split_id_nombre = id_nombre.split('_')
                id_campana = split_id_nombre[0]
                campana = Campana.objects.get(pk=id_campana)
                type_campana = campana.type
                nombre_campana = '_'.join(split_id_nombre[1:])
                ids_campanas.append((id_campana, nombre_campana, type_campana))
        return ids_campanas


    def get_queryset(self):

        queryset = LlamadaPerdida.objects.all()

        if not (self.request.GET.get('estado') == '' or self.request.GET.get('estado') == None):
            f_estado = self.request.GET.get('estado')
        else:
            f_estado = 'todas'

        if not (f_estado == '' ):
            if(f_estado == 'todas'):
                queryset = queryset

            elif (f_estado == 'Nuevas'):
                queryset = queryset.filter(
                    estado='nuevo'
                )

            elif(f_estado == 'Gestionadas'):
                queryset = queryset.filter(
                    estado='getionado'
                )

            else:
                queryset = queryset.filter(
                    estado=f_estado
                )

        agente = self.request.GET.get('agente')
        if not(agente == '' or agente == None):
            if (agente == 'todas'):
                queryset = queryset
            else:
                agente = User.objects.get(pk=agente)
                queryset = queryset.filter(agente=agente)

        # END si el settings == True
        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            for termino in buscar.split():
                print("buscando..", termino)
                queryset_temp = queryset.filter(
                    Q(contacto__nombres__icontains=termino) |
                    Q(contacto__apellidos__icontains=termino) |
                    Q(contacto__documento__icontains=termino))
                queryset_final = queryset_temp | queryset_temp
            queryset = queryset_final

        if not (self.request.GET.get('f_inicial') == '' or self.request.GET.get('f_inicial') == 'None' or self.request.GET.get('f_inicial') == None):
            print("queryset hasta aqui: ", queryset.count())
            f_inicio_str = self.request.GET.get('f_inicial')
            f_inicio_llamada = datetime.strptime('{} 00:00:00'.format(f_inicio_str,'%d/%m/%Y'), '%d/%m/%Y %H:%M:%S')
            f_fin_str = self.request.GET.get('f_final')

            f_fin_llamada = datetime.strptime('{} 23:59:59'.format(f_fin_str,'%d/%m/%Y'), '%d/%m/%Y %H:%M:%S')
            queryset = queryset.filter(modified__range=(f_inicio_llamada, f_fin_llamada))

        return queryset

    def get_context_data(self, **kwargs):

        grupo_supervisor = Group.objects.get(name='Supervisor')

        filter_obj = {'active': False, 'url': ''}

        user = self.request.user

        context = super(LlamadasPerdidasListView, self).get_context_data(**kwargs)

        context['contacto_campanas'] = self._obtener_campanas()

        if not (grupo_supervisor in user.groups.all()):
            f_estado = 'todas'
        else:
            f_estado = 'todas'

        if not (self.request.GET.get('estado') == '' or self.request.GET.get('estado') == None):
            f_estado = self.request.GET.get('estado')
            print('estado ', f_estado)
            filter_obj['active'] = True
            filter_obj['url'] = f"{filter_obj['url']}&estado={f_estado}"
            filter_obj['f_estado'] = f_estado

        context['estados'] = [
            ('nuevo', 'Nuevo'),
            ('pendiente', 'Pendiente'),
            ('gestionado', 'Gestionada'),
        ]

        context['f_estado'] = f_estado

        f_inicial = self.request.GET.get('f_inicial')
        if not (f_inicial == None or f_inicial == '' ):
            print("FEcha final de recepcion: ", f'{f_inicial} 00:00:00')
            f_inicio =  datetime.strptime(f'{f_inicial} 00:00:00', '%d/%m/%Y %H:%M:%S')
        else:
            f_inicio = None

        f_f_final = self.request.GET.get('f_final')
        if not (f_f_final == None or f_f_final == '' ):
            print("FEcha final de recepcion: ", f'{f_f_final} 23:59:59')
            f_fin =  datetime.strptime(f'{f_f_final} 23:59:59', '%d/%m/%Y %H:%M:%S')
        else:
            f_fin = None

        if (f_inicio and f_fin):

            filter_obj['active'] = True
            filter_obj['url'] = f"{filter_obj['url']}&f_inicial={f_inicial}&f_final={f_f_final}"
            filter_obj['f_inicial'] = f_inicial
            filter_obj['f_fin'] = f_f_final

        buscar =  self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar

        agente = user.agenteprofile
        id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])
        campana = Campana.objects.get(pk=id_campana)

        lista_agentes = []

        for agente in campana.obtener_agentes():
            if not (grupo_supervisor in agente.user.groups.all()):
                lista_agentes.append(
                    (str(agente.user.pk), str(agente.user.username))
                )
        
        print('los agentes son',lista_agentes)

        agente = self.request.GET.get('agente')
        if not(agente == '' or agente == None):
            filter_obj['active'] = True
            filter_obj['url'] = f"{filter_obj['url']}&agente={agente}"
            filter_obj['f_agente'] = agente


        context['lista_agentes'] = lista_agentes
        context['filter_obj'] = filter_obj
        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number

        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context

@method_decorator(login_required, name='dispatch')
class PuedeGestionarPerdida(View):

    def get(self, request, *args, **kwargs):
        id_perdida = request.GET.get('id_perdida')
        print(request.GET)
        user = self.request.user
        if (LlamadaPerdida.objects.filter(pk=id_perdida).exclude(estado='gestionado').exists()):
            perdida = LlamadaPerdida.objects.get(pk=id_perdida)
            print(perdida.estado)
            if ( perdida.agente == None):
                return JsonResponse({'puede_gestionar': True})

            if (perdida.agente == user):
                return JsonResponse({'puede_gestionar': True})

            if (perdida.agente != None and perdida.creado_por != user ):
                return JsonResponse({'puede_gestionar': False})


# ============================== EXPORT CSV ====================================
class Echo:         #Buffer de apoyo para streaming
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value

def stream_data(encabezados, data):
    if encabezados:
        yield encabezados
    for obj in data:
        yield object_get_row(obj)

def object_get_row(obj):
    # encabezados = ['LLAMADA ID', 'NOMBRE CONTACTO', 'DOCUMENTO CONTACTO',
    #         'FECHA LLAMADA','ESTADO LLAMADA', 'GESTION ID', 'GESTION FECHA', 'GESTION MEDIO',
    #         'GESTION TIPO', 'GESTION RESULTADO', 'GESTION CALIFICACION',
    #     ]

    row = [
        obj.pk,
        obj.contacto.get_full_name() if obj.contacto else '',
        obj.numero if obj.numero else '',
        obj.fecha_ultima_llamada.strftime('%d/%m/%Y %H:%M:%S') if obj.fecha_ultima_llamada else '',
        obj.get_estado_display(),
        obj.llamadas if obj.llamadas else ''
    ]
    print('gestion es ', obj.gestion)
    if(obj.gestion):
        row = row + [
            obj.gestion.pk if obj.gestion else '',
            obj.gestion.fecha_creacion.strftime('%d/%m/%Y %H:%M:%S') if obj.gestion else '',
            obj.gestion.motivo.nombre if obj.gestion else '',
            obj.gestion.resul.nombre if obj.gestion else '',
            obj.gestion.especialidad.nombre if obj.gestion else '',
            obj.gestion.entidad.nombre if obj.gestion else '',
        ]
    else:
        row = row + [
            'Gestion Cerrada Por Sistema',
            '',
            '',
            '',
            '',
            '',
        ]
    return row

@method_decorator(login_required, name='dispatch')
class PerdidasListExportView(View):

    def get(self, request, *args, **kwargs):
        print("REQUEST ", request.GET)

        queryset = LlamadaPerdida.objects.all()

        if not (self.request.GET.get('estado') == '' or self.request.GET.get('estado') == None):
            f_estado = self.request.GET.get('estado')
        else:
            f_estado = 'todas'

        if not (f_estado == '' ):
            if(f_estado == 'todas'):
                queryset = queryset

            elif (f_estado == 'Nuevas'):
                queryset = queryset.filter(
                    estado='nuevo'
                )

            elif(f_estado == 'Gestionadas'):
                queryset = queryset.filter(
                    estado='getionado'
                )

            else:
                queryset = queryset.filter(
                    estado=f_estado
                )

        agente = self.request.GET.get('agente')
        if not(agente == '' or agente == None):
            if (agente == 'todas'):
                queryset = queryset
            else:
                agente = User.objects.get(pk=agente)
                queryset = queryset.filter(agente=agente)

        # END si el settings == True
        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            for termino in buscar.split():
                print("buscando..", termino)
                queryset_temp = queryset.filter(
                    Q(contacto__nombres__icontains=termino) |
                    Q(contacto__apellidos__icontains=termino) |
                    Q(contacto__documento__icontains=termino))
                queryset_final = queryset_temp | queryset_temp
            queryset = queryset_final

        if not (self.request.GET.get('f_inicial') == '' or self.request.GET.get('f_inicial') == 'None' or self.request.GET.get('f_inicial') == None):
            print("queryset hasta aqui: ", queryset.count())
            f_inicio_str = self.request.GET.get('f_inicial')
            f_inicio_llamada = datetime.strptime('{} 00:00:00'.format(f_inicio_str,'%d/%m/%Y'), '%d/%m/%Y %H:%M:%S')
            f_fin_str = self.request.GET.get('f_final')

            f_fin_llamada = datetime.strptime('{} 23:59:59'.format(f_fin_str,'%d/%m/%Y'), '%d/%m/%Y %H:%M:%S')
            queryset = queryset.filter(modified__range=(f_inicio_llamada, f_fin_llamada))

        pseudo_buffer = Echo()  # Metodo para grabar archivos muy grandes con apoyo de buffer temporal
        encabezados = ['LLAMADA ID', 'NOMBRE CONTACTO', 'NÚMERO',
            'FECHA LLAMADA','ESTADO LLAMADA', 'NÚMERO DE LLAMADAS','GESTION ID', 'GESTION FECHA', 'GESTION MOTIVO',
            'GESTION RESULTADO', 'GESTION ESPECIALIDAD', 'GESTION ENTIDAD',
        ]
        writer = csv.writer(pseudo_buffer)
        response = StreamingHttpResponse((writer.writerow(row) for row in stream_data(encabezados, queryset.order_by('-id'))) , content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="informe_llamadas_perdidas_{0}.csv"'.format(datetime.now(LOCAL_TZ).strftime('%d_%m_%Y'))
        return response
