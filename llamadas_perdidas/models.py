# Django
from django.db import models

# Modelo de la app de omnileads
from ominicontacto_app.models import User

# Modelo de la app de contactos
from truecontact.contactos.models import *

class LlamadaPerdida(models.Model):
    """ Modelo para guardar las llamadas perdidas. """

    ESTADO_LLAMADA = [
        ('nuevo', 'Nuevo'),
        ('pendiente', 'Pendiente'),
        ('gestionado', 'Gestionado'),
    ]

    contacto = models.ForeignKey(Contacto, on_delete=models.PROTECT, blank=True, null=True)
    numero = models.CharField('Número por el cual se llamó', max_length=15)
    estado = models.CharField(max_length=32, choices=ESTADO_LLAMADA, default='nuevo', blank=True, null=True)
    gestion = models.ForeignKey(GestionContacto, on_delete=models.PROTECT, blank=True, null=True)
    llamadas = models.PositiveIntegerField('Llamadas perdidas', default=0)
    llamadas_agente = models.PositiveIntegerField('Llamadas hechas por el agente', default=0)
    medio = models.CharField('Medio por el cual desea que lo comuniquen', max_length=20)
    fecha_ultima_llamada = models.DateTimeField('Fecha última llamada', blank=True, null=True)
    agente = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True)
    created = models.DateTimeField(
        'create at',
        auto_now_add=True,
        help_text='Fecha y hora en la cual la llamada fue hecha.'
    )

    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Fecha y hora de la última modificación de la llamada.'
    )

    class Meta:
        """ Opciones Meta. """
        get_latest_by = 'created'
        ordering = ['-created', '-modified']

