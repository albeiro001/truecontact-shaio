from django.urls import path

from truecontact.llamadas_perdidas import views

urlpatterns = [
    path('llamadas_perdidas/list/', views.LlamadasPerdidasListView.as_view(), name='tc_llamadas_perdidas_list'),
    path('puede_gestionar_llamada_perdida/', views.PuedeGestionarPerdida.as_view(), name='tc-puede'),
    path('llamadas/export/csv/', views.PerdidasListExportView.as_view(), name='tc-llamadas-export'),
]