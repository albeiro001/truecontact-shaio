from django.contrib import messages
from django.http import JsonResponse, HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, permission_required
import csv, io
from django.http import StreamingHttpResponse
from datetime import datetime, timezone
from truecontact.contactos.models import *
from django.db.models import Q
from datetime import timedelta
import datetime as dt
import ast

# ====================          RECURSOS PARA DESCARGAR POR CSV       ========================================================
class Echo:         #Buffer de apoyo para streaming
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


# ======  GESTIONES csv   =====================================

def streamGestiones(encabezados, data):
    if encabezados:
        yield encabezados
    for obj in data:
        yield gestiones_get_row(obj)

def gestiones_get_row(obj):

    obj.fecha_creacion = obj.fecha_creacion - timedelta(hours=5)
    tipo = obj.tipo.nombre if obj.tipo else ''
    metadata = ast.literal_eval(obj.metadata)

    if obj.contacto != None  :
        row = [obj.pk, obj.contacto.documento, obj.contacto, obj.creado_por, obj.medio, tipo, metadata['telefono'], obj.fecha_creacion.strftime('%d/%m/%Y'), obj.fecha_creacion.strftime('%H:%M:%S'),
            obj.motivo, obj.resul, obj.especialidad, obj.entidad, obj.tipo_consulta, obj.observaciones]
        
    else:
        row = [obj.pk, "", "", obj.creado_por, obj.medio, tipo, metadata['telefono'], obj.fecha_creacion.strftime('%d/%m/%Y'), obj.fecha_creacion.strftime('%H:%M:%S'),
            obj.motivo, obj.resul, obj.especialidad, obj.entidad, obj.tipo_consulta, obj.observaciones, '', '']
    return row

def CsvGestionesView(request):
    # print("REQUEST: Encuestas:...", request.GET)
    user = request.user
    if user.groups.filter(name = 'Supervisor').exists():
        queryset = GestionContacto.objects.all().order_by('-id')
    else:
        queryset = GestionContacto.objects.filter(creado_por=user).order_by('-id')

    try:
        buscar = request.GET.get('buscar')
    except:
        buscar = ''

    f_inicial = request.GET.get('f_inicial')
    f_final = request.GET.get('f_final')
    if(buscar !=''):
        queryset = queryset.filter( Q(metadata__icontains=buscar) |
                                    Q(creado_por__username__icontains=buscar) |
                                    Q(contacto__documento__icontains=buscar) )
    if not (f_inicial == None or f_final == None):
        f_inicial = dt.datetime.strptime(f_inicial, '%Y-%m-%d')
        f_final = dt.datetime.strptime(f_final, '%Y-%m-%d') + dt.timedelta(days=1)
        queryset = queryset.filter(fecha_creacion__range=(f_inicial, f_final))

    pseudo_buffer = Echo()  # Metodo para grabar archivos muy grandes con apoyo de buffer temporal
    encabezados = ['#', 'Documento', 'Cliente', 'Agente', 'Medio', 'Tipo', 'Numero', 'Fecha Gestion', 'Hora Gestion', 'Motivo', 'Resultado', 'Especialidad', 'Entidad', 'Tipo de consulta', 'Observaciones']
    writer = csv.writer(pseudo_buffer)
    response = StreamingHttpResponse((writer.writerow(row) for row in streamGestiones(encabezados, queryset.order_by('-id'))) , content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="Informe Gestiones {0}.csv"'.format(dt.datetime.now())
    return response
