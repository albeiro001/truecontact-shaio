from django.conf import settings
from django.db import models
from ominicontacto_app.models import User
from truecontact.contactos.models import Contacto


# Create your models here.
class MensajeSms(models.Model):
    contacto = models.ForeignKey(Contacto, on_delete=models.PROTECT)
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT)
    mensaje = models.TextField(default='msj')
    fecha_creacion = models.DateTimeField(auto_now_add=True)
