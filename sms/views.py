from django.conf import settings
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic  import (
    View, ListView, CreateView, UpdateView, DeleteView, FormView, TemplateView, DetailView
)
from truecontact.contactos.models import Contacto
from truecontact.contactos.forms import *
from .models import *

import json
import requests

# Create your views here.
@method_decorator(login_required, name='dispatch') #CONSTRUYENDO ACA OJOJOJOJOJOOJOJ
class EnviarSmsContacto(View):
    def post(self, request, *args, **kwargs):
        print("ENTRANDO AL METODO SMS", request.POST)
        tipo_campana = request.POST.get('tipo_campana_sms')
        campana_id = request.POST.get('pk_campana_sms')
        mensaje = request.POST.get('text_sms')
        telefonos = request.POST.getlist('telefono_sms')
        id_contacto = request.POST.get('pk_contacto_sms')
        mensaje_sms = MensajeSms(
            contacto = Contacto.objects.get(pk=id_contacto),
            creado_por = request.user,
            mensaje = mensaje
        )
        mensaje_sms.save()
        call_data = {}
        call_data = [{'tipo_campana':tipo_campana, 'campana_id':campana_id,'mensaje': mensaje, 'telefonos': telefonos, 'id_contacto': id_contacto, 'sms_pk':mensaje_sms.pk }]
        tipo = TipoGestion.objects.get(nombre='SMS')
        context = {}            #PREPARO CONTEXTO PARA MANDARLO A TEMPLATE
        diccionario = {}        #PREPARO DICCIONARIO PARA MANDARLO AL SERVER SMS
        lista_telefonos = []
        for telefono in telefonos:
            lista_telefonos.append({'to': '57'+telefono})
        diccionario["bulkId"] = 'Identificador_bulkID_0001'
        diccionario["messages"] = [{'from': 'DA2 tech', 'destinations': lista_telefonos, 'text': mensaje}]
        url_sms ='http://api.messaging-service.com/sms/1/text/advanced'
        headers = {'Authorization': 'Basic REEyOk1uZGVmczEz==', 'Content-Type': 'application/json', 'Accept': 'application/json'}  # el acceso es basic y va cificado user: password en base64
        r = requests.post(url_sms, data=json.dumps(diccionario), headers=headers, verify=False)
        if (r.status_code == 200): # Si fue exitoso debe devolder 200 de status
            rta_sms = r.json()     #debe devolver 200
            respuesta = rta_sms    # si es correcto debe devolver la RTA del server
        else:
            respuesta = 'Rechazado'
        #     # respuesta="ok"
        #     # return JsonResponse({'respuesta': respuesta })
        # # else:
        #
        #
        # if id_contacto == '':
        #     id_contacto = None

        if (Contacto.objects.filter(pk=id_contacto).exists() ):
            contacto = Contacto.objects.get(pk=id_contacto)
            template = 'contactos/contacto_gestion.html'
            medio = 'sms'
            context['contacto'] = contacto
            context['gestiones'] = GestionContacto.objects.filter(contacto=contacto)
            dic_gestion_guardar = {
                'tipo': tipo,
                'medio': medio,
                'resultado': None,
                'calificacion': None,
                'contacto': contacto,
                'metadata': call_data,
                'agente': request.user,
            }
            gestion = autoguardadoGestion(dic_gestion_guardar)
            context['form_gestion'] = GestionContactoForm(initial={
                'gestion': gestion.pk,
                'call_data': json.dumps(call_data),
                'tipo': tipo,
                'medio': medio,
                'contacto': contacto.pk,
            })
        else:
            lista_contactos = list(
                Contacto.objects.filter(
                    telefono=telefono
                ).values_list(
                    'id'
                )
            )
            lista_contactos = lista_contactos + list(
                ContactoDatosContacto.objects.filter(
                    tipo='telefono',
                    value_1=telefono,
                ).values_list(
                    'contacto'
                )
            )
            lista_contactos = list (dict.fromkeys(lista_contactos))

            if len(lista_contactos) == 1:
                contacto = Contacto.objects.get(pk=lista_contactos[0][0])
                template = 'contactos/contacto_gestion.html'
                medio = call_data['medio']
                context['contacto'] = contacto
                context['gestiones'] = GestionContacto.objects.filter(contacto=contacto)
                dic_gestion_guardar = {
                    'tipo': tipo,
                    'medio': medio,
                    'resultado': None,
                    'calificacion': None,
                    'contacto': contacto,
                    'metadata': call_data,
                    'agente': request.user
                }
                gestion = autoguardadoGestion(dic_gestion_guardar)
                context['form_gestion'] = GestionContactoForm(initial={
                    'gestion': gestion.pk,
                    'call_data': json.dumps(call_data),
                    'tipo': tipo,
                    'medio': medio,
                    'contacto': contacto.pk,
                })
            else:
                contactos = []
                for contacto_id in lista_contactos:
                    contacto = Contacto.objects.get(pk=contacto_id[0])
                    contactos.append(contacto)
                template = 'contactos/contacto_identificar.html'
                context['form'] = self.form_contacto
                context['telefono'] = telefono
                context['contactos'] = contactos


        return render(request, template, context)



def autoguardadoGestion(dic_gestion):
    tipo = dic_gestion['tipo']
    medio = dic_gestion['medio']
    contacto = dic_gestion['contacto']
    agente = dic_gestion['agente']
    metadata = dic_gestion['metadata']
    gestion = GestionContacto(
        tipo = tipo,
        medio = medio,
        contacto = contacto,
        creado_por = agente,
        metadata = metadata,
    )
    if not dic_gestion['resultado'] == None:
        resultado = dic_gestion['resultado']
        gestion.resultado = resultado
    if not dic_gestion['calificacion'] == None:
        calificacion = dic_gestion['calificacion']
        gestion.calificacion = calificacion

    gestion.save()
    return gestion
