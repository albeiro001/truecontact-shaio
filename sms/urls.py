from django.urls import path
from . import views

urlpatterns = [
    path('sms/enviar', views.EnviarSmsContacto.as_view(), name='tc-agente-sms-enviar'),
]
