from django.contrib import admin
from .models import *

@admin.register(MensajeSms)
class MensajeSmsAdmin(admin.ModelAdmin):
    list_display = ('pk','contacto', 'creado_por', 'fecha_creacion' )
