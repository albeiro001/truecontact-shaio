from django.shortcuts import render

# Create your views here.
from django.db.models import Q
from django.shortcuts import render
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core import serializers

from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views import View
from django.views.generic.edit import CreateView, UpdateView, FormView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

import json

from truecontact.catalogo.models import *
from .models import *
from truecontact.contactos.forms import *

#=============================================================================
#=============================================================================
#---------      MAIN AJUSTES VIEWS            -------------------------------.
class MainAjustesView( View ):
    template_name = 'main_ajustes.html'
    def get(self, request,*args, **kwargs):
        context = {}
        return render(self.request, self.template_name, context)
#=============================================================================


#---------      Guion VIEWS            -------------------------------.
class GuionCreate( CreateView ):
    model = Guion
    fields = '__all__'
    template_name = 'guion/create.html'
    success_url = reverse_lazy('guion-list')

class GuionUpdate( UpdateView ):
    model = Guion
    fields = '__all__'
    template_name = 'guion/update.html'
    success_url = reverse_lazy('guion-list')

class GuionList( ListView ):

    model = Guion
    paginate_by = 20
    template_name = 'guion/list.html'
    context_object_name = 'guiones'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

def deleteguion(request, pk):
    instancia = Guion.objects.get(id=pk)
    instancia.delete()
    return redirect('/tc/ajustes/guiones/list/')

class GuionConsultaAjaxView(View):

    def get(self, request, *args, **kwargs):
        # operacion_pk = request.GET.get('operacion_pk') 
        context = {}
        lista_giones = []
        # print("este es el pk",operacion_pk)
        guiones =Guion.objects.all().filter(activo=True)
        # print("este es el guion",guiones.titulo)
        context['guion'] = guiones
        # context['operacion'] = operacion
        # print(elementos[0].ubicacion)
        for guion in guiones:
            lista_giones.append(
                {
                    'id': guion.id,
                    'titulo': guion.titulo,
                    'descripcion': guion.descripcion,
                }
            )
        respuesta = json.dumps({
            'guiones': lista_giones
        })
        print('estos son los datos enviados',respuesta)
        mimetype = 'aplication/json'
        return HttpResponse(respuesta, mimetype)


#---------      Ayudas VIEWS            -------------------------------.
class AyudaCreate( CreateView ):
    model = Ayuda
    fields = '__all__'
    template_name = 'ayuda/create.html'
    success_url = reverse_lazy('ayuda-list')

class AyudaUpdate( UpdateView ):
    model = Ayuda
    fields = '__all__'
    template_name = 'ayuda/update.html'
    success_url = reverse_lazy('ayuda-list')

class AyudaList( ListView ):

    model = Ayuda
    paginate_by = 20
    template_name = 'ayuda/list.html'
    context_object_name = 'ayudas'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

def deleteayuda(request, pk):
    instancia = Ayuda.objects.get(id=pk)
    instancia.delete()
    return redirect('/tc/ajustes/ayuda/list/')

class AyudaConsultaAjaxView(View):

    def get(self, request, *args, **kwargs):
        # operacion_pk = request.GET.get('operacion_pk') 
        context = {}
        lista_ayudas = []
        # print("este es el pk",operacion_pk)
        ayudas =Ayuda.objects.all().filter(activo=True)
        # print("este es el ayuda",ayudaes.titulo)
        context['ayuda'] = ayudas
        # context['operacion'] = operacion
        # print(elementos[0].ubicacion)
        for ayuda in ayudas:
            lista_ayudas.append(
                {
                    'id': ayuda.id,
                    'titulo': ayuda.titulo,
                    'descripcion': ayuda.descripcion,
                }
            )
        respuesta = json.dumps({
            'ayudas': lista_ayudas
        })
        print('estos son los datos enviados',respuesta)
        mimetype = 'aplication/json'
        return HttpResponse(respuesta, mimetype)
