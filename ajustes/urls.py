from django.contrib import admin
from django.urls import include, path

from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# from django.conf import settings

from . import views

urlpatterns = [
    path('ajustes/',                            views.MainAjustesView.as_view(),                      name='ajustes-tc'),
    path('ajustes/guiones/create/',             views.GuionCreate.as_view(),       name='guion-create'),
    path('ajustes/guiones/list/',               views.GuionList.as_view(),         name='guion-list'),
    path('ajustes/guiones/update/<int:pk>',     views.GuionUpdate.as_view(),       name='guion-update'),
    path('ajustes/guiones/list/',               views.GuionList.as_view(),         name='guion-list'),
    path('ajustes/guion/delete/<int:pk>',           views.deleteguion ,                           name='tc-guion-delete'),
    path('ajustes/guion/consulta/ajax/',        views.GuionConsultaAjaxView.as_view(), name='ajustes-guion-consulta-ajax'),

    path('ajustes/ayuda/create/',               views.AyudaCreate.as_view(),       name='ayuda-create'),
    path('ajustes/ayuda/list/',                 views.AyudaList.as_view(),         name='ayuda-list'),
    path('ajustes/ayuda/update/<int:pk>',       views.AyudaUpdate.as_view(),       name='ayuda-update'),
    path('ajustes/ayuda/list/',                 views.AyudaList.as_view(),         name='ayuda-list'),
    path('ajustes/ayuda/delete/<int:pk>',        views.deleteayuda ,                     name='tc-ayuda-delete'),
    path('ajustes/ayuda/consulta/ajax/',        views.AyudaConsultaAjaxView.as_view(), name='ajustes-ayuda-consulta-ajax'),


]