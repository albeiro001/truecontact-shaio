from django.contrib import admin

from .models import Guion, Ayuda

@admin.register(Guion)
class GuionAdmin(admin.ModelAdmin):
    list_display = ( 'pk', 'titulo','descripcion', 'activo', 'f_registro')

@admin.register(Ayuda)
class AyudaAdmin(admin.ModelAdmin):
    list_display = ( 'pk', 'titulo','descripcion', 'activo', 'f_registro')
