from django.db import models
from ominicontacto_app.models import User

class Guion(models.Model):
    titulo = models.TextField(max_length=256,blank=True, null=True)
    descripcion = models.TextField(blank=True, null=True)
    activo = models.BooleanField(default=True)
    f_registro = models.DateTimeField(verbose_name="fecha_registro_guion", auto_now_add=True,)

    class Meta:
        ordering = ['pk']
        verbose_name = 'Guion'
        verbose_name_plural = 'Guiones'

    def __str__(self):
        return str(self.titulo)

class Ayuda(models.Model):
    titulo = models.TextField(max_length=256,blank=True, null=True)
    descripcion = models.TextField(blank=True, null=True)
    activo = models.BooleanField(default=True)
    f_registro = models.DateTimeField(verbose_name="fecha_registro_guion", auto_now_add=True,)

    class Meta:
        ordering = ['pk']
        verbose_name = 'ayuda'
        verbose_name_plural = 'ayudas'

    def __str__(self):
        return str(self.titulo)
