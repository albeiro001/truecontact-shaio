from django.urls import path, re_path
from django.contrib.auth.decorators import login_required

from . import views, views_agente, views_agenda, parametros_views
from . import calificaciones_views

urlpatterns = [
    path('contacto/', views.ContactoListView.as_view(), name='tc-contacto'),
    path('contacto/ajax/', views.ContactoListAjaxView.as_view(), name='tc-contacto-ajax'),
    path('contacto/add/', views.ContactoAddView.as_view(), name='tc-contacto-add'),
    path('contacto/add/ajax/', views.ContactoAddAjaxView.as_view(), name='tc-contacto-add-ajax'),
    # path('contacto/<int:pk>/detail/', views.ContactoDetailView.as_view(), name='tc-contacto-detail'),
    path('contacto/<int:pk>/update/', views.ContactoUpdateView.as_view(), name='tc-contacto-update'),
    re_path(r'contacto/calificar_llamada/(?P<call_data_json>.+)/$', views.ContactoCalificarLlamadaView.as_view(),
        name='tc-contacto-calificar'
    ),
    path('contacto/identificar_llamada/', views.ContactoIdentificarLlamadaView.as_view(),
        name='tc-contacto-identificar'
    ),

    path('contacto/buscar/documento/',      views.BuscarDocumentoAjax,      name='tc-buscar-documento-ajax'),

    path('contacto/<int:pk>/gestion/',      views.ContactoGestionView.as_view(), name='tc-contacto-gestion'),
    path('contacto/<int:pk>/gestion/add/',  views.ContactoGestionAddAjaxView.as_view(), name='tc-contacto-gestion-add-ajax'),

    path('contacto/<int:pk>/telefono/',     views.ContactoTelefonoList.as_view(), name='tc-contacto-telefono'),
    path('contacto/telefono/add/',          views.ContactoTelefonoAdd.as_view(), name='tc-contacto-telefono-add'),
    path('contacto/telefono/delete/',       views.ContactoTelefonoDelete.as_view(), name='tc-contacto-telefono-delete'),

    path('eliminar/telefono/ajax/view/', views.EliminarTelefonoAjaxView.as_view(), name='tc_eliminar-telefono-ajax'),
    path('editar/telefono/ajax/view/', views.EditarTelefonoAjaxView.as_view(), name='tc_editar-telefono-ajax'),

    path('contacto/<int:pk>/email/',        views.ContactoEmailList.as_view(), name='tc-contacto-email'),
    path('contacto/email/add/',             views.ContactoEmailAdd.as_view(), name='tc-contacto-email-add'),
    path('contacto/email/delete/',          views.ContactoEmailDelete.as_view(), name='tc-contacto-email-delete'),

    path('contacto/<int:pk>/direccion/',    views.ContactoDireccionList.as_view(), name='tc-contacto-direccion'),
    path('contacto/direccion/add/',         views.ContactoDireccionAdd.as_view(), name='tc-contacto-direccion-add'),
    path('contacto/direccion/delete/',      views.ContactoDireccionDelete.as_view(), name='tc-contacto-direccion-delete'),

    path('contacto/<int:pk>/dialogo/',     views.ContactoDialogoList.as_view(), name='tc-contacto-dialogo'),
    path('contacto/<int:pk>/numeros/',     views.ContactoNumerosList.as_view(), name='tc-numeros'),

    path('gestion/<int:pk>/',           views.GestionView.as_view(), name='tc-gestion'),
    path('gestion/cargar/',             views.GestionCargarView.as_view(), name='tc-gestion-cargar'),


    path('test_template/', views.TestTemplateView.as_view(), name='tc-test-template'),

    # =======================   AGENDA CONTACTOS ======================////

    path('contacto/programacion/list/', views_agenda.ProgramacionListView.as_view(), name='tc-programacion-list'),
    path('contacto/programacion/add/', views_agenda.ProgramacionAddView.as_view(), name='tc-programacion-add'),
    path('contacto/programacion/<int:pk>/update/', views_agenda.ProgramacionUpdateView.as_view(), name='tc-programacion-update'),


    # ======================== VISTAS DE OMNILEADS RE UTILIZADAS =========================
    path('agente/llamar/',
        login_required(views_agente.LlamarContactoView.as_view() ),
        name='tc-agente-llamar-contacto', ),

    # =======================   PARAMETROS ======================////
    path('parametros/agentes/permisos', parametros_views.AgentesPermisosView.as_view(), name='agentes-permisos'),
    path('parametros/formulario_configuracion/', parametros_views.FormularioConfiguracion.as_view(), name='configuracion-formulario'),

    path('parametros/resultado/', parametros_views.ResultadoGestionView.as_view(), name='resultado-list'),
    path('parametros/resultado/add/', parametros_views.ResultadoGestionCreateView.as_view(), name='resultado-create'),
    path('parametros/resultado/<int:pk>/update/', parametros_views.ResultadoGestionUpdateView.as_view(), name='resultado-update'),
    path('parametros/resultado/consulta/', parametros_views.ResultadosGestionAjax, name='resultados_ajax'),

    path('parametros/calificacion/', parametros_views.CalificacionGestionView.as_view(), name='calificacion-list'),
    path('parametros/calificacion/add/', parametros_views.CalificacionGestionCreateView.as_view(), name='calificacion-create'),
    path('parametros/calificacion/<int:pk>/update/', parametros_views.CalificacionGestionUpdateView.as_view(), name='calificacion-update'),
    path('parametros/calificacion/consulta/', parametros_views.CalificacionGestionAjax, name='calificacion_ajax'),
    
    path('parametros/tipo/', parametros_views.TipoGestionView.as_view(), name='tipo-list'),
    path('parametros/tipo/add/', parametros_views.TipoGestionCreateView.as_view(), name='tipo-create'),
    path('parametros/tipo/<int:pk>/update/', parametros_views.TipoGestionUpdateView.as_view(), name='tipo-update'),

    # Funciones AJAX para nuevo TC
    path('gestion/ajax/telefonia/',         views.GestionTelefoniaViewAjax.as_view(),   name='tc-gestion-ajax-telefonia'),
    path('contacto/ajax/view/',             views.ContactoAjaxView.as_view(),           name='tc-contacto-ajax-ver'),
    path('contacto/ajax/update/',           views.ContactoAjaxUpdate.as_view(),         name='tc-contacto-ajax-update'),
    path('contacto/ajax/gestionar/',        views.ContactoAjaxGestionar.as_view(),      name='tc-contacto-ajax-gestionar'),
    path('contacto/add/ajax/telefono/',     views.ContactoAddAjaxTelefono.as_view(),    name='tc-contacto-telefono-add-ajax'),
    path('contacto/add/ajax/correo/',       views.ContactoAddAjaxCorreo.as_view(),      name='tc-contacto-correo-add-ajax'),
    path('contacto/add/ajax/direccion/',    views.ContactoAddAjaxDireccion.as_view(),   name='tc-contacto-direccion-add-ajax'),
    path('contacto/validar/ajax/telefono/', views.ContactoValidarTelefonoAjax.as_view(),name='tc-validar-telefono-ajax'),
    path('validar/whastapp/ajax/',          views.ValidarWhastappAjax.as_view(),        name='tc-validar-whatsapp-ajax'),

    path('gestion/ajax/guardar',        views.GestionGuardarAjax.as_view(),     name='tc_gestion-ajax-guardar'),
    path('gestion/ajax/consulta/',      views.GestionViewAjax.as_view(),        name='tc_gestion-ajax-consulta'),

                           ############# Calificaciones Views ####################

    path('contacto/calificacion/list/', calificaciones_views.CalificacionListView.as_view(), name='tc-contacto-calificacion-list'),
    path('contacto/calificacion/<int:pk>/update/',calificaciones_views.CalificacionUpdateView.as_view(),name='tc-contacto-calificacion-update'),                      
    path('contacto/calificacion/add/', calificaciones_views.CalificacionAddView.as_view(),name='tc-contacto-calificacion-add'),

    path('agente/contacto/campana/', views.AgenteContactoCampanas.as_view(),name='tc-agente-contacto-campana'),
    path('contacto/calificacion/campana/', views.ContactoCalificacionCampanas.as_view(),name='tc-contacto-calificacion-campana'),

                           ############# Vistas relacionadas callcenter shaio ####################

    path('buscarmotivo/resultados/', views_agente.ObtenerDetalleMotivo.as_view(), name="tc-obtener-resultado-motivo"),

]
