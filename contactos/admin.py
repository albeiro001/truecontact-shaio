from django.contrib import admin
from .models import *

from import_export.admin import ExportActionMixin

@admin.register(TipoIdentificacion)
class TipoIdentificacionAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')

@admin.register(Contacto)
class ContactoAdmin(ExportActionMixin, admin.ModelAdmin):
    list_display = ('nombres', 'apellidos', 'documento', 'fecha_creacion')
    list_filter = ('tipo_identificacion',)

@admin.register(ContactoRelacion)
class ContactoRelacionAdmin(admin.ModelAdmin):
    list_display = ('contacto', 'contacto_relacionado', 'relacion')

@admin.register(ContactoDatosContacto)
class ContactoDatosContactoAdmin(admin.ModelAdmin):
    list_display = ('contacto', 'label', 'tipo')
    list_filter = ('tipo', )


@admin.register(CalificacionGestion)
class CalificacionGestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'accion_programacion', 'campana')
    list_filter = ('accion_programacion', )

@admin.register(ResultadoGestion)
class ResultadoGestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'tipogestion')
    list_filter = ('tipogestion', )

@admin.register(GestionContacto)
class GestionContactoAdmin(admin.ModelAdmin):
    list_display = ('id', 'tipo', 'contacto', 'estado')
    list_filter = ('estado', 'creado_por' )

@admin.register(TipoGestion)
class TipoGestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'descripcion')

@admin.register(Programacion)
class ProgramacionAdmin(admin.ModelAdmin):
    list_display = ('id', 'contacto', 'agente', 'gestion', 'n_intentos', 'estado_programacion', 'created_on')
    list_filter = ('estado_programacion', )

@admin.register(Motivo)
class MotivoAdmin(admin.ModelAdmin):
    list_display = ('id', 'campaña', 'nombre')

@admin.register(Resultado)
class MotivoAdmin(admin.ModelAdmin):
    list_display = ('id', 'motivo', 'nombre')

@admin.register(Especialidad)
class MotivoAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')

@admin.register(Entidad)
class MotivoAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')


admin.site.register(Industria)
admin.site.register(Relacion)
