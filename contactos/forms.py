from django import forms
from truecontact.chatapi.models import Dialogo, Mensaje
from ominicontacto_app.models import Campana


from truecontact.contactos.models import (Contacto, ContactoDatosContacto, Programacion,
    TipoIdentificacion, CalificacionGestion)


from .models import *

import ast
import datetime as dt
import pytz

class AgregarContactoForm(forms.ModelForm):
    class Meta:
        model = Contacto
        fields = ['nombres', 'apellidos', 'imagen', 'tipo_identificacion', 'documento', 'genero', 'cargo', 'nacimiento',
            'sitioweb', 'es_compania', 'ciudad', 'codigo_postal', 'industria', 'observaciones_contacto',
            'bool_1', 'bool_2', 'bool_3',
        ]




class GestionContactoForm(forms.ModelForm):
    # def __init__(self, campanas_ids=None, *args, **kwargs):

    #     print("PREPARANDO..........................<<<<<<<<<<<<<<<<<<<<<<<<<<<")
    #     print("filtrando camapanas: ", campanas_ids)
    #     queryset_calificaciones = []
    #     if campanas_ids:
    #         print("filtrando camapanas: ", campanas_ids)
    #         for campanapk in campanas_ids:
    #             print("buscando", campanapk)
    #             campana = Campana.objects.get(pk = campanapk)
    #             queryset_calificaciones.extend(list(CalificacionGestion.objects.filter(campana=campana)))
    #             print("CALIFICACIONES....................", queryset_calificaciones)
    #         self.fields['calificacion'].queryset = queryset_calificaciones
    class Meta:
        model = GestionContacto
        fields = [ 'tipo', 'medio', 'resultado', 'calificacion', 'contacto', 'metadata', 'observaciones']

class GestionContactoForm1(forms.ModelForm):
    class Meta:
        model = GestionContacto
        fields = [ 'tipo', 'medio', 'motivo', 'resul', 'especialidad', 'entidad', 'tipo_consulta','observaciones', 'tyc']

class GestionContactoFormViejo(forms.Form):
    gestion = forms.CharField(widget=forms.HiddenInput, required=False)
    contacto = forms.CharField(widget=forms.HiddenInput)
    call_data = forms.CharField(widget=forms.HiddenInput, required=True)
    tipo = forms.ModelChoiceField(
        queryset = TipoGestion.objects.all().order_by("nombre"),
        empty_label="Seleccione",
        required=True
    )
    medio = forms.CharField(widget=forms.HiddenInput, required=True)
    resultado = forms.ModelChoiceField(
        queryset = ResultadoGestion.objects.all().order_by("nombre"),
        empty_label="Seleccion",
        required=True
    )

    observaciones = forms.CharField(widget=forms.Textarea, required=False)

    def __init__(self, *args, **kwargs):
        super(GestionContactoForm, self).__init__(*args, **kwargs)
        if 'initial' in kwargs:
            pass
            if 'tipo' in kwargs['initial']:
                lista_resultado = ResultadoGestion.objects.filter(tipogestion__nombre__icontains=kwargs['initial']['tipo'])
                self.fields['resultado'] = forms.ModelChoiceField(queryset=lista_resultado, empty_label="Seleccione", required=True)
            # if 'medio' in kwargs['initial']:
            #     lista_resultado = ResultadoGestion.objects.filter(tipogestion__nombre__icontains=kwargs['initial']['medio'])
            #     self.fields['resultado'] = forms.ModelChoiceField(queryset=lista_resultado, empty_label="Seleccione", required=True)


    def save(self, user, contacto):
        print ("CLEANED DATA: ", self.cleaned_data)
        id_gestion = self.cleaned_data['gestion']
        resultado = self.cleaned_data['resultado']
        # calificacion = self.cleaned_data['calificacion']
        metadata = self.cleaned_data['call_data']
        observaciones = self.cleaned_data['observaciones']
        tipo = self.cleaned_data['tipo']
        medio = self.cleaned_data['medio']


        if not(id_gestion == '' or id_gestion == None):
            gestion = GestionContacto.objects.get(pk=id_gestion)
            gestion.resultado = resultado
            # gestion.calificacion = calificacion
            gestion.modificado_por = user
            gestion.fecha_finalizacion = dt.datetime.now()
            gestion.estado = 'finalizado'
            gestion.observaciones = observaciones
            medio = gestion.medio

        else:
            gestion = GestionContacto(
                tipo = tipo,
                medio = medio,
                resultado = resultado,
                # calificacion = calificacion,
                contacto = contacto,
                creado_por = user,
                modificado_por = user,
                fecha_finalizacion = dt.datetime.now(),
                metadata = metadata,
                estado = 'finalizado',
                observaciones = observaciones,
            )

        if (medio == 'whatsapp'):
            call_data = ast.literal_eval(gestion.metadata)
            dialogo = Dialogo.objects.get(pk=call_data['chat_id'])
            dialogo.estado = 'gestionado'
            dialogo.fecha_modificacion = dt.datetime.now(pytz.utc)
            dialogo.user = None
            dialogo.ultimo_mensaje_gestion = (Mensaje.objects
                                .obtener_ultimo_mensaje(
                                    dialogo=dialogo
                                )['ultimo_mensaje_numero'])
            dialogo.save()
            call_data['mensaje_final'] = dialogo.ultimo_mensaje_gestion
            gestion.metadata = call_data

        gestion.save()
        return gestion

class TelefonoForm(forms.Form):
    contacto = forms.CharField(widget=forms.HiddenInput)
    label = forms.CharField(max_length=32)
    tipo = 'telefono'
    telefono = forms.CharField(max_length=256)

    def validate(self):
        contacto = self.cleaned_data['contacto']
        label = self.cleaned_data['label']
        telefono = self.cleaned_data['telefono']
        return 0

    def save(self):
        contacto = Contacto.objects.get(pk=self.cleaned_data['contacto'])
        label = self.cleaned_data['label']
        telefono = self.cleaned_data['telefono']
        print ("""
            contacto_id: {}\n
            label: {}\n
            telefono: {}
        """.format(contacto.pk, label, telefono))
        contacto_datocontacto = ContactoDatosContacto.objects.create(
            contacto = contacto,
            label = label,
            tipo = self.tipo,
            value_1 = telefono,
        )
        return {
                    'status': 'ok',
                    'telefono': {
                        'id': contacto_datocontacto.pk,
                        'label': contacto_datocontacto.get_label_display(),
                        'telefono': contacto_datocontacto.value_1,
                    }
                }

class EmailForm(forms.Form):
    contacto = forms.CharField(widget=forms.HiddenInput)
    label = forms.CharField(max_length=32)
    tipo = 'email'
    email = forms.CharField(max_length=256)

    def validate(self):
        contacto = self.cleaned_data['contacto']
        label = self.cleaned_data['label']
        email = self.cleaned_data['email']
        return 0

    def save(self):
        contacto = Contacto.objects.get(pk=self.cleaned_data['contacto'])
        label = self.cleaned_data['label']
        email = self.cleaned_data['email']
        print ("""
            contacto_id: {}\n
            label: {}\n
            email: {}
        """.format(contacto.pk, label, email))
        contacto_datocontacto = ContactoDatosContacto.objects.create(
            contacto = contacto,
            label = label,
            tipo = self.tipo,
            value_1 = email,
        )
        return {
                    'status': 'ok',
                    'email': {
                        'id': contacto_datocontacto.pk,
                        'label': contacto_datocontacto.get_label_display(),
                        'email': contacto_datocontacto.value_1,
                    }
                }

class DireccionForm(forms.Form):
    contacto = forms.CharField(widget=forms.HiddenInput)
    label = forms.CharField(max_length=32)
    tipo = 'direccion'
    calle_1 = forms.CharField(max_length=256)
    calle_2 = forms.CharField(max_length=256, required=False)
    codigo_postal = forms.CharField(max_length=256, required=False)
    ciudad = forms.CharField(max_length=256)
    pais = forms.CharField(max_length=256)

    def validate(self):
        return 0

    def save(self):
        contacto = Contacto.objects.get(pk=self.cleaned_data['contacto'])
        label = self.cleaned_data['label']
        calle_1 = self.cleaned_data['calle_1']
        calle_2 = self.cleaned_data['calle_2']
        codigo_postal = self.cleaned_data['codigo_postal']
        ciudad = self.cleaned_data['ciudad']
        pais = self.cleaned_data['pais']
        print ("""
            contacto_id: {}\n
            label: {}\n
            calle_1: {}\n
            calle_2: {}\n
            codigo_postal: {}\n
            ciudad: {}\n
            pais: {}\n
        """.format(contacto.pk, label, calle_1, calle_2, codigo_postal, ciudad, pais))
        contacto_datocontacto = ContactoDatosContacto.objects.create(
            contacto = contacto,
            label = label,
            tipo = self.tipo,
            value_1 = calle_1,
            value_2 = calle_2,
            value_3 = codigo_postal,
            value_4 = ciudad,
            value_5 = pais,
        )
        return {
                    'status': 'ok',
                    'direccion': {
                        'id': contacto_datocontacto.pk,
                        'label': contacto_datocontacto.get_label_display(),
                        'calle_1': contacto_datocontacto.value_1,
                        'calle_2': contacto_datocontacto.value_2,
                        'codigo_postal': contacto_datocontacto.value_3,
                        'ciudad': contacto_datocontacto.value_4,
                        'pais': contacto_datocontacto.value_5,
                    }
                }



ACCIONES_PROGRAMACION = [
    ('sin_accion', 'MANTENER PROGRAMACION'),
    ('finalizar', 'FINALIZAR'),
    ('reprogramar', 'REPRORAMAR'),
    ('rellamar', 'RELLAMAR')
]

class CalificacionForm(forms.ModelForm):

    campana = forms.ModelChoiceField(
        queryset = Campana.objects.all(),
        label = 'Campaña Asociada'
    )

    nombre = forms.CharField(
        max_length = 512,
        label = "Nombres"
    )


    accion_programacion = forms.ChoiceField(
        choices = ACCIONES_PROGRAMACION, 
        label = 'Accion Calificacion'
    ) 

    def __init__(self, *args, **kwargs):
        campana_queryset = kwargs.pop('campana_queryset', None)
        super().__init__(*args, **kwargs)

        if campana_queryset:
            self.fields['campana'].queryset = campana_queryset
        
    class Meta:
        model = CalificacionGestion
        fields = ['campana', 'nombre', 'accion_programacion']

    def save(self):
        calificacion = super(CalificacionForm, self).save(commit=False)
        calificacion.save()

        return calificacion

    def update(self):
        calificacion = super(CalificacionForm, self).save(commit=False)
        calificacion.save()
        return calificacion