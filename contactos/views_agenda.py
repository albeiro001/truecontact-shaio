from django.db.models import Q
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required, permission_required
# from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic  import (View, ListView, CreateView, UpdateView, DeleteView, FormView, TemplateView, DetailView )
from openpyxl import load_workbook
from tablib import Dataset


from ominicontacto_app.models import Campana
from truecontact.chatapi.models import Dialogo

import ast
import json
import pytz
import requests
import datetime as dt

from .models import *
from .forms_agenda import ProgramacionForm

class ProgramacionListView(ListView):
    model = Programacion
    context_object_name = 'programaciones'
    paginate_by = 25
    template_name = 'agenda/programaciones_list.html'

    def _obtener_campanas(self):
        agente = self.request.user.get_agente_profile()
        campanas_queues = agente.get_campanas_activas_miembro()
        ids_campanas = []
        for id_nombre in campanas_queues.values_list('id_campana', flat=True):
            split_id_nombre = id_nombre.split('_')
            id_campana = split_id_nombre[0]
            campana = Campana.objects.get(pk=id_campana)
            type_campana = campana.type
            nombre_campana = '_'.join(split_id_nombre[1:])
            ids_campanas.append((id_campana, nombre_campana, type_campana))
        return ids_campanas

    def get_queryset(self):
        r_supervisor = Group.objects.get(name='Supervisor')
        user = self.request.user

        queryset = Programacion.objects.all().order_by('contacto')

        if not (r_supervisor in user.groups.all()):
            queryset = queryset.filter(agente=user).order_by('-id')

        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            queryset = queryset.filter(
                    Q(contacto__documento__icontains=buscar) |
                    Q(contacto__nombres__icontains=buscar) |
                    Q(contacto__apellidos__icontains=buscar) |
                    Q(eps__icontains=buscar) |
                    Q(agente__username__icontains=buscar)
            )
        # -----------FILTROS DE FECHA------------------------------------------------------------------
        f_inicial = self.request.GET.get('f_inicial')
        f_final = self.request.GET.get('f_final')
        if not (f_inicial == None or f_inicial==''):
            f_inicial = dt.datetime.strptime(f_inicial, '%Y-%m-%d')
        else:
            f_inicial = None
        if not (f_final == None or f_final==''):
            f_final = dt.datetime.strptime(f_final, '%Y-%m-%d') + dt.timedelta(days=1)
        else:
            f_final = None
        if not (f_inicial == None or f_final == None):
            queryset = queryset.filter(created_on__range=(f_inicial, f_final))
        return queryset.filter('-id')

    def get_context_data(self, **kwargs):
        context = super(ProgramacionListView, self).get_context_data(**kwargs)

        context['contacto_campanas'] = self._obtener_campanas()

        buscar =  self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar
        is_filter = False
        f_inicial = self.request.GET.get('f_inicial')
        f_final = self.request.GET.get('f_final')
        if not (f_inicial == None or f_inicial==''):
            context['f_inicial'] = f_inicial
        else:
            f_inicial = None
        if not (f_final == None or f_final==''):
            context['f_final'] = f_final
        else:
            f_final = None
        if not (f_inicial == None or f_final == None):
            context['filtro'] = '&f_inicial={0}&f_final={1}'.format(context['f_inicial'], context['f_final'])
            is_filter = True

        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context

class ProgramacionAddView(View):
    form_class = ProgramacionForm
    template_name = 'agenda/programacion_add.html'
    success_url = reverse_lazy('tc-programacion-list')

    def get(self, request, *args, **kwargs):
        context = {'form': self.form_class}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        if (request.FILES):
            archivo = request.FILES['archivo']
            format_file = archivo.name.split('.')[-1]
            if (format_file in ['xls', 'xlsx']):
                self._guardar_masivo(archivo)
            else:
                context = {'form': self.form_class}
                context['error_masivo'] = '''
                    Formato de archivo incompatible.
                    Formatos permitidos (.xls) - (.xlsx) '''
                return render(request, self.template_name, context)

        else:
            form = self.form_class(request.POST)
            if form.is_valid():
                programacion = form.save()
                grupo_supervisor = Group.objects.get(name='Supervisor')
                if not (grupo_supervisor in request.user.groups.all()):
                    programacion.agente = request.user
                    programacion.save()

        return HttpResponseRedirect(self.success_url)

    def _guardar_masivo(self, archivo):
        workbook = load_workbook(filename=archivo, read_only=True)
        first_sheet = workbook.sheetnames[0]
        worksheet = workbook[first_sheet]
        registros_procesados = 0
        registros_omitidos = 0
        resultado = []
        for i, row in enumerate(worksheet.iter_rows()):
            if not i == 0:
                campana = row[0].value
                programacion = row[1].value
                agente = row[2].value
                documento = row[3].value
                nombre = row[4].value
                telefono1 = row[5].value
                telefono2 = row[6].value
                tipo_estudio = row[7].value
                entidad = row[8].value
                fecha_cita = row[9].value
                hora_cita = row[10].value
                doctor = row[11].value

                if (nombre and documento):
                    nombres = str(nombre).strip().title()
                    documento = str(documento).strip()

                    if not (Contacto.objects.filter(documento=documento).exists()):
                        contacto = Contacto.objects.create(
                            nombres = nombres,
                            documento = documento,
                        )
                        contacto.save()                       

                    for telefono in [telefono1, telefono2]:
                        if not (telefono == '' or telefono == None):
                            telefono = str(telefono)
                            if not (ContactoDatosContacto.objects.filter(
                                tipo='telefono', value_1=telefono).exists()):

                                telefono = ContactoDatosContacto(
                                    contacto = contacto,
                                    label = 'otro',
                                    tipo = 'telefono',
                                    value_1 = telefono
                                )
                                telefono.save()
                                if (len(str(telefono.value_1)) == 10):
                                    num_wp = "57" + telefono.value_1
                                    chat_id = '{}@c.us'.format(num_wp)

                                    if not(Dialogo.objects.filter(chat_id=chat_id).exists()):
                                        dialogo = Dialogo(
                                            chat_id = chat_id,
                                            numero = chat_id.split('@')[0],
                                            es_grupo = False,
                                            contacto = contacto
                                        )
                                        dialogo.save()

                    if (User.objects.filter(username=agente).exists()):
                        agente = User.objects.get(username=agente)
                    else:
                        agente = None

                    if not(Programacion.objects.filter(contacto=contacto).exists()):
                        programacion = Programacion.objects.create(
                            contacto = contacto,
                            agente = agente,
                            char_1 = tipo_estudio,
                            char_2 = entidad,
                            char_3 = doctor,
                            char_4 = campana,
                            date_1 = fecha_cita
                        )
                        programacion.save()
                        
                    registros_procesados +=1
                    r_cargue = "Datos en la linea cargados a programaciones!"

                else:
                    registros_omitidos += 1
                    r_cargue = "Error, no se pudo cargar los datos en la linea!"

                resultado.append(
                    {
                        "linea-{}".format(i): r_cargue
                    }
                )

        return registros_procesados, registros_omitidos, resultado

class ProgramacionUpdateView(View):
    form_class = ProgramacionForm
    template_name = 'agenda/programacion_update.html'
    success_url = reverse_lazy('tc-programacion-list')

    def get(self, request, *args, **kwargs):
        programacion = get_object_or_404(Programacion, pk=kwargs['pk'])
        context = {
            'form': self.form_class(initial={
                'contacto': programacion.contacto,
                'nombres': programacion.contacto.nombres,
                'apellidos': programacion.contacto.apellidos,
                'tipo_identificacion': programacion.contacto.tipo_identificacion,
                'documento': programacion.contacto.documento,
                'agente': programacion.agente,
                'observaciones': programacion.observaciones,
            })
        }
        return render(request, self.template_name, context)


    def post(self, request, *args, **kwargs):
        programacion = get_object_or_404(Programacion, pk=kwargs['pk'])

        form = self.form_class(request.POST, instance=programacion)
        if form.is_valid():
            programacion = form.update()

        else:
            print(form.errors)
        return HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        context = super(ProgramacionUpdateView, self).get_context_data(**kwargs)
        context['programacion_pk'] = self.kwargs['pk']
        return context
