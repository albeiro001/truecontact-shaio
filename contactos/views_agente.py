from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.generic import FormView, UpdateView, TemplateView, View
from django.views.generic.base import RedirectView
from ominicontacto_app.models import (
    AgenteProfile, CalificacionCliente, Campana, AgenteEnContacto
)
from ominicontacto_app.services.click2call import Click2CallOriginator

from .models import Contacto, Motivo, Resultado


class LlamarContactoView(RedirectView):
    """
    Esta vista realiza originate hacia Asterisk para llamar dentro de una campana
    """

    pattern_name = 'view_blanco'

    def post(self, request, *args, **kwargs):
        # TODO: Analizar bien el caso de que se este agregando un contacto
        agente = AgenteProfile.objects.get(pk=request.POST['pk_agente'])
        click2call_type = request.POST.get('click2call_type', 'false')
        tipo_campana = request.POST.get('tipo_campana')
        campana_id = request.POST.get('pk_campana')
        telefono = request.POST.get('telefono', '')

        # Si el pk es 0 es porque no se quiere identificar al contacto.
        # El tipo de click2call no sera "preview".
        contacto_id = request.POST['pk_contacto']
        
        print("LlamarContactoView >>>> ", agente)
        print("LlamarContactoView >>>> ", click2call_type)
        print("LlamarContactoView >>>> ", tipo_campana)
        print("LlamarContactoView >>>> ", campana_id)
        print("LlamarContactoView >>>> ", telefono)
        print("LlamarContactoView >>>> ", contacto_id)

        campana = Campana.objects.get(id=campana_id)

        if campana.nombre != "Conmutador":
            if not contacto_id == '-1':
                contacto = Contacto.objects.get(pk=contacto_id)

            if not telefono:
                telefono = contacto.telefono

        if campana_id == '':
            calificacion_cliente = CalificacionCliente.objects.filter(
                contacto=contacto, agente=agente).order_by('-fecha')
            if calificacion_cliente.exists():
                campana = calificacion_cliente[0].campana
                campana_id = str(campana.pk)
                tipo_campana = str(campana.type)

        elif click2call_type == 'preview':
            asignado = AgenteEnContacto.asignar_contacto(contacto.id, campana_id, agente)
            if not asignado:
                message = _(u'No es posible llamar al contacto.'
                            ' Para poder llamar un contacto debe obtenerlo'
                            ' desde el menu de Campañas Preview.'
                            ' Asegurese de no haber perdido la reserva')
                messages.warning(self.request, message)
                return HttpResponseRedirect(
                    reverse('campana_preview_activas_miembro'))

        originator = Click2CallOriginator()
        originator.call_originate(
            agente, campana_id, tipo_campana, contacto_id, telefono, click2call_type)
        return HttpResponseRedirect(reverse('view_blanco'))

#### Estas son las vistas del callcenter de shaio 

class ObtenerDetalleMotivo(View):
    def get(self, request, *args, **kwargs):
        pk_motivo = request.GET['pk_motivo']

        resultados = []

        motivo_seleccionado = Motivo.objects.get(pk=pk_motivo)
        all_resultados = Resultado.objects.filter(motivo = motivo_seleccionado)
        for resultado in all_resultados:
            resultados.append(
                {
                    'pk': resultado.pk,
                    'nombre' : resultado.nombre,
                }
            )
        print('>>>> ObtenerDetalleMotivo >>> resultados son:', resultados)
        return JsonResponse(resultados, safe=False)

