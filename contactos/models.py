from django.conf import settings
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import Group
from django.db.models.deletion import PROTECT
from django.db.models.lookups import EndsWith
from ominicontacto_app.models import User
from ominicontacto_app.utiles import fecha_local
from reportes_app.models import LlamadaLog
from ominicontacto_app.models import Campana

from django.utils.timezone import now

import ast
import json
import datetime as dt
import pytz


LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)

# ==============================================================================
class Industria(models.Model):
    nombre = models.CharField(max_length=512, blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="industria_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="industria_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Industria'
        verbose_name_plural = 'Industrias'
        unique_together = ['nombre']

    def __str__(self):
        return self.nombre
# ==============================================================================
class Relacion(models.Model):
    nombre = models.CharField(max_length=512, blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="relacion_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="relacion_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Relacion'
        verbose_name_plural = 'Relaciones'

    def __str__(self):
        return self.nombre
# ==============================================================================
GENEROS = [('M', 'Masculino'), ('F', 'Femenino'), ('O', 'Otro')]

DOC_CHOICES = [
    ('CEDULA', 'Cedula Ciudadania'),
    ('PASAPORTE', 'Pasaporte'),
    ('CEDULA_EXTRANJERIA', 'Cedula Extraneria'),
    ('NIT', 'NIT'),
    ('OTRO', 'Otro')
]

class TipoIdentificacion(models.Model):
    id = models.CharField(primary_key=True, max_length=16)
    nombre = models.CharField(max_length=64)

    class Meta:
        ordering = ['id']
        verbose_name = 'Tipo de Identificacion'
        verbose_name_plural = 'Tipos de Identificacion'

    def __str__(self):
        return '{}-{}'.format(self.id, self.nombre)

class Contacto(models.Model):
    nombres = models.CharField(max_length=512, default = '')
    apellidos = models.CharField(max_length=512, default= '', blank=True, null=True)
    imagen = models.ImageField(upload_to='contactos/perfil/' ,blank=True, null=True)
    tipo_identificacion = models.ForeignKey(
        TipoIdentificacion, on_delete=models.SET_NULL, blank=True, null=True
    )
    documento = models.CharField(max_length=512, blank=True, null=True, help_text="Identification number of partner")
    genero = models.CharField(max_length=16, choices=GENEROS, blank=True)
    nacimiento = models.DateField(blank=True, null=True)
    cargo = models.CharField(max_length=254, default="", blank=True, null=True)
    idioma = models.CharField(max_length=512, default="", blank=True, null=True)
    zona_horaria = models.CharField(max_length=512, default="", blank=True, null=True)
    usuario = models.OneToOneField(User, models.DO_NOTHING, blank=True, null=True, related_name='contacto')
    sitioweb = models.CharField(max_length=512, default="", blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    es_compania = models.BooleanField(blank=True, default=False)
    ciudad = models.CharField(max_length=512, default="", blank=True, null=True)
    codigo_postal = models.CharField(max_length=512, default="", blank=True, null=True)
    industria = models.ManyToManyField(Industria, blank=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="contacto_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="contacto_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)
    token_logueo = models.CharField(max_length=512, default="", blank=True, null=True)
    tipo_logueo = models.CharField(max_length=512, default="", blank=True, null=True)
    vencimiento_logueo = models.DateTimeField(blank=True, null=True)
    observaciones_contacto = models.TextField(default="", blank=True, null=True)
    asesor = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name='asesor')
    fecha_aceptaciontyc = models.DateTimeField(blank=True, null=True)

    # Campos adicionales
    bool_1 = models.BooleanField(default=False, verbose_name='')
    bool_2 = models.BooleanField(default=False, verbose_name='')
    bool_3 = models.BooleanField(default=False, verbose_name='')
    char_1 = models.CharField(max_length=512, verbose_name='', default='', blank=True, null=True)
    char_2 = models.CharField(max_length=512, verbose_name='', default='', blank=True, null=True)
    char_3 = models.CharField(max_length=1024, verbose_name='', default='', blank=True, null=True)

    gestion_aceptatyc = models.ForeignKey("GestionContacto", on_delete=PROTECT, blank=True, null=True, related_name="gestion_aceptatyc")


    class Meta:
        ordering = ['nombres', 'apellidos']
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'


    def __str__(self):
        return self.get_short_name()

    def get_full_name(self):
        return "{} {}".format(self.nombres, self.apellidos)

    def get_short_name(self):
        nombres = self.nombres
        apellidos = self.apellidos
        if self.es_compania:
            nombre_corto = "{}".format(self.nombres)
        else:
            nombres = self.nombres.split(' ')[0]
            if not (self.apellidos == '' or self.apellidos == None):
                apellidos = self.apellidos.split(' ')[0]
            else:
                apellidos = ''
            nombre_corto = "{} {}".format(nombres, apellidos)
        return nombre_corto

    def get_contacto_gestiones(self):
        contacto_gestiones = GestionContacto.objects.filter(contacto=self.pk)
        return contacto_gestiones

    def obtener_gestiones_pendientes(self):
        contacto_gestiones = GestionContacto.objects.filter(contacto=self.pk).exclude(estado='finalizado')
        return contacto_gestiones

    def get_contacto_relaciones(self):
        contacto_relaciones = ContactoRelacion.objects.filter(
            Q(contacto=self.pk) | Q(contacto_relacionado=self.pk))
        return contacto_relaciones

    def obtener_imagen_url(self):
        if self.imagen:
            imagen_url = self.imagen.url
        else:
            imagen_url = ''
        return imagen_url

    def obtener_telefonos(self):
        lista_telefonos = []

        for dato_contacto in ContactoDatosContacto.objects.filter(
            contacto=self, tipo='telefono').exclude(activo=False):

            lista_telefonos.append(
                {
                    'id': dato_contacto.pk,
                    'label': dato_contacto.label,
                    'telefono': dato_contacto.value_1,
                    'telefono_mask': 'x'*6 + dato_contacto.value_1[-4:]
                }
            )

        return lista_telefonos

    def obtener_emails(self):
        lista_emails = []

        for dato_contacto in ContactoDatosContacto.objects.filter(
            contacto=self, tipo='email'):

            lista_emails.append(
                {
                    'id': dato_contacto.pk,
                    'label': dato_contacto.label,
                    'email': dato_contacto.value_1,
                    # 'email_mask': dato_contacto.value_1.split('@')[0][:4] + '{}@{}'.format('x'*8, dato_contacto.value_1.split('@')[1])

                }
            )

        return lista_emails

    def obtener_direcciones(self):
        lista_direcciones = []

        for dato_contacto in ContactoDatosContacto.objects.filter(
            contacto=self, tipo='direccion'):

            lista_direcciones.append(
                {
                    'id': dato_contacto.pk,
                    'label': dato_contacto.label,
                    'd1': dato_contacto.value_1,
                    'd2': dato_contacto.value_2 if dato_contacto.value_2 else '',
                    'd3': dato_contacto.value_3 if dato_contacto.value_3 else '',
                    'lat': dato_contacto.value_5 if dato_contacto.value_5 else '',
                    'lon': dato_contacto.value_6 if dato_contacto.value_6 else ''
                }
            )

        return lista_direcciones

    def obtener_dialogos_activos(self):
        lista_dialogos = []

        for dialogo in self.dialogo_set.all():
            lista_dialogos.append(
                {
                    'chat_id': dialogo.pk,
                    'numero': dialogo.numero,
                    'numero_mask': 'x'*6 + dialogo.numero[-4:]
                }
            )

        return lista_dialogos

    def get_industrias(self):
        cadena = ""
        etiquetas = self.industria.all()
        for b in etiquetas:
            cadena += "#{} ".format(b)
            cadena = cadena.capitalize()
        return cadena
    
    
# ==============================================================================
class ContactoRelacion(models.Model):
    contacto = models.ForeignKey(Contacto, models.DO_NOTHING, related_name="relacion_contacto")
    contacto_relacionado = models.ForeignKey(Contacto, models.DO_NOTHING, related_name="relacion_contacto2")
    relacion = models.ForeignKey(Relacion, models.DO_NOTHING)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="relacion_contacto_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="relacion_contacto_modificado_por")
    modificado_fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'relacion-contacto'
        verbose_name_plural = 'relaciones-contacto'

    def __str__(self):
        return "{} - {}".format(self.contacto.get_short_name(), self.contacto_relacionado.get_short_name())

# ==============================================================================
OPTION_DATO_CONTACTO = [
    ('whatsapp', 'Whatsapp'),
    ('principal', 'Principal'),
    ('casa', 'Casa'),
    ('trabajo', 'Trabajo'),
    ('movil', 'Movil'),
    ('estudio', 'Estudio'),
    ('particular', 'Particular'),
    ('otro', 'Otro'),
]

TYPE_DATO_CONTACTO = [
    ('telefono', 'Telefono'),
    ('email', 'Email'),
    ('direccion', 'Direccion'),
    ('perfilsocial', 'Perfil_Social'),
    ('otro', 'Otro'),
]

class ContactoDatosContacto(models.Model):
    contacto = models.ForeignKey(Contacto, on_delete=models.PROTECT)
    label = models.CharField(max_length=32, choices=OPTION_DATO_CONTACTO)
    tipo = models.CharField(max_length=32, choices=TYPE_DATO_CONTACTO)
    activo = models.BooleanField(default=True)
    value_1 = models.CharField(max_length=256, blank=True, null=True) # valores para telefono y email, primer elemento para direccion
    value_2 = models.CharField(max_length=256, blank=True, null=True) # segundo elemento para direccion
    value_3 = models.CharField(max_length=256, blank=True, null=True) # observacion para direccion
    value_4 = models.CharField(max_length=256, blank=True, null=True) #
    value_5 = models.CharField(max_length=256, blank=True, null=True) # latitud para direccion
    value_6 = models.CharField(max_length=256, blank=True, null=True) # longitud para direccion

    class Meta:
        ordering = ['id']
        verbose_name = 'contacto-dato-contacto'
        verbose_name_plural = 'contacto-datos-contacto'

    def get_pais_display(self):
        pais_display = ''
        if not (self.value_5 == ''):
            if (Pais.objects.filter(id=self.value_5).exists() ):
                pais = Pais.objects.get(id=self.value_5)
                pais_display = pais.nombre
        return pais_display

    def __str__(self):
        return "{}".format(self.contacto.get_short_name())

# ==============================================================================
class TipoGestion(models.Model):
    nombre = models.CharField(max_length=32)
    descripcion = models.TextField(blank=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'tipo - gestion'
        verbose_name_plural = 'tipos - gestion'

    def __str__(self):
        return str(self.nombre)


class ResultadoGestion(models.Model):
    nombre = models.CharField(max_length=32)
    tipogestion = models.ForeignKey(TipoGestion, on_delete=models.PROTECT, related_name='tipo_doc', blank=True, null=True,)
    descripcion = models.TextField(blank=True)

    class Meta:
        ordering = ['id']
        verbose_name = 'resultado - gestion'
        verbose_name_plural = 'resultados - gestion'

    def __str__(self):
        return str(self.nombre)
# ==============================================================================

ACCIONES_PROGRAMACION = [
    ('sin_accion', 'MANTENER PROGRAMACION'),
    ('sin_accion', 'MANTENER OPORTUNIDAD'),
    ('finalizar', 'FINALIZAR'),
    ('reprogramar', 'REPRORAMAR'),
    ('rellamar', 'RELLAMAR')
]

class CalificacionGestion(models.Model):
    campana = models.ForeignKey(Campana, on_delete=models.PROTECT, related_name='campana_de_calificacion', blank=True, null=True)
    nombre = models.CharField(max_length=32)
    accion_programacion = models.CharField(max_length=32,
        choices=ACCIONES_PROGRAMACION, blank=True, null=True,
        default = 'sin_accion'
    )
    descripcion = models.TextField(blank=True)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'calificacion - gestion'
        verbose_name_plural = 'calificaciones - gestion'

    def __str__(self):
        return str(self.nombre)

class Motivo(models.Model):
    campaña = models.ForeignKey(Campana, on_delete=PROTECT, blank=True, null=True)
    nombre = models.CharField(max_length=32)

    class Meta:
        ordering = ['id']
        verbose_name = 'Motivo'
        verbose_name_plural = 'Motivos'
    
    def __str__(self):
        return str(self.nombre)

class Resultado(models.Model):
    motivo = models.ForeignKey(Motivo, on_delete=models.PROTECT, null=True, blank=True)
    nombre = models.CharField(max_length=64)

    class Meta:
        ordering = ['id']
        verbose_name = 'Resultado'
        verbose_name_plural = 'Resultados'
    
    def __str__(self):
        return str(self.nombre)

class Especialidad(models.Model):
    nombre = models.CharField(max_length=32)

    class Meta:
        ordering = ['id']
        verbose_name = 'Especilidad'
        verbose_name_plural = 'Especialidades'

    def __str__(self):
        return str(self.nombre)

class Entidad(models.Model):
    nombre = models.CharField(max_length=32)

    class Meta:
        ordering = ['id']
        verbose_name = 'Entidad'
        verbose_name_plural = 'Entidades'
    
    def __str__(self):
        return str(self.nombre)
# ==============================================================================
MEDIO_GESTION = [
    ('otro', 'Otro'),
    ('telefonia', 'Telefonía'),
    ('whatsapp', 'WhatsApp'),
    ('sms', 'SMS'),
    ('email', 'Email'),
    ('chat', 'Chat'),
    ('visita', 'Visita'),
    ('manual', 'Manual'),
]

ESTADO_GESTION = [
    ('pendiente', 'Pendiente'),
    ('en_proceso', 'En Seguimiento'),
    ('finalizado', 'Finalizada'),
]

TIPO_CONSULTA = [
    ('informacion', 'Información'),
    ('presencial', 'Presencial'),
    ('teleconsulta', 'Teleconsulta'),
    ('no_confirmada', 'No confirmada')
]

class GestionContacto(models.Model):
    tipo = models.ForeignKey(TipoGestion, on_delete=models.PROTECT,  blank=True, null=True)
    medio = models.CharField(max_length=64, choices=MEDIO_GESTION, default='otro')
    resultado = models.ForeignKey(ResultadoGestion, on_delete=models.PROTECT,  blank=True, null=True)
    calificacion = models.ForeignKey(CalificacionGestion, on_delete=models.PROTECT,  blank=True, null=True)
    contacto = models.ForeignKey(Contacto, on_delete=models.PROTECT, related_name="gestion_contacto", blank=True, null=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="gestion_creado_por")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    modificado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="gestion_modificado_por")
    fecha_finalizacion = models.DateTimeField(blank=True, null=True)
    metadata = models.TextField(default='no_data')
    estado = models.CharField(max_length=64, choices=ESTADO_GESTION, default='pendiente')
    observaciones = models.TextField(default='', blank=True, null=True)
    fecha_primer_contacto = models.DateTimeField(blank=True, null=True)
    fecha_primer_contacto_agente = models.DateTimeField(blank=True, null=True)
    fecha_primer_contacto_robot = models.DateTimeField(blank=True, null=True)
    fecha_entrada_cola = models.DateTimeField(blank=True, null=True)
    motivo = models.ForeignKey(Motivo, on_delete=models.PROTECT, null=True, blank=True)
    resul = models.ForeignKey(Resultado, on_delete=models.PROTECT, null=True, blank=True)
    tyc = models.BooleanField(null=True, blank=True)
    especialidad = models.ForeignKey(Especialidad, on_delete=models.PROTECT, null=True, blank=True)
    entidad = models.ForeignKey(Entidad, on_delete=models.PROTECT, null=True, blank=True)
    tipo_consulta = models.CharField(max_length=16, choices=TIPO_CONSULTA, null=True, blank=True)

    class Meta:
        ordering = ['-id']
        verbose_name = 'gestion - contacto'
        verbose_name_plural = 'gestiones - contacto'
        permissions = (
                       ("ver_historial_whatsapp", "Puede ver Historial whastapp"),
                       ("ver_datos_contacto", "Puede ver los datos del contacto"),
                       ("ver_gestiones", "Puede ver gestiones"),
                       ("ver_detalle_contacto", "Puede ver Contacto al Detalle"),
                      )

    def __str__(self):
        return str(self.pk)

    def get_telefono(self):
        call_data = ast.literal_eval(self.metadata)
        if 'telefono' in call_data:
            telefono = call_data['telefono']
        else:
            telefono = ''
        return str(telefono)

    def get_call_id(self):
        call_data = ast.literal_eval(self.metadata)
        if 'call_id' in call_data:
            call_id = call_data['call_id']
        else:
            call_id = ''
        return str(call_id)

    def get_chat_id(self):
        temp = self.metadata
        call_data = ast.literal_eval(self.metadata)
        if 'chat_id' in call_data:
            chat_id = call_data['chat_id']
        else:
            chat_id = ''
        return str(chat_id)


    def obtener_grabaciones_by_filtro(self, fecha_desde, fecha_hasta, tipo_llamada, tel_cliente,
                                      callid, id_contacto_externo, agente, campana, campanas,
                                      marcadas, duracion, gestion):
        INCLUDED_EVENTS = ['COMPLETEAGENT', 'COMPLETEOUTNUM', 'BT-COMPLETE',
                           'COMPLETE-BT', 'CT-COMPLETE', 'COMPLETE-CT', 'CAMPT-COMPLETE',
                           'COMPLETE-CAMPT', 'BTOUT-COMPLETE', 'COMPLETE-BTOUT', 'CTOUT-COMPLETE',
                           'COMPLETE-CTOUT']
        campanas_id = [campana.id for campana in campanas]
        grabaciones = self.filter(campana_id__in=campanas_id,
                                  archivo_grabacion__isnull=False,
                                  duracion_llamada__gt=0,
                                  event__in=INCLUDED_EVENTS)

        grabaciones = grabaciones.exclude(
            archivo_grabacion='-1').exclude(event='ENTERQUEUE-TRANSFER')

        if fecha_desde and fecha_hasta:
            fecha_desde = datetime_hora_minima_dia(fecha_desde)
            fecha_hasta = datetime_hora_maxima_dia(fecha_hasta)
            grabaciones = grabaciones.filter(time__range=(fecha_desde,
                                                          fecha_hasta))
        if tipo_llamada:
            grabaciones = grabaciones.filter(tipo_llamada=tipo_llamada)
        if tel_cliente:
            grabaciones = grabaciones.filter(
                numero_marcado__contains=tel_cliente)
        if callid:
            grabaciones = grabaciones.filter(callid=callid)
        if agente:
            grabaciones = grabaciones.filter(agente_id=agente.id)
        if campana:
            grabaciones = grabaciones.filter(campana_id=campana)
        if duracion and duracion > 0:
            grabaciones = grabaciones.filter(duracion_llamada__gte=duracion)
        if id_contacto_externo:
            telefonos_contacto = Contacto.objects.values('telefono')
            telefono_id_externo = telefonos_contacto.filter(
                id_externo=id_contacto_externo)
            grabaciones = grabaciones.filter(
                numero_marcado__in=[t['telefono'] for t in telefono_id_externo])
        if marcadas:
            total_grabaciones_marcadas = self.obtener_grabaciones_marcadas()
            grabaciones = grabaciones & total_grabaciones_marcadas
        if gestion:
            calificaciones_gestion_campanas = CalificacionCliente.obtener_califs_gestion_campanas(
                campanas)
            callids_calificaciones_gestion = list(calificaciones_gestion_campanas.values_list(
                'callid', flat=True))
            grabaciones = grabaciones.filter(
                callid__in=callids_calificaciones_gestion)

        return grabaciones.order_by('-time')


    def get_url_grabacion(self):
        INCLUDED_EVENTS = ['COMPLETEAGENT', 'COMPLETEOUTNUM', 'BT-COMPLETE',
                           'COMPLETE-BT', 'CT-COMPLETE', 'COMPLETE-CT', 'CAMPT-COMPLETE',
                           'COMPLETE-CAMPT', 'BTOUT-COMPLETE', 'COMPLETE-BTOUT', 'CTOUT-COMPLETE',
                           'COMPLETE-CTOUT']

        call_data = ast.literal_eval(self.metadata)
        grabaciones = LlamadaLog.objects.filter(archivo_grabacion__isnull=False,
                                  duracion_llamada__gt=0,
                                  event__in=INCLUDED_EVENTS)

        grabaciones = grabaciones.exclude(
            archivo_grabacion='-1').exclude(event='ENTERQUEUE-TRANSFER')


        if (call_data['medio'] == 'telefonia'):

            if (grabaciones.filter(callid=call_data['call_id']).exists()):
                grabacion = grabaciones.filter(callid=call_data['call_id'])[0]
                return grabacion.url_archivo_grabacion

            else:
                return ''
        else:
            return ''
    def get_agenda1(self):
        call_data = ast.literal_eval(self.metadata)
        if 'id_agenda' in call_data:
            agenda1 = call_data['id_agenda']
        else:
            agenda1 = ''
        return str(agenda1)
    def get_agenda2(self):
        call_data = ast.literal_eval(self.metadata)
        if 'id_agenda_refuerzo' in call_data:
            agenda2 = call_data['id_agenda_refuerzo']
        else:
            agenda2 = ''
        return str(agenda2)

    def get_programacion(self):
        call_data = ast.literal_eval(self.metadata)
        if 'programacion_id' in call_data:
            programacion_pk = call_data['programacion_id']
        else:
            programacion_pk = None
        return programacion_pk
    
    def obtener_campana(self):
        call_data = ast.literal_eval(self.metadata)
        if 'id_campana' in call_data:
            campana_pk = call_data['id_campana']
            nombre = Campana.objects.get(pk=campana_pk).nombre
        else:
            nombre = ""
        return str(nombre)


# ==============================================================================

ESTADO_PROGRAMACION = [
    ('nuevo', 'Nuevo'),
    ('pendiente', 'Pendiente'),
    ('gestionado', 'Gestionado'),


]

class Programacion(models.Model):
    contacto = models.ForeignKey(Contacto, on_delete=models.PROTECT, blank=True, null=True)
    agente = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True)
    estado_programacion = models.CharField(max_length=64, choices=ESTADO_PROGRAMACION, default='nuevo')
    observaciones = models.TextField(default='', blank=True, null=True)
    gestion = models.ForeignKey(GestionContacto, on_delete=models.PROTECT, blank=True, null=True, default="")
    n_intentos = models.IntegerField(default=0)
    fecha_gestionar = models.DateTimeField(verbose_name='Proximo Contacto',blank=True, null=True)
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="programacion_creado_por")
    created_on = models.DateTimeField(verbose_name="fecha registro", auto_now_add=True ) # <<< volver a este formato

    # Campos adicionales
    char_1 = models.CharField(max_length=1024, verbose_name='TIPO DE ESTUDIO', default='', blank=True, null=True)
    char_2 = models.CharField(max_length=1024, verbose_name='ENTIDAD', default='', blank=True, null=True)
    char_3 = models.CharField(max_length=1024, verbose_name='DOCTOR', default='', blank=True, null=True)
    char_4 = models.ForeignKey(Campana, on_delete=models.PROTECT, blank=True, null=True)
    char_5 = models.CharField(max_length=1024, verbose_name='TELEFONO 1', default='', blank=True, null=True)
    char_6 = models.CharField(max_length=1024, verbose_name='TELEFONO 2', default='', blank=True, null=True)
    bool_1 = models.BooleanField(default=False, verbose_name='')
    bool_2 = models.BooleanField(default=False, verbose_name='')
    bool_3 = models.BooleanField(default=False, verbose_name='')
    bool_4 = models.BooleanField(default=False, verbose_name='')
    bool_5 = models.BooleanField(default=False, verbose_name='')
    date_1 = models.DateTimeField(verbose_name='FECHA CITA', blank=True, null=True)
    date_2 = models.DateTimeField(verbose_name='PROGRAMACION FECHA', blank=True, null=True)
    date_3 = models.DateTimeField(verbose_name='', blank=True, null=True)
    date_4 = models.DateTimeField(verbose_name='', blank=True, null=True)
    date_5 = models.DateTimeField(verbose_name='', blank=True, null=True)
    text_1 = models.TextField(verbose_name='', default='', blank=True, null=True)
    text_2 = models.TextField(verbose_name='', default='', blank=True, null=True)
    text_3 = models.TextField(verbose_name='', default='', blank=True, null=True)
    text_4 = models.TextField(verbose_name='', default='', blank=True, null=True)
    text_5 = models.TextField(verbose_name='', default='', blank=True, null=True)

    class Meta:
        ordering = ['-created_on']
        permissions = (
            ('add_bulk_programacion', 'can add bulk programacion'),
        )

    def __str__(self):
        return str(self.pk)
# ==============================================================================
