import re
from django.db.models import Q
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.utils.decorators import method_decorator
from django.views.generic  import (
    View, ListView, CreateView, UpdateView, TemplateView, DetailView
)
from ominicontacto_app.models import Campana

from .forms import AgregarContactoForm, GestionContactoForm, GestionContactoForm1, TelefonoForm, EmailForm, DireccionForm
from .models import *
from django.urls import reverse_lazy

from truecontact.chatapi.models import Dialogo, Mensaje, Encuesta_bot
from truecontact.chatapi.controlador import ChatApi

from truecontact.custom_client.bot.bot import Bot

from truecontact.llamadas_perdidas.models import LlamadaPerdida

from truecontact.custom_client.models import Conmutador

import ast
import json
import pytz
import datetime as dt

from datetime import datetime

# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)
ENCUESTA_ACTIVA = False


#
@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.add_contacto', raise_exception=True), name='dispatch')
class ContactoListView(ListView):
    """docstring for UserListView """
    model = Contacto
    context_object_name = 'contactos'
    template_name = 'contactos/contacto_list.html'
    paginate_by = 18

    def _obtener_campanas(self):
        if (self.request.user.get_agente_profile()):
            agente = self.request.user.get_agente_profile()
            campanas_queues = agente.get_campanas_activas_miembro()
            ids_campanas = []
            for id_nombre in campanas_queues.values_list('id_campana', flat=True):
                split_id_nombre = id_nombre.split('_')
                id_campana = split_id_nombre[0]
                campana = Campana.objects.get(pk=id_campana)
                type_campana = campana.type
                nombre_campana = '_'.join(split_id_nombre[1:])
                ids_campanas.append((id_campana, nombre_campana, type_campana))
            return ids_campanas
        else:
            ids_campanas = []
            return ids_campanas


    def get_queryset(self):
        # queryset = Contacto.objects.all()
        # Si en el settings esta relacionado agente con contacto guardo la relacion
        if self.request.user.groups.filter(name = 'supervisor').exists():
            queryset = Contacto.objects.filter(activo=True)
        elif settings.CONTACTO_ASOCIADO_AGENTE :
            queryset = Contacto.objects.filter(Q(asesor=self.request.user) |
                                               Q(asesor=None) |
                                               Q(activo=True)) #todos mas los que no tienen asesor
        else:
            queryset = Contacto.objects.filter(activo=True)
        # END si el settings == True
        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            for termino in buscar.split():
                queryset_temp = queryset.filter(
                    Q(nombres__icontains=termino) |
                    Q(apellidos__icontains=termino) |
                    Q(documento__icontains=termino)
                )
                queryset_final = queryset_temp | queryset_temp
            queryset = queryset_final



        return queryset

    def get_context_data(self, **kwargs):
        context = super(ContactoListView, self).get_context_data(**kwargs)
        if settings.CONTACTO_ASOCIADO_AGENTE :
            context['total_contactos'] = Contacto.objects.filter(Q(asesor=self.request.user) | Q(asesor=None) ).count() #todos mas los que no tienen asesor
        else:
            context['total_contactos'] = Contacto.objects.all().count()

        context['contacto_campanas'] = self._obtener_campanas()
        display = self.request.GET.get('display')
        if display == None:
            display = 'kanban'
        context['display'] = display

        buscar =  self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar

        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.add_contacto', raise_exception=True), name='dispatch')
class ContactoListAjaxView(View):
    def get(self, request, *args, **kwargs):
        lista_contactos = []
        buscar = request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            queryset = Contacto.objects.filter(
                Q(nombres__icontains=buscar) |
                Q(apellidos__icontains=buscar) |
                Q(documento__icontains=buscar))
            lista_contactos = list(queryset.values('id', 'nombres', 'apellidos', 'cargo'))
            return JsonResponse({'status': 'ok', 'contactos': lista_contactos})
        else:
            return JsonResponse({'status': 'error', 'errors': ['Error al buscar el contacto'] })


@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.add_contacto', raise_exception=True), name='dispatch')
class ContactoAddView(CreateView):
    model = Contacto
    template_name = 'contactos/contacto_add.html'
    fields = '__all__'
    success_url = reverse_lazy('tc-contacto')


    def get_context_data(self, **kwargs):
        context = super(ContactoAddView, self).get_context_data(**kwargs)
        context['options_dato_contacto'] = OPTION_DATO_CONTACTO
        # context['countries'] = Pais.objects.all()
        context['agentes'] = User.objects.filter(is_agente=True).order_by('first_name')


        return context

    def form_valid(self, form):
        documento = self.request.POST.get('documento')
        if not(documento == '' or documento == None):
            if (Contacto.objects.filter(documento=documento).exists() ):
                form.add_error('documento', 'Ya existe un contacto con el documento especificado')
                return self.form_invalid(form)
        self.object = form.save()
        contacto = self.object
        contacto.activo = True
        contacto.save()

        # Si en el settings esta relacionado agente con contacto guardo la relacion
        if settings.CONTACTO_ASOCIADO_AGENTE :
            contacto.asesor = self.request.user
            contacto.save()

        label_telefonos_contacto = self.request.POST.getlist('label_telefonos_contacto')
        numeros_telefono_contacto = self.request.POST.getlist('telefonos_contacto')
        a=0
        for i in label_telefonos_contacto:
            contactodatos = ContactoDatosContacto(contacto=contacto,
                    label = label_telefonos_contacto[a],
                    value_1 = numeros_telefono_contacto[a],
                    tipo = 'telefono',
            )
            contactodatos.save() # Grabo los telefonos de contacto
            if label_telefonos_contacto[a] == 'whatsapp':
                num_wp = "57" + numeros_telefono_contacto[a]
                chat_id = '{}@c.us'.format(num_wp)
                if not (Dialogo.objects.filter(pk=chat_id).exists()):
                    dialogo = Dialogo(
                        chat_id = chat_id,
                        numero = chat_id.split('-')[0].split('@')[0],
                        es_grupo = True if len(chat_id.split('-')) > 1 else False,
                    )
                    dialogo.save()
                    dialogo.contacto = contacto
                    dialogo.save()

                    bot = Bot()
                    dialogo.metadata = bot.ini_metadata(dialogo)
                    dialogo.save()

                else:
                    print('devolver Error al formulario')

            a = a +1

        label_correos = self.request.POST.getlist('label_correo')
        correos_contacto = self.request.POST.getlist('correo_contacto')
        a=0
        for i in label_correos:
            contactodatos = ContactoDatosContacto(contacto=contacto,
                    label = label_correos[a],
                    value_1 = correos_contacto[a],
                    tipo = 'email',
            )
            contactodatos.save() # Grabo los telefonos de contacto
            a = a +1

        label_direcciones = self.request.POST.getlist('label_direccion')
        direcciones_ppal = self.request.POST.getlist('direccion_principal')
        direcciones_obs = self.request.POST.getlist('direccion_observaciones')
        a=0

        for i in label_direcciones:
            contactodatos = ContactoDatosContacto(contacto=contacto,
                    label = label_direcciones[a],
                    value_1 = direcciones_ppal[a],
                    value_2 = direcciones_obs[a],
                    tipo = 'direccion',
            )
            contactodatos.save() # Grabo los telefonos de contacto
            a = a +1

        return HttpResponseRedirect(self.get_success_url())

@method_decorator(login_required, name='dispatch')


@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.add_contacto', raise_exception=True), name='dispatch')
class ContactoUpdateView(UpdateView):
    template_name = 'contactos/contacto_detail.html'
    model =  Contacto
    fields = '__all__'
    success_url = reverse_lazy('tc-contacto')

    def get_context_data(self, **kwargs):
        context = super(ContactoUpdateView, self).get_context_data(**kwargs)
        contacto = self.get_object()
        context['agentes'] = User.objects.filter(is_agente=True).order_by('first_name')
        context['telefonos'] = contacto.obtener_telefonos()
        context['emails'] = contacto.obtener_emails()
        context['direcciones'] = contacto.obtener_direcciones()
        context['gestiones'] = GestionContacto.objects.filter(contacto=contacto)
        return context


@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.change_contacto', raise_exception=True), name='dispatch')
class ContactoCalificarLlamadaView(View):
    form_contacto = AgregarContactoForm


    def get(self, request, *args, **kwargs):
        context = {}
        call_data = json.loads(kwargs['call_data_json'])
        print("Call data ", call_data)
        context['call_data'] = json.dumps(call_data)
        id_contacto = call_data['id_contacto']
        tipo_llamada = call_data['call_type'] # call_type=1 is OUTBOUND CALL call_type=3 is INBOUND CALL

        if tipo_llamada == '1':
            tipo = 'LLAMADA SALIENTE'
        elif tipo_llamada == '2':
            tipo = 'MANUAL'
        elif tipo_llamada == '3':
            tipo = 'LLAMADA ENTRANTE'
        elif tipo_llamada == '4':
            tipo = 'WHATSAPP'
        else:
            tipo = 'MANUAL'

        if id_contacto == '':
            id_contacto = None
        telefono = call_data['telefono']

        if (Contacto.objects.filter(pk=id_contacto).exists() ):
            contacto = Contacto.objects.get(pk=id_contacto)
            template = 'contactos/contacto_gestion.html'
            medio = call_data['medio']
            context['contacto'] = contacto
            context['gestiones'] = GestionContacto.objects.filter(contacto=contacto)[:10]
            dic_gestion_guardar = {
                'tipo': tipo,
                'medio': medio,
                'resultado': None,
                'calificacion': None,
                'contacto': contacto,
                'metadata': call_data,
                'agente': request.user,
            }
            gestion = autoguardadoGestion(dic_gestion_guardar)
            context['form_gestion'] = GestionContactoForm(initial={
                'gestion': gestion.pk,
                'call_data': json.dumps(call_data),
                'tipo': TipoGestion.objects.get(nombre=tipo),
                'medio': medio,
                'contacto': contacto.pk,

            })

            print('===============se fue con encuestaaaaaaaaaaaaaaaaa===============')
            context['telefonos'] = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='telefono')
            context['emails'] = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='email')
            context['direcciones'] = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='direccion')

        else:
            lista_contactos = list(
                Contacto.objects.filter(
                    telefono=telefono
                ).values_list(
                    'id'
                )
            )
            lista_contactos = lista_contactos + list(
                ContactoDatosContacto.objects.filter(
                    tipo='telefono',
                    value_1=telefono,
                ).values_list(
                    'contacto'
                )
            )
            lista_contactos = list (dict.fromkeys(lista_contactos))

            if len(lista_contactos) == 1:
                contacto = Contacto.objects.get(pk=lista_contactos[0][0])
                template = 'contactos/contacto_gestion.html'
                medio = call_data['medio']
                context['contacto'] = contacto
                context['gestiones'] = GestionContacto.objects.filter(contacto=contacto)[:10]
                dic_gestion_guardar = {
                    'tipo': tipo,
                    'medio': medio,
                    'resultado': None,
                    'calificacion': calificaciones_campanas,
                    'contacto': contacto,
                    'metadata': call_data,
                    'agente': request.user
                }
                gestion = autoguardadoGestion(dic_gestion_guardar)
                context['form_gestion'] = GestionContactoForm(initial={
                    'gestion': gestion.pk,
                    'call_data': json.dumps(call_data), #de call data a Json
                    'tipo': TipoGestion.objects.get(nombre=tipo),
                    'medio': medio,
                    'contacto': contacto.pk,
                })

                context['telefonos'] = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='telefono')
                context['emails'] = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='email')
                context['direcciones'] = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='direccion')


            else:
                contactos = []
                for contacto_id in lista_contactos:
                    contacto = Contacto.objects.get(pk=contacto_id[0])
                    contactos.append(contacto)
                template = 'contactos/contacto_identificar.html'
                context['form'] = self.form_contacto
                context['telefono'] = telefono
                context['contactos'] = contactos

        return render(request, template, context)
        #except:
        #    return render(request, 'contactos/contacto_identificar.html', {})


@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.change_contacto', raise_exception=True), name='dispatch')
class ContactoIdentificarLlamadaView(View):

    def get(self, request, *args, **kwargs):
        call_data = json.loads(request.GET.get('call_data'))
        telefono = call_data['telefono']

        lista_contactos_tmp = []

        for dato_contacto in ContactoDatosContacto.objects.filter(
            tipo='telefono', value_1=telefono).distinct():

            contacto = dato_contacto.contacto

            lista_contactos_tmp.append(contacto)



        lista_contactos_unica = list(set(lista_contactos_tmp))

        lista_contactos = []

        for contacto in lista_contactos_unica:

            lista_contactos.append(
                {
                    'id': contacto.pk,
                    'nombre_corto': contacto.get_short_name(),
                    'nombre_largo': contacto.get_full_name(),
                    'imagen': contacto.obtener_imagen_url(),
                    'documento': contacto.documento,
                }
            )

        respuesta = {
            'cantidad_contactos': len(lista_contactos),
            'contactos': lista_contactos,
        }

        return JsonResponse(respuesta)


@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.change_contacto', raise_exception=True), name='dispatch')
class ContactoGestionView(DetailView):
    template_name = 'contactos/contacto_gestion.html'
    model =  Contacto
    fields = '__all__'
    success_url = '/contacto/'

    def get_context_data(self, **kwargs):
        context = super(ContactoGestionView, self).get_context_data(**kwargs)
        contacto = self.get_object()
        context['gestiones'] = GestionContacto.objects.filter(contacto=contacto)[:50]
        form_gestion_contacto = GestionContactoForm(initial={
            'call_data': {
                'id_contacto': contacto.id,
                'medio': 'manual',
            },
            'tipo': 'otro',
            'medio': 'manual',
            'contacto': contacto.pk,
        })
        context['telefonos'] = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='telefono')
        context['emails'] = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='email')
        context['direcciones'] = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='direccion')
        context['form_gestion'] = form_gestion_contacto
        return context

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.change_contacto', raise_exception=True), name='dispatch')
class GestionCargarView(View):

    def get(self, request, *args, **kwargs):
        print("REQUEST: ", request.GET)
        id_gestion = request.GET.get('id_gestion')
        gestion = GestionContacto.objects.get(pk=id_gestion)
        if gestion.contacto != None:
            historial = []
            gestiones = GestionContacto.objects.filter(contacto = gestion.contacto)[1:7]
            for pasadas in gestiones:
                historial.append(
                    {
                        'id': pasadas.pk,
                        'fecha_creacion': pasadas.fecha_creacion.strftime('%d/%m/%Y, %H:%M:%S %p'),
                        'medio': "No.{}: {}".format(pasadas.pk, pasadas.medio),
                        'estado': pasadas.estado
                    }
                )
        else:
            historial = []

        try:
            hechotyc = True if gestion.contacto.fecha_aceptaciontyc else False
        except:
            hechotyc = False

        call_data = ast.literal_eval(gestion.metadata)
        respuesta = {
            'id': gestion.pk,
            'id_contacto': gestion.contacto.pk if gestion.contacto else '',
            'contacto_full_name': gestion.contacto.get_full_name() if gestion.contacto else call_data['telefono'],
            'contacto_imagen': gestion.contacto.obtener_imagen_url() if gestion.contacto else '',
            'call_data': call_data,
            'tipo': gestion.tipo.pk if gestion.tipo else '',
            'tipo_nombre': gestion.tipo.nombre if gestion.tipo else '',
            'medio': gestion.medio,
            # 'resultado': gestion.resultado.pk if gestion.resultado else '',
            # 'calificacion': gestion.calificacion.pk if gestion.calificacion else '',
            'observaciones': gestion.observaciones,
            'estado': gestion.estado,
            'url_grabacion': gestion.get_url_grabacion(),
            'agente': gestion.creado_por.get_full_name(),
            'metadata': gestion.metadata,
            'telefono' : gestion.contacto.obtener_telefonos() if gestion.contacto else '',
            'historial': historial,
            'motivo' : gestion.motivo.pk if gestion.motivo else '',
            'resul' : gestion.resul.pk if gestion.resul else '',
            'especialidad' : gestion.especialidad.pk if gestion.especialidad else '',
            'entidad' : gestion.entidad.pk if gestion.entidad else '',
            'tipo_consulta' : gestion.tipo_consulta,
            'tyc' : gestion.tyc,
            'hechotyc' : hechotyc
        }

        info_programacion = {
            'has_data': False,
            'data': ''
        }

        if gestion.contacto:
            print('A la gestión se le atrubuyó el contacto')
        else: 
            print('A la gestión no se le atrubuyó el contacto')

        if (call_data['modelo_gestionar']['origen'] != 'None'):
            origen_gestion = call_data['modelo_gestionar']['origen']
            if (origen_gestion == 'programacion'):
                pk_programacion = call_data['modelo_gestionar']['pk']
                if (Programacion.objects.filter(pk=pk_programacion).exists()):
                    print('existe programacion')
                    programacion = Programacion.objects.get(pk=pk_programacion)
                    info_programacion['has_data'] = True
                    info_programacion['data'] = {
                        'id': programacion.pk,
                        'documento_contacto': programacion.contacto.documento,
                        'doctor': programacion.char_3,
                        'tipoes': programacion.char_1,
                        'entidad': programacion.char_2,
                        'fechacit': programacion.date_1.strftime("%Y-%m-%d %H:%M"),
                        'fechapro': programacion.date_2.strftime("%Y-%m-%d %H:%M"),
                    }
                    print('info de la programacion ', info_programacion['data'])

        #



        respuesta['info_programacion'] = info_programacion

        return JsonResponse({'respuesta': respuesta})

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.change_contacto', raise_exception=True), name='dispatch')
class ContactoGestionAddAjaxView(View):

    def post(self, request, *args, **kwargs):
        #print("##################### GUARDANDO GESTION ########################")
        #print("REQUEST: ", request.POST)
        id_contacto = kwargs['pk']
        user = request.user
        contacto = Contacto.objects.get(pk=id_contacto)
        form_gestion_contacto = GestionContactoForm(request.POST)
        if form_gestion_contacto.is_valid():
            gestion = form_gestion_contacto.save(user, contacto)
            return JsonResponse({'status': 'ok', 'gestion': {
                'id': gestion.pk,
                'fecha': gestion.fecha_creacion.astimezone(LOCAL_TZ).strftime('%d %b %Y'),
                'agente': gestion.creado_por.username,
                'medio': gestion.medio,
                'estado': gestion.estado,
            }})
        else:
            print("Formulario de Gestion Invalido")
            print("Formulario errores: ", form_gestion_contacto.errors)
            return JsonResponse({'status': 'error', 'errors': form_gestion_contacto.errors })

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.view_contacto', raise_exception=True), name='dispatch')
class GestionView(View):
    """ Docstring """
    def get(self, request, *args, **kwargs):
        id_gestion = kwargs['pk']
        gestion = GestionContacto.objects.get(pk=id_gestion)
        respuesta = {
            'id': gestion.pk,
            'contacto': {
                'id': gestion.contacto.pk if gestion.contacto else '',
                'nombre_corto': gestion.contacto.get_short_name() if gestion.contacto else '',
            },
            'call_data': ast.literal_eval(gestion.metadata),
            'tipo': gestion.tipo.pk if not gestion.tipo == None else '',
            'tipo_nombre': gestion.tipo.nombre if not gestion.tipo == None else '',
            'medio': gestion.medio,
            'motivo' : gestion.motivo.nombre if gestion.motivo else '',
            'resul' : gestion.resul.nombre if gestion.resul else '',
            'especialidad' : gestion.especialidad.nombre if gestion.especialidad else '',
            'entidad' : gestion.entidad.nombre if gestion.entidad else '',
            'tipo_consulta' : gestion.tipo_consulta,
            'tyc' : gestion.tyc,
            'estado': gestion.estado,
            'url_grabacion': gestion.get_url_grabacion() if gestion.medio == 'telefonia' else '',
            'agente': gestion.creado_por.username,
            'fecha_gestion': gestion.fecha_creacion.strftime("%d/%m/%Y %H:%M"),
            'observaciones' : gestion.observaciones
        }
        return JsonResponse({'status': 'ok', 'gestion': respuesta})


@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.view_contacto', raise_exception=True), name='dispatch')
class ContactoTelefonoList(View):

    def get(self, request, *args, **kwargs):
        contacto = get_object_or_404(Contacto, pk=kwargs['pk'])
        list_telefonos = contacto.obtener_telefonos()
        respuesta = {
            'telefonos': list_telefonos,
            'contacto': {
                'id': contacto.pk,
                'nombre': contacto.get_full_name()
            }
        }
        print ("Respuesta telefonos: ", respuesta)
        return JsonResponse(respuesta)


@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.view_contacto', raise_exception=True), name='dispatch')
class ContactoDialogoList(View):

    def get(self, request, *args, **kwargs):
        contacto = get_object_or_404(Contacto, pk=kwargs['pk'])
        # id_campana=self.request.GET.get('id_campana')
        # print("id_campana", id_campana)
        # campana = Campana.objects.get(pk=id_campana)
        # dialogo = Dialogo.objects.get(contacto=contacto)
        # dialogo.campana = campana.nombre
        # dialogo.save()


        list_dialogos = contacto.obtener_dialogos_activos()
        respuesta = {
            'dialogos': list_dialogos,
            'contacto': {
                'id': contacto.pk,
                'nombre': contacto.get_full_name()
            }
        }
        print ("Respuesta telefonos: ", respuesta)
        return JsonResponse(respuesta)

class ContactoNumerosList(View):

    def get(self, request, *args, **kwargs):
        contacto = get_object_or_404(Contacto, pk=kwargs['pk'])
        list_dialogos = contacto.obtener_dialogos_activos()
        list_telefonos = contacto.obtener_telefonos()

        respuesta = {
            'dialogos': list_dialogos,
            'telefonos': list_telefonos,
            'contacto': {
                'id': contacto.pk,
                'nombre': contacto.get_full_name()
            }
        }

        print ("Respuesta telefonos: ", respuesta)
        return JsonResponse(respuesta)

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.add_contacto', raise_exception=True), name='dispatch')
class ContactoTelefonoAdd(View):

    def post(self, request, *args, **kwargs):
        form_telefono = TelefonoForm(request.POST)
        if form_telefono.is_valid():
            response = form_telefono.save()
        else:
            response = {
                            'status': 'error',
                            'errors': form_telefono.errors
                        }
        return JsonResponse({'response': response})

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.delete_contacto', raise_exception=True), name='dispatch')
class ContactoTelefonoDelete(View):

    def get(self, request, *args, **kwargs):
        id = self.request.GET.get('id')
        contacto_datocontacto = ContactoDatosContacto.objects.get(pk=id)
        try:
            contacto_datocontacto.delete()
            response = {'status': 'ok'}
        except:
            response = {'status': 'error'}
        return JsonResponse({'response': response})

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.view_contacto', raise_exception=True), name='dispatch')
class ContactoEmailList(View):

    def get(self, request, *args, **kwargs):
        contacto = get_object_or_404(Contacto, pk=kwargs['pk'])
        queryset = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='email')
        lista = contacto.obtener_telefonos()
        for dato in queryset:
            lista.append(
                {
                    'id': dato.pk,
                    'label': dato.label,
                    'correo': dato.value_1
                }
            )
        return JsonResponse({'emails': lista})

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.add_contacto', raise_exception=True), name='dispatch')
class ContactoEmailAdd(View):
    def post(self, request, *args, **kwargs):
        form_email = EmailForm(request.POST)
        if form_email.is_valid():
            response = form_email.save()
        else:
            response = {
                            'status': 'error',
                            'errors': form_email.errors
                        }
        return JsonResponse({'response': response})

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.delete_contacto', raise_exception=True), name='dispatch')
class ContactoEmailDelete(View):

    def get(self, request, *args, **kwargs):
        id = self.request.GET.get('id')
        contacto_datocontacto = ContactoDatosContacto.objects.get(pk=id)
        try:
            contacto_datocontacto.delete()
            response = {'status': 'ok'}
        except:
            response = {'status': 'error'}
        return JsonResponse({'response': response})

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.view_contacto', raise_exception=True), name='dispatch')
class ContactoDireccionList(View):

    def get(self, request, *args, **kwargs):
        contacto = get_object_or_404(Contacto, pk=kwargs['pk'])
        queryset = ContactoDatosContacto.objects.filter(contacto=contacto, tipo='direccion')
        lista = []
        for dato in queryset:
            lista.append(
                {
                    'id': dato.pk,
                    'label': dato.label,
                    'calle': dato.value_1,
                    'calle 2': dato.value_1,
                    'calle': dato.value_2,
                    'codigo_postal': dato.value_3,
                    'ciudad': dato.value_4,
                    'pais': dato.value_5,
                }
            )
        return JsonResponse({'direcciones': lista})

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.add_contacto', raise_exception=True), name='dispatch')
class ContactoDireccionAdd(View):
    def post(self, request, *args, **kwargs):
        form_direccion = DireccionForm(request.POST)
        if form_direccion.is_valid():
            response = form_direccion.save()
        else:
            response = {
                            'status': 'error',
                            'errors': form_direccion.errors
                        }
            print (response)
        return JsonResponse({'response': response})

@method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('contactos.delete_contacto', raise_exception=True), name='dispatch')
class ContactoDireccionDelete(View):

    def get(self, request, *args, **kwargs):
        id = self.request.GET.get('id')
        contacto_datocontacto = ContactoDatosContacto.objects.get(pk=id)
        try:
            contacto_datocontacto.delete()
            response = {'status': 'ok'}
        except:
            response = {'status': 'error'}
        return JsonResponse({'response': response})

class TestTemplateView(TemplateView):
    template_name = 'contactos/test_template.html'

def autoguardadoGestion(dic_gestion):
    tipo = TipoGestion.objects.get(nombre__icontains=dic_gestion['tipo'])
    medio = dic_gestion['medio']
    agente = dic_gestion['agente']
    metadata = dic_gestion['metadata']
    id_campana =  metadata['id_campana']
    campana = Campana.objects.get(pk=id_campana)
    if campana.nombre != "Conmutador":
        gestion = GestionContacto(
            tipo = tipo,
            medio = medio,
            creado_por = agente,
            metadata = metadata,
            )
        gestion.save()
    
    else: 
        gestion = Conmutador(
            tipo = tipo,
            medio = medio,
            creado_por = agente,
            metadata = metadata,
        )
        gestion.save()
    
    if not dic_gestion['contacto'] == None:
        contacto = dic_gestion['contacto']
        gestion.contacto = contacto
    gestion.save()
    print(">>>> autoguardadoGestion >>> gestion salvada: ", gestion.pk)
    # logger.error('GESTION GRABADA', gestion.pk)

    return gestion
######==============================================================================================
##################################################################################################
# OJO ACA LE METO LAS NUEVAS FUNCIONES EN AJAX ALGUNAS SON RECICLADAS


# @method_decorator(permission_required('contactos.add_contacto', raise_exception=True), name='dispatch')
class ContactoAddAjaxView(View):
    def post(self, request, *args, **kwargs):
        id_contacto = request.POST.get('id_contacto')
        call_data = json.loads(request.POST.get('call_data'))
        if (id_contacto == '' or id_contacto == None):
            form_contacto = AgregarContactoForm(request.POST, request.FILES)
        else:
            contacto = Contacto.objects.get(pk=id_contacto)
            form_contacto = AgregarContactoForm(request.POST, request.FILES, instance=contacto)

        if form_contacto.is_valid():
            contacto = form_contacto.save()

            # Si en el settings esta relacionado agente con contacto guardo la relacion
            if settings.CONTACTO_ASOCIADO_AGENTE :
                contacto.asesor = self.request.user
                contacto.save()
            # END si el settings == True
            medio = call_data['medio']
            if (medio == 'whatsapp'):
                chat_id = call_data['chat_id']
                dialogo = Dialogo.objects.get(chat_id = chat_id)
                dialogo.contacto = contacto
                dialogo.save()

            #Si el numero ya existe no lo vuelve a guardar
            if ContactoDatosContacto.objects.filter(value_1=call_data['telefono']).exists():
                print("Ya existe el telefono ")
            else:
                contacto_telefono = ContactoDatosContacto(
                    contacto=contacto,
                    label='principal',
                    value_1=call_data['telefono'],
                    tipo='telefono',
                )
                contacto_telefono.save()
            # end
            return JsonResponse({
                'status': 'ok',
                'contacto': {
                    'id': contacto.pk,
                    'nombre_corto': contacto.get_short_name(),
                    'nombre_largo': contacto.get_full_name(),
                    'imagen': contacto.obtener_imagen_url(),
                }

            })

        else:
            return JsonResponse({'status': 'error', 'errors': form_contacto.errors })

class ContactoAjaxUpdate(View):
    def post(self, request, *args, **kwargs):
        print("Request", self.request.POST)
        id_contacto = request.POST['id_contacto']
        contacto = Contacto.objects.get(pk=id_contacto)
        form_contacto = AgregarContactoForm(request.POST, instance=contacto)
        call_data = json.loads(request.POST.get('call_data'))
        if form_contacto.is_valid():
            print("··········· ACTUALIZANDO ______", contacto)
            print("The following fields changed: %s" % ", ".join(form_contacto.changed_data))
            form_contacto.save()
            medio = call_data['medio']
        

            return JsonResponse({
                'status': 'ok',
                'contacto': {
                    'id': contacto.pk,
                    'nombre_corto': contacto.get_short_name(),
                    'nombre_largo': contacto.get_full_name(),
                    'imagen': contacto.obtener_imagen_url(),}
            })
        else:
            return JsonResponse({'status': 'error', 'errors': form_contacto.errors })

class ContactoAjaxGestionar(View):

    def get(self, request, *args, **kwargs):
        if request.is_ajax():

            call_data_json = self.request.GET['call_data_json'] # Llega sin serializar
            call_data = json.loads(call_data_json) # Metodo para serializarlo y tenerlo como un diccionario de python
            call_data['programacion_id'] = ''
            medio = call_data['medio']
            id_contacto = call_data['id_contacto']
            tipo_llamada = call_data['call_type'] # call_type=1 is OUTBOUND CALL call_type=3 is INBOUND CALL

            print('>>>> ContactoAjaxGestionar  >>> ', call_data)

            if tipo_llamada == '1':
                tipo = 'LLAMADA SALIENTE'
                campana_d = call_data['id_campana']
                campana_d = Campana.objects.get(pk=campana_d)
                campana_d = campana_d.nombre

            elif tipo_llamada == '2':
                tipo = 'MANUAL'
            elif tipo_llamada == '3':
                tipo = 'LLAMADA ENTRANTE'
                campana_d = call_data['id_campana']
                campana_d = Campana.objects.get(pk=campana_d)
                campana_d = campana_d.nombre

            elif tipo_llamada == '4':
                tipo = 'WHATSAPP'
            else:
                tipo = 'MANUAL'

            if (tipo_llamada == '4'):
                chat_id = call_data['chat_id']
                dialogo = Dialogo.objects.get(chat_id=chat_id)
                campana_d = dialogo.campana

            campana = Campana.objects.get(nombre= campana_d)

            if campana.nombre != "Conmutador":
                if not (id_contacto == '' or id_contacto == None): #Tengo identificado el contacto por el ID
                    contacto = Contacto.objects.get(pk=id_contacto)

                    print("telefono: ", call_data['telefono'])
                    telefono_marcado = call_data['telefono']

                    if (call_data['modelo_gestionar']['origen'] != 'None'):
                        modelo_gestionar = call_data['modelo_gestionar']['origen']

                        if modelo_gestionar == 'programacion':
                            pk_programacion = call_data['modelo_gestionar']['pk']
                            if (Programacion.objects.filter(pk= pk_programacion, contacto=contacto,char_4 = campana).exclude(estado_programacion='gestionado').exists()):
                                programacion = Programacion.objects.get(pk = pk_programacion, contacto = contacto ,char_4 = campana )
                                print('encontrooooo programaciooon :', programacion.pk)
                                print('encontrooooo programaciooon :', programacion.contacto)
                                print('encontrooooo programaciooon :', programacion.n_intentos)
                                programacion.estado_programacion = 'pendiente'
                                programacion.n_intentos += 1
                                programacion.save()
                                print('Se sumo intentoooo :', programacion.n_intentos)

                                call_data['programacion_id'] = programacion.pk

                                info_programacion = {
                                    'has_data': True,
                                    'data': {
                                        'id': programacion.pk,
                                        'documento_contacto': programacion.contacto.documento,
                                        'observaciones': programacion.observaciones,
                                    }
                                }
                            else:
                                info_programacion = {
                                    'has_data': False,
                                    'data': ''
                                }
                            
                            if (LlamadaPerdida.objects.filter(numero=telefono_marcado).exclude(estado='gestionado').exists()):
                                llamadas_perdidas = LlamadaPerdida.objects.filter(numero=telefono_marcado)
                                for llamada in llamadas_perdidas:
                                    llamada.agente = request.user
                                    llamada.estado = 'gestionado'
                                    llamada.n_intentos += 1
                                    llamada.save()

                        elif modelo_gestionar == 'perdida':

                                pk_perdida = call_data['modelo_gestionar']['pk']
                                if LlamadaPerdida.objects.filter(pk = pk_perdida).exclude(estado = 'gestionado').exists():
                                    perdida = LlamadaPerdida.objects.get(pk = pk_perdida)
                                    perdida.estado = 'pendiente'
                                    perdida.agente = request.user
                                    perdida.llamadas_agente += 1
                                    perdida.save()

                                    call_data['perdida_id'] = perdida.pk

                                    info_programacion = {
                                        'has_data': False,
                                        'data': ''
                                    }

                    else: #SI NO LLAMO POR PROGRAMACION PERO DEPRONTO TIENE UNA SE LA MUESTRO AL AGENTE
                        if (Programacion.objects.filter(contacto=contacto,char_4 = campana).exclude(estado_programacion='gestionado').exists()):
                            programacion = Programacion.objects.filter(contacto = contacto ,char_4 = campana ).exclude(estado_programacion='gestionado').order_by('pk')[0]
                            print('encontrooooo  gestion sin origen :', programacion.pk)
                            print('encontrooooo  gestion sin origen :', programacion.contacto)
                            print('encontrooooo  gestion sin origen :', programacion.n_intentos)
                            programacion.estado_programacion = 'pendiente'
                            programacion.n_intentos += 1
                            programacion.save()
                            print('Se sumo intentoooo :', programacion.n_intentos)

                            call_data['programacion_id'] = programacion.pk

                            info_programacion = {
                                'has_data': True,
                                'data': {
                                    'id': programacion.pk,
                                    'documento_contacto': programacion.contacto.documento,
                                    'observaciones': programacion.observaciones,
                                }
                            }
                            if (LlamadaPerdida.objects.filter(numero=telefono_marcado).exclude(estado='gestionado').exists()):
                                llamadas_perdidas = LlamadaPerdida.objects.filter(numero=telefono_marcado)
                                for llamada in llamadas_perdidas:
                                    llamada.agente = request.user
                                    llamada.estado = 'gestionado'
                                    llamada.n_intentos += 1
                                    llamada.save()

                        else:
                            info_programacion = {
                                'has_data': False,
                                'data': ''
                            }

                    dic_gestion_guardar = {
                        'tipo': tipo,
                        'medio': medio,
                        'contacto': contacto,
                        'metadata': call_data,
                        'agente': request.user,
                    }

                else:
                    contacto = None
                    dic_gestion_guardar = {
                        'tipo': tipo,
                        'medio': medio,
                        'contacto': None,
                        'metadata': call_data,
                        'agente': request.user,
                    }

                    info_programacion = {
                        'has_data': False,
                        'data': ''
                    }
                
                gestion = autoguardadoGestion(dic_gestion_guardar)

                if not (dic_gestion_guardar['metadata']['programacion_id'] == ''):
                    id_programacion = dic_gestion_guardar['metadata']['programacion_id']
                    programacion = Programacion.objects.get(pk=id_programacion)
                    programacion.gestion = gestion
                    programacion.save() #ACA LE ACTUALIZO A LA PROGRAMACION SU PK GESTION

                if (medio == 'whatsapp'):
                    print(">>>> ContactoAjaxGestionar >>> Entro a almacenar la gestion {} en el dialogo {} ".format(medio, call_data['chat_id']))
                    dialogo = Dialogo.objects.get(pk=call_data['chat_id'])
                    dialogo.gestion = gestion
                    dialogo.save()

            else:
                if not (id_contacto == '' or id_contacto == None):
                    contacto = Contacto.objects.get(pk=id_contacto)
                    dic_gestion_guardar = {
                        'tipo': tipo,
                        'medio': medio,
                        'contacto': contacto,
                        'metadata': call_data,
                        'agente': request.user,
                    }
                else:
                    dic_gestion_guardar = {
                        'tipo': tipo,
                        'medio': medio,
                        'contacto': None,
                        'metadata': call_data,
                        'agente': request.user,
                    }

                info_programacion = {
                        'has_data': False,
                        'data': ''
                    }
                
                gestion = autoguardadoGestion(dic_gestion_guardar)


            respuesta = {
                'gestion': gestion.pk,
                'info_programacion': info_programacion,
                'nombre_campana': campana_d,
            }

            return JsonResponse({'respuesta': respuesta})

        else:
            return HttpResponse("Peticion no aceptada")

class ContactoAjaxView(View):
    """ Docstring """
    def get(self, request, *args, **kwargs):
        print("ContactoAjaxView........\n")
        # dialogo = get_object_or_404(Dialogo, pk=kwargs['pk'])
        pk_contacto = self.request.GET.get('pk_contacto')
        print("PK del Contacto:\n", pk_contacto)
        contacto = get_object_or_404(Contacto, pk = pk_contacto)
        respuesta = {
            'id': contacto.pk,
            'nombres': contacto.nombres,
            'apellidos': contacto.apellidos,
            'tipo_identificacion': contacto.tipo_identificacion.pk if contacto.tipo_identificacion else '',
            'documento': contacto.documento if contacto.documento else '',
            'ciudad': contacto.ciudad if contacto.ciudad else '',
            'es_compania': contacto.es_compania,
            'es_cliente': contacto.bool_1,
            'es_proveedor': contacto.bool_2,
            'posible_cliente': contacto.bool_3,
            'industria': list(contacto.industria.values_list('id', flat=True)),
            'telefonos': contacto.obtener_telefonos(),
            'emails': contacto.obtener_emails(),
            'direcciones': contacto.obtener_direcciones(),
            'observaciones': contacto.observaciones_contacto
        }
        print("respuesta", respuesta)
        return JsonResponse({'status': 'ok', 'contacto': respuesta})

class GestionViewAjax(View):
    """ Docstring """
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            call_data_json = self.request.GET['call_data_json'] # Llega una String NO ME SIRVE ES COMO LLEGO
            print("GestionViewAjax ..", call_data_json)
            call_data = json.loads(call_data_json) # Metodo para pasarlo a Tipo Objeto para Django
            tipo_llamada = call_data['call_type'] # call_type=1 is OUTBOUND CALL call_type=3 is INBOUND CALL
            if tipo_llamada == '4':#SI ES UN WHATSAPP
                gestion = GestionContacto.objects.filter(metadata__contains = call_data['chat_id'])[0]
            else: #es una llamada
                gestion = GestionContacto.objects.filter(metadata__contains = call_data['call_id'])[0]
            # call_id = self.request.GET.get('call_id')
            # print('consultando gestion: ', call_id)
            # try:        #Busquelo por por call_id
            #     gestion = GestionContacto.objects.filter(metadata__contains = call_id)[0]
            #     print("gestion de telefonia", gestion.pk)
            # except:
            #     gestion = GestionContacto.objects.get(pk = call_id)
            #     print("gestion de WHATSAPP", gestion.pk)

            respuesta = {
                'id': gestion.pk,
                'contacto_pk': gestion.contacto.pk if gestion.contacto else '',
                'contacto_full_name': gestion.contacto.get_full_name() if gestion.contacto else gestion.get_telefono(),
                'contacto_imagen': gestion.contacto.obtener_imagen_url() if gestion.contacto else '',
                'call_data': ast.literal_eval(gestion.metadata),
                'tipo': gestion.tipo.pk if gestion.tipo else '',
                'tipo_nombre': gestion.tipo.nombre if gestion.tipo else '',
                'medio': gestion.medio,
                'resultado': gestion.resultado.pk if gestion.resultado else '',
                'calificacion': gestion.calificacion.pk if gestion.calificacion else '',
                'observaciones': gestion.observaciones,
                'estado': gestion.estado,
                'url_grabacion': gestion.get_url_grabacion(),
                'agente': gestion.creado_por.get_full_name(),
                'metadata': gestion.metadata,
            }
            return JsonResponse({'status': 'ok', 'respuesta': respuesta})
        else:
            return JsonResponse({'status': 'fail', 'respuesta': 'Noajax'})

class GestionTelefoniaViewAjax(View):
    def get(self, request, *args, **kwargs):

        call_id = self.request.GET.get('call_id')
        print('consultando gestion: ', call_id)
        gestion = GestionContacto.objects.filter(metadata__contains = call_id)[0]
        print('Gestion es: ', gestion)

        respuesta = {
            'id': gestion.pk,
            'contacto_pk': gestion.contacto.pk if gestion.contacto else '',
            'contacto_full_name': gestion.contacto.get_full_name() if gestion.contacto else gestion.get_telefono(),
            'contacto_imagen': gestion.contacto.obtener_imagen_url() if gestion.contacto else '',
            'call_data': ast.literal_eval(gestion.metadata),
            'tipo': gestion.tipo.pk if gestion.tipo else '',
            'tipo_nombre': gestion.tipo.nombre if gestion.tipo else '',
            'medio': gestion.medio,
            'resultado': gestion.resultado.pk if gestion.resultado else '',
            'calificacion': gestion.calificacion.pk if gestion.calificacion else '',
            'observaciones': gestion.observaciones,
            'estado': gestion.estado,
            'url_grabacion': gestion.get_url_grabacion(),
            'agente': gestion.creado_por.get_full_name(),
            'metadata': gestion.metadata,
        }
        return JsonResponse({'status': 'ok', 'gestion': respuesta})


@method_decorator(login_required, name='dispatch')
class GestionGuardarAjax(View):
    def post(self, request, *args, **kwargs):
        id_gestion = request.POST['id_gestion']
        print('>>>> GestionGuardarAjax >>> EL POST eeeeeees:', request.POST)
        if id_gestion == '':   #GUARDANDO FORMULARIO NUEVO puede ser whatsapp o FACEBOOK Telefonia
            form = GestionContactoForm1(request.POST)
            if form.is_valid():
                obj = form.save()
                obj.estado= "en_proceso"
                obj.save()
                numero = obj.pk
#             print("Nueva gestion registrada___", obj.pk , obj.estado)
                return JsonResponse({'respuesta': 'Formulario Parcial', 'valido': True, 'numero':numero, 'estado':obj.estado })
        
            else: 
                return JsonResponse({'valido': False})
        

        else:
            gestion = GestionContacto.objects.get(pk=id_gestion) # ACTUALIZANDO FORMULARIO EXISTENTE
            id_contacto = request.POST['contacto']
            if not (id_contacto == ''):
                if(Contacto.objects.filter(pk=id_contacto).exists()):
                    gestion.contacto = Contacto.objects.get(pk=id_contacto)
                    gestion.save()
                    metadata = ast.literal_eval(gestion.metadata)
                    metadata['id_contacto'] = gestion.contacto.pk
                    gestion.metadata = str(metadata)
                    gestion.save()

            
            
            form = GestionContactoForm1(request.POST, instance=gestion)

            # print("tipo datos: {} valor datos: {}".format(type(gestion.metadata), gestion.metadata))
            # print("Voy a evaluar los datos del metadata antes del form gestioncontacto form")
            # print("METADATA: ", ast.literal_eval(gestion.metadata))
            # print("Voy a evaluar los datos del metadata despues del form gestioncontacto form")
            # print("METADATA: ", ast.literal_eval(gestion.metadata))
            if form.is_valid():
                if (gestion.medio == 'whatsapp'):   # SI FUE UN WHASTAPP ACTUALIZO EL DIALOGO Y LO DEJO "gestionado"
                    div_gestion = 'gestion-{}'.format(gestion.get_chat_id()) #gestion STR me sirve para ir y borrrar de la cola de pendientes si es WHATSAPP
                else:
                    div_gestion = 'gestion-{}'.format(gestion.get_call_id()) #gestion STR me sirve para ir y borrrar de la cola de pendientes si es LLAMADA

                if form.has_changed(): #Si el formulario cambio es por que esta actualizando, si no es que esta guardando y completado ya
                    # print("Actualizando gestion pk:", gestion)
                    form.save()
                if request.POST.get('finalizar') == 'true':
                    print(">>>> GestionGuardarAjax >>> Finalizando gestion pk:", gestion)
                    contacto = gestion.contacto
                    if contacto:
                        if not(contacto.fecha_aceptaciontyc):
                            if not (request.POST.get('tyc') == 'unknown'):
                                contacto.fecha_aceptaciontyc = dt.datetime.now()
                                contacto.gestion_aceptatyc = gestion
                                contacto.save()

                    gestion.estado = "finalizado"
                    gestion.fecha_finalizacion = dt.datetime.now()
                    gestion.save()
                    call_data = ast.literal_eval(gestion.metadata)

                    if (gestion.medio == 'whatsapp'):   # SI FUE UN WHASTAPP ACTUALIZO EL DIALOGO Y LO DEJO "gestionado"
                        print("si es una de whatsapp para tomar sus msj finales")
                        print("===============================")
                        print("CALL DATA: ", call_data)
                        print("===============================")
                        dialogo = Dialogo.objects.get(pk=call_data['chat_id'])
                        if ENCUESTA_ACTIVA :
                            dialogo.estado = 'encuesta'
                            dialogo.fecha_modificacion = dt.datetime.now(pytz.utc)
                            dialogo.user = None
                            dialogo.ultimo_mensaje_gestion = (Mensaje.objects.obtener_ultimo_mensaje(dialogo=dialogo)['ultimo_mensaje_numero'])
                            dialogo.esperar_respuesta = True
                            dialogo.bot = True
                            dialogo.modulo = 11
                            dialogo.save()
                            encuesta = Encuesta_bot(
                            gestion = gestion,
                            )
                            encuesta.save()
                            call_data['encuesta'] = encuesta.pk
                            gestion.metadata = str(call_data)
                            gestion.save()
                        else:
                            dialogo.estado = 'gestionado'
                            dialogo.user = None
                            dialogo.ultimo_mensaje_gestion = (Mensaje.objects.obtener_ultimo_mensaje(dialogo=dialogo)['ultimo_mensaje_numero'])
                            dialogo.gestion = None
                            dialogo.campana = None
                            dialogo.modulo = 0
                            dialogo.bot = False
                            dialogo.esperar_respuesta = False
                            dialogo.save()


                        call_data['mensaje_final'] = dialogo.ultimo_mensaje_gestion

                        gestion.metadata = str(call_data)
                        gestion.save()
                        mensaje_inicial = int(call_data['mensaje_inicial']+1)
                        mensaje_final = int(call_data['mensaje_final'])
                        mensajes=Mensaje.objects.filter(dialogo=dialogo).order_by('mensaje_numero')
                        gestion.fecha_entrada_cola = dialogo.fecha_entrada_cola
                        gestion.save()
                        #No funciona este pedazo bien
                        if(len(mensajes)!=0):
                            for mensaje in mensajes:
                                if (mensaje.mensaje_numero != None ):
                                    mensaje_int = int((mensaje.mensaje_numero))
                                    
                                    if mensaje_inicial <= mensaje_int  <= mensaje_final:
                                        print("i",mensaje_int, Mensaje.objects.get(mensaje_numero=mensaje_int).creado_por)

                                        if((Mensaje.objects.get(mensaje_numero=mensaje_int)).creado_por == 'usuario'):
                                            print("entro al usuario", Mensaje.objects.get(mensaje_numero=mensaje_int).fecha_recibido)
                                            gestion.fecha_primer_contacto = Mensaje.objects.get(mensaje_numero=mensaje_int).fecha_recibido
                                            gestion.save()
                                            print(gestion.fecha_primer_contacto)
                                            break
                            for mensaje in mensajes:
                                if (mensaje.mensaje_numero != None ):
                                    mensaje_int = int(mensaje.mensaje_numero)

                                    if mensaje_inicial <= mensaje_int  <= mensaje_final:
                                        print("i",mensaje_int, Mensaje.objects.get(mensaje_numero=mensaje_int).creado_por)

                                        if((Mensaje.objects.get(mensaje_numero=mensaje_int)).creado_por == 'bot'):
                                            print("entro al bot")
                                            gestion.fecha_primer_contacto_robot = Mensaje.objects.get(mensaje_numero=mensaje_int).fecha_recibido
                                            print("entro al bot", Mensaje.objects.get(mensaje_numero=mensaje_int).fecha_recibido)
                                            gestion.save()
                                            print(gestion.fecha_primer_contacto_robot)
                                            break
                            for mensaje in mensajes:
                                if (mensaje.mensaje_numero != None ):
                                    mensaje_int = int(mensaje.mensaje_numero)
                                    if mensaje_inicial <= mensaje_int  <= mensaje_final:
                                        print("i",mensaje_int, Mensaje.objects.get(mensaje_numero=mensaje_int).creado_por)
                                        if((Mensaje.objects.get(mensaje_numero=mensaje_int)).creado_por == 'agente'):
                                            print("entro al agente", Mensaje.objects.get(mensaje_numero=mensaje_int).fecha_recibido)
                                            gestion.fecha_primer_contacto_agente = Mensaje.objects.get(mensaje_numero=mensaje_int).fecha_recibido
                                            gestion.save()
                                            print(gestion.fecha_primer_contacto_agente)
                                            break

                    if (call_data['modelo_gestionar']['origen'] != 'None'):
                        modelo_gestionar = call_data['modelo_gestionar']['origen']
                        if modelo_gestionar == 'programacion':
                            pk_programacion = call_data['modelo_gestionar']['pk']
                            if (pk_programacion != '' ):
                                programacion = Programacion.objects.get(pk=pk_programacion)
                                if (gestion.tipo_consulta != 'no_confirmada'):
                                    programacion.estado_programacion = 'gestionado'
                                    programacion.gestion = gestion
                                    programacion.save()

                                    if (LlamadaPerdida.objects.filter(contacto=gestion.contacto).exclude(estado='gestionado').exists()):
                                            llamadas_perdidas = LlamadaPerdida.objects.filter(contacto_datos=gestion.contacto)
                                            for llamada in llamadas_perdidas:
                                                llamada.gestion = gestion
                                                llamada.creado_por = request.user
                                                llamada.estado = 'gestionado'
                                                llamada.save()
                    
                        elif modelo_gestionar == 'perdida':
                                pk_perdida = call_data['modelo_gestionar']['pk']
                                if LlamadaPerdida.objects.filter(pk = pk_perdida).exclude(estado = 'gestionado').exists():
                                    perdida = LlamadaPerdida.objects.get(pk = pk_perdida)
                                    if (gestion.tipo_consulta != 'no_confirmada'):
                                        perdida.estado = 'gestionado'
                                        perdida.gestion = gestion
                                        perdida.agente = self.request.user
                                        perdida.save()
                                    
                                    else:
                                        perdida.gestion = gestion
                                        perdida.agente = self.request.user
                                        perdida.save()
                        
                    else:
                        # print(request.POST)
                        if (gestion.calificacion and gestion.calificacion.accion_programacion == 'reprogramar'):
                            fecha_reprogramar_tmp = request.POST.get('fecha_reprogramar')
                            if not (fecha_reprogramar_tmp == '' or fecha_reprogramar_tmp == None):
                                fecha_reprogramar = request.POST['fecha_reprogramar']
                                print('mi fecha es que tipo ,' , type(fecha_reprogramar))
                                fecha_str = str(fecha_reprogramar)
                                fecha_dt = datetime.strptime(fecha_reprogramar, '%Y-%m-%dT%H:%M')
                                print(fecha_dt)
                                programacion_nueva = Programacion(
                                    contacto = gestion.contacto,
                                    agente = gestion.creado_por,
                                    estado_programacion = 'nuevo',

                                    observaciones = gestion.observaciones,
                                    fecha_gestionar = fecha_dt,
                                )
                                programacion_nueva.save()

                                if (gestion.medio == 'whatsapp'):
                                    programacion_nueva.char_5=dialogo.campana
                                    programacion_nueva.save()

                                else:
                                    id_campana=call_data['id_campana']
                                    campana=Campana.objects.get(pk=id_campana)
                                    programacion_nueva.char_5=campana.nombre
                                    programacion_nueva.save()



            else:
                print('FORMS ERRORS: {}'.format(form.errors))
                gestion_str = 'gestion'

            numero = id_gestion
            # print("···estado de la gestion·······", gestion.estado)
            return JsonResponse({'div_gestion': div_gestion , 'valido': True, 'numero':numero, 'estado': gestion.estado, 'pk_gestion': gestion.pk })

class ContactoAddAjaxTelefono(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            contacto = Contacto.objects.get(pk=request.GET.get('contacto_pk'))
            contactodatos = ContactoDatosContacto(contacto=contacto,
                    label = request.GET.get('label'),
                    value_1 = request.GET.get('numero'),
                    tipo = 'telefono',
            )

            contactodatos.save() # GRABO EL DATO DEL CONTACTO

            if request.GET.get('label') == 'whatsapp': # si es un numero de whatsapp se guarda su dialogo
                num_wp = "57" + request.GET.get('numero')
                chat_id = '{}@c.us'.format(num_wp)
                if not (Dialogo.objects.filter(pk=chat_id).exists()):
                    dialogo = Dialogo(
                        chat_id = chat_id,
                        numero = chat_id.split('-')[0].split('@')[0],
                        es_grupo = True if len(chat_id.split('-')) > 1 else False,
                    )
                    dialogo.save()
                    dialogo.contacto = contacto
                    dialogo.save()
                    bot = Bot() #Complemento para el metadata del dialogo
                    dialogo.metadata = bot.ini_metadata(dialogo)
                    dialogo.save()


            return JsonResponse({'guardado':True, 'contactodatos_pk': '1'})   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO
        else:
            return JsonResponse({'guardado':False, 'contactodatos_pk': None })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

class ContactoAddAjaxCorreo(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            contacto = Contacto.objects.get(pk=request.GET.get('contacto_pk'))
            contactodatos = ContactoDatosContacto(contacto=contacto,
                    label = request.GET.get('label'),
                    value_1 = request.GET.get('correo'),
                    tipo = 'email',
            )
            contactodatos.save() # Grabo la presentacion del producto

            return JsonResponse({'guardado':True, 'contactodatos_pk': contactodatos.pk })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO
        else:
            return JsonResponse({'guardado':False, 'contactodatos_pk': None })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

class ContactoAddAjaxDireccion(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            contacto = Contacto.objects.get(pk=request.GET.get('contacto_pk'))
            contactodatos = ContactoDatosContacto(contacto=contacto,
                    label = request.GET.get('label'),
                    value_1 = request.GET.get('direccion'),
                    value_2 = request.GET.get('observaciones'),
                    tipo = 'direccion',
            )
            contactodatos.save() # Grabo la presentacion del producto

            return JsonResponse({'guardado':True, 'contactodatos_pk': contactodatos.pk })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO
        else:
            return JsonResponse({'guardado':False, 'contactodatos_pk': None })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

def BuscarDocumentoAjax(request):
    if request.is_ajax():
        tipo = request.GET.get('tipo')
        if (tipo == 'consulta_cc'):
            documento = request.GET.get('doc') # validar si existe algo con ese datos
            if Contacto.objects.filter(documento=documento).exists():             #   Si existe  Devuelvo los datos para renderizar el campo
                print("SI EXIXTE UN MAN.....")
                contacto = Contacto.objects.filter(documento=documento)[0]
                print("nombre:..........", contacto.nombres)
                #lista_gestiones_json = []
                #gestiones = GestionContacto.objects.filter(contacto=contacto).exclude(estado="pendiente").order_by('-id')[:5] #serialzo las gestiones del paciente para pintarlas
                # for dato in gestiones:
                #     print("metiendo gestiones:..........", dato)
                #     objeto_dato={}
                #     dato.fecha_creacion = dato.fecha_creacion
                #     objeto_dato["pk"]= dato.id
                #     objeto_dato["creado_por"]= dato.creado_por.username
                #     objeto_dato["fecha_creacion"]= dato.fecha_creacion.strftime('%d/%m/%Y %H:%M')
                #     objeto_dato["eps"]= dato.entidad.nombre if not dato.entidad == None else ''
                #     objeto_dato["medio"]= dato.medio
                #     objeto_dato["contacto"]= dato.contacto.nombres
                #     objeto_dato["motivo"]= dato.motivo.nombre if not dato.motivo == None else ''
                #     objeto_dato["resultado"]= dato.resultado.nombre if not dato.resultado == None else ''
                #     objeto_dato["descripcion"]= dato.descripcion
                #     objeto_dato["estado"]= dato.estado
                #     lista_gestiones_json.append(objeto_dato)
                # meter el 'gestiones': lista_gestiones_json, en el json response cuando lo habilite
                return JsonResponse({'id_contacto': contacto.id,'tipo_identificacion': contacto.tipo_identificacion.pk,
                    'nombres': contacto.nombres, 'apellidos': contacto.apellidos, 'documento': contacto.documento,
                     })
            else:
                print("PAILA NO EXISTE DEVUELVO BLANCO.....")
                return JsonResponse({'id_cliente': '','tipo_identificacion': '',
                    'nombres': '', 'apellidos': '', 'documento':'',
                     })

class ContactoValidarTelefonoAjax(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            numero_telefono = self.request.GET.get('numero_telefono')
            print('>>>> ContactoValidarGestionTelefonoAjax >>> numero: ', numero_telefono)

            if Dialogo.objects.filter(chat_id = numero_telefono ).exists():
                print(" >>>> ContactoValidarGestionTelefonoAjax >>> DIALOGO EXISTE ")
                dialogo = Dialogo.objects.get(chat_id = numero_telefono)
                if dialogo.estado == 'en_gestion': #SI TIENE CONTACTO ASOCIADO es que ya alguien lo creo
                    contacto = dialogo.contacto
                    agente = dialogo.user
                    respuesta = {
                        'estado': 'en_gestion',
                        'contacto_full_name': contacto.get_full_name() if contacto != None else "",
                        'agente_full_name': agente.get_full_name() if agente != None else "No asignado",
                    }

                elif dialogo.estado == 'bot': #SI esta en le robot
                    print(">>>> ContactoValidarGestionTelefonoAjax >>> retorno informacion de robot")
                    contacto = dialogo.contacto
                    agente = dialogo.user
                    respuesta = {
                        'estado': 'bot',
                        'contacto_full_name': contacto.get_full_name() if contacto != None else "",
                        'agente_full_name': agente.get_full_name() if agente != None else "Agente Virtual",
                    }

                elif dialogo.estado == 'encuesta': #SI esta en encuesta
                    print(" >>>> ContactoValidarGestionTelefonoAjax >>> retorno informacion de encuesta")
                    contacto = dialogo.contacto
                    agente = dialogo.user
                    respuesta = {
                        'estado': 'encuesta',
                        'contacto_full_name': contacto.get_full_name() if contacto != None else "",
                        'agente_full_name': agente.get_full_name() if agente != None else "Agente Virtual",
                    }

                elif dialogo.estado == 'nuevo': #SI esta en Nuevo
                    print(" >>>> ContactoValidarGestionTelefonoAjax >>> retorno informacion de cola")
                    contacto = dialogo.contacto
                    agente = dialogo.user
                    respuesta = {
                        'estado': 'nuevo',
                        'contacto_full_name': contacto.get_full_name() if contacto != None else "",
                        'agente_full_name': agente.get_full_name() if agente != None else "Mensaje en Cola",
                    }

                else:
                    #existe el dialogo pero no tiene contacto
                    print(">>>> ContactoValidarGestionTelefonoAjax >>> existe el dialogo y esta gestionado no tiene nada pendiente")
                    respuesta = {'estado': 'gestionado' }
            else:
                #No existe el dialogo
                print(">>>> ContactoValidarGestionTelefonoAjax >>> No existe el dialogo")
                respuesta = {'estado': 'sin_gestion' }
            return JsonResponse({'status': 'ok', 'respuesta': respuesta})

        else:
            return JsonResponse({'guardado':False, 'contactodatos_pk': None })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO


class AgenteContactoCampanas(View):
    def get(self, request, *args, **kwargs):
        id_campana=self.request.GET.get('id_campana')
        id_contacto=self.request.GET.get('id_contacto')
        campana = Campana.objects.get(pk=id_campana)
        contacto = Contacto.objects.get(pk=id_contacto)
        dialogo = Dialogo.objects.get(contacto=contacto)
        dialogo.campana = campana.nombre
        dialogo.save()


        return JsonResponse({})

class ContactoCalificacionCampanas(View):
    def get(self, request, *args, **kwargs):
        id_gestion=self.request.GET.get('id_gestion')
        gestion = GestionContacto.objects.get(pk=id_gestion)
        metadata= ast.literal_eval(gestion.metadata)
        id_campana = metadata['id_campana']

        campana = Campana.objects.get(pk=id_campana)
        calificaciones = CalificacionGestion.objects.filter(campana=campana)
        lista_calificaciones=[]
        for calificacion in calificaciones:

            lista_calificaciones.append({
                'id_calificacion':calificacion.pk,
                'nombre_calificacion' : calificacion.nombre
            })

        return JsonResponse({'calificaciones' : lista_calificaciones})

class ValidarWhastappAjax(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            numero_validar = self.request.GET.get('numero_validar')
            # print('>>>> ValidarWhastappAjax >> Numero: ', numero_validar)
            chat_api = ChatApi()
            respuesta = chat_api.validar_telefono_existe(phone = numero_validar)
            print('>>>> ValidarWhastappAjax >> Respuesta: ', respuesta)
        return JsonResponse({'respuesta' : respuesta})


class EliminarTelefonoAjaxView(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():

            contacto = Contacto.objects.get(pk=request.GET.get('id_contacto'))
            print ("contacto ", contacto )
            telefonos = ContactoDatosContacto.objects.filter(value_1 =request.GET.get('numero_telefono'), contacto=contacto)
            print ("############## ", telefonos)
            for telefono in telefonos:
                telefono.activo=False
                telefono.save()
        return JsonResponse({'status': 'ok'})
class EditarTelefonoAjaxView(View):

     def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print("WWWWWWWWWWWWWWWWWWWWWWww",request.GET.get)
            contacto = Contacto.objects.get(pk=request.GET.get('contacto_pk'))
            numero_telefonos = ContactoDatosContacto.objects.filter(value_1= request.GET.get('numero_telefono'), contacto=contacto )
            for numero_telefono in numero_telefonos:
                numero_telefono.label = request.GET.get('label')
                numero_telefono.value_1 = request.GET.get('numero')
                numero_telefono.tipo = 'telefono'

                numero_telefono.save() # GRABO EL DATO DEL CONTACTO

            if request.GET.get('label') == 'whatsapp': # si es un numero de whatsapp se guarda su dialogo
                num_wp = "57" + request.GET.get('numero')
                chat_id = '{}@c.us'.format(num_wp)
                if not (Dialogo.objects.filter(pk=chat_id).exists()):
                    dialogo = Dialogo(
                        chat_id = chat_id,
                        numero = chat_id.split('-')[0].split('@')[0],
                        es_grupo = True if len(chat_id.split('-')) > 1 else False,
                    )
                    dialogo.save()
                    dialogo.contacto = contacto
                    dialogo.save()

            return JsonResponse({'guardado':True, 'contactodatos_pk': '1'})   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO
        else:
            return JsonResponse({'guardado':False, 'contactodatos_pk': None })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO
