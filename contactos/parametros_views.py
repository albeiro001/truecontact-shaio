from django.db.models import Q
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib import messages
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import View, ListView, DeleteView, CreateView, UpdateView, TemplateView

from .models import *

from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission, Group


# def supervisor_or_agente(user):
#     return user.groups.filter(name='Supervisor').exists() or user.groups.filter(name='agentes').exists()

# @user_passes_test(supervisor_or_agente, raise_exception=True)
# para que funcione debe instalar pip install django-guardian
# y colocarlo en las apps 

class AgentesPermisosView(TemplateView):
    template_name = 'parametros/permisos_agentes.html'

    def get_context_data(self, **kwargs):
        context = super(AgentesPermisosView, self).get_context_data(**kwargs)
        # Necesita Lista de permisos
        content_type = ContentType.objects.get_for_model(GestionContacto, for_concrete_model=False)
        permisos = Permission.objects.filter(content_type=content_type)
        permisos_list = []
        grupo_agentes = Group.objects.get(name= 'agente')
        agentes_permisos = grupo_agentes.permissions.all()
        for permiso in permisos:
            permisos_list.append({
                    'nombre':permiso.name,
                    'pk':permiso.pk,
                    'permitido': agentes_permisos.filter(pk=permiso.pk).exists(),

                }) #CONSTRUYO EL LIST PERSONALIZADO
        context['permisos'] = permisos_list
        return context


class FormularioConfiguracion( View ):
    template_name = 'parametros/formulario_configuracion.html'
    def get(self, request,*args, **kwargs):
        context = {}
        context['resultados'] = ResultadoGestion.objects.all().count()
        context['calificaciones'] = CalificacionGestion.objects.all().count()
        return render(self.request, self.template_name, context)

@method_decorator(login_required, name='dispatch')
class ResultadoGestionView(ListView):
    template_name = 'parametros/ResultadoGestion_list.html'
    model = ResultadoGestion
    paginate_by = 20

    def get_queryset(self):
        print("ENTRANDO ACÁ.........XXXX")
        queryset = ResultadoGestion.objects.all()
        buscar = self.request.GET.get('buscar')
        if not (buscar == None or buscar == ''):
            queryset = queryset.filter(
                Q(nombre__icontains=buscar)
            ).order_by('-id')

        return queryset

    def get_context_data( self, **kwargs ):
        context = super().get_context_data( **kwargs )
        buscar = self.request.GET.get('buscar')
        if not (buscar == None or buscar == ''):
            context['buscar'] = {'text': buscar}
        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context

@method_decorator(login_required, name='dispatch')
#@method_decorator(permission_required('cotizaciones.view_cotizacion', raise_exception=True), name='dispatch')
class ResultadoGestionCreateView(CreateView):
    template_name = 'parametros/ResultadoGestion_create_update.html'
    model = ResultadoGestion
    fields = '__all__'
    success_url = reverse_lazy('resultado-list')
    def get_context_data(self, **kwargs):
        kwargs['vista'] = 'Crear Resultado Gestion'
        context = super(ResultadoGestionCreateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'ResultadoGestion creada con exito !')
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
#@method_decorator(permission_required('cotizaciones.view_cotizacion', raise_exception=True), name='dispatch')
class ResultadoGestionUpdateView(UpdateView):
    template_name = 'parametros/ResultadoGestion_create_update.html'
    model = ResultadoGestion
    fields = '__all__'
    success_url = reverse_lazy('resultado-list')
    def get_context_data(self, **kwargs):
        kwargs['vista'] = 'Actualizar Resultado Gestion'
        context = super(ResultadoGestionUpdateView, self).get_context_data(**kwargs)
        return context
    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'ResultadoGestion actualizada exitosamente !')
        return super().form_valid(form)

# def ResultadosGestionAjax(request):
#     if request.is_ajax():
#         id_tipo = request.GET['tipo']
#         tipo = TipoGestion.objects.get(pk=id_tipo)
#         resultados = ResultadoGestion.objects.filter(tipogestion=tipo).order_by('nombre')
#         lista_respuesta = []
#         for resultado in resultados:
#             nombre_visualizar = '{0}'.format(resultado.nombre)
#             lista_respuesta.append({'id':resultado.pk, 'nombre': nombre_visualizar})
#         respuesta = json.dumps({'resultados': lista_respuesta})
#         mimetype = 'aplication/json'
#         return HttpResponse(respuesta, mimetype)
#     else:
#         return HttpResponse("Error 404")

def ResultadosGestionAjax(request):
    if request.is_ajax():
        id_motivo = request.GET['motivo']
        motivo = Motivo.objects.get(pk=id_motivo)
        resultados = Resultado.objects.filter(motivo=motivo).order_by('nombre')
        lista_respuesta = []
        for resultado in resultados:
            nombre_visualizar = '{0}'.format(resultado.nombre)
            lista_respuesta.append({'id':resultado.pk, 'nombre': nombre_visualizar})
        respuesta = json.dumps({'resultados': lista_respuesta})
        mimetype = 'aplication/json'
        return HttpResponse(respuesta, mimetype)
    else:
        return HttpResponse("Error 404")


#===============================                Calificacion views          ================================#


@method_decorator(login_required, name='dispatch')
class CalificacionGestionView(ListView):
    template_name = 'parametros/CalificacionGestion_list.html'
    model = CalificacionGestion
    paginate_by = 20

    def get_queryset(self):
        print("ENTRANDO ACÁ.........XXXX")
        queryset = CalificacionGestion.objects.all()
        buscar = self.request.GET.get('buscar')
        if not (buscar == None or buscar == ''):
            queryset = queryset.filter(
                Q(nombre__icontains=buscar)
            ).order_by('-id')

        return queryset

    def get_context_data( self, **kwargs ):
        context = super().get_context_data( **kwargs )
        buscar = self.request.GET.get('buscar')
        if not (buscar == None or buscar == ''):
            context['buscar'] = {'text': buscar}
        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context

@method_decorator(login_required, name='dispatch')
class CalificacionGestionCreateView(CreateView):
    template_name = 'parametros/CalificacionGestion_create_update.html'
    model = CalificacionGestion
    fields = '__all__'
    success_url = reverse_lazy('calificacion-list')
    def get_context_data(self, **kwargs):
        kwargs['vista'] = 'Crear Resultado Gestion'
        context = super(CalificacionGestionCreateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'Calificacion  creada con exito !')
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class CalificacionGestionUpdateView(UpdateView):
    template_name = 'parametros/CalificacionGestion_create_update.html'
    model = CalificacionGestion
    fields = '__all__'
    success_url = reverse_lazy('calificacion-list')
    def get_context_data(self, **kwargs):
        kwargs['vista'] = 'Actualizar Resultado Gestion'
        context = super(CalificacionGestionUpdateView, self).get_context_data(**kwargs)
        return context
    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'Calificacion actualizada exitosamente !')
        return super().form_valid(form)

#===============================                TipoGrstion views          ================================#

@method_decorator(login_required, name='dispatch')
class TipoGestionView(ListView):
    template_name = 'parametros/TipoGestion_list.html'
    model = TipoGestion
    paginate_by = 20

    def get_queryset(self):
        print("ENTRANDO ACÁ.........XXXX")
        queryset = TipoGestion.objects.all()
        buscar = self.request.GET.get('buscar')
        if not (buscar == None or buscar == ''):
            queryset = queryset.filter(
                Q(nombre__icontains=buscar)
            ).order_by('-id')

        return queryset

    def get_context_data( self, **kwargs ):
        context = super().get_context_data( **kwargs )
        buscar = self.request.GET.get('buscar')
        if not (buscar == None or buscar == ''):
            context['buscar'] = {'text': buscar}
        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages

        return context

@method_decorator(login_required, name='dispatch')
class TipoGestionCreateView(CreateView):
    template_name = 'parametros/TipoGestion_create_update.html'
    model = TipoGestion
    fields = '__all__'
    success_url = reverse_lazy('tipo-list')
    def get_context_data(self, **kwargs):
        kwargs['vista'] = 'Crear Tipo Gestion'
        context = super(TipoGestionCreateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'Calificacion  creada con exito !')
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class TipoGestionUpdateView(UpdateView):
    template_name = 'parametros/TipoGestion_create_update.html'
    model = TipoGestion
    fields = '__all__'
    success_url = reverse_lazy('tipo-list')
    def get_context_data(self, **kwargs):
        kwargs['vista'] = 'Actualizar Tipo Gestion'
        context = super(TipoGestionUpdateView, self).get_context_data(**kwargs)
        return context
    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, 'Calificacion actualizada exitosamente !')
        return super().form_valid(form)

def CalificacionGestionAjax(request):
    if request.is_ajax():
        id_calificacion = request.GET['calificacion']
        print(request.GET)
        print ('ID CALIFICACION :', id_calificacion)
        calificacion = CalificacionGestion.objects.get(pk=id_calificacion)
        respuesta = {'accion_programacion': calificacion.accion_programacion,'calificacion_nombre' :calificacion.nombre}
        return JsonResponse(respuesta)
    else:
        return HttpResponse("ERROR 404")
