from django.db.models import Q
from django.conf import settings
from django.contrib.auth.decorators import login_required, permission_required
from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic  import (
    View, ListView, CreateView, UpdateView, DeleteView, FormView, TemplateView, DetailView
)
from ominicontacto_app.models import Campana

from .forms import AgregarContactoForm, GestionContactoForm, TelefonoForm, EmailForm, DireccionForm
from .models import *
from django.urls import reverse_lazy

from truecontact.chatapi.models import Dialogo, Mensaje

import ast
import json
import pytz
import requests
import datetime as dt
from pytz import timezone

from datetime import datetime
from .forms import CalificacionForm


LOCAL_TZ = pytz.timezone(settings.TIME_ZONE)

local_tz = timezone("America/Bogota")

#===============================================================================
class CalificacionListView(ListView):
    model = CalificacionGestion
    paginate_by = 50
    template_name = 'calificacion/calificacion_list.html'


    def _obtener_campanas(self):
        agente = self.request.user.get_agente_profile()
        campanas_queues = agente.get_campanas_activas_miembro()
        ids_campanas = []
        for id_nombre in campanas_queues.values_list('id_campana', flat=True):
            split_id_nombre = id_nombre.split('_')
            id_campana = split_id_nombre[0]
            campana = Campana.objects.get(pk=id_campana)
            type_campana = campana.type
            nombre_campana = '_'.join(split_id_nombre[1:])
            ids_campanas.append((id_campana, nombre_campana, type_campana))
        return ids_campanas

    def get_queryset(self):
        grupo_supervisor = Group.objects.get(name='Supervisor')
        grupo_soat = Group.objects.get(name='SOAT')
        grupo_encuestas = Group.objects.get(name='ENCUESTAS')
        user = self.request.user
        agente_soporte = User.objects.get(username='soporte_agente')
        queryset = CalificacionGestion.objects.exclude(pk=3)
        print(user.groups.all())
        if (grupo_supervisor in user.groups.all()):
            print('entro a todas')
            queryset = queryset

        if (grupo_encuestas in user.groups.all()):
            print('entro a encuestas')
            queryset = queryset.filter(campana__nombre='ENCUESTAS')
        
        if (grupo_soat in user.groups.all()):
            print('entro a soat')
            queryset = queryset.filter(campana__nombre='SOAT')


        print(queryset)
        buscar = self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            queryset = queryset.filter(
                    Q(nombre__icontains=buscar) 
            )

        return queryset.order_by('-pk')
    
    def get_context_data(self, **kwargs):
        context = super(CalificacionListView, self).get_context_data(**kwargs)
        grupo_supervisor = Group.objects.get(name='Supervisor')
        grupo_soat = Group.objects.get(name='SOAT')
        grupo_encuestas = Group.objects.get(name='ENCUESTAS')
        user = self.request.user

        hoy = now().astimezone(local_tz)

        context['contacto_campanas'] = self._obtener_campanas()
        
        buscar =  self.request.GET.get('buscar')
        if not (buscar == '' or buscar == None):
            context['buscar'] = buscar

        is_filter = False

        paginator = context['paginator']
        num_pages = paginator.num_pages
        current_page = context['page_obj']
        page_no = current_page.number
        if num_pages <= 8 or page_no <= 5:  # case 1 and 2
            pages = [x for x in range(1, min(num_pages + 1, 9))]

        elif page_no > num_pages - 4:  # case 4
            pages = [x for x in range(num_pages - 5, num_pages + 1)]
        else:  # case 3
            pages = [x for x in range(page_no - 3, page_no + 3)]
        context['pages'] = pages
        print(context)
        return context

class CalificacionUpdateView(View):
    form_class = CalificacionForm
    template_name = 'calificacion/calificacion_update.html'
    success_url = reverse_lazy('tc-contacto-calificacion-list')

    def get(self, request, *args, **kwargs):
        context = {}
        calificacion = get_object_or_404(CalificacionGestion, pk=kwargs['pk'])
        
        grupo_supervisor = Group.objects.get(name='Supervisor')
        grupo_encuestas = Group.objects.get(name='ENCUESTAS')
        grupo_soat = Group.objects.get(name='SOAT')

        usuario_agente = self.request.user
        agente_profile = usuario_agente.get_agente_profile()
        

        if grupo_soat in usuario_agente.groups.all():
            campanas = Campana.objects.filter(nombre=grupo_soat.name)
        
        else:
            campanas = Campana.objects.filter(nombre=grupo_encuestas.name)
        
        print('campana' , campanas)

        form_calificacion = self.form_class(
            instance = calificacion,
            campana_queryset = campanas
        )

        context['form'] = form_calificacion

        return render(request, self.template_name, context)


    def post(self, request, *args, **kwargs):
        context = {}
        calificacion = get_object_or_404(CalificacionGestion, pk=kwargs['pk'])

        form = self.form_class(request.POST)
        print(self.request.POST)
        if form.is_valid():
            calificacion_update = CalificacionGestion.objects.get(pk=kwargs['pk'])
            calificacion_update.campana = Campana.objects.get(pk=self.request.POST.get('campana'))
            calificacion_update.nombre = self.request.POST.get('nombre')
            calificacion_update.accion_programacion = self.request.POST.get('accion_programacion')
            calificacion_update.save()
            
            print('valido el formulario ', calificacion_update.nombre)
        else:
            context['form'] = form
            print(' no valido el formulario')
            return render(request, self.template_name, context)
        return HttpResponseRedirect(self.success_url)

class CalificacionAddView(View):
    form_class = CalificacionForm
    template_name = 'calificacion/calificacion_add.html'
    success_url = reverse_lazy('tc-contacto-calificacion-list')

    def get(self, request, *args, **kwargs):
        context = {}

        grupo_supervisor = Group.objects.get(name='Supervisor')
        grupo_encuestas = Group.objects.get(name='ENCUESTAS')
        grupo_soat = Group.objects.get(name='SOAT')

        usuario_agente = self.request.user
        agente_profile = usuario_agente.get_agente_profile()
        

        if grupo_soat in usuario_agente.groups.all():
            campanas = Campana.objects.filter(nombre=grupo_soat.name)
        
        else:
            campanas = Campana.objects.filter(nombre=grupo_encuestas.name)
        
        print('campana' , campanas)

        form_calificacion = self.form_class(campana_queryset = campanas)

        context['form'] = form_calificacion
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        user = self.request.user
        context = {}        
        form = self.form_class(request.POST)
        print(self.request.POST)
        print(form)

        if form.is_valid():
            calificacion = CalificacionGestion(
                campana = Campana.objects.get(pk=self.request.POST.get('campana')),
                nombre = self.request.POST.get('nombre'),
                accion_programacion = self.request.POST.get('accion_programacion')
            )
            calificacion.save()
            print('calificacion creada ', calificacion.nombre)
        else:
            print("Error de formulario", form.errors)
            context['form'] = form
            return render(request, self.template_name, context)

        return HttpResponseRedirect(self.success_url)



