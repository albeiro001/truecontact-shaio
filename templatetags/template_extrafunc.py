from django import template

from ominicontacto_app.models import Campana

register = template.Library()

@register.filter(name='has_group')
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists()

@register.filter(name='has_campana')
def has_campana(user, campana_name):
    agente = user.agenteprofile
    id_campana = int(agente.get_campanas_activas_miembro().values()[0]['id_campana'].split('_')[0])
    campana = Campana.objects.get(pk=id_campana)
    if campana.nombre == campana_name:
        return True
    else:
        return False