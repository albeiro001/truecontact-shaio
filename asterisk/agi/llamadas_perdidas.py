#!/opt/omnileads/virtualenv/bin/python

import django
import sys

from asterisk import agi

agi = agi.AGI()

agi.verbose("Iniciando aplicacion METODO DE CONTACTO ")

sys.path.append('/opt/omnileads/ominicontacto')
django.setup()

from truecontact.contactos.models import Contacto, ContactoDatosContacto
from truecontact.llamadas_perdidas.models import LlamadaPerdida
from truecontact.chatapi.models import Dialogo

from datetime import datetime

accion = agi.get_variable("accion")

if (accion == 'contactar_cliente'):
    medio_contacto = agi.get_variable("medio")

    if str(agi.get_variable("numero")) == '1':
        caller_id = agi.get_variable("CALLERID(num)")
    else:
        caller_id = agi.get_variable("numero")

    agi.verbose("Contactar cliente: {} atraves de: {}".format(caller_id, medio_contacto))

    telefono = str(caller_id)
    encontro_contacto = False
    agi.verbose("Analizando telefono: {}".format(telefono))

    try:
        int(telefono)
        procesar = True
    except:
        procesar = False

    if (procesar and len(telefono) >= 4):

        if not (telefono == '' or telefono == None):
            telefono = str(telefono)
            if not (ContactoDatosContacto.objects.filter(tipo='telefono', value_1=telefono).exists()):
                encontro_contacto = False
                agi.verbose("No se encuentran Contacto con telefono: {}".format(telefono))
                contacto = Contacto(
                    nombres = 'Cliente',
                    apellidos = '{}'.format(caller_id)
                )
                contacto.save()

                telefono = ContactoDatosContacto(
                    contacto = contacto,
                    label = 'otro',
                    tipo = 'telefono',
                    value_1 = telefono
                )
                telefono.save()

                if (telefono.value_1[0] == '3' and len(telefono.value_1) == 10):
                    num_wp = "57" + telefono.value_1
                    chat_id = '{}@c.us'.format(num_wp)
                    if not(Dialogo.objects.filter(chat_id=chat_id).exists()):
                        dialogo = Dialogo(
                            chat_id = chat_id,
                            numero = chat_id.split('@')[0],
                            es_grupo = False,
                            contacto = contacto,
                            bot = False,
                            modulo = 88888,
                            estado = 'gestionado',
                            campana = "Call"
                        )
                        dialogo.save()

            else:
                encontro_contacto = True
                telefono = ContactoDatosContacto.objects.filter(tipo='telefono', value_1=telefono)[0]
                contacto = telefono.contacto
                agi.verbose("Se encuentra Contacto: {} {} con el telefono: {}".format(contacto.nombres, contacto.apellidos, telefono.value_1))

        telefono = str(caller_id)

        if(medio_contacto == 'Telefonia'):
            descripcion_contacto = "Quiero ser contactado por {} al {}".format(medio_contacto, telefono)

        elif(medio_contacto == 'WhatsApp'):
            descripcion_contacto = "Quiero ser contactado por {} al {}".format(medio_contacto, telefono)

        else:
            descripcion_contacto = "No se reconoce el medio de contacto"

        agi.verbose("Medio de contacto: {}".format(descripcion_contacto))

        agi.verbose("Creando Llamada perdida")

        if LlamadaPerdida.objects.filter(numero=telefono).exclude(estado="gestionado").exists():
            llamada_perdida = LlamadaPerdida.objects.filter(numero=telefono).exclude(estado="gestionado")[0]
            llamada_perdida.llamadas += 1
            llamada_perdida.fecha_ultima_llamada = datetime.now()
            llamada_perdida.save()
        else:
            llamada_perdida = LlamadaPerdida(
                contacto = contacto,
                medio = medio_contacto,
                numero = telefono,
                llamadas = 1,
                fecha_ultima_llamada = datetime.now()
            )
            llamada_perdida.save()
        
        agi.verbose("Creada la llamada perdida: {}".format(llamada_perdida.pk))

agi.verbose("Finalizando aplicacion METODO DE CONTACTO")