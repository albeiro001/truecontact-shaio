from django.contrib import admin
from django.urls import include, path

from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# from django.conf import settings

from . import views

urlpatterns = [
    path('catalogo/',                views.MainView.as_view(),                      name='catalogo-main'),
    path('catalogo/categoria/create/',       views.CategoriaCreate.as_view(),       name='categoria-create'),
    path('catalogo/categoria/list/',         views.CategoriaList.as_view(),         name='categoria-list'),
    path('catalogo/categoria/update/<int:pk>',       views.CategoriaUpdate.as_view(),       name='categoria-update'),
    path('catalogo/categoria/producto/list/',        views.CategoriaProductos.as_view(),   name='categoria-productos'),

    path('catalogo/tag/create/',       views.TagCreate.as_view(),       name='tag-create'),
    path('catalogo/tag/list/',         views.TagList.as_view(),         name='tag-list'),
    path('catalogo/tag/update/<int:pk>',       views.TagUpdate.as_view(),       name='tag-update'),
    path('catalogo/tag/producto/list/',        views.TagProductos.as_view(),   name='tag-productos'),

    path('catalogo/producto/create/',       views.ProductoCreate.as_view(),       name='producto-create'),
    path('catalogo/producto/list/',         views.ProductoList.as_view(),         name='producto-list'),
    path('catalogo/producto/update/<int:pk>',       views.ProductoUpdate.as_view(),       name='producto-update'),
    path('catalogo/producto/ajax/detail/',          views.ProductoDetail.as_view(),       name='producto-detail'),

    # validar junto con producto-list, debemos llegar al punto de unicamente generar una url y poder detectar si respondo un html o un json
    path('catalogo/producto/buscar/', views.ProductoBuscar.as_view(), name='producto-buscar'),

    path('catalogo/presentacion/ajax/add/',          views.AddPresentacion.as_view(),        name='presentacion-add-ajax'),
    path('catalogo/presentacion/ajax/accion/',       views.AccionPresentacion.as_view(),     name='presentacion-accion-ajax'),
    path('catalogo/presentacion/ajax/update/',       views.UpdatePresentacion.as_view(),     name='update-presentacion-ajax'),

    path('catalogo/pedido/ajax/crear/',              views.CrearPedido.as_view(),            name='pedido-crear'),
    path('catalogo/pedido/ajax/consultar/',          views.PedidoConsultarAjax.as_view(),        name='pedido-consultar-ajax'),

    # path('catalogo/pedido/create/',          views.PedidoCreate.as_view(),       name='pedido-create'),
    path('catalogo/pedido/list/',            views.PedidoList.as_view(),         name='pedido-list'),
    path('catalogo/pedido/detail/<int:pk>',  views.PedidoDetail.as_view(),       name='pedido-detail'),

]
