from django.contrib import admin
from .models import *

@admin.register(Categoria)
class Categoria(admin.ModelAdmin):
    list_display = ( 'pk', 'nombre', 'activo', 'f_registro')

@admin.register(Producto)
class Producto(admin.ModelAdmin):
    list_display = ( 'pk', 'nombre', 'activo', 'f_registro')

@admin.register(Presentacion)
class Presentacion(admin.ModelAdmin):
    list_display = ( 'pk', 'producto', 'nombre_presentacion', 'precio_presentacion', 'activo_presentacion', 'f_registro')

@admin.register(Pedido)
class Pedido(admin.ModelAdmin):
    list_display = ( 'pk', 'precio_total', 'f_registro')

@admin.register(Item)
class Item(admin.ModelAdmin):
    list_display = ( 'pk', 'producto', 'cantidad')

@admin.register(Tag)
class Tag(admin.ModelAdmin):
    list_display = ( 'pk', 'nombre')