from django.db.models import Q
from django.shortcuts import render
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core import serializers

from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views import View
from django.views.generic.edit import CreateView, UpdateView, FormView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

import json

from .models import *
from .forms import *

#=============================================================================
#=============================================================================
#---------      MAIN VIEWS            -------------------------------.
class MainView( View ):
    template_name = 'main.html'
    def get(self, request,*args, **kwargs):
        context = {}
        context['categorias'] = Categoria.objects.all().count()
        context['productos'] = Producto.objects.all().count()
        context['tags'] = Tag.objects.all().count()
        context['pedidos'] = Pedido.objects.all().count()
        return render(self.request, self.template_name, context)
#=============================================================================


#---------      CATEGORIAS VIEWS            -------------------------------.
class CategoriaCreate( CreateView ):
    model = Categoria
    fields = '__all__'
    template_name = 'categoria/create.html'
    success_url = reverse_lazy('categoria-list')

class CategoriaUpdate( UpdateView ):
    model = Categoria
    fields = '__all__'
    template_name = 'categoria/update.html'
    success_url = reverse_lazy('categoria-list')

class CategoriaList( ListView ):

    model = Categoria
    paginate_by = 20
    template_name = 'categoria/list.html'
    context_object_name = 'categorias'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

@method_decorator(login_required, name='dispatch')
class CategoriaProductos(View):

    def get(self, request, *args, **kwargs):
        # user = request.user
        id_categoria = request.GET.get('id_categoria')
        if (Categoria.objects.filter(pk=id_categoria).exists()):
            categoria = Categoria.objects.get(pk=id_categoria)
            producto_queryset = Producto.objects.filter(categoria=categoria)
        else:
            producto_queryset = Producto.objects.all()

        productos_list = []
        for producto in producto_queryset:
            productos_list.append(
                {
                    'pk': producto.pk,
                    'nombre': producto.nombre,
                    'valor': producto.precio,
                    'imagen': producto.obtener_imagen_url(),
                    'descripcion': producto.descripcion,
                    'serial': 'SR{}'.format(producto.pk),
                    'tags': producto.get_tags_2()
                }
            )
        print("CategoriaProducto View: ", productos_list)
        return JsonResponse(productos_list, safe=False)


#=============================================================================
#=============================================================================
#---------      PRODUCTO VIEWS            -------------------------------.
class ProductoBuscar(View):

    def get(self, request, *args, **kwargs):
        valor_buscar = request.GET.get('buscar')

        print(f'Valor a buscar {valor_buscar}')

        producto_queryset = Producto.objects.filter(
            Q(nombre__icontains=valor_buscar) | Q(tags__nombre__icontains=valor_buscar)
        ).distinct()

        print(f'productos: {producto_queryset}')
        productos_list = []
        for producto in producto_queryset:
            productos_list.append(
                {
                    'pk': producto.pk,
                    'nombre': producto.nombre,
                    'valor': producto.precio,
                    'imagen': producto.obtener_imagen_url(),
                    'descripcion': producto.descripcion,
                    'serial': 'SR{}'.format(producto.pk),
                    'tags': producto.get_tags_2()
                }
            )

        return JsonResponse(productos_list, safe=False)


class ProductoCreate( CreateView ):
    model = Producto
    fields = '__all__'
    template_name = 'producto/create.html'
    success_url = reverse_lazy('producto-list')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.creado_por = self.request.user
        self.object.save()
        new_producto = self.object
        print("el Pk del objecto es", new_producto.pk )
        # Con el producto guardado le acomodo sus presentaciones
        print("esta es mi lista de presentaciones ", self.request.POST.getlist('nombre_presentacion'))
        name_presentaciones = self.request.POST.getlist('nombre_presentacion')
        price_presentaciones = self.request.POST.getlist('precio_presentacion')
        # active_presentacion = self.request.POST.getlist('activo_presentacion')

        a=0
        for i in name_presentaciones:
            presentacion = Presentacion(producto=new_producto,
                    nombre_presentacion=name_presentaciones[a],
                    precio_presentacion=price_presentaciones[a],)
            presentacion.save() # Grabo la presentacion del producto
            new_producto.presentaciones.add(presentacion) # y se la reasigno a su producto
            print("new_producto", new_producto.pk)
            print("presentacion", presentacion.pk)
            a = a +1

        return HttpResponseRedirect(self.get_success_url())


class ProductoUpdate( UpdateView ):
    model = Producto
    fields = '__all__'
    template_name = 'producto/update.html'
    success_url = reverse_lazy('producto-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        producto = Producto.objects.get(pk=self.object.pk)
        context['producto_pk'] = producto.pk
        context['presentaciones'] = Presentacion.objects.filter(producto=producto)
        return context

class ProductoList( ListView ):

    model = Producto
    paginate_by = 20
    template_name = 'producto/list.html'
    context_object_name = 'productos'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

# @method_decorator(login_required, name='dispatch')
class ProductoDetail(View):

    def get(self, request, *args, **kwargs):
        print("REQUEST: ", request)
        producto = get_object_or_404(Producto, pk=request.GET.get('id_producto'))

        producto_list = Producto.objects.filter(pk=request.GET.get('id_producto')).values('pk', 'nombre', 'imagen', 'descripcion','categoria__nombre','tags__nombre')

        presentaciones = list(
            Presentacion.objects.filter(producto=producto).
                filter(activo_presentacion=True).
                values('pk', 'nombre_presentacion', 'precio_presentacion')
        )

        respuesta_producto = {
            'pk': producto.pk,
            'nombre': producto.nombre,
            'imagen': producto.obtener_imagen_url(),
            'descripcion': producto.descripcion,
            'categoria': producto.categoria.nombre,
            'presentaciones': presentaciones,
            'tags': producto.get_tags_2(),
        }

        return JsonResponse({'producto_detail': respuesta_producto})


class AddPresentacion(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            producto = Producto.objects.get(pk=request.GET.get('id_producto'))
            presentacion = Presentacion(producto=producto,
                    nombre_presentacion = request.GET.get('nombre'),
                    precio_presentacion = request.GET.get('precio'),
            )
            presentacion.save() # Grabo la presentacion del producto
            producto.presentaciones.add(presentacion) #Se la añado a su producto

            return JsonResponse({'guardado':True, 'presentacion_pk': presentacion.pk })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO
        else:
            return JsonResponse({'guardado':False, 'presentacion_pk': None })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

class AccionPresentacion(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            accion = request.GET.get('accion') #Si es False es para desactivar si no es activar
            print("accion fue...", accion)
            if accion == "false":
                presentacion = Presentacion.objects.get(pk=request.GET.get('id_presentacion'))
                presentacion.activo_presentacion = False
                presentacion.save() # Desactivo la presentacion del producto
                print("presentacion DESACTIVADA", presentacion.pk)
                return JsonResponse({'activado':True, 'presentacion_pk': presentacion.pk })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

            else:
                presentacion = Presentacion.objects.get(pk=request.GET.get('id_presentacion'))
                presentacion.activo_presentacion = True
                presentacion.save() # Activo la presentacion del producto
                print("presentacion ACTIVADA", presentacion.pk)
                return JsonResponse({'activado':False, 'presentacion_pk': presentacion.pk })   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

        else:
            return JsonResponse({'guardado':'error no ajax'})   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

class UpdatePresentacion(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            accion = request.GET.get('accion') #Si es False es para consultar si no es guardar
            print("accion fue...", accion)
            if accion == "false":
                presentacion = Presentacion.objects.get(pk=request.GET.get('id_presentacion'))
                return JsonResponse({"presentacion_pk": presentacion.pk, "precio": presentacion.precio_presentacion, "nombre": presentacion.nombre_presentacion})   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

            else:
                presentacion = Presentacion.objects.get(pk=request.GET.get('pk_presentacion'))
                presentacion.nombre_presentacion = request.GET.get('nombre')
                presentacion.precio_presentacion = request.GET.get('precio')
                presentacion.save() # Grabo la presentacion del producto
                print("actualice la presentacion..", presentacion.pk)
                return JsonResponse({'guardado':True, 'presentacion_pk': presentacion.pk })
        else:
            return JsonResponse({'guardado':'error no ajax'})   #DEVUELVO LA INFORMACION DEL QUE ESTABA CONSULTANDO

@method_decorator(login_required, name='dispatch')
class CrearPedido(View):
    def post(self, request, *args, **kwargs):
        user = request.user
        print("POST del CREAR PEDIDO", self.request.POST)
        pk_pedido = self.request.POST.get('id_pedido')
        cliente = Contacto.objects.get(pk=self.request.POST.get('cliente'))
        gestion = GestionContacto.objects.get(pk=self.request.POST.get('id_gestioncontacto'))
        print("CrearPedido>pedido pk es", pk_pedido )
         # primero creo el pedido si no existe
        if pk_pedido == '':
            print("NUEVO PEDIDO")
            pedido = Pedido(vendedor=user,
                            cliente = cliente,
                            gestion = gestion, )
            pedido.save()
            pedido_presentacion = self.request.POST.getlist('pedido_presentacion')
            pedido_cantidad = self.request.POST.getlist('pedido_cantidad')
            gestion.pedido = pedido# Asigno el pedido a su gestion
            gestion.save()
            #Ahora creo los items
            a = 0
            subtotal = 0
            for i in pedido_presentacion:
                presentacion = Presentacion.objects.get(pk=pedido_presentacion[a])
                producto = Producto.objects.get(pk=presentacion.producto.pk)
                cantidad = int(pedido_cantidad[a])
                item = Item(
                        producto = producto,
                        presentacion = presentacion,
                        cantidad = cantidad,
                        )
                item.save() # Grabo los items del pedido
                pedido.item.add(item) # y se la los items a su producto
                subtotal = subtotal + int((presentacion.precio_presentacion)*cantidad)
                a = a +1
            pedido.precio_total = subtotal
            pedido.save()
            return JsonResponse({'pedido_pk':pedido.pk})
        #De lo contario debe estar PAGANDO
        elif (pk_pedido != '') and (self.request.POST.get('estado_pedido') == 'finalizado'):
            print("ACTUALIZANDO PEDIDO ", pk_pedido )
            pedido = Pedido.objects.get(pk=self.request.POST.get('id_pedido'))
            pedido.estado = 'finalizado'
            pedido.save()

            return JsonResponse({'pedido_pk':pedido.pk, 'informacion': 'Pedido no.{} Pago procesado'.format(pedido.pk) })
        #De lo contario debe estar ACTUALIZANDO     nota: actualizacion debe volver a hacerce para no crear tanto item suelto
        else:
            print("ACTUALIZANDO PEDIDO ", pk_pedido )
            pedido = Pedido.objects.get(pk=self.request.POST.get('id_pedido'))
            pedido_presentacion = self.request.POST.getlist('pedido_presentacion')
            pedido_cantidad = self.request.POST.getlist('pedido_cantidad')
            #Como estoy actualizando voy a destruir los items que tenia
            pedido.item.clear()
            #Ahora creo los items actualizados
            a = 0
            subtotal = 0
            for i in pedido_presentacion:
                presentacion = Presentacion.objects.get(pk=pedido_presentacion[a])
                producto = Producto.objCategoriaProductosects.get(pk=presentacion.producto.pk)
                cantidad = int(pedido_cantidad[a])
                item = Item(
                        producto = producto,
                        presentacion = presentacion,
                        cantidad = cantidad,
                        )
                item.save() # Grabo los items del pedido
                pedido.item.add(item) # y se la los items a su producto
                subtotal = subtotal + int((presentacion.precio_presentacion)*cantidad)
                a = a +1
            pedido.precio_total = subtotal
            pedido.save()
            return JsonResponse({'pedido_pk':pedido.pk})

@method_decorator(login_required, name='dispatch')
class PedidoConsultarAjax(View):
    def get(self, request, *args, **kwargs):
        # primero busco la gestion
        gestion = GestionContacto.objects.get(pk=self.request.GET.get('gestion_pk'))
        if Pedido.objects.filter(gestion=gestion).exists():
            item_list =  list(gestion.pedido.item.all().values('pk', 'producto__nombre', 'presentacion__precio_presentacion', 'presentacion__nombre_presentacion', 'cantidad', 'presentacion__pk'))
            return JsonResponse({'gestion': gestion.pk, 'pedido': gestion.pedido.pk, 'item_list': item_list, 'cliente_pk':gestion.contacto.pk })

        else:
            cliente_pk = gestion.contacto.pk if gestion.contacto else ''
            print("No hay pedido pero el pk cliente fue", cliente_pk )
            return JsonResponse({'gestion': gestion.pk, 'pedido': "", 'item_list': "", 'cliente_pk':cliente_pk })

#---------      TAGS VIEWS            -------------------------------.
class TagCreate( CreateView ):
    model = Tag
    fields = '__all__'
    template_name = 'tag/create.html'
    success_url = reverse_lazy('tag-list')

class TagUpdate( UpdateView ):
    model = Tag
    fields = '__all__'
    template_name = 'tag/update.html'
    success_url = reverse_lazy('tag-list')

class TagList( ListView ):

    model = Tag
    paginate_by = 20
    template_name = 'tag/list.html'
    context_object_name = 'tags'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

# @method_decorator(login_required, name='dispatch')
class TagProductos(View):
    def post(self, request, *args, **kwargs):
        # user = request.user
        try:
            tag = get_object_or_404(Tag, nombre=request.POST.get('id_tag'))
            # print("Tag que busco fue", tag)
            productos = Producto.objects.filter(tags=tag).values('pk', 'nombre', 'imagen')
            productos_list = list(productos)
            # print("productos_list que busco fue", productos_list)
            return JsonResponse(productos_list, safe=False)
        except:
            # print("Sin tags")
            productos = Producto.objects.all().values('pk', 'nombre', 'imagen')
            productos_list = list(productos)
            # print("productos_list todos  que busco fue", productos_list)
            return JsonResponse(productos_list, safe=False)


class BuscarProductos(View):

    def get(self, request, *args, **kwargs):
        lista_productos = []
        return JsonResponse({'productos': lista_productos}, safe=False)
#---------      PEDIDO VIEWS            -------------------------------.

class PedidoCreate( CreateView ):
    model = Pedido
    fields = '__all__'
    template_name = 'pedido/create.html'
    success_url = reverse_lazy('pedido-list')

class PedidoDetail( DetailView ):
    model = Pedido
    fields = '__all__'
    template_name = 'pedido/detail.html'
    success_url = reverse_lazy('pedido-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pedido = Pedido.objects.get(pk=self.kwargs['pk'])
        print("pedido es", pedido)
        list_items = pedido.item.all()
        print("lista es", list_items)
        context['list_items'] = list_items
        return context

class PedidoList( ListView ):

    model = Pedido
    paginate_by = 15
    template_name = 'pedido/list.html'
    context_object_name = 'pedidos'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context
