from django.db import models
from ominicontacto_app.models import User
from truecontact.contactos.models import Contacto, GestionContacto
# Create your models here.
class Categoria(models.Model):
    nombre = models.CharField(max_length=32)
    activo = models.BooleanField(default=True)
    imagen = models.ImageField(upload_to='imagenes/catalogos/', blank=True, null=True)
    descripcion = models.TextField(blank=True, null=True)
    f_registro = models.DateTimeField(verbose_name="fecha_registro_cat", auto_now_add=True,)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return str(self.nombre)

    def obtener_imagen_url(self):
        if self.imagen:
            imagen_url = self.imagen.url
        else:
            imagen_url = ''

        return imagen_url

    def obtener_productos(self):
        queryset = Producto.objects.filter(categoria=self)
        cantidad = Producto.objects.filter(categoria=self).count()
        return {
            'cantidad': cantidad,
            'queryset': queryset,
        }

class Tag(models.Model):
    nombre = models.CharField(max_length=32)
    activo = models.BooleanField(default=True)
    f_registro = models.DateTimeField(verbose_name="fecha_registro_Tag", auto_now_add=True,)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'

    def __str__(self):
        return str(self.nombre)

class Producto(models.Model):
    creado_por = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, related_name="producto_por")
    nombre = models.CharField(max_length=64)
    activo = models.BooleanField(default=True)
    unidad = models.CharField(max_length=32, blank=True, null=True)
    imagen = models.ImageField(upload_to='imagenes/productos/')
    descripcion = models.TextField(blank=True, null=True)
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT, related_name='hacia_su_categoria')
    f_registro = models.DateTimeField(verbose_name="fecha_registro_prod", auto_now_add=True,)
    presentaciones = models.ManyToManyField('Presentacion', related_name='presentaciones_productos', blank=True)
    tags = models.ManyToManyField(Tag, related_name='tag_productos', blank=True)
    precio = models.IntegerField(blank=True, null=True)

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'


    def __str__(self):
        return str(self.nombre)

    def get_precio(self):
        return "$" + str(self.precio)

    def get_tags(self):
        cadena = ""
        etiquetas = self.tags.all()
        for b in etiquetas:
            # print(b.nombre)
            cadena = str(b) +" "+ cadena
        return cadena

    def get_tags_2(self):
        etiquetas = self.tags.values('nombre')

        return list(etiquetas)

    def obtener_imagen_url(self):
        if self.imagen:
            imagen_url = self.imagen.url
        else:
            imagen_url = ''

        return imagen_url

class Presentacion(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.PROTECT, related_name='hacia_su_producto',blank=True, null=True)
    nombre_presentacion = models.CharField(max_length=64)
    precio_presentacion = models.IntegerField()
    activo_presentacion = models.BooleanField(default=True)
    f_registro = models.DateTimeField(verbose_name="fecha_registro_presentacion", auto_now_add=True,)

    class Meta:
        ordering = ['producto', 'nombre_presentacion']
        verbose_name = 'Presentacion'
        verbose_name_plural = 'Presentaciones'

    def __str__(self):
        return str(self.nombre_presentacion)

ESTADO_PEDIDO = [
    ('pendiente', 'Pendiente'),
    ('en_proceso', 'En Seguimiento'),
    ('finalizado', 'Finalizada'),
]

class Pedido(models.Model):
    estado = models.CharField(max_length=64, choices=ESTADO_PEDIDO, default='en_proceso')
    gestion = models.ForeignKey(GestionContacto, on_delete=models.PROTECT, related_name='gestion', blank=True, null=True)
    item = models.ManyToManyField('Item',related_name='productos_pedido', blank=True)
    precio_total = models.IntegerField(blank=True, null=True)
    cliente = models.ForeignKey(Contacto, on_delete=models.PROTECT, related_name='cliente',blank=True, null=True)
    vendedor = models.ForeignKey(User, on_delete=models.PROTECT )
    f_registro = models.DateTimeField(verbose_name="fecha_registro_pedido", auto_now_add=True,)
    class Meta:
        ordering = ['-pk']
        verbose_name = 'Pedido'
        verbose_name_plural = 'Pedidos'

    def __str__(self):
        return str(self.pk)

class Item(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.PROTECT, related_name='producto_item')
    presentacion = models.ForeignKey(Presentacion, on_delete=models.PROTECT, related_name='producto_presentacion_item', blank=True)
    cantidad = models.IntegerField(blank=True, null=True)
    f_registro = models.DateTimeField(verbose_name="fecha_registro_item", auto_now_add=True,)

    def get_price(self):
        subtotal = self.presentacion.precio_presentacion * self.cantidad
        return subtotal


class Sucursal(models.Model):
    nombre = models.CharField(max_length=64)
    direccion = models.CharField(max_length=128)
    telefono = models.CharField(max_length=16)
    telefono2 = models.CharField(max_length=64)
    f_registro = models.DateTimeField(verbose_name="fecha_registro_sucursal", auto_now_add=True,)

    # nit = models.CharField(max_length=64)
