from django import forms
from .models import Presentacion

class PresentacionForm(forms.ModelForm):
    class Meta:
        model = Presentacion
        fields = ['nombre_presentacion', 'activo_presentacion', 'precio_presentacion']